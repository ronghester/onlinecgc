#ifndef INFOBOX_H
#define INFOBOX_H

#include <QDialog>

namespace Ui {
class InfoBox;
}

class InfoBox : public QDialog
{
    Q_OBJECT
    
public:
    explicit InfoBox(QWidget *parent = 0);
    ~InfoBox();
    void setMessage(QString msg);
    
private slots:
    void on_toolButton_clicked();

    void on_toolButton_2_clicked();

private:
    Ui::InfoBox *ui;
};

#endif // INFOBOX_H
