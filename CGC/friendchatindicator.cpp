#include "friendchatindicator.h"
#include "ui_friendchatindicator.h"
#include <mainwindow.h>
extern MainWindow* w;
FriendChatIndicator::FriendChatIndicator(QString Fname,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FriendChatIndicator)
{
    ui->setupUi(this);
    ui->name->setText(Fname);
    thisname = Fname;
}

FriendChatIndicator::~FriendChatIndicator()
{
    delete ui;
}

void FriendChatIndicator::setActivationDue(){
    //ui->name->setStyleSheet("{border-image:none;background-color:rgb(100,0,0);}");
   // w->setStyleSheet("QStatusBar::item {background-color:rgb(100,0,0) }; ");
    ui->name->setStyleSheet("color:rgb(0,0,233)");
     //border-image: url(:/new/prefix1/resources/button-plain.png);
     this->repaint();

}

void FriendChatIndicator::mousePressEvent(QMouseEvent *e){
    w->onFriendIndicatorClicked(thisname,false);
   // ui->name->setStyleSheet("{border-image: url(:/new/prefix1/resources/button-plain.png);}");
   // w->setStyleSheet("QStatusBar::item {background-color:rgb(200,200,200) }; ");
    ui->name->setStyleSheet("color:rgb(220,220,233)");
}

void FriendChatIndicator::on_toolButton_clicked()
{
    this->hide();
     w->onFriendIndicatorClicked(thisname,true);
}
