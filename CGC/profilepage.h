#ifndef PROFILEPAGE_H
#define PROFILEPAGE_H

#include <QWidget>
#include <QListWidget>
#include <QLabel>
#include <QNetworkReply>
#include <jsonparser.h>
#include <QTableWidgetItem>

namespace Ui {
class Profilepage;
}

class Profilepage : public QWidget
{
    Q_OBJECT
    
public:
    explicit Profilepage(QWidget *parent = 0,QString whichuser=0);
    ~Profilepage();
    QListWidget* teamlist;
    QLabel* username;
    int count;
    QString testuser;

private slots:
     void plainRequest(QNetworkReply* reply);
     void addFriendRequest(QNetworkReply* reply );
     void on_addfriendbtn_clicked();
     void createFriendsPanel();
     void on_pushButton_3_clicked();

private:
     Ui::Profilepage *ui;
     QTableWidgetItem* createTableWidgetItem( const QString& text );

};

#endif // PROFILEPAGE_H
