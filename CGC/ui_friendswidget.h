/********************************************************************************
** Form generated from reading UI file 'friendswidget.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRIENDSWIDGET_H
#define UI_FRIENDSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FriendsWidget
{
public:
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *userpic;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout;
    QLabel *username;
    QLabel *userstatus;
    QLabel *userchips;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *onlinestatus;

    void setupUi(QWidget *FriendsWidget)
    {
        if (FriendsWidget->objectName().isEmpty())
            FriendsWidget->setObjectName(QString::fromUtf8("FriendsWidget"));
        FriendsWidget->resize(178, 65);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FriendsWidget->sizePolicy().hasHeightForWidth());
        FriendsWidget->setSizePolicy(sizePolicy);
        FriendsWidget->setMinimumSize(QSize(0, 24));
        FriendsWidget->setMaximumSize(QSize(16777215, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(12);
        FriendsWidget->setFont(font);
        FriendsWidget->setStyleSheet(QString::fromUtf8("\n"
"\n"
"\n"
"#username{\n"
"\n"
"\n"
"color: rgb(113, 211, 243);\n"
"	font: 75 12pt \"Candara\";\n"
"\n"
"}\n"
"\n"
"#userstatus{\n"
"color:#FFF;\n"
"	font: 10pt \"Candara\";\n"
"\n"
"}\n"
"\n"
"#userchips{\n"
"color:#FFF;\n"
"	font: 10pt \"Candara\";\n"
"\n"
"}"));
        horizontalLayout_2 = new QHBoxLayout(FriendsWidget);
        horizontalLayout_2->setSpacing(9);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(9, 0, 0, 0);
        frame_2 = new QFrame(FriendsWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy1);
        frame_2->setMinimumSize(QSize(0, 0));
        frame_2->setMaximumSize(QSize(50, 50));
        frame_2->setStyleSheet(QString::fromUtf8(""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        userpic = new QLabel(frame_2);
        userpic->setObjectName(QString::fromUtf8("userpic"));
        sizePolicy1.setHeightForWidth(userpic->sizePolicy().hasHeightForWidth());
        userpic->setSizePolicy(sizePolicy1);
        userpic->setMaximumSize(QSize(52, 52));
        userpic->setStyleSheet(QString::fromUtf8("border:none;\n"
"background-color: rgba(255, 255, 255, 0);"));

        verticalLayout_3->addWidget(userpic);


        horizontalLayout_2->addWidget(frame_2);

        frame = new QFrame(FriendsWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("border:none;\n"
"background-color: none;"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(1);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame_4 = new QFrame(frame);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy);
        frame_4->setMouseTracking(true);
        frame_4->setStyleSheet(QString::fromUtf8(""));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_4);
        verticalLayout->setSpacing(4);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 8, 0, 8);
        username = new QLabel(frame_4);
        username->setObjectName(QString::fromUtf8("username"));
        username->setMouseTracking(true);
        username->setStyleSheet(QString::fromUtf8(""));

        verticalLayout->addWidget(username);

        userstatus = new QLabel(frame_4);
        userstatus->setObjectName(QString::fromUtf8("userstatus"));

        verticalLayout->addWidget(userstatus);

        userchips = new QLabel(frame_4);
        userchips->setObjectName(QString::fromUtf8("userchips"));
        userchips->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(userchips);


        horizontalLayout->addWidget(frame_4);

        frame_3 = new QFrame(frame);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        sizePolicy.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy);
        frame_3->setMaximumSize(QSize(36, 16777215));
        frame_3->setLayoutDirection(Qt::LeftToRight);
        frame_3->setStyleSheet(QString::fromUtf8(""));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_3);
        verticalLayout_2->setSpacing(9);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(4, 0, 4, 0);
        label = new QLabel(frame_3);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setMaximumSize(QSize(24, 24));
        label->setLayoutDirection(Qt::RightToLeft);
        label->setTextFormat(Qt::RichText);
        label->setScaledContents(false);
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(true);
        label->setMargin(-1);
        label->setIndent(0);

        verticalLayout_2->addWidget(label);

        onlinestatus = new QLabel(frame_3);
        onlinestatus->setObjectName(QString::fromUtf8("onlinestatus"));
        sizePolicy1.setHeightForWidth(onlinestatus->sizePolicy().hasHeightForWidth());
        onlinestatus->setSizePolicy(sizePolicy1);
        onlinestatus->setMaximumSize(QSize(12, 12));
        onlinestatus->setLayoutDirection(Qt::RightToLeft);
        onlinestatus->setStyleSheet(QString::fromUtf8(""));
        onlinestatus->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/white-dot.png")));
        onlinestatus->setScaledContents(true);
        onlinestatus->setAlignment(Qt::AlignCenter);
        onlinestatus->setWordWrap(true);
        onlinestatus->setMargin(1);
        onlinestatus->setIndent(0);

        verticalLayout_2->addWidget(onlinestatus);


        horizontalLayout->addWidget(frame_3);


        horizontalLayout_2->addWidget(frame);


        retranslateUi(FriendsWidget);

        QMetaObject::connectSlotsByName(FriendsWidget);
    } // setupUi

    void retranslateUi(QWidget *FriendsWidget)
    {
        FriendsWidget->setWindowTitle(QApplication::translate("FriendsWidget", "Form", 0, QApplication::UnicodeUTF8));
        userpic->setText(QString());
        username->setText(QApplication::translate("FriendsWidget", "TextLabel", 0, QApplication::UnicodeUTF8));
        userstatus->setText(QApplication::translate("FriendsWidget", "My Status", 0, QApplication::UnicodeUTF8));
        userchips->setText(QApplication::translate("FriendsWidget", "TextLabel", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        onlinestatus->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FriendsWidget: public Ui_FriendsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRIENDSWIDGET_H
