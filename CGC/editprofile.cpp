#include "editprofile.h"
#include "ui_editprofile.h"
#include <QFileDialog>
#include <QDebug>
#include <jsonparser.h>
#include <QBuffer>
#include <mainwindow.h>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
 extern QString cind;
 extern QString thisuser;
 extern QString token;
 extern QByteArray userspic;
 extern int userchips;
extern MainWindow* w;
EditProfile::EditProfile(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::EditProfile)
{
    ui->setupUi(this);
    fileName = "";

    QString surl = "<url>api/user/token";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
     request2.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
     QUrl params;
    manager->post(request2,params.encodedQuery());


}

void EditProfile::plainRequest(QNetworkReply* reply){
     QString replyText;
     QByteArray rawData = reply->readAll();
     QString textData(rawData);
      qDebug() << "RM"+ textData;
      if(textData.length() < 5) {
          qDebug()<<"hey";
          return;
      }
      Json::Reader reader;
      Json::Value root;
      QVBoxLayout *newsbox=new QVBoxLayout();
      bool parsingSuccessful = reader.parse(textData.toStdString(), root );

      if(!parsingSuccessful || root.size() == 0){
            return;
      }
      token = QString().fromStdString(root.get("token","user").asString());
      qDebug()<<"Token is "<<token;
}

EditProfile::~EditProfile()
{
    delete ui;
}

void EditProfile::on_pushButton_2_clicked()
{
    fileName = QFileDialog::getOpenFileName(this,
         tr("Open Image"), "/home/jana", tr("Image Files (*.png *.jpg *.bmp)"));

       QImage image(fileName);
       QByteArray byteArray;
       QBuffer buffer(&byteArray);
       buffer.open(QIODevice::WriteOnly);
       image.save(&buffer, "PNG");
       //userspic = byteArray;
       QImage imagee = QImage::fromData(byteArray, "PNG");
       QIcon icon;
       QSize size;
       if (!image.isNull())
         icon.addPixmap(QPixmap::fromImage(imagee), QIcon::Normal, QIcon::On);

       QPixmap pixmap = icon.pixmap(QSize(100,100), QIcon::Normal, QIcon::On);
         ui->userimg->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

       ui->userimg->setPixmap(pixmap);
       ui->userimg->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
       ui->userimg->show();
    //ui->userimg->setPixmap(QPixmap().fromImage(QImage(fileName)));
}

void EditProfile::on_toolButton_clicked()
{
    this->close();
}
void EditProfile::requestReceived(QNetworkReply* reply){
    //qDebug() << "Received ";
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << "received" << textData;
    ui->label_4->setText("profile Updated!");
    w->setUserImage();
}
void EditProfile::on_pushButton_clicked()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    QUrl params; QString encoded="";
    if(fileName.length() != 0){
    QImage image(fileName);
    QPixmap pixmap;
    pixmap = pixmap.fromImage(image.scaled(100,100,Qt::IgnoreAspectRatio,Qt::SmoothTransformation));

        QByteArray byteArray;
        QBuffer buffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pixmap.save(&buffer, "PNG");
       //encoded = QString::fromLatin1(ba.toBase64().data());
      // return iconBase64;
      // encoded.remove(" ");
//userspic = byteArray.toBase64();
    // encoded = QString(image.toBase64());
    //int encodedSize = encoded.size();

    //params.addQueryItem("uid", QString::number(mainid));
   // params.addQueryItem("title", ui->plainTextEdit->toPlainText());
   // params.addQueryItem("desc", encoded);
        userspic = byteArray;
    qDebug()<<encoded;
    params.addQueryItem("field_displaypic[und][0][value]",byteArray.toBase64());
    }
QString status = ui->plainTextEdit->toPlainText();
//QString surl = "<url>UpdateUser?desc="+ui->plainTextEdit->toPlainText();
QString surl = "<url>api/user/"+QString().number(mainid);
/*QString generatequery = "data%5Bcurrent_pass%5D=1234&data%5Bname%5D="+thisuser+
        "&field_statusline%5Bund%5D%5B0%5D%5Bvalue%5D="+ui->plainTextEdit->toPlainText()+
        "&field_displaypic%5Bund%5D%5B0%5D%5Bvalue%5D="+byteArray.toBase64();*/
params.addQueryItem("data[name]", thisuser);
params.addQueryItem("data[current_pass]", "1234");
if(ui->plainTextEdit->toPlainText()!=0)
  params.addQueryItem("field_statusline[und][0][value]",ui->plainTextEdit->toPlainText());

QNetworkRequest request = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request.setRawHeader("Cookie",ss.toUtf8() );
    request.setRawHeader("X-CSRF-Token",token.toUtf8());
    //request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
//QByteArray bba = generatequery.toUtf8();
    manager->put(request,params.encodedQuery());

    ui->label_4->setText("Connecting....");
}

void EditProfile::on_reloadchip_clicked()
{
    //call a service to reload the chips for this user..
    QString surl = "<url>reloadChips?uid="+QString().number(mainid);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived2(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    request2.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->get(request2);
}

void EditProfile::requestReceived2(QNetworkReply* reply){
    //qDebug() << "Received ";
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << "received" << textData;
    textData.mid(2,textData.length()-2);
    ui->label_4->setText(textData);
    if(textData == "[\"done\"]"){
        userchips+=500;
        w->updateUserChips(QString().number(userchips));
    }
}
