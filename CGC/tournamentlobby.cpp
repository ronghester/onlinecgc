#include "tournamentlobby.h"
#include "ui_tournamentlobby.h"
#include <QTableWidget>
#include <QDebug>
#include <mainwindow.h>
 extern MainWindow* w;
 extern int mainid;
 extern QString sessionname;
 extern QString sessionid;
 extern QString cind;
 extern QString thisuser;
 extern QString statusline;
 extern int userchips;
  int teamsize;
  QStringList alphanumeric;
  extern QString currentArenaName;
extern std::vector<arenaobject> availablearenas;

TournamentLobby::TournamentLobby(QWidget *parent) : QWidget(parent),ui(new Ui::TournamentLobby)
{
    ui->setupUi(this);
    pcounter = 0; //Counter of the player
    row = 0;
    column = 0;
    isSignedup = false;
    troomID = "0";
    ui->join_tlobby->hide();
    teamsize = 0;
    alphanumeric << "A" << "B" << "C" << "D" << "E" << "F" <<"G"<<"H"<<"I"<<"J"<<"K"<<"L"<<"M"<<"N"<<"O"<<"P"<<"Q"<<"R";
}

TournamentLobby::~TournamentLobby()
{
    delete ui;
}

void TournamentLobby::setValues(Json::Value root){
         const Json::Value tem = root;
         QString title = QString().fromStdString(tem.get("title","none").asCString());
         //qDebug()<<"Title: "<<title;
         QString des = QString().fromStdString(tem.get("body","none").asCString());
         QString mgametype = QString().fromStdString(tem.get("gametype","none").asCString());
         QString playercount = QString().fromStdString(tem.get("playercount","none").asCString());
         QString wager = QString().fromStdString(tem.get("wager","none").asCString());
         QString time = QString().fromStdString(tem.get("time","none").asCString());
         QString status = QString().fromStdString(tem.get("status","none").asCString());
         QByteArray by = QByteArray::fromBase64(tem.get("arenaimage","none").asCString());
         arenatitle = QString().fromStdString(tem.get("arenatitle","none").asCString());
         tid = QString().fromStdString(tem.get("tournamentID","none").asCString());
         matchlevel = QString().fromStdString(tem.get("match_level","none").asCString());
       //  troomdesc = title;


         try{
             qDebug()<<"Reading...1";
             Json::Value tds = tem.get("troomId","none");
             qDebug()<<"Reading...2";
             if(tds != "none"){
                  QString thearenaname = QString().fromStdString(tds.get("arenausername","none").asCString());
                  if(thearenaname == currentArenaName){
                 troomID = QString().fromStdString(tds.get("troom","none").asCString());
                 troomName = QString().fromStdString(tds.get("teamname","none").asCString());
                 if(troomID != "0") ui->join_tlobby->show();
                  }
             }
             //

             Json::Value matches = tem.get("matchId","none");
             if(matches!="none"){
                 for(int k=0;k<matches.size();k++){
                      Json::Value thismatch = matches[k];
                      QString thisteam_name = QString().fromStdString(thismatch.get("winning_team","none").asCString());
                      QString lobbies_name = QString().fromStdString(thismatch.get("teamname","none").asCString());
                      qDebug()<<thisteam_name<<":"<<lobbies_name;
                      if(thisteam_name == "A"||thisteam_name == "B"){
                          lobbies_map[lobbies_name] = thisteam_name;
                      }
                 }
             }
         }
         catch(...){

         }

        // if(status=="Closed") ui->join_tlobby->show();
         //
         wageramount = wager;
         gametype = mgametype;
         //
         Json::Value players = tem.get("players",0);
         totalplayer = playercount.toInt();
         QImage image = QImage::fromData(by, "JPG");
         ui->tname->setText(title);
         ui->tdesc->setText(des);
         ui->tfee->setText("Entry Fee "+wager+" Chips");
         ui->starttime->setText("Start Time: "+time);
         ui->label_5->setText(QString().number(players.size())+"/"+playercount);
          QSize buttonSize(124,122);
          ui->t_logo->setFixedSize(buttonSize);
          ui->t_logo->setStyleSheet("border-image: url(:/new/prefix1/resources/games-back.png);");
          ui->t_logo->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
          QString html = "<img style='display:block; width:100px;height:100px;' id='base64image'";
          QString part2 = "src='data:image/jpeg;base64,"+QString().fromStdString(tem.get("arenaimage","none").asCString())+"'/><br/><br/>";
          ui->t_logo->setTextFormat(Qt::RichText);
          ui->t_logo->setText(html+part2);
          //Lets set the table

          int pcount = playercount.toInt();
          QTableWidget* tableWidget = ui->tableWidget;
        // int teamsize = 0;
         if(gametype == "1V1") teamsize = 1;
         else if(gametype == "2V2") teamsize = 2;
         else if(gametype == "3V3") teamsize = 3;
         else if(gametype == "4V4") teamsize = 4;
         else if(gametype == "5V5") teamsize = 5;
              //2 Teams so equally devide the player
               column = pcount/teamsize; row = pcount/column;
              tableWidget->horizontalHeader()->show();
              tableWidget->setColumnCount(column);
              tableWidget->setRowCount(row);
              QStringList m_TableHeader;
             // tableWidget->verti

              qDebug()<<column<<" cr "<<row;
              for ( int i = 0 ; i < column ; i++)
              {
                // tableWidget->setHorizontalHeaderItem( i, new QTableWidgetItem("Team "+QString::number(i)) );
                  m_TableHeader<<"Team "+alphanumeric.at(i);
                 for ( int j = 0 ; j < row ; j++ )
                 {
                    // tableWidget->insertRow(j);
                     if(players.size() > pcounter){
                         int index = 0; // Iterates over the sequence elements.
                          QString pname =  QString().fromStdString(players[pcounter].asCString());
                          if(pname == currentArenaName) isSignedup = true;
                          tableWidget->setItem( j, i, new QTableWidgetItem(pname));
                          pcounter++;
                     }
                     else
                         tableWidget->setItem( j,i, new QTableWidgetItem(" "));
                 }
              }
              // m_TableHeader<<"Game Name"<<"Number of Players"<<"Description"<<"Players"<<"Wager";
              tableWidget->setHorizontalHeaderLabels(m_TableHeader);
              // tableWidget->setStyleSheet("{background-color:rgba(142,113,112,90); font: 12pt \"Candara\";color: #fff;}");
              tableWidget->horizontalHeader()->setResizeMode( QHeaderView::Stretch);
              tableWidget->show();


}
QTableWidgetItem* TournamentLobby::createTableWidgetItem( const QString& text )
{
    QTableWidgetItem* item = new QTableWidgetItem( text );
    item->setTextAlignment( Qt::AlignCenter );
    return item;
}

void TournamentLobby::on_pushButton_2_clicked()
{
    int lobbysize = teamsize + teamsize;
    w->showTournamentBracket(column,lobbies_map,lobbysize);
}

void TournamentLobby::on_signup_clicked()
{
    if(isSignedup){
        IBox = new InfoBox(this);
        IBox->setMessage("You are already signed up");
        IBox->show();
    }
    else if(pcounter == totalplayer){
        IBox = new InfoBox(this);
        IBox->setMessage("Tournament is already full!");
        IBox->show();
    }

    else{
        bool gate = false;
        for(int h=0;h<availablearenas.size();h++){
            qDebug()<<"ArenaTitle "<<availablearenas[h].arenaname;
            if(availablearenas[h].arenaname == arenatitle){
                gate = true;
            }
        }
        if(!gate){
            IBox = new InfoBox(this);
            IBox->setMessage("Please sign up for the arena first");
            IBox->show();
            return;
        }
        QString surl = "<url>api/TModule/tournamentSignup?uid="+currentArenaName+"&nid="+tid;
        qDebug()<<surl;
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest2(QNetworkReply*)));
        QNetworkRequest request2 = QNetworkRequest(surl);
        QString ss = sessionname+"="+sessionid;
        request2.setRawHeader("Cookie",ss.toUtf8() );
        manager->get(request2);
    }
}
void TournamentLobby::plainRequest2(QNetworkReply* reply ){
        QString replyText;
        QByteArray rawData = reply->readAll();
        QString textData(rawData);
        qDebug() << "RM"+ textData;
        if(textData.length() < 5) {
            qDebug() << "No data received";
            return;
        }
        Json::Reader reader;
        Json::Value groot;

        bool parsingSuccessful = reader.parse(textData.toStdString(), groot );

        if(!parsingSuccessful || groot.size() == 0){
              return;
        }
        else{
            const Json::Value friends = groot;
            QString messageType = QString().fromStdString(friends.get("MessageType","none").asCString());
            if(messageType == "Tournament"){
                 QString Subject = QString().fromStdString(friends.get("Subject","none").asCString());
                 if(Subject == "PlayerAdded"){
                     QString tuser = QString().fromStdString(friends.get("AddedUser","none").asCString());
                     //if(Pname)
                     //addTeamPlayer(tuser);
                      int newuserchips = userchips - wageramount.toInt();
                      w->updateUserChips(QString().number(newuserchips));
                      isSignedup = true;
                 }
                 if(Subject == "PlayerRejected"){
                     IBox = new InfoBox(this);
                     IBox->setMessage("Room is already Full.");
                     IBox->show();
                 }
            }

        }
}

void TournamentLobby::addTeamPlayer(Json::Value pnames){
    int n=0; int j=0;ui->tableWidget->clearContents();
    for(int i=0;i<pnames.size();i++){
       QString plr = QString().fromStdString(pnames[i].asCString());
       if(plr == thisuser) isSignedup = true;
       qDebug()<<plr;
         ui->tableWidget->setItem( j, n, new QTableWidgetItem(plr));
         n++; if(i>column){j++;n=0;}
    }

   /* int fakecounter =0 ;
    if(Pname == thisuser) isSignedup = true;
    for ( int i = 0 ; i < column ; i++)
    {
       for ( int j = 0 ; j < row ; j++ )
       {
           qDebug()<<i<<"--"<<j<<pcounter<<fakecounter;
           if(pcounter == fakecounter){
              ui->tableWidget->setItem( j, i, new QTableWidgetItem(Pname));
              pcounter++;
              return;
           }
            fakecounter++;

       }

    }*/
    repaint();
}

void TournamentLobby::on_join_tlobby_clicked()
{
    //If the room is filled then let him join the room..we know the roomID now..
    if(troomID == "0"){
        IBox = new InfoBox(this);
        IBox->setMessage("There no lock-in room found for you, Please wait!");
        IBox->show();
    }
    else{
        //QString wageramount,QString nid,QString gametype,QString title
        w->Tournament_LockInScreen(wageramount,troomID,gametype,troomdesc,troomName,matchlevel,totalplayer);
    }

}

void TournamentLobby::Tclose(Json::Value Teams){
    for(int i=0;i<Teams.size();i++){
        Json::Value tds = Teams[i];
        Json::Value TMate = tds.get("teammates","none");
        for(int j=0;j<TMate.size();j++){
             QString matename =  QString().fromStdString(TMate[j].asCString());
             qDebug()<<"Mate Name:"<<matename;
             if(currentArenaName == matename){
                 troomID = QString().fromStdString(tds.get("troom","none").asCString());
                 troomName = QString().fromStdString(tds.get("teamname","none").asCString());
             }
        }
    }

    ui->join_tlobby->show();
    IBox = new InfoBox(this);
    IBox->setMessage("Tournament is started! Join the lobby Now.");
    IBox->show();
}
