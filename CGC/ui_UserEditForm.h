/********************************************************************************
** Form generated from reading UI file 'UserEditForm.ui'
**
** Created: Fri 25. Jul 02:40:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USEREDITFORM_H
#define UI_USEREDITFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_NewCreateRoom
{
public:
    QFrame *frame;
    QLabel *label;
    QFrame *frame_2;
    QFrame *frame_3;

    void setupUi(QDialog *NewCreateRoom)
    {
        if (NewCreateRoom->objectName().isEmpty())
            NewCreateRoom->setObjectName(QString::fromUtf8("NewCreateRoom"));
        NewCreateRoom->resize(351, 269);
        NewCreateRoom->setStyleSheet(QString::fromUtf8("QDialog{\n"
"\n"
"border-image: url(:/new/prefix1/resources/loginbox.png);\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
"\n"
"#toolButton{\n"
"height:28px;\n"
"width:38px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}\n"
"\n"
"#toolButton_2{\n"
"height:28px;\n"
"width:32px;\n"
"	border-image: url(:/new/prefix1/resources/minimize.png);\n"
"	\n"
"}\n"
"#toolButton_2:hover{\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"#toolButton_3{\n"
"height:28px;\n"
"width:32px;\n"
"border-image: url(:/new/prefix1/resources/maximize.png);\n"
"}\n"
"#toolButton_3:hover{\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-image: url(:/new/p"
                        "refix1/resources/menu.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"	\n"
"\n"
"\n"
"}"));
        frame = new QFrame(NewCreateRoom);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(10, 10, 331, 41));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(80, 10, 161, 16));
        frame_2 = new QFrame(NewCreateRoom);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(10, 70, 311, 101));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        frame_3 = new QFrame(NewCreateRoom);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(20, 209, 271, 61));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);

        retranslateUi(NewCreateRoom);

        QMetaObject::connectSlotsByName(NewCreateRoom);
    } // setupUi

    void retranslateUi(QDialog *NewCreateRoom)
    {
        NewCreateRoom->setWindowTitle(QApplication::translate("NewCreateRoom", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("NewCreateRoom", "Profile ", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NewCreateRoom: public Ui_NewCreateRoom {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USEREDITFORM_H
