/********************************************************************************
** Form generated from reading UI file 'loginbox.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINBOX_H
#define UI_LOGINBOX_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_LoginBox
{
public:
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QPushButton *pushButton;
    QLabel *label_5;
    QLabel *label_2;
    QPushButton *pushButton_2;
    QToolButton *toolButton;
    QLabel *label_3;
    QLineEdit *lineEdit_4;
    QLabel *label_7;
    QLineEdit *lineEdit_3;
    QLabel *label_4;
    QCheckBox *checkBox;

    void setupUi(QDialog *LoginBox)
    {
        if (LoginBox->objectName().isEmpty())
            LoginBox->setObjectName(QString::fromUtf8("LoginBox"));
        LoginBox->resize(385, 358);
        LoginBox->setStyleSheet(QString::fromUtf8("#LoginBox{\n"
" \n"
"	\n"
"\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/loginbox.png);\n"
"}\n"
"\n"
"QLineEdit{\n"
"border:none;\n"
"\n"
"}\n"
"QLabel\n"
"{\n"
" color:#fff;\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"	border-image: url(:/new/prefix1/resources/loginbtn.png);\n"
"color: #000;\n"
"height:28px;\n"
"}\n"
"\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    color:#fff;\n"
"	\n"
"}\n"
"\n"
""));
        lineEdit = new QLineEdit(LoginBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(179, 125, 161, 21));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(14);
        lineEdit->setFont(font);
        lineEdit->setStyleSheet(QString::fromUtf8(""));
        lineEdit_2 = new QLineEdit(LoginBox);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(178, 165, 161, 21));
        lineEdit_2->setFont(font);
        lineEdit_2->setEchoMode(QLineEdit::Password);
        label = new QLabel(LoginBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 123, 111, 21));
        label->setFont(font);
        pushButton = new QPushButton(LoginBox);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(144, 211, 101, 41));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Candara"));
        font1.setPointSize(16);
        pushButton->setFont(font1);
        pushButton->setStyleSheet(QString::fromUtf8(""));
        label_5 = new QLabel(LoginBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(40, 162, 111, 21));
        label_5->setFont(font);
        label_2 = new QLabel(LoginBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(90, 3, 200, 105));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);
        label_2->setMinimumSize(QSize(0, 105));
        label_2->setMaximumSize(QSize(200, 100));
        label_2->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/logo1.png);"));
        pushButton_2 = new QPushButton(LoginBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(144, 268, 101, 41));
        pushButton_2->setFont(font1);
        pushButton_2->setStyleSheet(QString::fromUtf8(""));
        toolButton = new QToolButton(LoginBox);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(348, 7, 31, 20));
        QFont font2;
        font2.setPointSize(11);
        toolButton->setFont(font2);
        toolButton->setStyleSheet(QString::fromUtf8(""));
        label_3 = new QLabel(LoginBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 338, 371, 16));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Candara"));
        font3.setPointSize(10);
        label_3->setFont(font3);
        lineEdit_4 = new QLineEdit(LoginBox);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(178, 197, 161, 21));
        lineEdit_4->setFont(font);
        lineEdit_4->setEchoMode(QLineEdit::Password);
        label_7 = new QLabel(LoginBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(40, 194, 111, 21));
        label_7->setFont(font);
        lineEdit_3 = new QLineEdit(LoginBox);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(178, 231, 161, 21));
        lineEdit_3->setFont(font);
        lineEdit_3->setStyleSheet(QString::fromUtf8(""));
        label_4 = new QLabel(LoginBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(39, 229, 111, 21));
        label_4->setFont(font);
        checkBox = new QCheckBox(LoginBox);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(60, 309, 311, 18));
        checkBox->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));

        retranslateUi(LoginBox);

        QMetaObject::connectSlotsByName(LoginBox);
    } // setupUi

    void retranslateUi(QDialog *LoginBox)
    {
        LoginBox->setWindowTitle(QApplication::translate("LoginBox", "Dialog", 0, QApplication::UnicodeUTF8));
        lineEdit->setText(QString());
        lineEdit_2->setText(QString());
        label->setText(QApplication::translate("LoginBox", "UserName:", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("LoginBox", "Log in", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("LoginBox", "Password:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QString());
        pushButton_2->setText(QApplication::translate("LoginBox", "Register", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("LoginBox", "X", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        lineEdit_4->setText(QString());
        label_7->setText(QApplication::translate("LoginBox", "Password:", 0, QApplication::UnicodeUTF8));
        lineEdit_3->setText(QString());
        label_4->setText(QApplication::translate("LoginBox", "Email", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("LoginBox", "You are above 18 and agree to CGC terms & Conditions.", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LoginBox: public Ui_LoginBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINBOX_H
