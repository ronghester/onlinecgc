#ifndef USERBLOCKINLOCKIN_H
#define USERBLOCKINLOCKIN_H

#include <QWidget>

namespace Ui {
class UserBlockInLockIn;
}

class UserBlockInLockIn : public QWidget
{
    Q_OBJECT
    
public:
    explicit UserBlockInLockIn(QWidget *parent = 0);
    ~UserBlockInLockIn();
    void setName(QString username,QString totalgames);
    void setUserImageandrank(QString mrank,QByteArray userpic);
    void setName(QString username,QString totalgames,QString bet);
    QString getUsername();
    void setBetAmount(QString betamount);
    void setRank(QString mrank);
    void setUserImage(QByteArray img);
    void setTotalGames(QString games);
    void setOnlineStatus(bool status);
private:
    Ui::UserBlockInLockIn *ui;
};

#endif // USERBLOCKINLOCKIN_H
