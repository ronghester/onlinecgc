#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <QObject>
#include <QDebug>
class Mediator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString msg READ msg WRITE setMessage)
    Q_PROPERTY(QString user READ user WRITE addUser)
  //  Q_PROPERTY(QString storeuser READ storeuser WRITE whoami)
  //  Q_PROPERTY(QString appmsg READ appmsg WRITE setappMessage)

    //Q_PROPERTY(QString appmsg READ msg WRITE setMessage)
signals:
    void msgReceived();
public:
    Mediator();

     QString msg() const{
         qDebug()<<"Inside MSg";
         return lastMsg;
     }
     //return all users
     QString user() const{
         return alluser;
     }
     QString storeuser() const{
         return thisuser;
     }
     QString appmsg() const{
         return lastappmsg;
     }
    void setMessage(QString msg);

    void setappMessage(QString appmsg){
        lastappmsg = appmsg;
    }

    void addUser(QString user){
        alluser = user;
        useradded = true;
    }

    void whoami(QString storeuser){
        thisuser = storeuser;
    }

    QString lastMsg;
    QString lastappmsg;
    QString alluser;
    QString thisuser;
    bool useradded;


    
};

#endif // MEDIATOR_H
