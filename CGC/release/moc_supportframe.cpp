/****************************************************************************
** Meta object code from reading C++ file 'supportframe.h'
**
** Created: Sat 4. Apr 00:09:49 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../supportframe.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'supportframe.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SupportFrame[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   14,   13,   13, 0x08,
      56,   49,   13,   13, 0x08,
      79,   49,   13,   13, 0x08,
     104,   13,   13,   13, 0x08,
     140,  135,  117,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SupportFrame[] = {
    "SupportFrame\0\0reply\0plainRequest(QNetworkReply*)\0"
    "RowNum\0CellButtonClicked(int)\0"
    "DeleteButtonClicked(int)\0enterAdmin()\0"
    "QTableWidgetItem*\0text\0"
    "createTableWidgetItem(QString)\0"
};

void SupportFrame::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SupportFrame *_t = static_cast<SupportFrame *>(_o);
        switch (_id) {
        case 0: _t->plainRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 1: _t->CellButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->DeleteButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->enterAdmin(); break;
        case 4: { QTableWidgetItem* _r = _t->createTableWidgetItem((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QTableWidgetItem**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SupportFrame::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SupportFrame::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SupportFrame,
      qt_meta_data_SupportFrame, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SupportFrame::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SupportFrame::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SupportFrame::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SupportFrame))
        return static_cast<void*>(const_cast< SupportFrame*>(this));
    return QWidget::qt_metacast(_clname);
}

int SupportFrame::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
