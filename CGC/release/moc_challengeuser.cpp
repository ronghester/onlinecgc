/****************************************************************************
** Meta object code from reading C++ file 'challengeuser.h'
**
** Created: Sat 4. Apr 00:09:44 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../challengeuser.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'challengeuser.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Challengeuser[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   15,   14,   14, 0x08,
      44,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Challengeuser[] = {
    "Challengeuser\0\0QNR\0Response(QNetworkReply*)\0"
    "on_createChallenge_clicked()\0"
};

void Challengeuser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Challengeuser *_t = static_cast<Challengeuser *>(_o);
        switch (_id) {
        case 0: _t->Response((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 1: _t->on_createChallenge_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Challengeuser::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Challengeuser::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Challengeuser,
      qt_meta_data_Challengeuser, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Challengeuser::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Challengeuser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Challengeuser::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Challengeuser))
        return static_cast<void*>(const_cast< Challengeuser*>(this));
    return QWidget::qt_metacast(_clname);
}

int Challengeuser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
