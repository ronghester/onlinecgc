/****************************************************************************
** Meta object code from reading C++ file 'editprofile.h'
**
** Created: Sat 4. Apr 00:09:43 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../editprofile.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'editprofile.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EditProfile[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      39,   12,   12,   12, 0x08,
      63,   12,   12,   12, 0x08,
      93,   87,   12,   12, 0x08,
     125,   87,   12,   12, 0x08,
     158,   87,   12,   12, 0x08,
     187,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_EditProfile[] = {
    "EditProfile\0\0on_pushButton_2_clicked()\0"
    "on_toolButton_clicked()\0on_pushButton_clicked()\0"
    "reply\0requestReceived(QNetworkReply*)\0"
    "requestReceived2(QNetworkReply*)\0"
    "plainRequest(QNetworkReply*)\0"
    "on_reloadchip_clicked()\0"
};

void EditProfile::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EditProfile *_t = static_cast<EditProfile *>(_o);
        switch (_id) {
        case 0: _t->on_pushButton_2_clicked(); break;
        case 1: _t->on_toolButton_clicked(); break;
        case 2: _t->on_pushButton_clicked(); break;
        case 3: _t->requestReceived((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: _t->requestReceived2((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 5: _t->plainRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 6: _t->on_reloadchip_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData EditProfile::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject EditProfile::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_EditProfile,
      qt_meta_data_EditProfile, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EditProfile::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EditProfile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EditProfile::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EditProfile))
        return static_cast<void*>(const_cast< EditProfile*>(this));
    return QDialog::qt_metacast(_clname);
}

int EditProfile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
