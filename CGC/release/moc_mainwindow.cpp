/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Fri 24. Apr 21:58:46 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      37,   11,   11,   11, 0x0a,
      54,   11,   11,   11, 0x0a,
      69,   11,   11,   11, 0x0a,
      79,   11,   11,   11, 0x0a,
     106,   11,   11,   11, 0x0a,
     129,   11,   11,   11, 0x0a,
     160,  155,   11,   11, 0x0a,
     252,  192,   11,   11, 0x0a,
     350,  329,   11,   11, 0x0a,
     395,   11,   11,   11, 0x0a,
     421,   11,   11,   11, 0x0a,
     437,   11,   11,   11, 0x0a,
     454,   11,   11,   11, 0x08,
     480,   11,   11,   11, 0x08,
     489,   11,   11,   11, 0x08,
     520,   11,   11,   11, 0x08,
     547,   11,   11,   11, 0x08,
     573,   11,   11,   11, 0x08,
     603,   11,   11,   11, 0x08,
     633,  627,   11,   11, 0x08,
     665,  627,   11,   11, 0x08,
     698,   11,   11,   11, 0x08,
     728,   11,   11,   11, 0x08,
     753,   11,   11,   11, 0x08,
     779,  627,   11,   11, 0x08,
     814,  627,   11,   11, 0x08,
     848,   11,   11,   11, 0x08,
     876,   11,   11,   11, 0x08,
     900,  627,   11,   11, 0x08,
     933,   11,   11,   11, 0x08,
     949,  944,   11,   11, 0x08,
     979,   11,   11,   11, 0x08,
     986,   11,   11,   11, 0x08,
    1000,  995,   11,   11, 0x08,
    1032,   11,   11,   11, 0x08,
    1065, 1058,   11,   11, 0x08,
    1113,   11,   11,   11, 0x08,
    1139,   11,   11,   11, 0x08,
    1165,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_loginButton_clicked()\0"
    "onLoginSuccess()\0onLoginClose()\0"
    "runLoop()\0on_profileButton_clicked()\0"
    "createFriendsWidgets()\0createChallengeToFriend()\0"
    "info\0createLockInScreen(Json::Value)\0"
    "wageramount,nid,gametype,title,roomname,matchlevel,playerct\0"
    "Tournament_LockInScreen(QString,QString,QString,QString,QString,QStrin"
    "g,int)\0"
    "roomid,room_type,nid\0"
    "enterLockIn_asAdmin(QString,QString,QString)\0"
    "showLastTournamentLobyy()\0onreInitArena()\0"
    "startChallenge()\0on_toolButton_2_clicked()\0"
    "rumble()\0on_leaderboardButton_clicked()\0"
    "on_competeButton_clicked()\0"
    "on_toolButton_3_clicked()\0"
    "on_gamelabel_clicked(QString)\0"
    "on_toolButton_clicked()\0reply\0"
    "requestReceived(QNetworkReply*)\0"
    "requestReceived2(QNetworkReply*)\0"
    "on_tournamentButton_clicked()\0"
    "on_arenaButton_clicked()\0"
    "on_pushButton_8_clicked()\0"
    "processFriendsInfo(QNetworkReply*)\0"
    "challengeRequest2(QNetworkReply*)\0"
    "on_registerButton_clicked()\0"
    "on_useroption_clicked()\0"
    "challengeRequest(QNetworkReply*)\0"
    "goToHome()\0item\0doSomething(QListWidgetItem*)\0"
    "quit()\0logout()\0link\0"
    "on_label_linkActivated(QString)\0"
    "on_toolButton_4_clicked()\0reason\0"
    "on_show_hide(QSystemTrayIcon::ActivationReason)\0"
    "on_toolButton_4_pressed()\0"
    "on_pushButton_3_clicked()\0"
    "on_username_clicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_loginButton_clicked(); break;
        case 1: _t->onLoginSuccess(); break;
        case 2: _t->onLoginClose(); break;
        case 3: _t->runLoop(); break;
        case 4: _t->on_profileButton_clicked(); break;
        case 5: _t->createFriendsWidgets(); break;
        case 6: _t->createChallengeToFriend(); break;
        case 7: _t->createLockInScreen((*reinterpret_cast< Json::Value(*)>(_a[1]))); break;
        case 8: _t->Tournament_LockInScreen((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7]))); break;
        case 9: _t->enterLockIn_asAdmin((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 10: _t->showLastTournamentLobyy(); break;
        case 11: _t->onreInitArena(); break;
        case 12: _t->startChallenge(); break;
        case 13: _t->on_toolButton_2_clicked(); break;
        case 14: _t->rumble(); break;
        case 15: _t->on_leaderboardButton_clicked(); break;
        case 16: _t->on_competeButton_clicked(); break;
        case 17: _t->on_toolButton_3_clicked(); break;
        case 18: _t->on_gamelabel_clicked((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->on_toolButton_clicked(); break;
        case 20: _t->requestReceived((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 21: _t->requestReceived2((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 22: _t->on_tournamentButton_clicked(); break;
        case 23: _t->on_arenaButton_clicked(); break;
        case 24: _t->on_pushButton_8_clicked(); break;
        case 25: _t->processFriendsInfo((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 26: _t->challengeRequest2((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 27: _t->on_registerButton_clicked(); break;
        case 28: _t->on_useroption_clicked(); break;
        case 29: _t->challengeRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 30: _t->goToHome(); break;
        case 31: _t->doSomething((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 32: _t->quit(); break;
        case 33: _t->logout(); break;
        case 34: _t->on_label_linkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 35: _t->on_toolButton_4_clicked(); break;
        case 36: _t->on_show_hide((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 37: _t->on_toolButton_4_pressed(); break;
        case 38: _t->on_pushButton_3_clicked(); break;
        case 39: _t->on_username_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
