/****************************************************************************
** Meta object code from reading C++ file 'arenascreen.h'
**
** Created: Wed 22. Apr 22:55:54 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../arenascreen.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'arenascreen.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ArenaScreen[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   13,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      37,   31,   12,   12, 0x0a,
      70,   31,   12,   12, 0x0a,
     102,   31,   12,   12, 0x0a,
     131,   31,   12,   12, 0x0a,
     162,   31,   12,   12, 0x0a,
     195,   31,   12,   12, 0x0a,
     233,   12,   12,   12, 0x0a,
     252,   12,   12,   12, 0x0a,
     272,  268,   12,   12, 0x0a,
     301,   12,   12,   12, 0x0a,
     319,   12,   12,   12, 0x0a,
     338,   12,   12,   12, 0x0a,
     353,   12,   12,   12, 0x0a,
     377,   12,   12,   12, 0x0a,
     391,  387,   12,   12, 0x0a,
     421,   12,   12,   12, 0x0a,
     433,   12,   12,   12, 0x08,
     464,   12,   12,   12, 0x08,
     496,   12,   12,   12, 0x08,
     519,   12,   12,   12, 0x08,
     537,   12,   12,   12, 0x08,
     550,   12,   12,   12, 0x08,
     573,   12,   12,   12, 0x08,
     585,   12,   12,   12, 0x08,
     610,   12,   12,   12, 0x08,
     629,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ArenaScreen[] = {
    "ArenaScreen\0\0pt\0moveme(QPoint)\0reply\0"
    "addArenaResponse(QNetworkReply*)\0"
    "requestReceived(QNetworkReply*)\0"
    "plainRequest(QNetworkReply*)\0"
    "allRoomRequest(QNetworkReply*)\0"
    "allRoomRequest_2(QNetworkReply*)\0"
    "LeaderBoard_per_Arena(QNetworkReply*)\0"
    "creatRoomClicked()\0trycreateroom()\0"
    "r,c\0onLobbyTableClicked(int,int)\0"
    "onReportingLoss()\0addUserToChannel()\0"
    "onAPCClicked()\0on_GameButton_clicked()\0"
    "on_home()\0cmd\0on_gamelabel_clicked(QString)\0"
    "showRanks()\0on_createTeamProfile_clicked()\0"
    "on_createArenaProfile_clicked()\0"
    "on_findATeam_clicked()\0clearArenaStuff()\0"
    "leaveTable()\0onPlaceButtonClicked()\0"
    "onSitBack()\0on_Leaderboard_clicked()\0"
    "reallyLeaveTable()\0on_toolButton_2_clicked()\0"
};

void ArenaScreen::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ArenaScreen *_t = static_cast<ArenaScreen *>(_o);
        switch (_id) {
        case 0: _t->moveme((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 1: _t->addArenaResponse((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 2: _t->requestReceived((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 3: _t->plainRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: _t->allRoomRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 5: _t->allRoomRequest_2((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 6: _t->LeaderBoard_per_Arena((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 7: _t->creatRoomClicked(); break;
        case 8: _t->trycreateroom(); break;
        case 9: _t->onLobbyTableClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: _t->onReportingLoss(); break;
        case 11: _t->addUserToChannel(); break;
        case 12: _t->onAPCClicked(); break;
        case 13: _t->on_GameButton_clicked(); break;
        case 14: _t->on_home(); break;
        case 15: _t->on_gamelabel_clicked((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->showRanks(); break;
        case 17: _t->on_createTeamProfile_clicked(); break;
        case 18: _t->on_createArenaProfile_clicked(); break;
        case 19: _t->on_findATeam_clicked(); break;
        case 20: _t->clearArenaStuff(); break;
        case 21: _t->leaveTable(); break;
        case 22: _t->onPlaceButtonClicked(); break;
        case 23: _t->onSitBack(); break;
        case 24: _t->on_Leaderboard_clicked(); break;
        case 25: _t->reallyLeaveTable(); break;
        case 26: _t->on_toolButton_2_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ArenaScreen::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ArenaScreen::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ArenaScreen,
      qt_meta_data_ArenaScreen, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ArenaScreen::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ArenaScreen::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ArenaScreen::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ArenaScreen))
        return static_cast<void*>(const_cast< ArenaScreen*>(this));
    return QWidget::qt_metacast(_clname);
}

int ArenaScreen::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    }
    return _id;
}

// SIGNAL 0
void ArenaScreen::moveme(QPoint _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
