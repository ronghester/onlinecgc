/****************************************************************************
** Meta object code from reading C++ file 'lockin.h'
**
** Created: Wed 22. Apr 22:55:54 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../lockin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lockin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LockIn[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      18,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      32,    7,    7,    7, 0x08,
      67,   58,    7,    7, 0x08,
     104,    7,    7,    7, 0x08,
     130,    7,    7,    7, 0x08,
     156,    7,    7,    7, 0x08,
     180,    7,    7,    7, 0x08,
     204,    7,    7,    7, 0x08,
     227,    7,    7,    7, 0x08,
     239,    7,    7,    7, 0x08,
     257,    7,    7,    7, 0x08,
     279,    7,    7,    7, 0x08,
     307,  301,    7,    7, 0x0a,
     336,    7,    7,    7, 0x0a,
     356,    7,    7,    7, 0x0a,
     371,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_LockIn[] = {
    "LockIn\0\0he_lost()\0joinChannel()\0"
    "on_pushButton_6_clicked()\0position\0"
    "on_horizontalSlider_sliderMoved(int)\0"
    "on_pushButton_4_clicked()\0"
    "on_pushButton_3_clicked()\0"
    "on_toolButton_clicked()\0on_sendtochat_clicked()\0"
    "onPlaceButtonClicked()\0onSitBack()\0"
    "onReportingLoss()\0on_teamAwin_clicked()\0"
    "on_teamBwin_clicked()\0reply\0"
    "plainRequest(QNetworkReply*)\0"
    "subscribethisroom()\0loadFinished()\0"
    "onReportLossClicked()\0"
};

void LockIn::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LockIn *_t = static_cast<LockIn *>(_o);
        switch (_id) {
        case 0: _t->he_lost(); break;
        case 1: _t->joinChannel(); break;
        case 2: _t->on_pushButton_6_clicked(); break;
        case 3: _t->on_horizontalSlider_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_pushButton_4_clicked(); break;
        case 5: _t->on_pushButton_3_clicked(); break;
        case 6: _t->on_toolButton_clicked(); break;
        case 7: _t->on_sendtochat_clicked(); break;
        case 8: _t->onPlaceButtonClicked(); break;
        case 9: _t->onSitBack(); break;
        case 10: _t->onReportingLoss(); break;
        case 11: _t->on_teamAwin_clicked(); break;
        case 12: _t->on_teamBwin_clicked(); break;
        case 13: _t->plainRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 14: _t->subscribethisroom(); break;
        case 15: _t->loadFinished(); break;
        case 16: _t->onReportLossClicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LockIn::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LockIn::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_LockIn,
      qt_meta_data_LockIn, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LockIn::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LockIn::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LockIn::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LockIn))
        return static_cast<void*>(const_cast< LockIn*>(this));
    return QWidget::qt_metacast(_clname);
}

int LockIn::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void LockIn::he_lost()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void LockIn::joinChannel()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
