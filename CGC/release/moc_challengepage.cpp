/****************************************************************************
** Meta object code from reading C++ file 'challengepage.h'
**
** Created: Sat 4. Apr 00:09:45 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../challengepage.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'challengepage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ChallengePage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      36,   30,   14,   14, 0x08,
      65,   30,   14,   14, 0x08,
      95,   14,   14,   14, 0x08,
     120,  113,   14,   14, 0x08,
     142,  113,   14,   14, 0x08,
     165,  113,   14,   14, 0x08,
     192,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ChallengePage[] = {
    "ChallengePage\0\0loadFinished()\0reply\0"
    "plainRequest(QNetworkReply*)\0"
    "plainRequest2(QNetworkReply*)\0"
    "acceptChallenge()\0RowNum\0declineChallenge(int)\0"
    "CellButtonClicked(int)\0"
    "enterInChallengeLobby(int)\0checkChallenges()\0"
};

void ChallengePage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ChallengePage *_t = static_cast<ChallengePage *>(_o);
        switch (_id) {
        case 0: _t->loadFinished(); break;
        case 1: _t->plainRequest((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 2: _t->plainRequest2((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 3: _t->acceptChallenge(); break;
        case 4: _t->declineChallenge((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->CellButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->enterInChallengeLobby((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->checkChallenges(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ChallengePage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ChallengePage::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ChallengePage,
      qt_meta_data_ChallengePage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ChallengePage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ChallengePage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ChallengePage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChallengePage))
        return static_cast<void*>(const_cast< ChallengePage*>(this));
    return QWidget::qt_metacast(_clname);
}

int ChallengePage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
