/********************************************************************************
** Form generated from reading UI file 'infobox.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFOBOX_H
#define UI_INFOBOX_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_InfoBox
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *infolabel;
    QToolButton *toolButton_2;

    void setupUi(QDialog *InfoBox)
    {
        if (InfoBox->objectName().isEmpty())
            InfoBox->setObjectName(QString::fromUtf8("InfoBox"));
        InfoBox->resize(319, 148);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(InfoBox->sizePolicy().hasHeightForWidth());
        InfoBox->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Rockwell"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        InfoBox->setFont(font);
        InfoBox->setStyleSheet(QString::fromUtf8("#InfoBox{\n"
"\n"
"border: 1px solid #000;\n"
"background-color:  rgb(38, 38, 38);\n"
"}"));
        verticalLayout = new QVBoxLayout(InfoBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(2, 2, 2, 1);
        infolabel = new QLabel(InfoBox);
        infolabel->setObjectName(QString::fromUtf8("infolabel"));
        infolabel->setFont(font);
        infolabel->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);\n"
"color:#fff"));
        infolabel->setAlignment(Qt::AlignCenter);
        infolabel->setWordWrap(true);
        infolabel->setMargin(0);

        verticalLayout->addWidget(infolabel);

        toolButton_2 = new QToolButton(InfoBox);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(toolButton_2->sizePolicy().hasHeightForWidth());
        toolButton_2->setSizePolicy(sizePolicy1);
        toolButton_2->setMaximumSize(QSize(16777215, 36));
        toolButton_2->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/loginbtn.png);"));

        verticalLayout->addWidget(toolButton_2);


        retranslateUi(InfoBox);

        QMetaObject::connectSlotsByName(InfoBox);
    } // setupUi

    void retranslateUi(QDialog *InfoBox)
    {
        InfoBox->setWindowTitle(QApplication::translate("InfoBox", "Dialog", 0, QApplication::UnicodeUTF8));
        infolabel->setText(QString());
        toolButton_2->setText(QApplication::translate("InfoBox", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class InfoBox: public Ui_InfoBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFOBOX_H
