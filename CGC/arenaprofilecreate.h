#ifndef ARENAPROFILECREATE_H
#define ARENAPROFILECREATE_H

#include <QDialog>
#include <QLineEdit>
namespace Ui {
class ArenaProfileCreate;
}

class ArenaProfileCreate : public QDialog
{
    Q_OBJECT
    
public:
    explicit ArenaProfileCreate(QWidget *parent = 0);
    ~ArenaProfileCreate();
    QPushButton* createarenaprofile;
    QLineEdit* profilename;
    
private slots:
    void on_toolButton_clicked();

    void on_pushButton_clicked();

private:
    Ui::ArenaProfileCreate *ui;
};

#endif // ARENAPROFILECREATE_H
