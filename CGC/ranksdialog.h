#ifndef RANKSDIALOG_H
#define RANKSDIALOG_H

#include <QDialog>

namespace Ui {
class RanksDialog;
}

class RanksDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit RanksDialog(QWidget *parent = 0);
    ~RanksDialog();
    
private:
    Ui::RanksDialog *ui;
};

#endif // RANKSDIALOG_H
