#ifndef TEAMFINDJOIN_H
#define TEAMFINDJOIN_H

#include <QDialog>
#include <QStringList>
#include <QNetworkReply>
#include <infobox.h>

namespace Ui {
class TeamFindJoin;
}

class TeamFindJoin : public QDialog
{
    Q_OBJECT
    
public:
    explicit TeamFindJoin(QWidget *parent = 0);
    ~TeamFindJoin();
    int count;
    void setValues(QStringList list);
    
private slots:
    void on_toolButton_clicked();

    void on_jointeam_clicked();

    void on_search_team_clicked();

      void plainRequest(QNetworkReply* reply);

private:
    Ui::TeamFindJoin *ui;
     InfoBox* IBox;
};

#endif // TEAMFINDJOIN_H
