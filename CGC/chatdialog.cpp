#include "chatdialog.h"
#include "ui_chatdialog.h"
#include <QDebug>
#include <mainwindow.h>
extern MainWindow* w;

extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern QString thatuser;


ChatDialog::ChatDialog(QWidget *parent,bool istype2,QString channel) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::ChatDialog)
{
    ui->setupUi(this);
    //Add this user to this chat group first
    if(istype2){
        chat_room = channel;
        thatuser = "notme";
    }
    else{
    chat_room = "Chatroom_"+thatuser+"_"+thisuser;

    }
    chatcounter = 0;
}

ChatDialog::~ChatDialog()
{
    hide();
    delete ui;
}



void ChatDialog::on_pushButton_clicked()
{
     sendChatMsg(ui->lineEdit->text());
}
void ChatDialog::addChatMsg(QString msg){
    QListWidgetItem* QWL = new QListWidgetItem(msg);
    ui->listWidget->insertItem(chatcounter,QWL);
    ui->listWidget->scrollToBottom();
    chatcounter++;
}

void ChatDialog::keyPressEvent(QKeyEvent *event){
     if(event->key() == Qt::Key_Return){
         sendChatMsg(ui->lineEdit->text());
     }
}

void ChatDialog::sendChatMsg(QString msg){
    if(msg.size()!=0){
          QString surl = "<url>Friends_chatmsg?nid="+chat_room+"&desc="+thisuser+":"+msg;
          qDebug()<<surl;
          QNetworkAccessManager *manager = new QNetworkAccessManager(this);
          connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
          QNetworkRequest request2 = QNetworkRequest(surl);
          QString ss = sessionname+"="+sessionid;
          request2.setRawHeader("Cookie",ss.toUtf8() );
          manager->get(request2);
         ui->lineEdit->setText("");
    }
}
void ChatDialog::plainRequest(QNetworkReply* reply){
     QString replyText;
     QByteArray rawData = reply->readAll();
     QString textData(rawData);
     qDebug() << "CCC"+ textData;



}
