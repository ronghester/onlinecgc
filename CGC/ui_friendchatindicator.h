/********************************************************************************
** Form generated from reading UI file 'friendchatindicator.ui'
**
** Created: Sat 4. Apr 00:08:42 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRIENDCHATINDICATOR_H
#define UI_FRIENDCHATINDICATOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FriendChatIndicator
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *name;
    QToolButton *toolButton;

    void setupUi(QWidget *FriendChatIndicator)
    {
        if (FriendChatIndicator->objectName().isEmpty())
            FriendChatIndicator->setObjectName(QString::fromUtf8("FriendChatIndicator"));
        FriendChatIndicator->resize(144, 22);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FriendChatIndicator->sizePolicy().hasHeightForWidth());
        FriendChatIndicator->setSizePolicy(sizePolicy);
        FriendChatIndicator->setMinimumSize(QSize(0, 0));
        FriendChatIndicator->setMaximumSize(QSize(16777215, 16777215));
        FriendChatIndicator->setAutoFillBackground(false);
        FriendChatIndicator->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/button-plain.png);\n"
""));
        horizontalLayout = new QHBoxLayout(FriendChatIndicator);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        name = new QLabel(FriendChatIndicator);
        name->setObjectName(QString::fromUtf8("name"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(name->sizePolicy().hasHeightForWidth());
        name->setSizePolicy(sizePolicy1);
        name->setMaximumSize(QSize(16777215, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(10);
        name->setFont(font);
        name->setStyleSheet(QString::fromUtf8(""));
        name->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(name);

        toolButton = new QToolButton(FriendChatIndicator);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/new/prefix1/resources/chatindicator_close.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton->setIcon(icon);
        toolButton->setIconSize(QSize(18, 18));

        horizontalLayout->addWidget(toolButton);


        retranslateUi(FriendChatIndicator);

        QMetaObject::connectSlotsByName(FriendChatIndicator);
    } // setupUi

    void retranslateUi(QWidget *FriendChatIndicator)
    {
        FriendChatIndicator->setWindowTitle(QApplication::translate("FriendChatIndicator", "Form", 0, QApplication::UnicodeUTF8));
        name->setText(QString());
        toolButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FriendChatIndicator: public Ui_FriendChatIndicator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRIENDCHATINDICATOR_H
