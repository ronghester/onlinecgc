#include "challengeuser.h"
#include "ui_challengeuser.h"
#include "jsonparser.h"
#include "mainwindow.h"
extern std::vector<arenaobject> availablearenas;
extern QStringList arenanames;
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern QString thatuser;
extern QString thatuser_id;


Challengeuser::Challengeuser(QWidget *parent,QString friendsname) :
    QWidget(parent),
    ui(new Ui::Challengeuser)
{
    ui->setupUi(this);
    this->friendsname = friendsname;
    ui->challenge_user->setText(thatuser);
    QStringList arenaitems;
    for(int i =0;i<availablearenas.size();i++){
        qDebug()<<"***"<<availablearenas[i].arenaname;
        arenaitems<<availablearenas[i].arenaname;
    }

    //qDebug()<<arenaitems;
    ui->arenacombo->addItems(arenaitems);
    ui->challenge_user->setReadOnly(true);
ui->dateTimeEdit->setDate(QDate::currentDate());
}

Challengeuser::~Challengeuser()
{
    delete ui;
}
void Challengeuser::GetRequest(QString url)
{
      QString surl = url;
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(Response(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
}

void Challengeuser::Response(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         QString username = "Some Error";
         return;
     }

     Json::Reader reader;
     Json::Value root;
     QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
        if(root.size() == 0) return;
         int index = 0;
         const Json::Value friends = root;
         QString messagetype = QString().fromStdString(friends.get("MessageType","none").asCString());
         QString response = QString().fromStdString(friends.get("Response","none").asCString());

         if(messagetype == "Challenge_Create_Error"){
             IBox = new InfoBox(this);
             IBox->setMessage("Failed! Other user is not signed up for this arena yet!");
             IBox->show();
         }
         else if(response == "Success"){
             IBox = new InfoBox(this);
             IBox->setMessage("Challenge Created! Invite Sent");
             IBox->show();
         }
         //QString onlinestatus = QString().fromStdString(friends.get("online","none").asCString());
        // QString userstatusline = QString().fromStdString(friends.get("statusline","none").asCString());
         //qDebug()<<username<<" ** "<<userchips<< " ** ";

        // FriendsWidget* thisfrnd = new FriendsWidget(0);
       // FC->addFriendElement(username,userchips,userstatusline,onlinestatus);
       //  myFriends.push_back(thisfrnd);
        //  QTimer::singleShot(2000, w, SLOT(createFriendsWidgets()));

     }
}

void Challengeuser::on_createChallenge_clicked()
{
    QString surl = "<url>createchallenge?";
    QString arenaid;
    for(int i =0 ;i<availablearenas.size();i++)
        if(availablearenas[i].arenaname == ui->arenacombo->currentText())
            arenaid = availablearenas[i].arenacode;

    surl += "title=newchallenge&desc="+ui->desc->text();
    surl += "&uid="+thatuser_id+"&nid="+arenaid+"&when="+ui->dateTimeEdit->text();
    surl += "&gametype="+ui->comboBox_2->currentText();
    surl += "&wager="+ui->lineEdit_3->text();

    QString wager = ui->lineEdit_3->text();
    bool ok;
    int w = wager.toInt(&ok);
    bool bets = w % 10 ==0 ? true : false;
    if(!ok | w < 10 | !bets) {
        IBox = new InfoBox(this);
        IBox->setMessage("Wager must be number & in order of 10");
        IBox->show();
        return;
    }


    qDebug()<<surl;
    GetRequest(surl);
}
