#include "arenascreen.h"
#include "ui_arenascreen.h"
#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include <QScrollArea>
#include <QInputDialog>
#include <QMessageBox>
#include <mainwindow.h>
#include <infobox.h>
#include <QTableWidget>
#include <QTimer>
#include <QPropertyAnimation>
#include <QtAlgorithms>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern MainWindow* w;
extern int userchips;
extern QString onlineuser_string;
extern QString currentArenaName;
extern std::vector<arenaobject> availablearenas;
//  Json::Value lastnews;
ArenaScreen::ArenaScreen(QWidget *parent, int type) :
    QWidget(parent),
    ui(new Ui::ArenaScreen)
{
    ui->setupUi(this);
   index = -1;
   lock =0;
   lobbies = 0;
   unlocking = false;
   backtolobby = false;
   currentGameType = "";
   currentWagerAmount = 0;
   currentRoomId = "";
   hasMovedToLobby = false;
   hasMovedToGame = false;
   hasMadeAPlaceBetRequest = false;
   issatdown  = false;
   room = NULL;
   //type = 0 and type = 1
   /*ui->listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   QListWidgetItem* QWL = new QListWidgetItem(" 200 Players Online");
   QWL->setIcon(QIcon(":/new/prefix1/resources/status-green.png"));
   ui->listWidget->insertItem(0,QWL);
   QWL = new QListWidgetItem(" 300 Players Active");
   QWL->setIcon(QIcon(":/new/prefix1/resources/white-dot.png"));
   ui->listWidget->insertItem(1,QWL);*/

   if(type == 0){
       //createArenaProfile
       is_subscribed = false;
   }
   else if(type ==1){
       ui->createArenaProfile->hide();
       is_subscribed = true;
   }

  // lock->hide();
   setPlayerCount(onlineuser_string);
   tableWidget = 0;


}

ArenaScreen::~ArenaScreen(){
//delete ui;
    qDebug()<<"Here: INDES";
}



void ArenaScreen::setPlayerCount(QString br){
    ui->onlineplayercount->setText(br);
}

void ArenaScreen::setValues(Json::Value news){
   // lastnews = news;
    QString cmd = QString().fromStdString(news.get("node_title","none").asCString());
     cind = QString().fromStdString(news.get("nid","none").asCString());
    //ui->ArenaScreen_GameLabel1->setText(cmd);
   // ui->ArenaScreen_GameLabel2->setText(cmd);
     currentGameName = cmd;
    QString  newsdetails = "<url>api/views/arenadetails?field_parent_arena_target_id="+cind;
    getRequest(newsdetails);

    QByteArray by = QByteArray::fromBase64(news.get("img","none").asCString());
    QImage image = QImage::fromData(by, "PNG");
   // QPixmap mypix (image);
  //  ui->label->setPixmap(QPixmap::fromImage(image));


   ui->label->setPixmap(QPixmap::fromImage(image));
    QObject::connect(ui->label,SIGNAL(clicked()),this,SLOT(on_home()));
   // QObject::connect(b41,SIGNAL(clicked()),this,SLOT(on_home()));
}

//This is to place lockin screen over the arena lobby screen...
//Initially thought to put this in the mainwindow but it is crashing the app also we need a easy way to
//to switch between lobbies and lobby room..
void ArenaScreen::on_home(){
      QTimer::singleShot(600, w, SLOT(onreInitArena()));
}

void ArenaScreen::clearArenaStuff(){
    //ui->frame_2->hide();
   //qDebug()<<"Inside Cleaning Code";

    hasMovedToGame = false;
    hasMovedToLobby = true;
    QLayout* layout = ui->ArenaScreenSlot->layout ();
    QWidget *w;
    if (layout != 0)
    {
       QLayoutItem *item;
       while ((item = layout->takeAt(0)) != 0){
          // qDebug()<<"Inside Cleaning Code ";
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}
       }
       delete layout;
     }

    lock  = new LockIn();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(lock);
    top->setSpacing(0);
    top->setContentsMargins(0,0,0,0);
    ui->ArenaScreenSlot->setLayout(top);
    ui->ArenaScreenSlot->show();
    if(index != -1 && lobbies.size()>index){
        Json::Value tem = lobbies[index];
        QString title = QString().fromStdString(tem.get("node_title","none").asCString());
        QString desc = QString().fromStdString(tem.get("body","none").asCString());
        QString wager = QString().fromStdString(tem.get("wager","none").asCString());
        currentWagerAmount = wager.toInt();
        currentGameType = QString().fromStdString(tem.get("number","none").asCString());
        QString gametype = QString().fromStdString(tem.get("gametype","none").asCString());
        currentRoomId = QString().fromStdString(tem.get("nid","none").asCString());
        QString when = QString().fromStdString(tem.get("when","none").asCString());
        title.replace("&#039;s","'s");
        QString lbtext = "Game Name: "+title+"\nNumber of Player: "+currentGameType+"\nWager Amount: "+wager+"\nChallenge Details: "+desc;
        lock->setDetailLabel(lbtext,currentWagerAmount);
        lock->betslider->setMinimum(currentWagerAmount);
        lock->wagerslide->setText(QString().number(currentWagerAmount));
        QString roomname = "room_"+cind+"lobbyid_"+currentRoomId;
        lock->This_Roomname = roomname;
        lock->roomname = roomname;
        lock->currentRoomId = currentRoomId;
        lock->currentGameType = gametype;

    }

     connect(lock->ViewLobby,SIGNAL(clicked()),this,SLOT(leaveTable()));
     connect(lock->placebet,SIGNAL(clicked()),this,SLOT(onPlaceButtonClicked()));
     connect(lock->sitdown,SIGNAL(clicked()),this,SLOT(onSitBack()));
     connect(lock,SIGNAL(he_lost()),this,SLOT(onReportingLoss()));
     //connect(lock,SIGNAL(joinChannel()),this,SLOT(addUserToChannel()));
     addUserToChannel();
   //  lock->ViewLobby->hide();
}

bool ArenaScreen::caseInsensitiveLessThan(const QString &s1, const QString &s2){
     QStringList pair = s1.split(":");
     QStringList pair1 = s2.split(":");
     return pair.at(1).toInt() > pair1.at(1).toInt();
}

void ArenaScreen::onReportingLoss(){
    //Instead of reporting the loss we are saying claim winner...
    //get all the bet amount and make him winnner..
    qDebug()<<"You are obsolte winner";
    QString roomname = "room_"+cind+"lobbyid_"+currentRoomId;
    //defeated team variable
    QString desc;
    QString nitems;
    //winner team variable
    QString wager;
    QString number;



    QStringList defeatedteam;
    QStringList winnerteam;
    for(int i=0;i<lock->betusers.size();i++){
         QStringList pair = lock->betusers.at(i).split(":");
          if(pair.at(2) == lock->getTeamType()){
              defeatedteam += lock->betusers.at(i);
          }
          else{
              winnerteam += lock->betusers.at(i);
          }
    }
    //Lets arrange defeated and winner team in ascending and descending order
    qSort(defeatedteam.begin(),defeatedteam.end(),caseInsensitiveLessThan);
    qSort(winnerteam.begin(),winnerteam.end(),caseInsensitiveLessThan);

  for(int i=0;i<defeatedteam.size();i++){

        QStringList pair =defeatedteam.at(i).split(":");

            nitems += pair.at(3)+":";
            number += pair.at(1)+":"; //They should loose what they bet for..

  }

  for(int i=0;i<winnerteam.size();i++){
      QStringList pair =winnerteam.at(i).split(":");

      wager += pair.at(3)+":";
      desc += pair.at(1)+":"; //They should gain what they bet for
  }

  /*  for(int i=0;i<lock->betusers.size();i++){
        QStringList pair = lock->betusers.at(i).split(":");
       // qDebug()<<lock->betusers.at(i);
        //Lets arrange them in the ascending order first.



        if(pair.at(2) == lock->getTeamType()){ //this is defeated team
            nitems += pair.at(3)+":";
            desc += pair.at(1)+":";
        }
        else{
            wager += pair.at(3)+":";
            number += pair.at(1)+":";
        }
    }*/

    QString surl = "<url>setWinner?nid="+roomname+"&gametype="+currentRoomId+"&desc="+desc+"&nitems="+nitems+"&uid="+QString().number(mainid)+"&wager="+wager+"&number="+number;
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

        lock->ViewLobby->show();
        lock->closelockin->hide();
}

void ArenaScreen::leaveTable(){

    if(lock->resultdeclared){
        backtolobby = true;
        hasMovedToGame = false;
         unlocking = false;
         ui->ArenaScreenSlot->hide();
        // issatdown = true;
         hasMadeAPlaceBetRequest = false;
        QTimer::singleShot(600, this, SLOT(on_GameButton_clicked()));
        //Also clear the lock variables
        lock->cleanup();
        issatdown = false;

       lock = 0;
    }

    else if(lock->gamestarted && !lock->resultdeclared){
        if(lock->satdownuserlist.contains(currentArenaName)){
           alert = new AlertDialog(this);
           alert->setText("Are you sure you want to leave the lobby?\n Leaving the room will forfeit the match");
           alert->show();
           issatdown = false;
           connect(alert,SIGNAL(okClicked()),this,SLOT(reallyLeaveTable()));
        }
        else{
            //let him go
            backtolobby = true;
            hasMovedToGame = false;
             unlocking = false;
             ui->ArenaScreenSlot->hide();
            // issatdown = true;
             hasMadeAPlaceBetRequest = false;
            QTimer::singleShot(600, this, SLOT(on_GameButton_clicked()));
            //Also clear the lock variables
            lock->cleanup();
           // issatdown = false;

           lock = 0;
        }

    }
    else{
        backtolobby = true;
        hasMovedToGame = false;
         unlocking = false;
         ui->ArenaScreenSlot->hide();
         issatdown = true;
         hasMadeAPlaceBetRequest = false;
         //Lets move this fella from the sitdown list..
         leavesitdown();
        QTimer::singleShot(600, this, SLOT(on_GameButton_clicked()));
        //Also clear the lock variables
        lock->cleanup();
        issatdown = false;

       lock = 0;
    }


}

void ArenaScreen::leavesitdown(){
    QString nid = "room_"+cind+"lobbyid_"+currentRoomId;
    QString surl = "<url>sendmessage_tochannel?uid="+QString().number(mainid)+"&since="+cind+"&nitems="+currentRoomId+"&title=unsitdown&desc="+currentArenaName+"&nid="+nid;

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
     // issatdown = true;


}

void ArenaScreen::reallyLeaveTable(){

    if(alert) alert->hide();

    lock->fillEmptyBets();
    onReportingLoss();
    /*backtolobby = true;
    hasMovedToGame = true;
     unlocking = false;
     ui->ArenaScreenSlot->hide();
     issatdown = true;
     hasMadeAPlaceBetRequest = false;
    QTimer::singleShot(600, this, SLOT(on_GameButton_clicked()));
    //Also clear the lock variables
    lock->cleanup();
    issatdown = false;

   lock = 0;*/
}

void ArenaScreen::getRequest(QString surl){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl(surl)));
}

void ArenaScreen::on_gamelabel_clicked(QString cmd){
    qDebug()<< "label clicked "+cmd;
    int cint = cmd.toInt();
    QLayout* layout = ui->frame_4->layout ();
    QWidget *w;
    if (layout != 0)
    {
       QLayoutItem *item;
       while ((item = layout->takeAt(0)) != 0){
          // qDebug()<<"Inside Cleaning Code ";
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}
       }
       delete layout;
     }

    //Now Create the layout
    QHBoxLayout *newsbox=new QHBoxLayout();
    QVBoxLayout *top = new QVBoxLayout;
    QWidget *test = new QWidget();

   newsbox->setContentsMargins(0,0,0,0);
  // top->setContentsMargins(0,0,0,0);
    newsbox->setAlignment(Qt::AlignTop);
    const int icmd = cmd.toInt();

    for(int i=0;i<newsarray.size();i++){

     const Json::Value tem = newsarray[i];
     QString title = QString().fromStdString(tem.get("node_title","none").asCString());
     QString icmd = QString().number(i);
     QString des = QString().fromStdString(tem.get("body","none").asCString());

     if(cmd == icmd){
         QLabel *b41 = new QLabel();

      b41->setTextFormat(Qt::RichText);
    // b41->setStyleSheet("background-color:rgba(133,0,0,0)");
      b41->setWordWrap(true);
      b41->setText("<h3 style=\"color:#FFFFFF\">"+title+"</h3><p style=\"color:#FFFFFF\">"+des+"</p>");
    // QListWidgetItem* QWL = new QListWidgetItem(stext);
          b41->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
          b41->setMaximumSize(400,300);
      top->addWidget(b41);

         // b41->setMaximumSize(400,300);
      test->setMaximumWidth(400);
      test->setMaximumHeight(300);
     test->setLayout(top);
      newsbox->addWidget(test);



     }
    }
     ui->frame_4->setLayout(newsbox);

    qDebug()<<"clicked on nid: "<<cmd;
}
void ArenaScreen::requestReceived(QNetworkReply* reply){
    if(!this->isVisible()) return;
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << "news" + textData;
    if(hasMovedToLobby) return;
    QVBoxLayout *newsbox=new QVBoxLayout();
      newsbox->setAlignment(Qt::AlignTop);
    newsbox->setContentsMargins(0,0,0,0);
    if(textData.length() < 5) {
        QLabel *b41=new QLabel("No News I am afraid!",0);
        QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
        effect->setBlurRadius(9.0);
        effect->setColor(QColor(254, 254, 254, 255));
        effect->setOffset(0.2);
         b41->setGraphicsEffect(effect);
         newsbox->addWidget(b41);

         ui->frame_4->setLayout(newsbox);
         return;
    }
     Json::Reader reader;
     Json::Value root;

     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     newsarray = root;
     if(!parsingSuccessful || root.size() == 0){
         QLabel *b41=new QLabel("No News I am afraid!",0);
         QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
         effect->setBlurRadius(9.0);
         effect->setColor(QColor(254, 254, 254, 255));
         effect->setOffset(0.2);
          b41->setGraphicsEffect(effect);
          newsbox->addWidget(b41);
     }
     else{
          signalMapper = new QSignalMapper(this);
          connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_gamelabel_clicked(QString)));
         for(int i=0;i<root.size();i++){


             QHBoxLayout *top = new QHBoxLayout;
             QWidget *test = new QWidget();

              const Json::Value tem = root[i];
              QString title = QString().fromStdString(tem.get("node_title","none").asCString());
              QString des = QString().fromStdString(tem.get("body","none").asCString());
              QByteArray by = QByteArray::fromBase64(tem.get("news_pic","none").asCString());
              QImage image = QImage::fromData(by, "JPG");

              GameLabel *b41=new GameLabel("",0);
              connect(b41, SIGNAL(clicked()), signalMapper, SLOT(map()));
              signalMapper->setMapping(b41, QString().number(i));
              QObject::connect(b41,SIGNAL(pressed(cmd)),this,SLOT(on_gamelabel_clicked(cmd)));
              b41->setTextFormat(Qt::RichText);
              b41->setStyleSheet("background-color:rgba(133,0,0,0)");
              b41->setWordWrap(true);
              if(root.size() > 1)
                 b41->setMaximumSize(248,68);

              b41->setText("<h3 style=\"color:#FFFFFF\">"+title+"</h3><p style=\"color:#FFFFFF\">"+des+"</p>");
                // QListWidgetItem* QWL = new QListWidgetItem(stext);
              QLabel *b42=new QLabel(this);
              b42->setStyleSheet("border-image:url(:/new/prefix1/resources/profile-frame-small.png)");
              b42->setPixmap(QPixmap().fromImage(image));
              b42->setMaximumSize(112,112);



              b41->setAlignment(Qt::AlignTop | Qt::AlignVCenter);
              top->addWidget(b41);
              top->addWidget(b42);
              b42->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
              //top->setContentsMargins(0,0,0,0);
             // top->setSpacing(0);

              //test->setStyleSheet("{QWidget{border: 2px solid white;}}");
              test->setMaximumWidth(410);
              //->addWidget(test);
              //newslist->insertItem(i,QWL);
              //QScrollArea *sca = new QScrollArea(this);
             // sca->setWidget(top);
               test->setLayout(top);
               newsbox->addWidget(test);

         }

     }

     ui->frame_4->setLayout(newsbox);
}

void ArenaScreen::on_createTeamProfile_clicked()
{
    createTeam = new SampleDialog(this);
    createTeam->show();
}

void ArenaScreen::checkForNewRooms(){
    if(tableWidget == 0) return;

    if(unlocking || lock != 0 || !tableWidget->isVisible()) return;
    QString urld =  "<url>api/views/room_view?&field_parent_arena_forroom_target_id="+cind;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(allRoomRequest(QNetworkReply*)));
    QNetworkRequest request = QNetworkRequest(urld);
    QString ss = sessionname+"="+sessionid;
    request.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request);
}

void ArenaScreen::on_GameButton_clicked()
{
    //This is button in the arena screen;
    //Ste 1 : Remove layout and add the lobbies
    if(!is_subscribed){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Create Profile\n For This game first!");
        IBox->show();
        return;
    }
try{
    hasMovedToLobby = true;
    QLayout* layout = ui->ArenaScreenSlot->layout ();
    QWidget *w;
       if (layout != 0)
       {
       QLayoutItem *item;
       while ((item = layout->takeAt(0)) != 0){
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}

       }
       delete layout;
       }
       //Step 2 : Create the top buttons and table

        //////////////////
        QHBoxLayout *TBLayout=new QHBoxLayout();
        QLabel *secondname = new QLabel( currentGameName+" Arena");
        secondname->setStyleSheet("QLabel {color:#fff}");
        QFont f( "Candara", 12, QFont::Bold);
        secondname->setFont(f);

       //secondname->setStyle(ui->ArenaScreen_GameLabel1->style());
       // secondname->setFont(QFont("Candara"));
        secondname->setAlignment(Qt::AlignTop| Qt::AlignLeft);
        QPushButton *b21=new QPushButton("Create Room");
        b21->setMaximumWidth(120);
       // QPushButton *b22=new QPushButton("Match Making");
       // QPushButton *b23=new QPushButton("Filters");
        TBLayout->addWidget(secondname);

         TBLayout->addSpacerItem(new QSpacerItem(40, 10, QSizePolicy::Minimum, QSizePolicy::Expanding));
          TBLayout->addWidget(b21);
           //TBLayout->addWidget(b22);
           // TBLayout->addWidget(b23);
             TBLayout->setContentsMargins(0,0,0,0);
            QHBoxLayout *MLayout = new QHBoxLayout();
            MLayout->setSpacing(0);
            MLayout->setContentsMargins(0,0,0,0);

            tableWidget = new QTableWidget(4, 5, this);
            tableWidget->setColumnWidth(0,124);
            tableWidget->setColumnWidth(1,124);
            tableWidget->setColumnWidth(2,144);
              tableWidget->setColumnWidth(3,84);
                tableWidget->setColumnWidth(4,74);

            tableWidget->verticalHeader()->hide();
            tableWidget->verticalHeader()->stretchLastSection();
            tableWidget->setStyleSheet("QTableView {color:#fff; selection-background-color: rgb(70,70,70);}");
           QStringList m_TableHeader;
           m_TableHeader<<"Game Name"<<"Number of Players"<<"Description"<<"Players"<<"Wager";
           tableWidget->setHorizontalHeaderLabels(m_TableHeader);
           tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
           tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
           tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
           tableWidget->hide();

            MLayout->addWidget(tableWidget);
           QWidget* slideWidget1=new QWidget();
           QWidget*  slideWidget2=new QWidget();
            slideWidget1->setLayout(TBLayout);
            slideWidget1->setMinimumHeight(34);
            slideWidget1->setMaximumHeight(40);
            slideWidget2->setLayout(MLayout);
            final = new QVBoxLayout();
            final->setSpacing(0);
            final->setContentsMargins(0,0,0,0);
            final->addWidget(slideWidget1);
            final->addWidget(slideWidget2);
            ui->ArenaScreenSlot->setLayout(final);
            tableWidget->setStyleSheet("{background-color:rgba(142,113,112,90); font: 12pt \"Candara\";color: #fff;}");

 tableWidget->show();
 connect(tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(onLobbyTableClicked(int,int)));
           connect( b21, SIGNAL( clicked() ), this, SLOT( creatRoomClicked() ) );

           //Also lets see if there are any lobbies available and populate them
           QString urld =  "<url>api/views/room_view?&field_parent_arena_forroom_target_id="+cind;
           QNetworkAccessManager *manager = new QNetworkAccessManager(this);
           connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(allRoomRequest(QNetworkReply*)));
           QNetworkRequest request = QNetworkRequest(urld);
           QString ss = sessionname+"="+sessionid;
           request.setRawHeader("Cookie",ss.toUtf8() );
           manager->get(request);

 ui->frame->show();
 ui->ArenaScreenSlot->show();
 currentGameType = "";
    }
    catch(...){
        qCritical()<<"Some error";
    }
    w->closeLockRoom();
}

void ArenaScreen::onSitBack(){
    //This button
    //call the service now with sit back code..
    if(issatdown){
        IBox = new InfoBox(this);
        IBox->setMessage("You are already Sat Down!\n Wait for the room to fill");
        IBox->show();
        return;
    }
    else if(!lock->has_connected){
        IBox = new InfoBox(this);
        IBox->setMessage("Please wait!");
        IBox->show();
        return;
    }
    QString nid = "room_"+cind+"lobbyid_"+currentRoomId;
   int gametype = 0;
    if(currentGameType == "") gametype = 0;
    else if(currentGameType == "1V1") gametype = 2;
    else if(currentGameType == "2V2") gametype = 4;
    else if(currentGameType == "3V3") gametype = 6;
    else if(currentGameType == "4V4") gametype = 8;
  QString teamType = lock->getTeamType();
  //don't allow him to place multiple bets..
 // lock->has_madethebet = true;
   //QString gametype = currentGameType;
   QString surl = "<url>sendmessage_tochannel?uid="+QString().number(mainid)+"&since="+cind+"&nitems="+currentRoomId+"&title=sitdown&desc="+currentArenaName+"&nid="+nid+"&gametype="+QString().number(gametype);
   QNetworkAccessManager *manager = new QNetworkAccessManager(this);

     connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
     QNetworkRequest request2 = QNetworkRequest(surl);
     QString ss = sessionname+"="+sessionid;
     request2.setRawHeader("Cookie",ss.toUtf8() );
     manager->get(request2);
     issatdown = true;
}

void ArenaScreen::on_createArenaProfile_clicked()
{
    //Add this game to this users list

    if(mainid == 0){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Login! To Sign up for Arena");
        IBox->show();
    }
    else{
        APC = new ArenaProfileCreate();
        APC->show();
        connect(APC->createarenaprofile,SIGNAL(clicked()),this, SLOT(onAPCClicked()));
       //Lets signup now


    }

}
void ArenaScreen::onAPCClicked(){
    QString pname = APC->profilename->text();
    if(pname == ""){

    }
    else{
      APC->close();
      QString surl = "<url>addarena?fn=some&nitems="+QString().number(mainid)+"&since="+cind+"&title="+pname;
      qDebug() << surl;
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(addArenaResponse(QNetworkReply*)));
      QNetworkRequest request = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request);

    }

}

void ArenaScreen::onLobbyTableClicked(int r, int c){

    //check if this guy having enough chips to enter the lobby

    qDebug()<<"Row Activated "+QString().number(r);


   // qDebug()<<"table widget "+QString().number(tableWidget->items());

    // QTableWidgetItem *wageritem = tableWidget->item(r,4);
    //qDebug()<<"Wager is: "+wageritem->text();

    QTableWidgetItem *item1;
    if(item1 = tableWidget->item(r,c)){
        if(!item1->text().isEmpty()){
            int wageramount = item1->text().toInt();
            if(userchips < wageramount){
                IBox = new InfoBox(this);
                IBox->setMessage("Don't Have sufficient chips to enter room!");
                IBox->show();
                return;
            }
            else{
             index = r;
             unlocking = true;    //
             ui->ArenaScreenSlot->hide();
             ui->frame->hide();
              QTimer::singleShot(600, this, SLOT(clearArenaStuff()));

             w->openLockRoom();
            }

        }
    }
 }




QTableWidgetItem* ArenaScreen::createTableWidgetItem( const QString& text )
{
    QTableWidgetItem* item = new QTableWidgetItem( text );
    item->setTextAlignment( Qt::AlignCenter );
    return item;
}

void ArenaScreen::LeaderBoard_per_Arena(QNetworkReply *reply){
      if(!this->isVisible()) return;
    try{
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         return;
     }
     Json::Reader reader;
     Json::Value Preroot;
      QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), Preroot );

     if(!parsingSuccessful || Preroot.size() == 0)
           return;

     tableWidget->clearContents();
     //tableWidget->clear();

    // tableWidget = new QTableWidget(4, 7, this);
   //  tableWidget->verticalHeader()->hide();
    // tableWidget->verticalHeader()->stretchLastSection();
       tableWidget->setColumnWidth(0,84);
       tableWidget->setColumnWidth(1,64);
       tableWidget->setColumnWidth(2,74);
       tableWidget->setColumnWidth(3,94);
       tableWidget->setColumnWidth(4,64);
       tableWidget->setColumnWidth(5,64);

       Json::Value root = Preroot.get("arenauser","none");
       Json::Value totaluser = Preroot.get("alluser","none");
     //tableWidget->row
      tableWidget->setRowCount(totaluser.size()+1);
   int rowcounts = 1;

   int totalct = 0;
    for(int i=0;i<root.size();i++){
          const Json::Value tem = root[i];
             QString point = QString().fromStdString(tem.get("chips","none").asCString());
             int newpt = point.toInt();
             if(newpt > 0){
                     totalct++;
             }
        }

       for(int j=0;j<root.size();j++){
            const Json::Value tem = root[j];

            QString name = QString().fromStdString(tem.get("uid","none").asCString());
            for(int k=0;k<totaluser.size();k++){
                  const Json::Value bb_user = totaluser[k];
                   QString kuser = QString().fromStdString(bb_user.get("username","none").asCString());
                   if(kuser == name ){
                       name =  QString().fromStdString(bb_user.get("arenaname","none").asCString());
                   }
            }
            QString number = QString().fromStdString(tem.get("totalgames","none").asCString());
            QString desc = QString().fromStdString(tem.get("wins","none").asCString());
            QString result = QString().fromStdString(tem.get("loss","none").asCString());
            QString pt = QString().fromStdString(tem.get("chips","none").asCString());
              QString thisrank = "Unranked";
               QString thisicon = ":/new/prefix1/resources/unranked.png";
            if(pt.toInt() > 0){
            //////////////////////////
            int rank = (j+1) * 100 /totalct;


            if(rank <= 10){thisrank = "Admiral";}
            else if(rank <= 25) {thisrank = "Platinum";}
            else if(rank <= 50) {thisrank = "Gold";}
            else if(rank <= 75) {thisrank = "Silver";}
            else  {thisrank = "Bronze";}
         }
            thisicon = ":/new/prefix1/resources/"+thisrank+".png";
            QTableWidgetItem* tt = new QTableWidgetItem(" "+thisrank);
            tt->setIcon(QIcon(QPixmap(thisicon)));
  //tt->setTextAlignment( Qt::AlignCenter );
            tableWidget->setItem(j,0,tt);
            tableWidget->setItem(j,1,createTableWidgetItem(QString::number(j+1)));
            tableWidget->setItem(j,2,createTableWidgetItem(name));
            tableWidget->setItem(j,3,createTableWidgetItem(number));
            tableWidget->setItem(j,4,createTableWidgetItem(desc));
            tableWidget->setItem(j,5,createTableWidgetItem(result));
            if(pt.toInt() > 0)
              tableWidget->setItem(j,6,createTableWidgetItem(pt));
            else
                tableWidget->setItem(j,6,createTableWidgetItem("0"));
            rowcounts++;
            }

       //No remaining stuff
       rowcounts--;
       for(int k=0;k<totaluser.size();k++){
             const Json::Value bb_user = totaluser[k];
              QString name = QString().fromStdString(bb_user.get("username","none").asCString());
              bool thisuserfound = false;int j=0;
              for(j=0;j<root.size();j++){
                   const Json::Value tem = root[j];
                   QString kname = QString().fromStdString(tem.get("uid","none").asCString());
                   if(kname == name){
                       thisuserfound = true;
                       name = QString().fromStdString(bb_user.get("arenaname","none").asCString());
                       break;
                   }
              }
              if(!thisuserfound){
                  /*QString thisrank = "unranked";
                  QTableWidgetItem* tt = new QTableWidgetItem(" "+thisrank);
                  tt->setIcon(QIcon(QPixmap(":/new/prefix1/resources/unranked.png")));
                  tableWidget->setItem(rowcounts,0,tt);
                  tableWidget->setItem(rowcounts,1,createTableWidgetItem(QString::number(rowcounts+1)));
                  tableWidget->setItem(rowcounts,2,createTableWidgetItem(name));
                  tableWidget->setItem(rowcounts,3,createTableWidgetItem("0"));
                  tableWidget->setItem(rowcounts,4,createTableWidgetItem("0"));
                  tableWidget->setItem(rowcounts,5,createTableWidgetItem("0"));
                  tableWidget->setItem(rowcounts,6,createTableWidgetItem("0"));
                  rowcounts++;*/
              }
       }

       //last one for stretch
       // tableWidget->insertRow(root.size());
        //tableWidget->verticalHeader()->setStretchLastSection(true);
        tableWidget->setStyleSheet("{background-color:rgba(12,113,112,190)}");
        tableWidget->show();
   }

      catch(...){
           qDebug()<<"[Arena Screen] : Error";
      }

}



//This to take user the room as soon as the room is created..NO QUESTIONS ASKED..
void ArenaScreen::allRoomRequest_2(QNetworkReply *reply){
      if(!this->isVisible()) return;
    try{
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << "RM"+ textData;
    /******************/
     if(textData.length() < 5) {
         qDebug()<<"hey";
         return;
     }
     Json::Reader reader;
     Json::Value root;
     QVBoxLayout *newsbox = new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
         lobbies = root;
         //if(tableWidget->isVisible()) return;
       tableWidget->clearContents();

       //tableWidget->row
         for(int i=0;i<root.size();i++){
              const Json::Value tem = root[i];
               QString title = QString().fromUtf8(tem.get("node_title","none").asCString());
               QString wager = QString().fromStdString(tem.get("wager","none").asCString());
               QString number = QString().fromStdString(tem.get("number","none").asCString());

               QString playerjoined = "0";
               playerjoined = QString().fromStdString(tem.get("players_joined","0").asCString());
               QString playersat = "0";
               playersat = QString().fromStdString(tem.get("players_satdown","0").asCString());

               int firstchar = QString(number[0]).toInt();
               int totalusersneeed = (firstchar + firstchar);
 qDebug()<<"total user is "<< totalusersneeed;
               //if(!playersat) playersat = "0";
               //if(playerjoined.length()>0){pla}
               QString players =playerjoined+"/"+totalusersneeed;
               // QString gametype = QString().fromStdString(tem.get("gametype","none").asCString());
                // QString when = QString().fromStdString(tem.get("when","none").asCString());
                  QString desc = QString().fromStdString(tem.get("body","none").asCString());
                 qDebug()<<title;
                 tableWidget->insertRow(i);
                 //tableWidget->setItem(i,0,createTableWidgetItem((QString().number(i))));
                 title.replace("&#039;s","'s");
                 tableWidget->setItem(i,0,createTableWidgetItem(title));
                  tableWidget->setItem(i,1,createTableWidgetItem(number));
                  tableWidget->setItem(i,2,createTableWidgetItem(desc));
                  tableWidget->setItem(i,3,createTableWidgetItem(players));
                     tableWidget->setItem(i,4,createTableWidgetItem(wager));
         }
         //last one for stretch
          tableWidget->insertRow(root.size());
          tableWidget->verticalHeader()->setStretchLastSection(true);

         tableWidget->setStyleSheet("{background-color:rgba(142,113,112,90); font: 12pt \"Candara\";color: #fff;}");
           tableWidget->show();

           onLobbyTableClicked(0,0);
     }
    }
    catch(...){
         qDebug()<<"[Arena Screen] : Error";
    }
}

void ArenaScreen::allRoomRequest(QNetworkReply *reply){
      if(!this->isVisible()) return;
    try{
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         return;
     }
     Json::Reader reader;
     Json::Value root;
      QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
         lobbies = root;
         //if(tableWidget->isVisible()) return;
       tableWidget->clearContents();

       //tableWidget->row
         for(int i=0;i<root.size();i++){
              const Json::Value tem = root[i];
               QString title = QString().fromUtf8(tem.get("node_title","none").asCString());
               QString wager = QString().fromStdString(tem.get("wager","none").asCString());
               QString number = QString().fromStdString(tem.get("number","none").asCString());

               QString playerjoined = "0";
               try{
               playerjoined = QString().fromStdString(tem.get("players_joined","0").asCString());
               } catch(...){}
               QString playersat = "0";
               try{
               playersat = QString().fromStdString(tem.get("players_satdown","0").asCString());
               } catch(...){}
               //if(!playersat) playersat = "0";
               //if(playerjoined.length()>0){pla}
               playerjoined = playerjoined == "0" ? "0" : QString().number(playerjoined.split(",").length());
               playersat = playersat == "0" ? "0" : QString().number(playersat.split(",").length());

               int firstchar = QString(number[0]).toInt();
               int totalusersneeed = (firstchar + firstchar);
               qDebug()<<"total user is "<< totalusersneeed;
               //if(!playersat) playersat = "0";
               //if(playerjoined.length()>0){pla}
               QString players =playersat+"/"+QString().number(totalusersneeed);

              // QString players =playerjoined+"/"+playerjoined;


               // QString gametype = QString().fromStdString(tem.get("gametype","none").());
                // QString when = QString().fromStdString(tem.get("when","none").asCString());
                  QString desc = QString().fromStdString(tem.get("body","none").asCString());
                 qDebug()<<title;
                 tableWidget->insertRow(i);
                 //tableWidget->setItem(i,0,createTableWidgetItem((QString().number(i))));
                 title.replace("&#039;s","'s");
                  tableWidget->setItem(i,0,createTableWidgetItem(title));
                  tableWidget->setItem(i,1,createTableWidgetItem(number));
                  tableWidget->setItem(i,2,createTableWidgetItem(desc));
                  tableWidget->setItem(i,3,createTableWidgetItem(players));
                  tableWidget->setItem(i,4,createTableWidgetItem(wager));
         }
         //last one for stretch
          tableWidget->insertRow(root.size());
          tableWidget->verticalHeader()->setStretchLastSection(true);

         tableWidget->setStyleSheet("{background-color:rgba(142,113,112,90); font: 12pt \"Candara\";color: #fff;}");
           tableWidget->show();
     }
    }
    catch(...){
         qDebug()<<"[Arena Screen] : Error";
    }
}

void ArenaScreen::addArenaResponse(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();

    QString textData(rawData);
    Json::Reader reader;
    Json::Value groot;
    qDebug()<<textData;
    bool parsingSuccessful = reader.parse(textData.toStdString(), groot );
    if(parsingSuccessful){
      QString Subject = QString().fromStdString(groot.get("subject","none").asCString());
      QString result = QString().fromStdString(groot.get("result","none").asCString());
      if(Subject == "addarena" && result == "success"){
          Json::Value tem = groot.get("data",0);
          QString cmd = QString().fromStdString(tem.get("nid","none").asCString());
          QString gamename = QString().fromStdString(tem.get("node_title","none").asCString());
          QString arenaname = QString().fromStdString(tem.get("user_arenaname","none").asCString());
          arenaobject thisobj; thisobj.arenacode = cmd; thisobj.arenaname = gamename;thisobj.userarenaname = arenaname;
          availablearenas.push_back(thisobj);
          currentArenaName = arenaname;
          ui->createArenaProfile->hide();
          is_subscribed = true;
      }
    }
}

void ArenaScreen::plainRequest(QNetworkReply* reply){
    //  if(!this->isVisible()) return;
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);

    try{
     qDebug() << textData;
     if(textData == "[\"user_added\"]"){
        IBox = new InfoBox(this);
        IBox->setMessage("You have successfully entered the lobby..\n You can sit down to place the bet");
        IBox->show();
        if(!hasMovedToGame) {lock->has_connected=true;lock->setRedLabel("Waiting For Players to Join");}
        return;
    }
    else if(textData == "[\"\room_closed\"]"){
        IBox = new InfoBox(this);
        IBox->setMessage("Lobby is full..Please start playing");
        IBox->show();
        return;
    }
    else if(textData == "[\"error:adding user\"]" ||textData == "[\"failed\"]"  ){
        IBox = new InfoBox(this);
        IBox->setMessage("Error: Too much traffic or your Internet is slow...\n Please try another room or contact administrator");
        IBox->show();
        return;
    }
    if(textData == "[\"Room Created Successfully\"]"){

       QString urld =  "<url>api/views/room_view?field_roomusers_target_id="+QString().number(mainid)+"&field_parent_arena_forroom_target_id="+cind;
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(allRoomRequest_2(QNetworkReply*)));
          QNetworkRequest request = QNetworkRequest(urld);
          QString ss = sessionname+"="+sessionid;
          request.setRawHeader("Cookie",ss.toUtf8() );
        manager->get(request);
        room->close();
        //Enter into this room

    }
    if(textData == "[\"Game Successfully Added to the list\"]"){
        ui->createArenaProfile->hide();
        is_subscribed = true;

    }
   // IBox = new InfoBox(this);
    //IBox->setMessage(textData);
    //IBox->show();
    }
    catch(...){
        qDebug()<<"[Arena Screen] : Error";
    }
}
void ArenaScreen::creatRoomClicked(){
    if(room!=NULL){
        room->close();
        room = new CreateRoom();
        room->show();
         connect( room->getButton(), SIGNAL( clicked() ), this, SLOT(trycreateroom()));
    }
    else{
    room = new CreateRoom();
    room->show();
     connect( room->getButton(), SIGNAL( clicked() ), this, SLOT(trycreateroom()));
    }
    //Now set up the trigger for button on create room panel.

}

void ArenaScreen::on_findATeam_clicked()
{
 //Lets Find the team And set it.
    TFJ = new TeamFindJoin(this);
    TFJ->show();
}

void ArenaScreen::trycreateroom(){
    //QString value = room-
    QString urd = room->getURLString(cind);
    if(urd == "null"){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Enter All Values");
        IBox->show();
        return;
    }

    //qDebug()<<"int value and base10 "<<wag % 2<<" wager:"<<wag;
    if(urd == "NAN"){
        IBox = new InfoBox(this);
        IBox->setMessage("Wager must be a number in order of 10");
        IBox->show();
        return;
    }
    if(room->onceclicked) return;
    QString urld  = "<url>createroom?"+urd;
    qDebug()<<urld;

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request = QNetworkRequest(urld);
      QString ss = sessionname+"="+sessionid;
      request.setRawHeader("Cookie",ss.toUtf8() );
     manager->get(request);
   // disconnect(room->getButton(), SIGNAL( clicked() ), this, SLOT(trycreateroom()));
  room->onceclicked = true;


}
void ArenaScreen::onPlaceButtonClicked(){
    if(lock->has_madethebet || hasMadeAPlaceBetRequest){
        lock->setRedLabel("You have already Placed the bet!\n wait for room to fill");
        return;
    }
    if(!lock->satdownuserlist.contains(currentArenaName)){
        IBox = new InfoBox(this);
        IBox->setMessage("Spectators can't make the bet");
        IBox->show();
        return;
    }
    //Make sure that he can bet..
    bool okay = false;
    int betamt = lock->betamountedit->text().toInt(&okay,10);
    int bet = betamt;

     bool orderoften = betamt%10 == 0 ? true : false;

    if(!okay || !orderoften){
        IBox = new InfoBox(this);
        IBox->setMessage("Bet amount must be number & in order of 10");
        IBox->show();
        return;
    }
    qDebug()<<bet<<" w:uchips "<<currentWagerAmount;
    if(bet < currentWagerAmount){
        IBox = new InfoBox(this);
        IBox->setMessage("Your bet must be greater than\n Wager Amount.!");
        IBox->show();
        return;
    }

    if(bet > userchips){
        IBox = new InfoBox(this);
        IBox->setMessage("You Don't have enough Chips to bet!");
        IBox->show();
        return;

    }

    //
    hasMadeAPlaceBetRequest = true;
    QString nid = "room_"+cind+"lobbyid_"+currentRoomId;
    int gametype = 0;
     if(currentGameType == "") gametype = 0;
     else if(currentGameType == "1V1") gametype = 2;
     else if(currentGameType == "2V2") gametype = 4;
     else if(currentGameType == "3V3") gametype = 6;
     else if(currentGameType == "4V4") gametype = 8;
   QString teamType = lock->getTeamType();
   //don't allow him to place multiple bets..
  // lock->has_madethebet = true;
    //QString gametype = currentGameType;
    QString surl = "<url>sendmessage_tochannel?nitems="+currentRoomId+"&title=placebet&desc="+currentArenaName+":"+QString().number(bet)+":"+teamType+":"+QString().number(mainid)+"&nid="+nid+"&gametype="+QString().number(gametype);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

}
void ArenaScreen::addUserToChannel(){
    try{
    int gametype = 2;
    qDebug()<<"Current Game Type: "+currentGameType;
     if(currentGameType == "") gametype = 2;
     else if(currentGameType == "1V1") gametype = 2;
     else if(currentGameType == "2V2") gametype = 4;
     else if(currentGameType == "3V3") gametype = 6;
     else if(currentGameType == "4V4") gametype = 8;

    QString nid = "room_"+cind+"lobbyid_"+currentRoomId;
    QString surl = "<url>addtochannel?desc="+currentArenaName+"&nitems="+currentRoomId+"&uid="+QString().number(mainid)+"&nid="+nid+"&since="+cind+"&gametype="+QString().number(gametype);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    qDebug()<<surl;
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
      //
}
    catch(...){
         qDebug()<<"Channel Error: ";
    }
}

void ArenaScreen::on_Leaderboard_clicked()
{

    //This is button in the arena screen;
    //Ste 1 : Remove layout and add the lobbies
    if(!is_subscribed){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Create Profile\n For This game first!");
        IBox->show();
        return;
    }
try{
    hasMovedToLobby = true;
    QLayout* layout = ui->ArenaScreenSlot->layout ();
    QWidget *w;
       if (layout != 0)
       {
       QLayoutItem *item;
       while ((item = layout->takeAt(0)) != 0){
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}

       }
       delete layout;
       }
       //Step 2 : Create the top buttons and table

        //////////////////
        QHBoxLayout *TBLayout=new QHBoxLayout();
        QLabel *secondname = new QLabel( currentGameName+" Arena");
        secondname->setStyleSheet("QLabel {color:#fff}");
        QFont f( "Candara", 12, QFont::Bold);
        secondname->setFont(f);

       //secondname->setStyle(ui->ArenaScreen_GameLabel1->style());
       // secondname->setFont(QFont("Candara"));
        secondname->setAlignment(Qt::AlignTop| Qt::AlignLeft);
        QPushButton *b21=new QPushButton("Ranks");
        b21->setMaximumWidth(120);
       // QPushButton *b22=new QPushButton("Match Making");
       // QPushButton *b23=new QPushButton("Filters");
        TBLayout->addWidget(secondname);

          TBLayout->addSpacerItem(new QSpacerItem(40, 10, QSizePolicy::Minimum, QSizePolicy::Expanding));
          TBLayout->addWidget(b21);
           //TBLayout->addWidget(b22);
           // TBLayout->addWidget(b23);
             TBLayout->setContentsMargins(0,0,0,0);
            QHBoxLayout *MLayout = new QHBoxLayout();
            MLayout->setSpacing(0);
            MLayout->setContentsMargins(0,0,0,0);

            tableWidget = new QTableWidget(4, 7, this);
            tableWidget->verticalHeader()->hide();
            tableWidget->verticalHeader()->stretchLastSection();
            tableWidget->setStyleSheet("QTableView {color:#fff; selection-background-color: rgb(70,70,70);}");
           QStringList m_TableHeader;
           m_TableHeader<<"Rank"<<"Position"<<"Player"<<"Total Games"<<"Wins"<<"Loss"<<"Chips";
           tableWidget->setHorizontalHeaderLabels(m_TableHeader);
           tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
           tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
           tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
          // tableWidget->setSt
           tableWidget->hide();

          // RD->setMaximumHeight(190);
          // RD->setFixedSize(160,190);
            MLayout->addWidget(tableWidget);
           // MLayout->addWidget(RD);

           QWidget* slideWidget1=new QWidget();
           QWidget*  slideWidget2=new QWidget();
            slideWidget1->setLayout(TBLayout);
            slideWidget1->setMinimumHeight(34);
            slideWidget1->setMaximumHeight(40);
            slideWidget2->setLayout(MLayout);
            final = new QVBoxLayout();
            final->setSpacing(0);
            final->setContentsMargins(0,0,0,0);
            final->addWidget(slideWidget1);
            final->addWidget(slideWidget2);
            ui->ArenaScreenSlot->setLayout(final);
            tableWidget->show();
 //connect(tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(onLobbyTableClicked(int,int)));
          connect( b21, SIGNAL( clicked() ), this, SLOT( showRanks() ) );

           //Also lets see if there are any lobbies available and populate them
           QString urld =  "<url>statperarena?&gametype="+cind;
           QNetworkAccessManager *manager = new QNetworkAccessManager(this);
           connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(LeaderBoard_per_Arena(QNetworkReply*)));
           QNetworkRequest request = QNetworkRequest(urld);
           QString ss = sessionname+"="+sessionid;
           request.setRawHeader("Cookie",ss.toUtf8() );
           manager->get(request);

 ui->frame->show();
 ui->ArenaScreenSlot->show();
 RD = new RanksDialog(b21);
 QPoint rpos = this->pos();
 //RD->move(rpos.x()+740,rpos.y()+300);
 RD->hide();

 currentGameType = "";
    }
    catch(...){
        qCritical()<<"Some error";
    }
}

void ArenaScreen::showRanks(){

    if(RD->isVisible()) RD->hide();


    else{
        RD->show();
        QPropertyAnimation *animation = new QPropertyAnimation(RD, "geometry");
        animation->setDuration(600);
       QPoint rpos = w->pos();
        RD->move(rpos.x()+520,rpos.y()+222);
        //Now animation to provide a fly-by effect
        QRect startRect(rpos.x()+520,rpos.y()+222,144,0);
        QRect endRect(rpos.x()+520,rpos.y()+222,155,160);
        animation->setStartValue(startRect);
        animation->setEndValue(endRect);
        animation->setEasingCurve(QEasingCurve::InOutSine);
        animation->start(QAbstractAnimation::DeleteWhenStopped);
    }

   //
}

void ArenaScreen::mousePressEvent(QMouseEvent *event){

  //  mpos = event->pos();
    w->mpos = event->pos();
}



void ArenaScreen::mouseMoveEvent(QMouseEvent *event){

    if (event->buttons() && Qt::LeftButton) {
        if(lock && lock->sliderFrame->geometry().contains(event->pos())){  w->mpos = event->pos();return;}
                w->mouseMoveEvent(event);
               /* if(RD) {
                    QPoint rpos = w->pos();
                    RD->move(rpos.x()+520,rpos.y()+222);
                }*/
            }

}


void ArenaScreen::on_toolButton_2_clicked()
{
    qDebug()<<"go home bud";
}
