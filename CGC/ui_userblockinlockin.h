/********************************************************************************
** Form generated from reading UI file 'userblockinlockin.ui'
**
** Created: Sat 4. Apr 00:08:42 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERBLOCKINLOCKIN_H
#define UI_USERBLOCKINLOCKIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserBlockInLockIn
{
public:
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *userpic;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout;
    QFrame *frame_5;
    QHBoxLayout *horizontalLayout_3;
    QLabel *username;
    QLabel *userrank;
    QFrame *frame_6;
    QHBoxLayout *horizontalLayout_4;
    QLabel *totalgames;
    QLabel *betamount;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *onlinestatus;
    QSpacerItem *verticalSpacer;
    QLabel *label;

    void setupUi(QWidget *UserBlockInLockIn)
    {
        if (UserBlockInLockIn->objectName().isEmpty())
            UserBlockInLockIn->setObjectName(QString::fromUtf8("UserBlockInLockIn"));
        UserBlockInLockIn->resize(275, 56);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(UserBlockInLockIn->sizePolicy().hasHeightForWidth());
        UserBlockInLockIn->setSizePolicy(sizePolicy);
        UserBlockInLockIn->setMinimumSize(QSize(0, 32));
        UserBlockInLockIn->setStyleSheet(QString::fromUtf8("#username{\n"
"\n"
"\n"
"color: rgb(113, 211, 243);\n"
"	font: 75 10pt \"Candara\";\n"
"\n"
"}\n"
"\n"
"#userstatus{\n"
"color:#fff;\n"
"	font: 9pt \"Candara\";\n"
"\n"
"}\n"
"\n"
"#userchips{\n"
"color:#FFF;\n"
"	font: 9pt \"Candara\";\n"
"\n"
"}\n"
"#totalgames{\n"
"color:#fff;\n"
"font: 9pt \"Candara\";\n"
"}\n"
"#betamount{\n"
"color:#fff;\n"
"font: 10pt \"Candara\";\n"
"}\n"
"#label{\n"
"color:#fff;\n"
"font: 12pt \"Candara\";\n"
"}"));
        horizontalLayout_2 = new QHBoxLayout(UserBlockInLockIn);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(6, 6, 14, 6);
        frame_2 = new QFrame(UserBlockInLockIn);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy1);
        frame_2->setMinimumSize(QSize(0, 0));
        frame_2->setMaximumSize(QSize(42, 42));
        frame_2->setStyleSheet(QString::fromUtf8("\n"
"border-image: url(:/new/prefix1/resources/newframe.png);\n"
""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(2, 2, 2, 2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        userpic = new QLabel(frame_2);
        userpic->setObjectName(QString::fromUtf8("userpic"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(userpic->sizePolicy().hasHeightForWidth());
        userpic->setSizePolicy(sizePolicy2);
        userpic->setMaximumSize(QSize(16777215, 16777215));
        userpic->setStyleSheet(QString::fromUtf8(""));

        verticalLayout_3->addWidget(userpic);


        horizontalLayout_2->addWidget(frame_2);

        frame = new QFrame(UserBlockInLockIn);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("border:none;\n"
"background-color: none;"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame_4 = new QFrame(frame);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy2.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy2);
        frame_4->setMinimumSize(QSize(0, 0));
        frame_4->setMouseTracking(true);
        frame_4->setStyleSheet(QString::fromUtf8(""));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_4);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_5 = new QFrame(frame_4);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_5);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        username = new QLabel(frame_5);
        username->setObjectName(QString::fromUtf8("username"));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        username->setFont(font);
        username->setMouseTracking(true);
        username->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_3->addWidget(username);

        userrank = new QLabel(frame_5);
        userrank->setObjectName(QString::fromUtf8("userrank"));

        horizontalLayout_3->addWidget(userrank);


        verticalLayout->addWidget(frame_5);

        frame_6 = new QFrame(frame_4);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_6);
        horizontalLayout_4->setSpacing(5);
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        totalgames = new QLabel(frame_6);
        totalgames->setObjectName(QString::fromUtf8("totalgames"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Candara"));
        font1.setPointSize(9);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        totalgames->setFont(font1);

        horizontalLayout_4->addWidget(totalgames);

        betamount = new QLabel(frame_6);
        betamount->setObjectName(QString::fromUtf8("betamount"));
        betamount->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(betamount);


        verticalLayout->addWidget(frame_6);


        horizontalLayout->addWidget(frame_4);

        frame_3 = new QFrame(frame);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        sizePolicy.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy);
        frame_3->setMaximumSize(QSize(24, 16777215));
        frame_3->setLayoutDirection(Qt::RightToLeft);
        frame_3->setStyleSheet(QString::fromUtf8(""));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_3);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 1);
        onlinestatus = new QLabel(frame_3);
        onlinestatus->setObjectName(QString::fromUtf8("onlinestatus"));
        sizePolicy1.setHeightForWidth(onlinestatus->sizePolicy().hasHeightForWidth());
        onlinestatus->setSizePolicy(sizePolicy1);
        onlinestatus->setMaximumSize(QSize(12, 12));
        onlinestatus->setStyleSheet(QString::fromUtf8(""));
        onlinestatus->setScaledContents(true);
        onlinestatus->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_2->addWidget(onlinestatus);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        label = new QLabel(frame_3);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Candara"));
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setWeight(50);
        label->setFont(font2);
        label->setLayoutDirection(Qt::RightToLeft);
        label->setStyleSheet(QString::fromUtf8(""));
        label->setAlignment(Qt::AlignCenter);
        label->setMargin(0);
        label->setIndent(0);

        verticalLayout_2->addWidget(label);


        horizontalLayout->addWidget(frame_3);


        horizontalLayout_2->addWidget(frame);


        retranslateUi(UserBlockInLockIn);

        QMetaObject::connectSlotsByName(UserBlockInLockIn);
    } // setupUi

    void retranslateUi(QWidget *UserBlockInLockIn)
    {
        UserBlockInLockIn->setWindowTitle(QApplication::translate("UserBlockInLockIn", "Form", 0, QApplication::UnicodeUTF8));
        userpic->setText(QString());
        username->setText(QApplication::translate("UserBlockInLockIn", "User", 0, QApplication::UnicodeUTF8));
        userrank->setText(QString());
        totalgames->setText(QApplication::translate("UserBlockInLockIn", "Games Played:", 0, QApplication::UnicodeUTF8));
        betamount->setText(QApplication::translate("UserBlockInLockIn", "Bet:", 0, QApplication::UnicodeUTF8));
        onlinestatus->setText(QString());
        label->setText(QApplication::translate("UserBlockInLockIn", "24", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class UserBlockInLockIn: public Ui_UserBlockInLockIn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERBLOCKINLOCKIN_H
