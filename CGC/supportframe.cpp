#include "supportframe.h"
#include "ui_supportframe.h"
#include <QDebug>
#include <QPushButton>
#include <jsonparser.h>
#include <QTableWidget>
#include <QTimer>
#include <mainwindow.h>
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern QString thatuser;
extern QString thatuser_id;
extern MainWindow* w;
extern int mainid;
extern int ADMIN;

 QString streamid;
 QString gametype;
 QString ticketid;

SupportFrame::SupportFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SupportFrame)
{
    ui->setupUi(this);
    tableWidget = ui->tableWidget;
   // tableWidget->hide();
    //**************************//
    if(mainid==ADMIN){
        QString surl = "<url>api/TModule/getAllTickets?";
        qDebug()<<surl;
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
        QNetworkRequest request2 = QNetworkRequest(surl);
        QString ss = sessionname+"="+sessionid;
        request2.setRawHeader("Cookie",ss.toUtf8() );
        manager->get(request2);
        tableWidget->show();
    }
    else
        tableWidget->hide();
}

SupportFrame::~SupportFrame()
{
    delete ui;
}

QWidget* SupportFrame::getButtonWidgets(int RowNum){
    QWidget *buttonset = new QWidget();
    QVBoxLayout *vbox = new QVBoxLayout();
    QPushButton* accept = new QPushButton("Enter");
    accept->setMaximumHeight(24);
    vbox->addWidget(accept);
    buttonset->setLayout(vbox);
    buttonset->setContentsMargins(0,0,0,0);
    connect(accept, SIGNAL(clicked()), ButtonSignalMapper, SLOT(map()));
    ButtonSignalMapper->setMapping(accept, RowNum);
    connect(ButtonSignalMapper, SIGNAL(mapped(int)), this, SLOT(CellButtonClicked(int)));
    return buttonset;
}

QWidget* SupportFrame::createDeleteButton(int RowNum){
    QWidget *buttonset = new QWidget();
    QVBoxLayout *vbox = new QVBoxLayout();
    QPushButton* accept = new QPushButton("Delete");
    accept->setMaximumHeight(24);
    vbox->addWidget(accept);
    buttonset->setLayout(vbox);
    buttonset->setContentsMargins(0,0,0,0);
    connect(accept, SIGNAL(clicked()), DeleteSignalMapper, SLOT(map()));
    DeleteSignalMapper->setMapping(accept, RowNum);
    connect(DeleteSignalMapper, SIGNAL(mapped(int)), this, SLOT(DeleteButtonClicked(int)));
    return buttonset;
}

void SupportFrame::DeleteButtonClicked(int RowNum){
    if(RowNum != rownumber){
     qDebug()<<"Accept Button clicked "<<RowNum;
     rownumber = RowNum;
     const Json::Value tem = root[rownumber];
     QString nid = QString().fromStdString(tem.get("nid","none").asCString());
     if(nid!= "none"){
         QString surl = "<url>api/TModule/deleteTicket?nid="+nid;
         qDebug()<<surl;
         QNetworkAccessManager *manager = new QNetworkAccessManager(this);
         connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
         QNetworkRequest request2 = QNetworkRequest(surl);
         QString ss = sessionname+"="+sessionid;
         request2.setRawHeader("Cookie",ss.toUtf8() );
         manager->get(request2);
     }
    }
}

void SupportFrame::CellButtonClicked(int RowNum){
    //if(RowNum != rownumber){
     qDebug()<<"Accept Button clicked "<<RowNum;
     rownumber = RowNum;
     const Json::Value tem = root[rownumber];
     streamid = QString().fromStdString(tem.get("stream","none").asCString());
     gametype = QString().fromStdString(tem.get("roomtype","none").asCString());
     ticketid = QString().fromStdString(tem.get("source","none").asCString());
     //qDebug()<<"stream ID: "<<when;
     QTimer::singleShot(2000, this, SLOT(enterAdmin()));
     //w->enterLockIn_asAdmin(when,"title_1");
   // }
}

void SupportFrame::enterAdmin(){
    w->enterLockIn_asAdmin(streamid,gametype,ticketid);
}

QTableWidgetItem* SupportFrame::createTableWidgetItem( const QString& text )
{
    QTableWidgetItem* item = new QTableWidgetItem( text );

    item->setTextAlignment( Qt::AlignCenter );

    return item;
}

void SupportFrame::plainRequest(QNetworkReply* reply ){
  try{
        QString replyText;

        QByteArray rawData = reply->readAll();
        QString textData(rawData);
         qDebug() << "RM"+ textData;
         if(textData == "[\"done\"]"){
             QString surl = "<url>api/TModule/getAllTickets?";
             qDebug()<<surl;
             QNetworkAccessManager *manager = new QNetworkAccessManager(this);
             connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
             QNetworkRequest request2 = QNetworkRequest(surl);
             QString ss = sessionname+"="+sessionid;
             request2.setRawHeader("Cookie",ss.toUtf8() );
             manager->get(request2);
             return;
         }
         if(textData.length() < 5) {
             //ui->label->setText("Challenges(No Chanllenges For YOU)-Create One!");
             return;
         }
         Json::Reader reader;
         Json::Value groot;
          QVBoxLayout *newsbox=new QVBoxLayout();
         bool parsingSuccessful = reader.parse(textData.toStdString(), groot );
      //
         if(!parsingSuccessful || groot.size() == 0){
               return;
         }
         else{
             //QTableWidget* tableWidget = ui->tableWidget;
             DeleteSignalMapper = new QSignalMapper(this);
             ButtonSignalMapper = new QSignalMapper(this);
            // DeleteSignalMapper.removeMappings();
             const Json::Value friends = groot;
             tableWidget->clearContents();
             tableWidget->setRowCount(0);
             QString messageType = QString().fromStdString(friends.get("MessageType","none").asCString());
             tableWidget->setHorizontalHeaderLabels(QStringList()<<"Name"<<"Stream"<<"Enter"<<"Delete");
             if(messageType == "Tickets"){
                tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
                QStringList m_TableHeader;
                tableWidget->verticalHeader()->hide();
                tableWidget->horizontalHeader()->show();
                tableWidget->setColumnWidth(0,64);
                tableWidget->setColumnWidth(1,44);
                tableWidget->setColumnWidth(2,138);
                tableWidget->setColumnWidth(3,138);
                Json::Value proot = friends.get("body","none");
                root = proot;
                 for(int i=0;i<proot.size();i++){
                      const Json::Value tem = proot[i];
                      QString title = QString().fromStdString(tem.get("title","none").asCString());
                      QString stream = QString().fromStdString(tem.get("stream","none").asCString());
                      QString source = QString().fromStdString(tem.get("source","none").asCString());
                       QString roomtype = QString().fromStdString(tem.get("roomtype","none").asCString());
                         tableWidget->insertRow(i);
                         tableWidget->setRowHeight(i,64);
                         qDebug()<<"Inserting the row";
                         tableWidget->setItem(i,0,createTableWidgetItem(title));
                        //tableWidget->setItem(i,1,createTableWidgetItem(stream));
                         tableWidget->setItem(i,1,createTableWidgetItem(source));
                         tableWidget->setCellWidget(i,2,getButtonWidgets(i));
                          tableWidget->setCellWidget(i,3,createDeleteButton(i));
                 }
                  tableWidget->insertRow(root.size());
                  tableWidget->show();
             }

         }
    }
    catch(...){
         // signalMapper = new QSignalMapper(this);
    }

     //QTimer::singleShot(1000,this,SLOT(checkChallenges()));
}
