#include "ranksdialog.h"
#include "ui_ranksdialog.h"

RanksDialog::RanksDialog(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::RanksDialog)
{
    ui->setupUi(this);
}

RanksDialog::~RanksDialog()
{
    delete ui;
}
