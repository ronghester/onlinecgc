#include "tournament.h"
#include "ui_tournament.h"
#include <QGraphicsDropShadowEffect>
#include <QDebug>
#include <mainwindow.h>
#include <friendswidget.h>
#include <gamelabel.h>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern MainWindow* w;
extern QString userimage;
extern FriendsContainer* FC;
extern QString thisuser_rank;
extern QString onlineuser_string;
extern QString currentVersion;
extern std::vector<arenaobject> availablearenas;
Tournament::Tournament(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Tournament)
{
    ui->setupUi(this);
   // ui->webView->settings()->setAttribute(QWebSettings::PluginsEnabled, true);
    //ui->webView->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
    QString  newsdetails = "<url>api/views/headlines";
    getRequest(newsdetails);
    //Also simulteniously make one more request to fetch the rank...
    QString surl = "<url>Initapp?";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    //
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);

}

void Tournament::setUserCount(QString ucount){
    ui->usernumber->setTextFormat(Qt::RichText);
    ui->usernumber->setText(ucount);
}



void Tournament::plainRequest(QNetworkReply* reply ){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         return;
     }
     Json::Reader reader;
     Json::Value root;
     Json::Value proot;
     QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), proot );
     root = proot.get("InitApp",0);
     if(!parsingSuccessful || root.size() == 0){
           return;
     }
      for(int i=0;i<root.size();i++){
           const Json::Value tem = root[i];
           QString username = QString().fromStdString(tem.get("users","none").asCString());
           QString onlineusr =QString().fromStdString(tem.get("onlineusers","none").asCString());
           QString subject =  QString().fromStdString(tem.get("news_subject","none").asCString());
           QString body =     QString().fromStdString(tem.get("news_body","none").asCString());
           QString rank =     QString().fromStdString(tem.get("userrank","none").asCString());
           currentVersion =   QString().fromStdString(tem.get("version","none").asCString());
           qDebug()<<"Current Version: "<<currentVersion;
           qDebug()<<thisuser<<":"<<username;
           thisuser_rank = rank;
           QString thisicon = ":/new/prefix1/resources/"+rank+".png";
         /* if(username == thisuser){
         int rank = (i+1) * 100 /(root.size()+1);
         QString thisrank = "--";
         QString thisicon = ":/new/prefix1/resources/silver.png";
         //qDebug()<<i<<" User rank :" <<rank;
         if(rank <= 10){thisrank = " Platinum";thisicon = ":/new/prefix1/resources/platinum.png";}
         else if(rank <= 25) {thisrank = " Gold";thisicon = ":/new/prefix1/resources/gold.png";}
         else if(rank <= 50) {thisrank = " Silver";thisicon = ":/new/prefix1/resources/silver.png";}*/
         w->setUserRank(rank,thisicon);
         onlineuser_string = onlineusr+"<br/>"+username;
         setUserCount(onlineusr+"<br/>"+username);
         w->setHeaderNews(subject,body);
         //Lets populate the friends Things..
         const Json::Value dt= tem["friends"];  //"none");
         std::vector<FriendsWidget*> frnds;
         for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.

             const Json::Value friends = dt[index];
             QString username = QString().fromStdString(friends.get("name","none").asCString());
             QString userchips = QString().fromStdString(friends.get("chips","none").asCString());
             QString onlinestatus = QString().fromStdString(friends.get("online","none").asCString());
             QString userstatusline = QString().fromStdString(friends.get("statusline","none").asCString());
             QString uranks = QString().fromStdString(friends.get("userrank","none").asCString());
              QByteArray userpic = friends.get("userpic","none").asCString();
             qDebug()<<username<<" ** "<<userchips<< " ** " <<onlinestatus;

              QByteArray thisupic = QByteArray::fromBase64(userpic.replace(" ","+"));
             //FriendsWidget* thisfrnd = new FriendsWidget(0);
             //thisfrnd->setValues(username,userstatusline,userchips,onlinestatus);
             FC->addFriendElement(username,userchips,userstatusline,onlinestatus,uranks);
             FC->friendsimages.push_back(thisupic);

             // myFriends.push_back(thisfrnd);
         }
           const Json::Value ainfo= tem["arenainfo"];  //"none");
           for ( int index = 0; index < ainfo.size(); ++index ) { // Iterates over the sequence elements.

               const Json::Value tem = ainfo[index];
               qDebug()<<QString().fromStdString(tem.toStyledString());
               QString cmd = QString().fromStdString(tem.get("nid","none").asCString());
               QString gamename = QString().fromStdString(tem.get("node_title","none").asCString());
               QString arenaname = QString().fromStdString(tem.get("user_arenaname","none").asCString());
               arenaobject thisobj; thisobj.arenacode = cmd; thisobj.arenaname = gamename;thisobj.userarenaname = arenaname;
               availablearenas.push_back(thisobj);
           }

        // myFriends = frnds;
          w->createFriendsWidgets();
          QString thisVersion = "1.0.14";
          qDebug()<<thisVersion<<" : "<<currentVersion;
          if(thisVersion != currentVersion.trimmed()){
          system ("start qtsparkal.exe "+thisVersion.toUtf8());
          QString program = "qtsparkal.exe";

          //isClosing = true;
           QCoreApplication::exit(0);
          }
         return;
      }      
}

Tournament::~Tournament()
{
    delete ui;
}
void Tournament::getRequest(QString surl){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl(surl)));
}

void Tournament::on_gamelabel_clicked(QString cmd){
    qDebug()<< "label clicked "+cmd;
    int cint = cmd.toInt();
    QLayout* layout = ui->frame->layout ();
    QWidget *w;
    if (layout != 0)
    {
       QLayoutItem *item;
       while ((item = layout->takeAt(0)) != 0){
          // qDebug()<<"Inside Cleaning Code ";
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}
       }
       delete layout;
     }

    //Now Create the layout
    QHBoxLayout *top = new QHBoxLayout;
    QWidget *test = new QWidget();
    QVBoxLayout *newsbox=new QVBoxLayout();
    //newsbox->setContentsMargins(9,9,9,9);
   // setContentsMargins()
    newsbox->setAlignment(Qt::AlignTop);
    const int icmd = cmd.toInt();

    for(int i=0;i<root.size();i++){

     const Json::Value tem = root[i];
     QString title = QString().fromStdString(tem.get("node_title","none").asCString());
     QString icmd = QString().fromStdString(tem.get("nid","none").asCString());
     QString des = QString().fromStdString(tem.get("body","none").asCString());

     if(cmd == icmd){
      GameLabel *b41=new GameLabel("",0);
      b41->setTextFormat(Qt::RichText);
    // b41->setStyleSheet("background-color:rgba(133,0,0,0)");
      b41->setWordWrap(true);
      b41->setText("<h3 style=\"color:#FFFFFF\">"+title+"</h3><p style=\"color:#FFFFFF\">"+des+"<br></br></p></br><p></p></br>");
    // QListWidgetItem* QWL = new QListWidgetItem(stext);
      top->addWidget(b41);
      top->setSpacing(9);
      //top->setContentsMargins(9,9,9,29);
      test->setLayout(top);
     // test->setContentsMargins(9,9,9,19);
      // test->setMaximumHeight(300);
      newsbox->addWidget(test);
      top->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
      ui->frame->setLayout(newsbox);
     }
    }

    qDebug()<<"clicked on nid: "<<cmd;
}
void Tournament::requestReceived(QNetworkReply* reply){
   // if(!this->isVisible()) return;
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    //qDebug() << "news" + textData;
    //if(hasMovedToLobby) return;
    QVBoxLayout *newsbox=new QVBoxLayout();
    newsbox->setContentsMargins(0,0,0,0);
    /*if(textData.length() < 5) {
        QLabel *b41=new QLabel("No News I am afraid!",0);
        QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
        effect->setBlurRadius(9.0);
        effect->setColor(QColor(254, 254, 254, 255));
        effect->setOffset(0.2);
         b41->setGraphicsEffect(effect);
         newsbox->addWidget(b41);
         ui->frame_4->setLayout(newsbox);
         return;
    }*/
     Json::Reader reader;


     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
         QLabel *b41=new QLabel("No News I am afraid!",0);
         QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
         effect->setBlurRadius(9.0);
         effect->setColor(QColor(254, 254, 254, 255));
         effect->setOffset(0.2);
          b41->setGraphicsEffect(effect);
          newsbox->addWidget(b41);
     }
     else{

         signalMapper = new QSignalMapper(this);
         connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_gamelabel_clicked(QString)));

         for(int i=0;i<root.size();i++){
             QHBoxLayout *top = new QHBoxLayout;
             QWidget *test = new QWidget();

              const Json::Value tem = root[i];
              QString title = QString().fromStdString(tem.get("node_title","none").asCString());
              QString cmd = QString().fromStdString(tem.get("nid","none").asCString());
              QString des = QString().fromStdString(tem.get("body","none").asCString());
              QByteArray by = QByteArray::fromBase64(tem.get("headline_pic","none").asCString());
              QImage image = QImage::fromData(by, "JPG");

              GameLabel *b41=new GameLabel("",0);
              connect(b41, SIGNAL(clicked()), signalMapper, SLOT(map()));
              signalMapper->setMapping(b41, cmd);
              QObject::connect(b41,SIGNAL(pressed(cmd)),this,SLOT(on_gamelabel_clicked(cmd)));

              b41->setTextFormat(Qt::RichText);
            //  b41->setStyleSheet("background-color: rgba(40, 40, 42,253)");
              b41->setWordWrap(true);
             //b41->scroll();
              if(des.length()>120){
                    des =  des.mid(0,120);
                    des += "...";
              }
              b41->setText("<h3 style=\"color:#FFFFFF\">"+title+"</h3><p style=\"color:#FFFFFF\">"+des+"</p>");
             // QListWidgetItem* QWL = new QListWidgetItem(stext);
              b41->setStyleSheet("QLabel:hover{background-color:rgb(40, 40, 62);}");
              QLabel *b42=new QLabel(this);
              b42->setStyleSheet("border-image:url(:/new/prefix1/resources/profile-frame-small.png)");
              b42->setPixmap(QPixmap().fromImage(image));

              b42->setMaximumSize(112,92);
             // b41->setMaximumSize(348,68);
              b42->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
              top->addWidget(b41);top->addWidget(b42);
              top->setContentsMargins(12,12,12,12);
              top->setSpacing(9);

              //test->setStyleSheet("{QWidget{border: 2px solid white;}}");
              //test->setMaximumWidth(410);
              test->setMaximumHeight(104);
              //->addWidget(test);
              //newslist->insertItem(i,QWL);
              //QScrollArea *sca = new QScrollArea(this);
             // sca->setWidget(top);
               test->setLayout(top);
               newsbox->addWidget(test);

         }

     }

     ui->frame->setLayout(newsbox);
}
