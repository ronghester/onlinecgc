#-------------------------------------------------
#
# Project created by QtCreator 2014-05-03T14:36:08
#
#-------------------------------------------------

QT       += core gui network webkit

TARGET = CGC
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    loginbox.cpp \
    leaderboard.cpp \
    jsonutil/json_writer.cpp \
    jsonutil/json_valueiterator.inl \
    jsonutil/json_value.cpp \
    jsonutil/json_reader.cpp \
    jsonutil/json_internalmap.inl \
    jsonutil/json_internalarray.inl \
    gamelabel.cpp \
    arenascreen.cpp \
    createroom.cpp \
    sampledialog.cpp \
    constant.cpp \
    infobox.cpp \
    teamfindjoin.cpp \
    tournament.cpp \
    lockin.cpp \
    mediator.cpp \
    profilepage.cpp \
    registerdialog.cpp \
    loginwidget.cpp \
    arenaprofilecreate.cpp \
    editprofile.cpp \
    friendswidget.cpp \
    friendscontainer.cpp \
    challengeuser.cpp \
    challengepage.cpp \
    chatdialog.cpp \
    friendchatindicator.cpp \
    realtournament.cpp \
    tournamentlobby.cpp \
    nodegraph/node.cpp \
    nodegraph/graphwidget.cpp \
    nodegraph/edge.cpp \
    supportframe.cpp \
    ranksform.cpp \
    ranksdialog.cpp \
    userblockinlockin.cpp \
    alertdialog.cpp

HEADERS  += mainwindow.h \
    loginbox.h \
    leaderboard.h \
    jsonutil/json_batchallocator.h \
    jsonparser.h \
    gamelabel.h \
    arenascreen.h \
    createroom.h \
    sampledialog.h \
    constant.h \
    infobox.h \
    teamfindjoin.h \
    tournament.h \
    lockin.h \
    mediator.h \
    profilepage.h \
    registerdialog.h \
    loginwidget.h \
    arenaprofilecreate.h \
    editprofile.h \
    friendswidget.h \
    friendscontainer.h \
    challengeuser.h \
    challengepage.h \
    alertdialog.h \
    chatdialog.h \
    friendchatindicator.h \
    realtournament.h \
    tournamentlobby.h \
    nodegraph/node.h \
    nodegraph/graphwidget.h \
    nodegraph/edge.h \
    supportframe.h \
    ranksform.h \
    ranksdialog.h \
    userblockinlockin.h

FORMS    += mainwindow.ui \
    loginbox.ui \
    leaderboard.ui \
    arenascreen.ui \
    createroom.ui \
    sampledialog.ui \
    infobox.ui \
    teamfindjoin.ui \
    tournament.ui \
    lockin.ui \
    profilepage.ui \
    registerdialog.ui \
    loginwidget.ui \
    arenaprofilecreate.ui \
    editprofile.ui \
    friendswidget.ui \
    challengeuser.ui \
    challengepage.ui \
    alertdialog.ui \
    chatdialog.ui \
    friendchatindicator.ui \
    realtournament.ui \
    tournamentlobby.ui \
    supportframe.ui \
    ranksform.ui \
    ranksdialog.ui \
    userblockinlockin.ui
RC_FILE = cgc.rc
RESOURCES += \
    res.qrc

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/winsparkal/ -lWinSparkle
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/winsparkal/ -lWinSparkled

#INCLUDEPATH += $$PWD/winsparkal
#DEPENDPATH += $$PWD/winsparkal

