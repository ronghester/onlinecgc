#include "challengepage.h"
#include "ui_challengepage.h"
#include <QDebug>
#include <mainwindow.h>
#include <QTimer>
#include <QLabel>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
extern Json::Value challengeInfo;
extern MainWindow* w;
extern std::vector<arenaobject> availablearenas;
extern QString currentArenaName;
int rownumber;
int rownumber2;
QString url;
//bool WIP;
ChallengePage::ChallengePage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChallengePage)
{
    ui->setupUi(this);
    tableWidget = ui->tableWidget;
    url = "<url>api/CGC/GetChallenges?uid="+QString().number(mainid);
    QTimer::singleShot(2000, this, SLOT(loadFinished()));


    root = 0;
    rownumber = -1;
    rownumber2 = -1;
    WIP = false;
    qDebug()<<"hey i am here";
}

ChallengePage::~ChallengePage()
{
    delete ui;
}

void ChallengePage::checkChallenges(){
    if(WIP) return;
    QString surl = "<url>CheckChallenges?uid="+QString().number(mainid);
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest2(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);
}

void ChallengePage::loadFinished(){
      QString surl = url;
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
}

QTableWidgetItem* ChallengePage::createTableWidgetItem( const QString& text )
{
    QTableWidgetItem* item = new QTableWidgetItem( text );
    item->setTextAlignment( Qt::AlignCenter );
    return item;
}

QWidget* ChallengePage::getEnterButton(int RowNum){
    QWidget *buttonset = new QWidget();
    QVBoxLayout *vbox = new QVBoxLayout();
    QPushButton* accept2 = new QPushButton("Enter Lobby");
    accept2->setMaximumHeight(24);

    vbox->addWidget(accept2);

    buttonset->setLayout(vbox);
    buttonset->setContentsMargins(0,0,0,0);

    connect(accept2, SIGNAL(clicked()), &ButtonSignalMapper_3, SLOT(map()));
    ButtonSignalMapper_3.setMapping(accept2, RowNum);
    connect(&ButtonSignalMapper_3, SIGNAL(mapped(int)), this, SLOT(enterInChallengeLobby(int)));



    return buttonset;
}

QWidget* ChallengePage::getButtonWidgets(int RowNum){
    QWidget *buttonset = new QWidget();
    QVBoxLayout *vbox = new QVBoxLayout();
    QPushButton* accept = new QPushButton("Accept");
    accept->setMaximumHeight(24);
    QPushButton* decline = new QPushButton("Decline");
    decline->setMaximumHeight(24);
    vbox->addWidget(accept);
    vbox->addWidget(decline);
    buttonset->setLayout(vbox);
    buttonset->setContentsMargins(0,0,0,0);

    connect(accept, SIGNAL(clicked()), &ButtonSignalMapper, SLOT(map()));
    ButtonSignalMapper.setMapping(accept, RowNum);
    connect(&ButtonSignalMapper, SIGNAL(mapped(int)), this, SLOT(CellButtonClicked(int)));

    connect(decline, SIGNAL(clicked()), &ButtonSignalMapper_2, SLOT(map()));
    ButtonSignalMapper_2.setMapping(decline, RowNum);
    connect(&ButtonSignalMapper_2, SIGNAL(mapped(int)), this, SLOT(declineChallenge(int)));


     //connect(accept,SIGNAL(clicked()),this,SLOT(acceptChallenge()));
    // connect(decline,SIGNAL(clicked()),this,SLOT(declineChallenge()));
   // buttonset->setMinimumHeight(8);

    return buttonset;
}
void ChallengePage::acceptChallenge(){
    qDebug() << "Accept the Challenge";
}

void ChallengePage::plainRequest2(QNetworkReply* reply ){
        QString replyText;
        QByteArray rawData = reply->readAll();
        QString textData(rawData);
         qDebug() << "Checking Challenges: "+ textData;
         if(textData.length() < 5) {
             ui->label->setText("Challenges(No Chanllenges For YOU)-Create One!");
             return;
         }
          Json::Reader reader;
          Json::Value groot;
          QVBoxLayout *newsbox=new QVBoxLayout();
          bool parsingSuccessful = reader.parse(textData.toStdString(), groot );
             if(!parsingSuccessful || groot.size() == 0){
                   return;
             }
             else{
             }

}

void ChallengePage::plainRequest(QNetworkReply* reply ){
  try{
        QString replyText;

        QByteArray rawData = reply->readAll();
        QString textData(rawData);
         qDebug() << "RM"+ textData;

         if(textData.length() < 5) {
             ui->label->setText("Challenges(No Chanllenges For YOU)-Create One!");
             return;
         }
         Json::Reader reader;
         Json::Value groot;
          QVBoxLayout *newsbox=new QVBoxLayout();
         bool parsingSuccessful = reader.parse(textData.toStdString(), groot );
      //
         if(!parsingSuccessful || groot.size() == 0){
               return;
         }
         else{
tableWidget->clearContents();tableWidget->clear();
QStringList Hlist;
Hlist<<"When"<<"Challenge TO"<<"Game Name"<<"Players"<<"Description"<<"Wager"<<"Status";
tableWidget->setHorizontalHeaderLabels(Hlist);
             const Json::Value friends = groot;
             QString messageType = QString().fromStdString(friends.get("MessageType","none").asCString());
             qDebug() << "RM-1";
             if(messageType == "AllChallenges"){
                tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
                QStringList m_TableHeader;
                 qDebug() << "RM-1";
                tableWidget->verticalHeader()->hide();
                tableWidget->horizontalHeader()->show();
                tableWidget->setColumnWidth(0,94);
                tableWidget->setColumnWidth(1,94);
                tableWidget->setColumnWidth(2,98);
                tableWidget->setColumnWidth(3,64);
                tableWidget->setColumnWidth(4,98);
                tableWidget->setColumnWidth(5,64);
                //tableWidget->setColumnWidth(6,94);
                Json::Value proot = friends.get("Response","none");
                root = proot;
                 for(int i=0;i<proot.size();i++){
                      const Json::Value tem = proot[i];
                      QString when = QString().fromStdString(tem.get("When","none").asCString());
                      QString challenger = QString().fromStdString(tem.get("Challenged_To","none").asCString());
                      QString gamename = QString().fromStdString(tem.get("Game_Name","none").asCString());
                     // QString challenger = QString().fromStdString(tem.get("Challenger","none").asCString());
                      QString gametype = QString().fromStdString(tem.get("Challenge_Type","none").asCString());
                      QString desc = QString().fromStdString(tem.get("Desc","none").asCString());
                      QString wager = QString().fromStdString(tem.get("Wager","none").asCString());
                      QString status = QString().fromStdString(tem.get("Status","none").asCString());
                      theparentarena = QString().fromStdString(tem.get("parentArenaID","none").asCString());

                         tableWidget->insertRow(i);
                         tableWidget->setRowHeight(i,64);
                         QTableWidgetItem* QTW = createTableWidgetItem(when);
                         tableWidget->setItem(i,0,QTW);
                         tableWidget->setItem(i,1,createTableWidgetItem(challenger));
                         tableWidget->setItem(i,2,createTableWidgetItem(gamename));
                         tableWidget->setItem(i,3,createTableWidgetItem(gametype));
                         tableWidget->setItem(i,4,createTableWidgetItem(desc));
                         tableWidget->setItem(i,5,createTableWidgetItem(wager));
                        // tableWidget->setItem(i,6,createTableWidgetItem(status));

                         if(challenger != thisuser){
                             if(status =="Pending")
                                tableWidget->setItem(i,6,createTableWidgetItem(status));
                             else if(status =="Accepted"){
                                    tableWidget->setCellWidget(i,6,getEnterButton(i));
                             }
                         }
                         else if(status =="Pending")
                             tableWidget->setCellWidget(i,6,getButtonWidgets(i));

                         else if(status =="Accepted"){
                                tableWidget->setCellWidget(i,6,getEnterButton(i));
                         }
                         /*else{
                             QTableWidgetItem* stauss = createTableWidgetItem(status);
                             stauss->setBackground(QBrush(QColor(Qt::blue)));
                             stauss->setTextColor(QColor::fromRgb(0,255,0));
                             stauss->setForeground(QColor::fromRgb(255,0,0));
                               tableWidget->setItem(i,6,stauss);
                         }*/
                 }
                  tableWidget->insertRow(root.size());
                  tableWidget->show();
             }
             else if(messageType == "Challenge_Update_Response"){
                 //Challenge is updated..
                  WIP = false;
             }
         }
    }
    catch(...){
         // signalMapper = new QSignalMapper(this);
    }

     //QTimer::singleShot(1000,this,SLOT(checkChallenges()));
}

void ChallengePage::enterInChallengeLobby(int RowNum){
    if(RowNum != rownumber){

        rownumber = RowNum;

        //Lets check if this user is signed up for that arena or not..

        for(int h=0;h<availablearenas.size();h++){
             qDebug()<<availablearenas[h].userarenaname<<":"<<availablearenas[h].arenacode;
            if(availablearenas[h].arenacode == theparentarena){
                cind = theparentarena;

                currentArenaName = availablearenas[h].userarenaname;
                const Json::Value tem = root[rownumber];
                QString challengeid = QString().fromStdString(tem.get("Challenge_ID","none").asCString());
                QString challenger = QString().fromStdString(tem.get("Challenger","none").asCString());
                QString challengedto = QString().fromStdString(tem.get("Challenged_To","none").asCString());
                QString wager = QString().fromStdString(tem.get("Wager","none").asCString());
                QString desc = QString().fromStdString(tem.get("Desc","none").asCString());

                 //Json::Value root["challenge_to"] = Json::Value(Json::stringValue(challengeid));
                 challengeInfo["nid"] = challengeid.toStdString().c_str();
                challengeInfo["challenge_to"] = challengedto.toStdString().c_str();

                challengeInfo["gametype"] = QString("1V1").toStdString().c_str();
                challengeInfo["wager"] = wager.toStdString().c_str();
                challengeInfo["desc"] = desc.toStdString().c_str();


QTimer::singleShot(1000,w,SLOT(startChallenge()));

           //  w->startChallenge();

            }
            else{
                IBox = new InfoBox(this);
                IBox->setMessage("You can't join this challenge,please sign up for this arena.");
                IBox->show();
            }
        }



    }
}

void ChallengePage::CellButtonClicked(int RowNum){
    if(RowNum != rownumber){
        WIP = true;
     qDebug()<<"Accept Button clicked "<<RowNum;
     rownumber = RowNum;
     const Json::Value tem = root[rownumber];
     QString when = QString().fromStdString(tem.get("Challenge_ID","none").asCString());
     qDebug()<<"challenge ID: "<<when;

     QWidget *pwidget;
     pwidget = tableWidget->cellWidget(rownumber,6);
     if(pwidget!= NULL)
          delete pwidget;

      QLabel* status = new QLabel("Accepted");
      status->setAlignment(Qt::AlignCenter );
       status->setStyleSheet("background-color:none;");
      tableWidget->setCellWidget(rownumber,6,status);


      url = "<url>api/CGC/UpdateChallenge?nid="+when+"&desc=Accepted";
      qDebug()<<url;
      QTimer::singleShot(2000, this, SLOT(loadFinished()));
    }
}
void ChallengePage::declineChallenge(int RowNum){
    if(RowNum != rownumber2){
     qDebug()<<"Decline Button clicked "<<RowNum;
     rownumber2 = RowNum;
     const Json::Value tem = root[rownumber2];
     QString when = QString().fromStdString(tem.get("Challenge_ID","none").asCString());
     qDebug()<<"challenge ID: "<<when;


     QWidget *pwidget;
     pwidget = tableWidget->cellWidget(rownumber2,6);
     if(pwidget!=NULL)
          delete pwidget;

     QLabel* status = new QLabel("Declined");
     status->setAlignment( Qt::AlignCenter );
     status->setStyleSheet("background-color:none;");
     tableWidget->setCellWidget(rownumber2,6,status);
     url = "<url>api/CGC/UpdateChallenge?nid="+when+"&desc=Declined";
     QTimer::singleShot(2000, this, SLOT(loadFinished()));
    }
}

