#ifndef CREATEROOM_H
#define CREATEROOM_H

#include <QDialog>

namespace Ui {
class CreateRoom;
}

class CreateRoom : public QDialog
{
    Q_OBJECT
    
public:
    explicit CreateRoom(QWidget *parent = 0);
    ~CreateRoom();
    QPushButton* getButton();
    QString getURLString(QString nid);
    bool onceclicked;

private slots:
    void on_toolButton_clicked();

    void on_pushButton_clicked();

private:
 Ui::CreateRoom *ui;
};

#endif // CREATEROOM_H
