#include "gamelabel.h"

GameLabel::GameLabel( QWidget * parent ):QLabel(parent){
     connect( this, SIGNAL( clicked() ), this, SLOT( slotClicked() ) );
}

GameLabel::GameLabel( const QString & text, QWidget * parent )
:QLabel(parent)
{
    connect( this, SIGNAL( clicked() ), this, SLOT( slotClicked() ) );
}



void GameLabel::slotClicked()
{
    //qDebug()<<"Clicked";
}

void GameLabel::mousePressEvent ( QMouseEvent * event )
{
    emit clicked();
}

void GameLabel::mouseMoveEvent(QMouseEvent *ev){
   // this->setStyleSheet("background-color:rgba(133,0,0,0)");
}
