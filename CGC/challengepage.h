#ifndef CHALLENGEPAGE_H
#define CHALLENGEPAGE_H

#include <QWidget>
#include <QNetworkReply>
#include <QTableWidget>
#include <jsonparser.h>
#include <QSignalMapper>
#include <infobox.h>

namespace Ui {
class ChallengePage;
}

class ChallengePage : public QWidget
{
    Q_OBJECT
    
    public:
        explicit ChallengePage(QWidget *parent = 0);
        ~ChallengePage();
bool WIP;
    QSignalMapper ButtonSignalMapper;
     QSignalMapper ButtonSignalMapper_2;
     QSignalMapper ButtonSignalMapper_3;

QString theparentarena;


    private slots:
        void loadFinished();
        void plainRequest(QNetworkReply* reply);
        void plainRequest2(QNetworkReply* reply);
        void acceptChallenge();
        void declineChallenge(int RowNum);
        void CellButtonClicked(int RowNum);

        void enterInChallengeLobby(int RowNum);
        void checkChallenges();


private:
        Ui::ChallengePage *ui;
        QTableWidget* tableWidget;
        QString suffix(int n);
        QTableWidgetItem* createTableWidgetItem( const QString& text );
        QWidget* getButtonWidgets(int Row);
         QWidget* getEnterButton(int RowNum);
        Json::Value root;
        InfoBox* IBox;

};

#endif // CHALLENGEPAGE_H
