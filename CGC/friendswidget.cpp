#include "friendswidget.h"
#include "ui_friendswidget.h"
#include <mainwindow.h>
#include <QDebug>
#include <QTimer>

 extern MainWindow* w;

FriendsWidget::FriendsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FriendsWidget)
{
    ui->setupUi(this);
}

void FriendsWidget::setValues(QString name,QString statusline,QString chips,QString mstatus,QString mrank){
    ui->username->setText(name);
    ui->userchips->setText("Chips Earned:"+chips);
    ui->userstatus->setText(statusline);
    status = mstatus;
    if(status=="online")
        ui->onlinestatus->setPixmap(QPixmap().fromImage(QImage(":/new/prefix1/resources/status-green.png")));
    else
        ui->onlinestatus->setPixmap(QPixmap().fromImage(QImage(":/new/prefix1/resources/white-dot.png")));

    if(mrank != "Unranked"){
        QString thisicon = ":/new/prefix1/resources/"+mrank+".png";
        ui->label->setPixmap(QPixmap(thisicon));
    }

}

void FriendsWidget::setImage(QByteArray userpic){
     ui->userpic->setStyleSheet("border-image:url(:/new/prefix1/resources/newframe.png)");
    QImage image = QImage::fromData(userpic, "PNG");
    QIcon icon;
    QSize size;
    if (!image.isNull())
      icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

    QPixmap pixmap = icon.pixmap(QSize(42,52), QIcon::Normal, QIcon::On);
      ui->userpic->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->userpic->setPixmap(pixmap);
}

void FriendsWidget::updateStatus(QString mstatus){
     status = mstatus;
    if(ui->username->text() == mstatus)
        ui->onlinestatus->setPixmap(QPixmap().fromImage(QImage(":/new/prefix1/resources/status-green.png")));
}



void FriendsWidget::mouseDoubleClickEvent ( QMouseEvent * e )
{

    if(ui->frame_2->rect().contains(e->pos())){
        w->createChatFriends( ui->username->text(),status,21);
    }
    else{
        w->whichuser = ui->username->text();
        QTimer::singleShot(1000, w, SLOT(on_profileButton_clicked()));
    }

}
void FriendsWidget::mouseReleaseEvent ( QMouseEvent * e )
{

}
FriendsWidget::~FriendsWidget()
{
    delete ui;
}

