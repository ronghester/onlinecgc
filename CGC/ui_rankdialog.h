/********************************************************************************
** Form generated from reading UI file 'rankdialog.ui'
**
** Created: Tue 20. Jan 22:07:05 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RANKDIALOG_H
#define UI_RANKDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_RankDialog
{
public:
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label;

    void setupUi(QDialog *RankDialog)
    {
        if (RankDialog->objectName().isEmpty())
            RankDialog->setObjectName(QString::fromUtf8("RankDialog"));
        RankDialog->resize(219, 221);
        RankDialog->setStyleSheet(QString::fromUtf8("QDialog{\n"
"\n"
"border: 1px solid grey;\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QLineEdit{\n"
"height:18;\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
"\n"
"#toolButton{\n"
"height:28px;\n"
"width:38px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}\n"
"\n"
"#toolButton_2{\n"
"height:28px;\n"
"width:32px;\n"
"	border-image: url(:/new/prefix1/resources/minimize.png);\n"
"	\n"
"}\n"
"#toolButton_2:hover{\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"#toolButton_3{\n"
"height:28px;\n"
"width:32px;\n"
"border-image: url(:/new/prefix1/resources/maximize.png);\n"
"}\n"
"#toolButton_3:hover{\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-image: url(:/ne"
                        "w/prefix1/resources/menu.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"	\n"
"\n"
"\n"
"}"));
        label_2 = new QLabel(RankDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(25, 24, 41, 31));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Admiral.png")));
        label_2->setScaledContents(false);
        label_3 = new QLabel(RankDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(20, 55, 51, 41));
        label_3->setTextFormat(Qt::RichText);
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Platinum.png")));
        label_4 = new QLabel(RankDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(27, 90, 41, 41));
        label_4->setTextFormat(Qt::RichText);
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Gold.png")));
        label_5 = new QLabel(RankDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(23, 128, 171, 41));
        label_5->setTextFormat(Qt::RichText);
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Silver.png")));
        label_6 = new QLabel(RankDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(23, 172, 171, 41));
        label_6->setTextFormat(Qt::RichText);
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Bronze.png")));
        label_7 = new QLabel(RankDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(80, 66, 131, 21));
        label_8 = new QLabel(RankDialog);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(80, 96, 111, 31));
        label_9 = new QLabel(RankDialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(84, 136, 101, 21));
        label_10 = new QLabel(RankDialog);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(83, 176, 111, 21));
        label = new QLabel(RankDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(80, 26, 141, 21));

        retranslateUi(RankDialog);

        QMetaObject::connectSlotsByName(RankDialog);
    } // setupUi

    void retranslateUi(QDialog *RankDialog)
    {
        RankDialog->setWindowTitle(QApplication::translate("RankDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label_2->setText(QString());
        label_3->setText(QString());
        label_4->setText(QString());
        label_5->setText(QString());
        label_6->setText(QString());
        label_7->setText(QApplication::translate("RankDialog", "Platinum : Top 10%", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("RankDialog", "Gold: Top 25%", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("RankDialog", "Silver: Top 50%", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("RankDialog", "Bronze: Top 75%", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("RankDialog", "Admiral: Top 5%", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RankDialog: public Ui_RankDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RANKDIALOG_H
