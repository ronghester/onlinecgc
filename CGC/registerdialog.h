#ifndef REGISTERDIALOG_H
#define REGISTERDIALOG_H

#include <QDialog>
#include <QNetworkReply>
#include <infobox.h>

namespace Ui {
class RegisterDialog;
}

class RegisterDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit RegisterDialog(QWidget *parent = 0);
    ~RegisterDialog();
    
private slots:
    void on_toolButton_clicked();

    void on_pushButton_clicked();
    void requestReceived(QNetworkReply* reply);

private:
    Ui::RegisterDialog *ui;
    InfoBox *IBox;
};

#endif // REGISTERDIALOG_H
