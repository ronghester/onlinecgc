#ifndef ALERTDIALOG_H
#define ALERTDIALOG_H

#include <QDialog>

namespace Ui {
class AlertDialog;
}

class AlertDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit AlertDialog(QWidget *parent = 0);
    ~AlertDialog();
    void setText(QString text);
    
private slots:


    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

private:
    Ui::AlertDialog *ui;

  signals:
    void okClicked();
};

#endif // ALERTDIALOG_H
