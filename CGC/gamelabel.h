#ifndef GAMELABEL_H
#define GAMELABEL_H

#include <QLabel>
#include <QMouseEvent>

class GameLabel : public QLabel
{
    Q_OBJECT
public:
    GameLabel( const QString & text, QWidget * parent = 0 );
    GameLabel( QWidget * parent = 0 );
    virtual ~GameLabel(){}

signals:
    void clicked();

public slots:
    void slotClicked();

protected:
    void mousePressEvent ( QMouseEvent * event ) ;
    void mouseMoveEvent(QMouseEvent *ev);

};


#endif // GAMELABEL_H
