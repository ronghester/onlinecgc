#ifndef FRIENDSCONTAINER_H
#define FRIENDSCONTAINER_H
#include <QStringList>
#include <friendswidget.h>
class FriendsContainer
{
public:
    FriendsContainer();
    QStringList lnames;
    QStringList lchips;
    QStringList lstatus;
    QStringList lonlinestatus;
    QStringList lranks;

   QVector<QByteArray> friendsimages;
    void addFriendElement(QString name,QString chips,QString status,QString onlinestatus,QString rank);
    std::vector<FriendsWidget*> getFriendsWidget();
    void updateOnlineStatusOfUser(QString name);
    void setImage(QByteArray by);

    bool isFriend(QString name);
};

#endif // FRIENDSCONTAINER_H
