/********************************************************************************
** Form generated from reading UI file 'sampledialog.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAMPLEDIALOG_H
#define UI_SAMPLEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_SampleDialog
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;
    QToolButton *toolButton;

    void setupUi(QDialog *SampleDialog)
    {
        if (SampleDialog->objectName().isEmpty())
            SampleDialog->setObjectName(QString::fromUtf8("SampleDialog"));
        SampleDialog->resize(302, 158);
        QFont font;
        font.setFamily(QString::fromUtf8("Rockwell"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        SampleDialog->setFont(font);
        SampleDialog->setStyleSheet(QString::fromUtf8("QLabel{\n"
"background-color:rgba(0,0,0,0);\n"
"color:#fff;\n"
"	font: 75 10pt \"Rockwell\";\n"
"\n"
"}\n"
"QLineEdit\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4d4d4d, stop: 0 #646464, stop: 1 #5d5d5d);\n"
"    padding: 1px;\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"}"));
        label = new QLabel(SampleDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 30, 91, 31));
        label_2 = new QLabel(SampleDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 70, 121, 31));
        lineEdit = new QLineEdit(SampleDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(160, 40, 113, 20));
        lineEdit->setStyleSheet(QString::fromUtf8(""));
        lineEdit_2 = new QLineEdit(SampleDialog);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(160, 70, 113, 20));
        pushButton = new QPushButton(SampleDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(100, 120, 91, 23));
        toolButton = new QToolButton(SampleDialog);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(270, 0, 25, 19));

        retranslateUi(SampleDialog);

        QMetaObject::connectSlotsByName(SampleDialog);
    } // setupUi

    void retranslateUi(QDialog *SampleDialog)
    {
        SampleDialog->setWindowTitle(QApplication::translate("SampleDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SampleDialog", "Team Name", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SampleDialog", "Team Description", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("SampleDialog", "Create Team", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("SampleDialog", "X", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SampleDialog: public Ui_SampleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAMPLEDIALOG_H
