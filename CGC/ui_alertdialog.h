/********************************************************************************
** Form generated from reading UI file 'alertdialog.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALERTDIALOG_H
#define UI_ALERTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AlertDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AlertDialog)
    {
        if (AlertDialog->objectName().isEmpty())
            AlertDialog->setObjectName(QString::fromUtf8("AlertDialog"));
        AlertDialog->resize(324, 120);
        AlertDialog->setStyleSheet(QString::fromUtf8("#AlertDialog{\n"
"\n"
"border: 1px solid #000;\n"
"background-color:  rgb(38, 38, 38);\n"
"}\n"
"\n"
"QLabel{\n"
"\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
"QPushButton{\n"
"width:100px;\n"
"height:24px;\n"
"	border-image: url(:/new/prefix1/resources/loginbtn.png);\n"
"}\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(AlertDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(AlertDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(12);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        label->setFont(font);
        label->setTextFormat(Qt::PlainText);
        label->setAlignment(Qt::AlignCenter);
        label->setMargin(9);

        verticalLayout->addWidget(label);

        buttonBox = new QDialogButtonBox(AlertDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(AlertDialog);

        QMetaObject::connectSlotsByName(AlertDialog);
    } // setupUi

    void retranslateUi(QDialog *AlertDialog)
    {
        AlertDialog->setWindowTitle(QApplication::translate("AlertDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AlertDialog", "Do you want to join the challenge?", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AlertDialog: public Ui_AlertDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALERTDIALOG_H
