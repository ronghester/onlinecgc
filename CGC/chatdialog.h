#ifndef CHATDIALOG_H
#define CHATDIALOG_H

#include <QDialog>
#include <QKeyEvent>
#include <QNetworkReply>

namespace Ui {
class ChatDialog;
}

class ChatDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ChatDialog(QWidget *parent = 0,bool istype2=false,QString channel="");
    ~ChatDialog();
    QString chat_room;
    void addChatMsg(QString msg);
    
private slots:
    void on_pushButton_clicked();
     void plainRequest(QNetworkReply* reply);

private:
    Ui::ChatDialog *ui;
    void keyPressEvent(QKeyEvent *event);
    void sendChatMsg(QString msg);

    int chatcounter;
};

#endif // CHATDIALOG_H
