/********************************************************************************
** Form generated from reading UI file 'ranksform.ui'
**
** Created: Sat 4. Apr 00:08:42 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RANKSFORM_H
#define UI_RANKSFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RanksForm
{
public:
    QFormLayout *formLayout;
    QLabel *label_10;
    QLabel *label;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_7;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;

    void setupUi(QWidget *RanksForm)
    {
        if (RanksForm->objectName().isEmpty())
            RanksForm->setObjectName(QString::fromUtf8("RanksForm"));
        RanksForm->resize(158, 169);
        RanksForm->setStyleSheet(QString::fromUtf8("#RanksForm{\n"
"\n"
"border: 2px solid #fff;\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QLineEdit{\n"
"height:18;\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 10pt \"Candara\";\n"
"}\n"
""));
        formLayout = new QFormLayout(RanksForm);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        formLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        formLayout->setHorizontalSpacing(7);
        formLayout->setVerticalSpacing(4);
        formLayout->setContentsMargins(12, 2, 2, 0);
        label_10 = new QLabel(RanksForm);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout->setWidget(11, QFormLayout::FieldRole, label_10);

        label = new QLabel(RanksForm);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(4, QFormLayout::FieldRole, label);

        label_8 = new QLabel(RanksForm);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout->setWidget(7, QFormLayout::FieldRole, label_8);

        label_9 = new QLabel(RanksForm);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(10, QFormLayout::FieldRole, label_9);

        label_7 = new QLabel(RanksForm);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMaximumSize(QSize(16777215, 24));

        formLayout->setWidget(6, QFormLayout::FieldRole, label_7);

        label_2 = new QLabel(RanksForm);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Admiral.png")));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(RanksForm);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(28, 23));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Platinum.png")));
        label_3->setScaledContents(true);

        formLayout->setWidget(6, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(RanksForm);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Gold.png")));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(RanksForm);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMaximumSize(QSize(20, 20));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Silver.png")));
        label_5->setScaledContents(true);

        formLayout->setWidget(10, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(RanksForm);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(24, 16777215));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Bronze.png")));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_6);


        retranslateUi(RanksForm);

        QMetaObject::connectSlotsByName(RanksForm);
    } // setupUi

    void retranslateUi(QWidget *RanksForm)
    {
        RanksForm->setWindowTitle(QApplication::translate("RanksForm", "Form", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("RanksForm", "Bronze: Top 75%", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("RanksForm", "Admiral: Top 5%", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("RanksForm", "Gold: Top 25%", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("RanksForm", "Silver: Top 50%", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("RanksForm", "Platinum: Top 10%", 0, QApplication::UnicodeUTF8));
        label_2->setText(QString());
        label_3->setText(QString());
        label_4->setText(QString());
        label_5->setText(QString());
        label_6->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class RanksForm: public Ui_RanksForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RANKSFORM_H
