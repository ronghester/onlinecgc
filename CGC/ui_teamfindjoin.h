/********************************************************************************
** Form generated from reading UI file 'teamfindjoin.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEAMFINDJOIN_H
#define UI_TEAMFINDJOIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TeamFindJoin
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QToolButton *toolButton;
    QLabel *label;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *teamname;
    QPushButton *search_team;
    QListWidget *listWidget;
    QLabel *searchresult;
    QPushButton *jointeam;

    void setupUi(QDialog *TeamFindJoin)
    {
        if (TeamFindJoin->objectName().isEmpty())
            TeamFindJoin->setObjectName(QString::fromUtf8("TeamFindJoin"));
        TeamFindJoin->resize(224, 280);
        QFont font;
        font.setFamily(QString::fromUtf8("Rockwell"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        TeamFindJoin->setFont(font);
        TeamFindJoin->setStyleSheet(QString::fromUtf8("#TeamFindJoin{\n"
"\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:0.171, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #fff;\n"
"	border-image: url(:/new/prefix1/resources/menu.png);\n"
"\n"
"}\n"
"QPushButton:hover{\n"
"	\n"
"	color: rgb(134, 134, 134);\n"
"	\n"
"\n"
"\n"
"}"));
        verticalLayout = new QVBoxLayout(TeamFindJoin);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(TeamFindJoin);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(0, 32));
        frame->setMaximumSize(QSize(16777215, 36));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        toolButton = new QToolButton(frame);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(180, 6, 31, 20));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 161, 16));

        verticalLayout->addWidget(frame);

        frame_2 = new QFrame(TeamFindJoin);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        teamname = new QLineEdit(frame_2);
        teamname->setObjectName(QString::fromUtf8("teamname"));
        teamname->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));

        verticalLayout_2->addWidget(teamname);

        search_team = new QPushButton(frame_2);
        search_team->setObjectName(QString::fromUtf8("search_team"));

        verticalLayout_2->addWidget(search_team);

        listWidget = new QListWidget(frame_2);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setMaximumSize(QSize(16777215, 200));

        verticalLayout_2->addWidget(listWidget);

        searchresult = new QLabel(frame_2);
        searchresult->setObjectName(QString::fromUtf8("searchresult"));
        sizePolicy.setHeightForWidth(searchresult->sizePolicy().hasHeightForWidth());
        searchresult->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(searchresult);

        jointeam = new QPushButton(frame_2);
        jointeam->setObjectName(QString::fromUtf8("jointeam"));

        verticalLayout_2->addWidget(jointeam);


        verticalLayout->addWidget(frame_2);


        retranslateUi(TeamFindJoin);

        QMetaObject::connectSlotsByName(TeamFindJoin);
    } // setupUi

    void retranslateUi(QDialog *TeamFindJoin)
    {
        TeamFindJoin->setWindowTitle(QApplication::translate("TeamFindJoin", "Dialog", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("TeamFindJoin", "X", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TeamFindJoin", "Team Search", 0, QApplication::UnicodeUTF8));
        search_team->setText(QApplication::translate("TeamFindJoin", "Search Team", 0, QApplication::UnicodeUTF8));
        searchresult->setText(QString());
        jointeam->setText(QApplication::translate("TeamFindJoin", "Join Team", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TeamFindJoin: public Ui_TeamFindJoin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEAMFINDJOIN_H
