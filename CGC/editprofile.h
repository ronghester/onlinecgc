#ifndef EDITPROFILE_H
#define EDITPROFILE_H
#include <QNetworkReply>
#include <QDialog>

namespace Ui {
class EditProfile;
}

class EditProfile : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditProfile(QWidget *parent = 0);
    ~EditProfile();
    
private slots:
     void on_pushButton_2_clicked();
     void on_toolButton_clicked();
     void on_pushButton_clicked();
     void  requestReceived(QNetworkReply* reply);
      void  requestReceived2(QNetworkReply* reply);
     void plainRequest(QNetworkReply* reply);
     void on_reloadchip_clicked();

private:
    Ui::EditProfile *ui;

    QString fileName;
};

#endif // EDITPROFILE_H
