#include "sampledialog.h"
#include "ui_sampledialog.h"
#include <mainwindow.h>
#include <infobox.h>
#include <QDebug>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern  QString cind;
SampleDialog::SampleDialog(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::SampleDialog)
{
    ui->setupUi(this);
}

SampleDialog::~SampleDialog()
{
    delete ui;
}

void SampleDialog::on_closedlg_clicked()
{
    this->close();
}

void SampleDialog::on_createTeam_clicked()
{
    //validation
    QString teamname = ui->label->text();
    QString teamdesc = ui->label_2->text();
    if(teamname.size()> 3){
    QString surl = "<url>createteam?title="+teamname+"&desc="+teamdesc+"&nid="+cind;
     qDebug() << surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request);
    }
    else{
        IBox = new InfoBox(this);
        IBox->setMessage("Please Provide Team Name (Must be 4 character long)");
        IBox->show();
    }




}
void SampleDialog::plainRequest(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << textData;

    IBox = new InfoBox(this);
    IBox->setMessage(textData);
    IBox->show();
}

void SampleDialog::on_pushButton_clicked()
{
    //validation
    QString teamname = ui->lineEdit->text();
    QString teamdesc = ui->lineEdit_2->text();
    if(teamname.size()> 3){
    QString surl = "<url>createteam?title="+teamname+"&desc="+teamdesc+"&nid="+cind;
     qDebug() << surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request);
    }
    else{
        IBox = new InfoBox(this);
        IBox->setMessage("Please Provide Team Name (Must be 4 character long)");
        IBox->show();
    }
}

void SampleDialog::on_toolButton_clicked()
{
    this->close();
}
