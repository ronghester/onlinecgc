#include "teamfindjoin.h"
#include "ui_teamfindjoin.h"
#include <QPushButton>
#include <QDebug>
#include <jsonparser.h>
#include <mainwindow.h>
#include <infobox.h>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
 extern QString cind;
TeamFindJoin::TeamFindJoin(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::TeamFindJoin)
{
    ui->setupUi(this);
    count=0;
    //
     //QListWidget *listWidget = new QListWidget(this);


        //Adding a button

        //ui->verticalLayout_2->addWidget(new QPushButton("Join",this));
}
void TeamFindJoin::setValues(QStringList tr){
    for(int i=0;i<tr.length();i++){
        QString stext =tr[i];
            QListWidgetItem* QWL = new QListWidgetItem(stext);
            QWL->setTextColor(Qt::white);
            ui->listWidget->insertItem(count,QWL);
    }
}

TeamFindJoin::~TeamFindJoin()
{
    delete ui;
}

void TeamFindJoin::on_toolButton_clicked()
{
    this->close();
}

void TeamFindJoin::on_jointeam_clicked()
{
    if(ui->listWidget->currentItem()){
    const QString& s = ui->listWidget->currentItem()->text();
     qDebug()<<s;
    }
}

void TeamFindJoin::on_search_team_clicked()
{
    if(!ui->teamname->text().isEmpty()){
        QString surl = "<url>findteam?title="+ui->teamname->text();
         qDebug() << surl;
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
          QNetworkRequest request = QNetworkRequest(surl);
          QString ss = sessionname+"="+sessionid;
          request.setRawHeader("Cookie",ss.toUtf8() );
        manager->get(request);
    }
    else{
        IBox = new InfoBox(this);
        IBox->setMessage("Please Provide Team Name (Must be 4 character long)");
        IBox->show();
    }
}

void TeamFindJoin::plainRequest(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << textData;

     Json::Reader reader;
     Json::Value root;

     reader.parse(textData.toStdString(), root );
     for(int i=0;i<root.size();i++){
         const Json::Value tem = root[i];
         QString stext = QString().fromStdString(tem.asString());
             QListWidgetItem* QWL = new QListWidgetItem(stext);
             QWL->setTextColor(Qt::white);
             ui->listWidget->insertItem(count,QWL);
     }

   /* IBox = new InfoBox(this);
    IBox->setMessage(textData);
    IBox->show();*/
}

