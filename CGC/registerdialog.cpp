#include "registerdialog.h"
#include "ui_registerdialog.h"
#include "QDebug"
RegisterDialog::RegisterDialog(QWidget *parent) : QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::RegisterDialog)
{
    ui->setupUi(this);
}

RegisterDialog::~RegisterDialog()
{
    delete ui;
}

void RegisterDialog::on_toolButton_clicked()
{
    this->close();
}

void RegisterDialog::on_pushButton_clicked()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl params;
    params.addQueryItem("name", ui->lineEdit->text().toLower());
    params.addQueryItem("pass", ui->lineEdit_2->text());
    qDebug()<<"Password :"+ui->lineEdit_2->text();
     params.addQueryItem("mail", ui->lineEdit_3->text());
     params.addQueryItem("field_name[und][0][value]", ui->lineEdit_4->text());
     params.addQueryItem("status", "1");
     params.addQueryItem("field_address[und][0][value]", ui->lineEdit_5->text());
     params.addQueryItem("field_dob[und][0][value]", ui->lineEdit_6->text());
     params.addQueryItem("field_statusline[und][0][value]",ui->lineEdit_7->text());
     //params.addQueryItem("pass", ui->lineEdit_2->text());
    //QSslConfiguration config(QSslConfiguration::defaultConfiguration());
    QString surl = "<url>updateuser";
    QNetworkRequest request = QNetworkRequest(surl);
    //request.setSslConfiguration(config);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    manager->post(request,params.encodedQuery());
    ui->label_9->setText("Connecting....");
}

void RegisterDialog::requestReceived(QNetworkReply* reply){
    qDebug() << "Received ";
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << textData;

    if(textData.length()<5){
         ui->label_9->setText("Could Not Connect Try Again Later ");
    }
    if(textData.contains("uid")){
        IBox = new InfoBox(this);
        IBox->setMessage("Registration Successfull! You can login now");
        IBox->show();
        this->close();

    }
    else{
        ui->label_9->setText("Error: " + textData);


    }


}
