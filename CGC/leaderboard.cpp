#include "leaderboard.h"
#include "ui_leaderboard.h"
#include <QDebug>
#include <mainwindow.h>
#include <QTimer>
#include <QPropertyAnimation>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString cind;
extern QString thisuser;
Json::Value root;
extern MainWindow* w;
LeaderBoard::LeaderBoard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LeaderBoard)
{
    ui->setupUi(this);
    tableWidget = ui->tableWidget;
    QTimer::singleShot(2000, this, SLOT(loadFinished()));
    root = 0;
    RD = new RanksDialog(ui->pushButton);
    QPoint rpos = this->pos();
    //RD->move(rpos.x()+740,rpos.y()+300);
    RD->hide();
}

void LeaderBoard::loadFinished(){
      QString surl = "<url>getusersonchannel?title=xxx";
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
}

LeaderBoard::~LeaderBoard()
{
    delete ui;
}



QString LeaderBoard::suffix(int n)
{
    if (n%100 == 11 || n%100 == 12 || n%100 == 13)
        return "th";
    else if (n%10 == 1)
        return "st";
    else if (n%10 == 2)
       return "nd";
    else if (n%10 == 3)
       return "rd";
    else
        return "th";
}

QTableWidgetItem* LeaderBoard::createTableWidgetItem( const QString& text )
{
    QString parola = text;
    parola.replace(0,1,text[0].toUpper());
    QTableWidgetItem* item = new QTableWidgetItem( parola );
    item->setTextAlignment( Qt::AlignCenter );
    return item;
}

void LeaderBoard::plainRequest(QNetworkReply* reply ){
    try{
   QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         return;
     }
     Json::Reader reader;

      QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
         //lobbies = root;
         // tableWidget = new QTableWidget(4, 6, this);
        // tableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
       tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);


      // tableWidget->clearContents();
       QStringList m_TableHeader;
       tableWidget->verticalHeader()->hide();

       //tableWidget->setShowGrid(false);
       //tableWidget->cornerWidget()->hide();
       //m_TableHeader<<"Rank"<<"Position"<<"Username"<<"Number of Games"<<"Win"<<"Lost"<<"Chips Earned"<<;
       //tableWidget->setHorizontalHeaderLabels(m_TableHeader);
       tableWidget->horizontalHeader()->show();
       tableWidget->setColumnWidth(0,84);
       tableWidget->setColumnWidth(1,64);
       tableWidget->setColumnWidth(2,108);
       tableWidget->setColumnWidth(3,124);
       tableWidget->setColumnWidth(4,44);
       tableWidget->setColumnWidth(5,44);
       tableWidget->setColumnWidth(6,94);
       //tableWidget->row
       int totalct = 0;
        for(int i=0;i<root.size();i++){
              const Json::Value tem = root[i];
                 QString point = QString().fromStdString(tem.get("point","none").asCString());
                 int newpt = point.toInt();
                 if(newpt > 0){
                         totalct++;
                 }
        }
          qDebug()<<"Total:"<<totalct;

         for(int i=0;i<root.size();i++){
              const Json::Value tem = root[i];
                QString username = QString().fromStdString(tem.get("username","none").asCString());
                QString win =   QString().fromStdString(tem.get("win","none").asCString());
                QString lost =  QString().fromStdString(tem.get("lost","none").asCString());
                QString point = QString().fromStdString(tem.get("point","none").asCString());
                QString games = QString().fromStdString(tem.get("games","none").asCString());
                 // QString id = QString().fromStdString(tem.get("when","none").asCString());
                 //qDebug()<<title;
                 int realpints = point.toInt();
                 if(realpints < 0) point = "0";
                 tableWidget->insertRow(i);


                 int rank = (i+1) * 100 /(totalct);
                 QString thisrank = "unranked";
                 QString thisicon = ":/new/prefix1/resources/unranked.png";

                 qDebug()<<"rank is:"<<rank;
                 if(games != "0" &&  point.toInt()>0){
                     if(rank <= 10){thisrank = "Admiral";}
                     else if(rank <= 25) {thisrank = "Platinum";}
                     else if(rank <= 50) {thisrank = "Gold";}
                     else if(rank <= 75) {thisrank = "Silver";}
                     else {thisrank = "Bronze";}
                 }

                 thisicon = ":/new/prefix1/resources/"+thisrank+".png";

                 QString parola = thisrank;
                 parola.replace(0,1,thisrank[0].toUpper());
                 QTableWidgetItem* tt = new QTableWidgetItem(" "+parola);
                 tt->setIcon(QIcon(QPixmap(thisicon)));

                 tableWidget->setItem(i,0,tt);
                 tableWidget->setItem(i,1,createTableWidgetItem(QString().number(i+1)+suffix(i+1)));
                // QString parola = QLatin1String(username);
                 if(username == thisuser){
                      QString thisicon = ":/new/prefix1/resources/"+thisrank+".png";
                      w->setUserRank(thisrank,thisicon);
                 }
                 username[0] = username.at(0).toTitleCase();

                 tableWidget->setItem(i,2,createTableWidgetItem(username));
                 tableWidget->setItem(i,3,createTableWidgetItem(games));
                 tableWidget->setItem(i,4,createTableWidgetItem(win));
                 tableWidget->setItem(i,5,createTableWidgetItem(lost));
                 tableWidget->setItem(i,6,createTableWidgetItem(point));


         }
         //last item to be empty
          tableWidget->insertRow(root.size());
         // tableWidget->horizontalHeader()->setMaximumWidth(tableWidget->width());
         tableWidget->show();

         //tableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
     }
    }
    catch(...){
         // signalMapper = new QSignalMapper(this);
    }
}



void LeaderBoard::on_tableWidget_cellDoubleClicked(int row, int column)
{
    qDebug()<<"Row clicked "+QString().number(row);
         const Json::Value tem = root[row];
         QString username = QString().fromStdString(tem.get("username","none").asCString());
         qDebug()<<"Username "+username;
         w->whichuser = username;
         QTimer::singleShot(1000, w, SLOT(on_profileButton_clicked()));

}

void LeaderBoard::on_tableWidget_cellClicked(int row, int column)
{
    tableWidget->selectRow(row);

}

void LeaderBoard::on_pushButton_clicked()
{
    if(RD->isVisible()) RD->hide();


    else{
        RD->show();
        QPropertyAnimation *animation = new QPropertyAnimation(RD, "geometry");
        animation->setDuration(600);
       QPoint rpos = w->pos();
        RD->move(rpos.x()+480,rpos.y()+160);
        //Now animation to provide a fly-by effect
        QRect startRect(rpos.x()+480,rpos.y()+160,144,0);
        QRect endRect(rpos.x()+480,rpos.y()+160,155,160);
        animation->setStartValue(startRect);
        animation->setEndValue(endRect);
        animation->setEasingCurve(QEasingCurve::InOutSine);
        animation->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void LeaderBoard::moveRD(){
    QPoint rpos = w->pos();
    RD->move(rpos.x()+480,rpos.y()+160);
}
