/********************************************************************************
** Form generated from reading UI file 'arenascreen.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARENASCREEN_H
#define UI_ARENASCREEN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <gamelabel.h>

QT_BEGIN_NAMESPACE

class Ui_ArenaScreen
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    GameLabel *label;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *GameButton;
    QToolButton *Leaderboard;
    QToolButton *toolButton;
    QFrame *ArenaScreenSlot;
    QHBoxLayout *horizontalLayout_3;
    QFrame *frame_4;
    QFrame *frame_5;
    QFrame *frame_6;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QLabel *onlineplayercount;
    QPushButton *createArenaProfile;

    void setupUi(QWidget *ArenaScreen)
    {
        if (ArenaScreen->objectName().isEmpty())
            ArenaScreen->setObjectName(QString::fromUtf8("ArenaScreen"));
        ArenaScreen->resize(678, 441);
        ArenaScreen->setStyleSheet(QString::fromUtf8("#ArenaScreen{\n"
"background-color: rgba(255, 255, 255, 0);\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-image: url(:/new/prefix1/resources/lockinBtns2.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"\n"
"}\n"
"\n"
"\n"
"QToolButton{\n"
"height: 28px;\n"
"width:108px;\n"
"color: #000;\n"
"	border-image: url(:/new/prefix1/resources/lockinBtns2.png);\n"
"	font: 75 10pt \"Candara\";\n"
"\n"
"}\n"
"QToolButton:hover{\n"
"	color: #fff;\n"
"\n"
"}\n"
"\n"
"QTableView{\n"
"background-color:rgba(40, 40, 42,43);\n"
"\n"
"gridline-color: white;\n"
"}\n"
"QHeaderView::section\n"
"{\n"
"background-color:rgb(40, 40, 42);\n"
"color: white;\n"
"border: 1px solid #fff;\n"
"text-align: right;\n"
"font-family: candara;\n"
"font-size:13px;\n"
"}\n"
"\n"
"QTableView::item {\n"
" \n"
"font: 11pt \"Candara\";\n"
"color: #fff;\n"
"}\n"
"\n"
"\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(ArenaScreen);
        verticalLayout->setSpacing(15);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 0, 0, -1);
        frame = new QFrame(ArenaScreen);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setMinimumSize(QSize(0, 32));
        frame->setMouseTracking(true);
        frame->setStyleSheet(QString::fromUtf8("#frame{\n"
"border:none;\n"
"background-color: rgba(70, 70, 70,0);\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new GameLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(123, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        frame_3 = new QFrame(frame);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(6);
        sizePolicy1.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy1);
        frame_3->setMinimumSize(QSize(496, 0));
        frame_3->setMaximumSize(QSize(300, 16777215));
        frame_3->setStyleSheet(QString::fromUtf8(""));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_3);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(94, 0, 94, 19);
        GameButton = new QToolButton(frame_3);
        GameButton->setObjectName(QString::fromUtf8("GameButton"));
        GameButton->setMinimumSize(QSize(64, 0));

        horizontalLayout_2->addWidget(GameButton);

        Leaderboard = new QToolButton(frame_3);
        Leaderboard->setObjectName(QString::fromUtf8("Leaderboard"));

        horizontalLayout_2->addWidget(Leaderboard);

        toolButton = new QToolButton(frame_3);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setMinimumSize(QSize(54, 0));

        horizontalLayout_2->addWidget(toolButton);


        horizontalLayout->addWidget(frame_3);


        verticalLayout->addWidget(frame);

        ArenaScreenSlot = new QFrame(ArenaScreen);
        ArenaScreenSlot->setObjectName(QString::fromUtf8("ArenaScreenSlot"));
        ArenaScreenSlot->setStyleSheet(QString::fromUtf8("\n"
"#frame_6{\n"
"background:none;\n"
"border:none;\n"
"}\n"
""));
        ArenaScreenSlot->setFrameShape(QFrame::StyledPanel);
        ArenaScreenSlot->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(ArenaScreenSlot);
        horizontalLayout_3->setSpacing(14);
        horizontalLayout_3->setContentsMargins(2, 2, 2, 2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        frame_4 = new QFrame(ArenaScreenSlot);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setStyleSheet(QString::fromUtf8("#frame_4{\n"
"border:none;\n"
"background-color: rgb(40, 40, 40);\n"
"}\n"
"\n"
"QWidget{\n"
"	background-color: rgba(66, 66, 66, 125);\n"
"border: 2px solid grey;\n"
"}\n"
"\n"
"QLabel{\n"
"border:none;\n"
"	font: 11pt \"Candara\";\n"
"color:#fff;\n"
"}"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);

        horizontalLayout_3->addWidget(frame_4);

        frame_5 = new QFrame(ArenaScreenSlot);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame_5->sizePolicy().hasHeightForWidth());
        frame_5->setSizePolicy(sizePolicy2);
        frame_5->setMinimumSize(QSize(200, 300));
        frame_5->setStyleSheet(QString::fromUtf8("background:none;"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        frame_6 = new QFrame(frame_5);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setGeometry(QRect(2, 2, 178, 48));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(frame_6->sizePolicy().hasHeightForWidth());
        frame_6->setSizePolicy(sizePolicy3);
        frame_6->setMinimumSize(QSize(178, 0));
        frame_6->setMaximumSize(QSize(187, 68));
        frame_6->setStyleSheet(QString::fromUtf8("#frame_6{\n"
"border:2px solid grey;\n"
"background-color: rgb(40, 40,40);\n"
"}"));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_6);
        horizontalLayout_4->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(frame_6);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_4->addWidget(label_2);

        onlineplayercount = new QLabel(frame_6);
        onlineplayercount->setObjectName(QString::fromUtf8("onlineplayercount"));
        onlineplayercount->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));

        horizontalLayout_4->addWidget(onlineplayercount);

        createArenaProfile = new QPushButton(frame_5);
        createArenaProfile->setObjectName(QString::fromUtf8("createArenaProfile"));
        createArenaProfile->setGeometry(QRect(0, 60, 196, 32));
        createArenaProfile->setMaximumSize(QSize(16777215, 32));

        horizontalLayout_3->addWidget(frame_5);


        verticalLayout->addWidget(ArenaScreenSlot);


        retranslateUi(ArenaScreen);

        QMetaObject::connectSlotsByName(ArenaScreen);
    } // setupUi

    void retranslateUi(QWidget *ArenaScreen)
    {
        ArenaScreen->setWindowTitle(QApplication::translate("ArenaScreen", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        GameButton->setText(QApplication::translate("ArenaScreen", "Games", 0, QApplication::UnicodeUTF8));
        Leaderboard->setText(QApplication::translate("ArenaScreen", "Leaderboard", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("ArenaScreen", "Staff", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ArenaScreen", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/new/prefix1/resources/status-green.png\" width=\"12\" height=\"12\" /><span style=\" font-size:8pt; color:#ffffff;\"> Player Online : </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/new/prefix1/resources/white-dot.png\" width=\"12\" height=\"12\" /><span style=\" font-size:8pt;\"> </span><span style=\" font-size:8pt; color:#ffffff;\">Active Player :</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        onlineplayercount->setText(QString());
        createArenaProfile->setText(QApplication::translate("ArenaScreen", "Create Arena Profile", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ArenaScreen: public Ui_ArenaScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARENASCREEN_H
