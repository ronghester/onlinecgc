/********************************************************************************
** Form generated from reading UI file 'arenaprofilecreate.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARENAPROFILECREATE_H
#define UI_ARENAPROFILECREATE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_ArenaProfileCreate
{
public:
    QLabel *label_3;
    QPushButton *pushButton;
    QFrame *frame;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QToolButton *toolButton;

    void setupUi(QDialog *ArenaProfileCreate)
    {
        if (ArenaProfileCreate->objectName().isEmpty())
            ArenaProfileCreate->setObjectName(QString::fromUtf8("ArenaProfileCreate"));
        ArenaProfileCreate->resize(295, 163);
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(12);
        ArenaProfileCreate->setFont(font);
        ArenaProfileCreate->setStyleSheet(QString::fromUtf8("QDialog{\n"
"\n"
"border-image: url(:/new/prefix1/resources/loginbox.png);\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
"\n"
"#toolButton{\n"
"height:28px;\n"
"width:38px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}\n"
"\n"
"#toolButton_2{\n"
"height:28px;\n"
"width:32px;\n"
"	border-image: url(:/new/prefix1/resources/minimize.png);\n"
"	\n"
"}\n"
"#toolButton_2:hover{\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"#toolButton_3{\n"
"height:28px;\n"
"width:32px;\n"
"border-image: url(:/new/prefix1/resources/maximize.png);\n"
"}\n"
"#toolButton_3:hover{\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-image: url(:/new/p"
                        "refix1/resources/menu.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"	\n"
"\n"
"\n"
"}"));
        label_3 = new QLabel(ArenaProfileCreate);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 80, 71, 19));
        pushButton = new QPushButton(ArenaProfileCreate);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(80, 120, 146, 31));
        frame = new QFrame(ArenaProfileCreate);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(9, 9, 277, 51));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 277, 22));
        label->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(0, 20, 277, 32));
        label_2->setAlignment(Qt::AlignCenter);
        lineEdit = new QLineEdit(ArenaProfileCreate);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(120, 80, 161, 31));
        toolButton = new QToolButton(ArenaProfileCreate);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(270, 0, 25, 19));

        retranslateUi(ArenaProfileCreate);

        QMetaObject::connectSlotsByName(ArenaProfileCreate);
    } // setupUi

    void retranslateUi(QDialog *ArenaProfileCreate)
    {
        ArenaProfileCreate->setWindowTitle(QApplication::translate("ArenaProfileCreate", "Dialog", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ArenaProfileCreate", "Username", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("ArenaProfileCreate", "CreateArenaProfile", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ArenaProfileCreate", "Create Arena Profile", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ArenaProfileCreate", "Please enter your in-game name", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("ArenaProfileCreate", "X", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ArenaProfileCreate: public Ui_ArenaProfileCreate {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARENAPROFILECREATE_H
