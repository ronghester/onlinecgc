#include "alertdialog.h"
#include "ui_alertdialog.h"

AlertDialog::AlertDialog(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::AlertDialog)
{
    ui->setupUi(this);
}

AlertDialog::~AlertDialog()
{
    delete ui;
}



void AlertDialog::on_buttonBox_rejected()
{
 this->hide();
}

void AlertDialog::on_buttonBox_accepted()
{
    emit okClicked();
}

void AlertDialog::setText(QString text){
    ui->label->setText(text);
}
