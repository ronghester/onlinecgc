/********************************************************************************
** Form generated from reading UI file 'editprofile.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITPROFILE_H
#define UI_EDITPROFILE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditProfile
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QToolButton *toolButton;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QFormLayout *formLayout;
    QLabel *label_2;
    QPlainTextEdit *plainTextEdit;
    QLabel *label_3;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *userimg;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QLabel *label_4;
    QFrame *frame_5;
    QVBoxLayout *verticalLayout_3;
    QPushButton *reloadchip;

    void setupUi(QDialog *EditProfile)
    {
        if (EditProfile->objectName().isEmpty())
            EditProfile->setObjectName(QString::fromUtf8("EditProfile"));
        EditProfile->resize(537, 346);
        EditProfile->setStyleSheet(QString::fromUtf8("\n"
"#EditProfile{\n"
"border:1px solid grey;\n"
"background-color: rgb(38, 38, 38);\n"
"}\n"
"\n"
"\n"
"QToolButton{\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"}\n"
"QPushButton{\n"
"height: 38px;\n"
"width:100px;\n"
"\n"
"	border-image: url(:/new/prefix1/resources/lockinBtns2.png);\n"
"	font: 10pt \"Candara\";\n"
"}\n"
"QPushButton:hover{\n"
"	\n"
"	color:#fff;\n"
"\n"
"}\n"
"QLabel{\n"
"	font: 11pt \"Candara\";\n"
"color:#fff;\n"
"}\n"
""));
        verticalLayout_2 = new QVBoxLayout(EditProfile);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame_2 = new QFrame(EditProfile);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setMinimumSize(QSize(35, 0));
        frame_2->setMaximumSize(QSize(16777215, 34));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_2);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label = new QLabel(frame_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(208, 18, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        toolButton = new QToolButton(frame_2);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));

        horizontalLayout->addWidget(toolButton);


        verticalLayout_2->addWidget(frame_2);

        frame_4 = new QFrame(EditProfile);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_4);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(frame_4);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        formLayout = new QFormLayout(frame);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(-1, -1, -1, 0);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        plainTextEdit = new QPlainTextEdit(frame);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(plainTextEdit->sizePolicy().hasHeightForWidth());
        plainTextEdit->setSizePolicy(sizePolicy1);
        plainTextEdit->setMinimumSize(QSize(1, 0));
        plainTextEdit->setMaximumSize(QSize(16777215, 64));
        plainTextEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(0, QFormLayout::FieldRole, plainTextEdit);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        frame_3 = new QFrame(frame);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy2);
        frame_3->setMinimumSize(QSize(124, 124));
        frame_3->setMaximumSize(QSize(64, 124));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border-image: url(:/new/prefix1/resources/profile-frame-small.png);\n"
"}\n"
""));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_3);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        userimg = new QLabel(frame_3);
        userimg->setObjectName(QString::fromUtf8("userimg"));

        horizontalLayout_2->addWidget(userimg);


        formLayout->setWidget(1, QFormLayout::FieldRole, frame_3);

        pushButton_2 = new QPushButton(frame);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, pushButton_2);

        pushButton = new QPushButton(frame);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy3);

        formLayout->setWidget(2, QFormLayout::FieldRole, pushButton);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(3, QFormLayout::SpanningRole, label_4);


        verticalLayout->addWidget(frame);


        verticalLayout_2->addWidget(frame_4);

        frame_5 = new QFrame(EditProfile);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        sizePolicy.setHeightForWidth(frame_5->sizePolicy().hasHeightForWidth());
        frame_5->setSizePolicy(sizePolicy);
        frame_5->setMinimumSize(QSize(0, 29));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_5);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        reloadchip = new QPushButton(frame_5);
        reloadchip->setObjectName(QString::fromUtf8("reloadchip"));
        sizePolicy3.setHeightForWidth(reloadchip->sizePolicy().hasHeightForWidth());
        reloadchip->setSizePolicy(sizePolicy3);
        reloadchip->setMinimumSize(QSize(20, 27));

        verticalLayout_3->addWidget(reloadchip);


        verticalLayout_2->addWidget(frame_5);


        retranslateUi(EditProfile);

        QMetaObject::connectSlotsByName(EditProfile);
    } // setupUi

    void retranslateUi(QDialog *EditProfile)
    {
        EditProfile->setWindowTitle(QApplication::translate("EditProfile", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("EditProfile", "Profile Edit", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("EditProfile", "X", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("EditProfile", "Status Line", 0, QApplication::UnicodeUTF8));
        plainTextEdit->setPlainText(QString());
        label_3->setText(QApplication::translate("EditProfile", "Avatar", 0, QApplication::UnicodeUTF8));
        userimg->setText(QString());
        pushButton_2->setText(QApplication::translate("EditProfile", "Find Image", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("EditProfile", "Update", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        reloadchip->setText(QApplication::translate("EditProfile", "Reload Chips", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EditProfile: public Ui_EditProfile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITPROFILE_H
