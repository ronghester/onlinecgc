/********************************************************************************
** Form generated from reading UI file 'lockin.ui'
**
** Created: Thu 23. Apr 01:51:08 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOCKIN_H
#define UI_LOCKIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LockIn
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *homebtn;
    QToolButton *toolButton;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_4;
    QFrame *frame_6;
    QVBoxLayout *verticalLayout_7;
    QListWidget *friendlylist;
    QLabel *totalchips_a;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_3;
    QLabel *onlineplayers_lb;
    QLabel *warning_lbl;
    QFrame *frame_13;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_3;
    QLabel *lobbydetail_lb;
    QFrame *frame_11;
    QFormLayout *formLayout;
    QLabel *username_lb;
    QSlider *horizontalSlider;
    QPushButton *placebet_button;
    QLineEdit *lineEdit;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_8;
    QListWidget *enemylist;
    QLabel *totalchips_b;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_5;
    QFrame *frame_4;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_5;
    QVBoxLayout *verticalLayout_5;
    QLabel *label;
    QListWidget *chatbox;
    QFrame *frame_12;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *usermsg;
    QToolButton *sendtochat;
    QFrame *frame_9;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_2;
    QListWidget *allplayers;
    QFrame *frame_10;
    QVBoxLayout *verticalLayout_2;
    QPushButton *sitdown_btn;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *teamAwin;
    QPushButton *teamBwin;

    void setupUi(QWidget *LockIn)
    {
        if (LockIn->objectName().isEmpty())
            LockIn->setObjectName(QString::fromUtf8("LockIn"));
        LockIn->resize(800, 514);
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(9);
        LockIn->setFont(font);
        LockIn->setStyleSheet(QString::fromUtf8("#LockIn{\n"
"\n"
"background-color: rgb(38, 38, 38);\n"
"}\n"
"QWidget{\n"
"background-color: rgb(38, 38, 38);\n"
"}\n"
"\n"
"QToolButton{\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"}\n"
"QPushButton{\n"
"height: 38px;\n"
"width:100px;\n"
"\n"
"	border-image: url(:/new/prefix1/resources/lockinBtns2.png);\n"
"	font: 10pt \"Candara\";\n"
"}\n"
"QPushButton:hover{\n"
"	\n"
"	color:#fff;\n"
"	\n"
"\n"
"\n"
"}\n"
"#frame{\n"
"background-color: rgb(38, 38, 38);\n"
"border:none;\n"
"\n"
"}\n"
"QStatusbar{\n"
"	background-color: rgb(0, 0, 0);\n"
"}\n"
"#frame_2{\n"
"	\n"
"border:0px;\n"
"\n"
"\n"
"}\n"
"#frame_3{\n"
"\n"
"\n"
"border: 1px solid rgba(224, 224,224, 184);\n"
" \n"
"}\n"
"\n"
"\n"
"QSlider::handle {\n"
"border-image: url(:/new/prefix1/resources/slidertip.png);\n"
"}\n"
""));
        verticalLayout = new QVBoxLayout(LockIn);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_2 = new QFrame(LockIn);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setMinimumSize(QSize(0, 46));
        frame_2->setStyleSheet(QString::fromUtf8("\n"
"#frame_2{\n"
"background-color: rgb(38, 38, 38);\n"
"border:none;\n"
"}\n"
""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_2);
        horizontalLayout_3->setSpacing(2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(105, 0, 9, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy1);
        label_7->setMinimumSize(QSize(108, 44));
        label_7->setMaximumSize(QSize(40, 16777215));
        label_7->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/logo1.png);\n"
"background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_3->addWidget(label_7);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        homebtn = new QToolButton(frame_2);
        homebtn->setObjectName(QString::fromUtf8("homebtn"));
        homebtn->setStyleSheet(QString::fromUtf8("#homebtn{\n"
"height:16px;\n"
"width:24px;\n"
"	border-image: url(:/new/prefix1/resources/maximize.png);\n"
"	\n"
"}\n"
"#homebtn:hover{\n"
"	border-image: url(:/new/prefix1/resources/Maximise Button.png);\n"
"\n"
"}"));

        horizontalLayout_3->addWidget(homebtn);

        toolButton = new QToolButton(frame_2);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setStyleSheet(QString::fromUtf8("#toolButton{\n"
"height:16px;\n"
"width:24px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}"));

        horizontalLayout_3->addWidget(toolButton);


        verticalLayout->addWidget(frame_2);

        frame = new QFrame(LockIn);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy2);
        frame->setStyleSheet(QString::fromUtf8("\n"
"\n"
"#frame_6{\n"
"background-color: rgb(53, 53, 54);\n"
"border: 1px solid rgba(224, 224,224, 184);\n"
"}\n"
"\n"
"#frame_7{\n"
"background-color: rgb(53, 53, 54);\n"
"border: 1px solid rgba(224, 224,224, 184);\n"
"}\n"
"\n"
"#frame_8{\n"
"background-color: rgb(53, 53, 54);\n"
"border: 1px solid rgba(224, 224,224, 184);\n"
"}\n"
"\n"
"QListWidget:item {\n"
"min-height:64px;\n"
"	border-image: url(:/new/prefix1/resources/Userbets.png);\n"
"}\n"
"\n"
""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        frame_6 = new QFrame(frame);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setStyleSheet(QString::fromUtf8(""));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(frame_6);
        verticalLayout_7->setSpacing(10);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        friendlylist = new QListWidget(frame_6);
        friendlylist->setObjectName(QString::fromUtf8("friendlylist"));
        sizePolicy.setHeightForWidth(friendlylist->sizePolicy().hasHeightForWidth());
        friendlylist->setSizePolicy(sizePolicy);
        friendlylist->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);\n"
"border:none;"));
        friendlylist->setAlternatingRowColors(false);
        friendlylist->setIconSize(QSize(38, 38));
        friendlylist->setSpacing(1);
        friendlylist->setViewMode(QListView::ListMode);

        verticalLayout_7->addWidget(friendlylist);

        totalchips_a = new QLabel(frame_6);
        totalchips_a->setObjectName(QString::fromUtf8("totalchips_a"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(totalchips_a->sizePolicy().hasHeightForWidth());
        totalchips_a->setSizePolicy(sizePolicy3);
        totalchips_a->setMaximumSize(QSize(16777215, 39));
        totalchips_a->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/Userbets.png);\n"
"font: 11pt \"Candara\";\n"
"padding-left:12px;\n"
"margin-left:94px;\n"
""));

        verticalLayout_7->addWidget(totalchips_a);


        horizontalLayout_4->addWidget(frame_6);

        frame_7 = new QFrame(frame);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setStyleSheet(QString::fromUtf8("QWidget{\n"
"background-color: rgb(53, 53, 54);\n"
"}\n"
"\n"
"#frame_11{\n"
"border:none;\n"
"background-color: rgb(53, 53, 54);\n"
"}\n"
"\n"
"\n"
"#QLabel{\n"
"\n"
"color:#fff;\n"
"	font: 11pt \"Rockwell\";\n"
"}\n"
"\n"
"#lobbydetail_lb{\n"
"border-top: 1px solid rgba(224, 224,224, 184);\n"
"}"));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_7);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(6, 3, 6, 3);
        onlineplayers_lb = new QLabel(frame_7);
        onlineplayers_lb->setObjectName(QString::fromUtf8("onlineplayers_lb"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Candara"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        onlineplayers_lb->setFont(font1);
        onlineplayers_lb->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"font: 12pt \"Candara\";"));

        verticalLayout_3->addWidget(onlineplayers_lb);

        warning_lbl = new QLabel(frame_7);
        warning_lbl->setObjectName(QString::fromUtf8("warning_lbl"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Candara"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        warning_lbl->setFont(font2);
        warning_lbl->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        warning_lbl->setScaledContents(true);
        warning_lbl->setAlignment(Qt::AlignCenter);
        warning_lbl->setWordWrap(true);

        verticalLayout_3->addWidget(warning_lbl);

        frame_13 = new QFrame(frame_7);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(frame_13->sizePolicy().hasHeightForWidth());
        frame_13->setSizePolicy(sizePolicy4);
        frame_13->setFrameShape(QFrame::StyledPanel);
        frame_13->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_13);
        verticalLayout_6->setSpacing(1);
        verticalLayout_6->setContentsMargins(2, 2, 2, 2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setSizeConstraint(QLayout::SetMinimumSize);
        label_3 = new QLabel(frame_13);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Candara"));
        font3.setPointSize(11);
        label_3->setFont(font3);
        label_3->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_3);

        lobbydetail_lb = new QLabel(frame_13);
        lobbydetail_lb->setObjectName(QString::fromUtf8("lobbydetail_lb"));
        lobbydetail_lb->setFont(font3);
        lobbydetail_lb->setStyleSheet(QString::fromUtf8("color:#fff\n"
""));
        lobbydetail_lb->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lobbydetail_lb->setWordWrap(true);

        verticalLayout_6->addWidget(lobbydetail_lb);


        verticalLayout_3->addWidget(frame_13);

        frame_11 = new QFrame(frame_7);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        sizePolicy.setHeightForWidth(frame_11->sizePolicy().hasHeightForWidth());
        frame_11->setSizePolicy(sizePolicy);
        frame_11->setMinimumSize(QSize(0, 0));
        frame_11->setMaximumSize(QSize(16777215, 92));
        frame_11->setStyleSheet(QString::fromUtf8(""));
        frame_11->setFrameShape(QFrame::StyledPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        formLayout = new QFormLayout(frame_11);
#ifndef Q_OS_MAC
        formLayout->setContentsMargins(9, 9, 9, 9);
#endif
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setHorizontalSpacing(26);
        formLayout->setVerticalSpacing(8);
        username_lb = new QLabel(frame_11);
        username_lb->setObjectName(QString::fromUtf8("username_lb"));
        username_lb->setMaximumSize(QSize(90, 16777215));
        username_lb->setStyleSheet(QString::fromUtf8("font: 11pt \"Candara\";\n"
"color:#fff;\n"
"background-color:rgba(0,0,0,0)"));

        formLayout->setWidget(1, QFormLayout::LabelRole, username_lb);

        horizontalSlider = new QSlider(frame_11);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(horizontalSlider->sizePolicy().hasHeightForWidth());
        horizontalSlider->setSizePolicy(sizePolicy5);
        horizontalSlider->setMinimumSize(QSize(120, 0));
        horizontalSlider->setMaximumSize(QSize(119, 34));
        horizontalSlider->setStyleSheet(QString::fromUtf8(""));
        horizontalSlider->setSingleStep(10);
        horizontalSlider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(2, QFormLayout::LabelRole, horizontalSlider);

        placebet_button = new QPushButton(frame_11);
        placebet_button->setObjectName(QString::fromUtf8("placebet_button"));
        placebet_button->setMaximumSize(QSize(94, 16777215));
        placebet_button->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/betbutton.png);\n"
""));
        placebet_button->setIconSize(QSize(18, 16));

        formLayout->setWidget(2, QFormLayout::FieldRole, placebet_button);

        lineEdit = new QLineEdit(frame_11);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color:#000;"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit);


        verticalLayout_3->addWidget(frame_11);


        horizontalLayout_4->addWidget(frame_7);

        frame_8 = new QFrame(frame);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setStyleSheet(QString::fromUtf8(""));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(frame_8);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        enemylist = new QListWidget(frame_8);
        enemylist->setObjectName(QString::fromUtf8("enemylist"));
        sizePolicy.setHeightForWidth(enemylist->sizePolicy().hasHeightForWidth());
        enemylist->setSizePolicy(sizePolicy);
        enemylist->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);\n"
"border:none;"));
        enemylist->setAlternatingRowColors(false);
        enemylist->setIconSize(QSize(38, 38));
        enemylist->setSpacing(4);

        verticalLayout_8->addWidget(enemylist);

        totalchips_b = new QLabel(frame_8);
        totalchips_b->setObjectName(QString::fromUtf8("totalchips_b"));
        sizePolicy3.setHeightForWidth(totalchips_b->sizePolicy().hasHeightForWidth());
        totalchips_b->setSizePolicy(sizePolicy3);
        totalchips_b->setMaximumSize(QSize(16777215, 39));
        totalchips_b->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/Userbets.png);\n"
"font: 11pt \"Candara\";\n"
"padding-left:12px;\n"
"margin-left:94px;\n"
""));

        verticalLayout_8->addWidget(totalchips_b);


        horizontalLayout_4->addWidget(frame_8);


        verticalLayout->addWidget(frame);

        frame_3 = new QFrame(LockIn);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        sizePolicy3.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy3);
        frame_3->setStyleSheet(QString::fromUtf8("border:none;"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_5 = new QHBoxLayout(frame_3);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(2, 0, 0, 0);
        frame_4 = new QFrame(frame_3);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        QSizePolicy sizePolicy6(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy6);
        frame_4->setMinimumSize(QSize(0, 94));
        frame_4->setMaximumSize(QSize(16777215, 224));
        frame_4->setStyleSheet(QString::fromUtf8("font: 11pt \"Candara\";\n"
"color:#fff;"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_4);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame_5 = new QFrame(frame_4);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setMaximumSize(QSize(16777215, 16777215));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_5);
        verticalLayout_5->setSpacing(4);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label = new QLabel(frame_5);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_5->addWidget(label);

        chatbox = new QListWidget(frame_5);
        chatbox->setObjectName(QString::fromUtf8("chatbox"));
        chatbox->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color:#000;\n"
"font: 12pt \"Candara\";"));
        chatbox->setAutoScroll(true);
        chatbox->setAutoScrollMargin(-1);
        chatbox->setAlternatingRowColors(false);
        chatbox->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        chatbox->setMovement(QListView::Static);
        chatbox->setSpacing(0);
        chatbox->setModelColumn(0);
        chatbox->setWordWrap(true);

        verticalLayout_5->addWidget(chatbox);

        frame_12 = new QFrame(frame_5);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        frame_12->setMinimumSize(QSize(0, 54));
        frame_12->setMaximumSize(QSize(16777215, 56));
        frame_12->setFrameShape(QFrame::StyledPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_12);
        horizontalLayout_2->setSpacing(8);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        usermsg = new QLineEdit(frame_12);
        usermsg->setObjectName(QString::fromUtf8("usermsg"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(usermsg->sizePolicy().hasHeightForWidth());
        usermsg->setSizePolicy(sizePolicy7);
        usermsg->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color:#000;"));
        usermsg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout_2->addWidget(usermsg);

        sendtochat = new QToolButton(frame_12);
        sendtochat->setObjectName(QString::fromUtf8("sendtochat"));
        sendtochat->setMinimumSize(QSize(24, 51));
        sendtochat->setStyleSheet(QString::fromUtf8("border-image:none;\n"
"background-image:none;"));

        horizontalLayout_2->addWidget(sendtochat);


        verticalLayout_5->addWidget(frame_12);


        horizontalLayout->addWidget(frame_5);

        frame_9 = new QFrame(frame_4);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        frame_9->setMaximumSize(QSize(254, 16777215));
        frame_9->setFrameShape(QFrame::StyledPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_9);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_2 = new QLabel(frame_9);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_4->addWidget(label_2);

        allplayers = new QListWidget(frame_9);
        allplayers->setObjectName(QString::fromUtf8("allplayers"));
        allplayers->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color:#000"));
        allplayers->setIconSize(QSize(4, 4));

        verticalLayout_4->addWidget(allplayers);


        horizontalLayout->addWidget(frame_9);


        horizontalLayout_5->addWidget(frame_4);

        frame_10 = new QFrame(frame_3);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        sizePolicy5.setHeightForWidth(frame_10->sizePolicy().hasHeightForWidth());
        frame_10->setSizePolicy(sizePolicy5);
        frame_10->setMinimumSize(QSize(164, 0));
        frame_10->setMaximumSize(QSize(169, 16777215));
        frame_10->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        frame_10->setFrameShape(QFrame::StyledPanel);
        frame_10->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_10);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        sitdown_btn = new QPushButton(frame_10);
        sitdown_btn->setObjectName(QString::fromUtf8("sitdown_btn"));

        verticalLayout_2->addWidget(sitdown_btn);

        pushButton_2 = new QPushButton(frame_10);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(frame_10);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_2->addWidget(pushButton_3);

        teamAwin = new QPushButton(frame_10);
        teamAwin->setObjectName(QString::fromUtf8("teamAwin"));

        verticalLayout_2->addWidget(teamAwin);

        teamBwin = new QPushButton(frame_10);
        teamBwin->setObjectName(QString::fromUtf8("teamBwin"));

        verticalLayout_2->addWidget(teamBwin);


        horizontalLayout_5->addWidget(frame_10);


        verticalLayout->addWidget(frame_3);


        retranslateUi(LockIn);

        chatbox->setCurrentRow(-1);


        QMetaObject::connectSlotsByName(LockIn);
    } // setupUi

    void retranslateUi(QWidget *LockIn)
    {
        LockIn->setWindowTitle(QApplication::translate("LockIn", "Form", 0, QApplication::UnicodeUTF8));
        homebtn->setText(QApplication::translate("LockIn", "...", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("LockIn", "X", 0, QApplication::UnicodeUTF8));
        totalchips_a->setText(QApplication::translate("LockIn", "Team A Chips:", 0, QApplication::UnicodeUTF8));
        onlineplayers_lb->setText(QApplication::translate("LockIn", "Player Online:", 0, QApplication::UnicodeUTF8));
        warning_lbl->setText(QString());
        label_3->setText(QApplication::translate("LockIn", "Details", 0, QApplication::UnicodeUTF8));
        lobbydetail_lb->setText(QString());
        username_lb->setText(QString());
        placebet_button->setText(QApplication::translate("LockIn", "PlaceBet", 0, QApplication::UnicodeUTF8));
        totalchips_b->setText(QApplication::translate("LockIn", "Team B Chips:", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("LockIn", "Chat", 0, QApplication::UnicodeUTF8));
        sendtochat->setText(QApplication::translate("LockIn", "Send", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("LockIn", "Player List", 0, QApplication::UnicodeUTF8));
        sitdown_btn->setText(QApplication::translate("LockIn", "Sit Down", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("LockIn", "Refresh", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("LockIn", "Call Refree", 0, QApplication::UnicodeUTF8));
        teamAwin->setText(QApplication::translate("LockIn", "TeamA-Winner", 0, QApplication::UnicodeUTF8));
        teamBwin->setText(QApplication::translate("LockIn", "TeamB_Winner", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LockIn: public Ui_LockIn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOCKIN_H
