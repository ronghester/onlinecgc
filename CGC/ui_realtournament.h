/********************************************************************************
** Form generated from reading UI file 'realtournament.ui'
**
** Created: Sat 4. Apr 00:08:42 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REALTOURNAMENT_H
#define UI_REALTOURNAMENT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_realTournament
{
public:
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QLabel *t_logo;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *tname;
    QLabel *tdesc;
    QSpacerItem *verticalSpacer_2;
    QLabel *starttime;
    QLabel *tfee;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_5;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton;

    void setupUi(QWidget *realTournament)
    {
        if (realTournament->objectName().isEmpty())
            realTournament->setObjectName(QString::fromUtf8("realTournament"));
        realTournament->resize(510, 126);
        realTournament->setStyleSheet(QString::fromUtf8("#realTournament{\n"
"background-color:  rgb(38, 38, 38);\n"
"\n"
"}\n"
"QLabel{\n"
"	font: 10pt \"Candara\";\n"
"color:#fff;\n"
"}\n"
"\n"
""));
        horizontalLayout = new QHBoxLayout(realTournament);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, 6, 6, 12);
        frame = new QFrame(realTournament);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("#frame{\n"
"border-left:1px solid #fff;\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"border-right:none;\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        t_logo = new QLabel(frame);
        t_logo->setObjectName(QString::fromUtf8("t_logo"));

        verticalLayout->addWidget(t_logo);


        horizontalLayout->addWidget(frame);

        frame_2 = new QFrame(realTournament);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setStyleSheet(QString::fromUtf8("#frame_2{\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"border-left:none;\n"
"}"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        tname = new QLabel(frame_2);
        tname->setObjectName(QString::fromUtf8("tname"));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(14);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        tname->setFont(font);
        tname->setStyleSheet(QString::fromUtf8("font: 75 14pt \"Candara\";"));

        verticalLayout_3->addWidget(tname);

        tdesc = new QLabel(frame_2);
        tdesc->setObjectName(QString::fromUtf8("tdesc"));

        verticalLayout_3->addWidget(tdesc);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        starttime = new QLabel(frame_2);
        starttime->setObjectName(QString::fromUtf8("starttime"));

        verticalLayout_3->addWidget(starttime);

        tfee = new QLabel(frame_2);
        tfee->setObjectName(QString::fromUtf8("tfee"));

        verticalLayout_3->addWidget(tfee);


        horizontalLayout->addWidget(frame_2);

        frame_3 = new QFrame(realTournament);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border-right:1px solid #fff;\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"}"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);
        label_5->setStyleSheet(QString::fromUtf8("font: 75 14pt \"Candara\";"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_2->addWidget(label_5);

        verticalSpacer = new QSpacerItem(20, 25, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        pushButton = new QPushButton(frame_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/placebet.png);\n"
"height:26px;"));

        verticalLayout_2->addWidget(pushButton);


        horizontalLayout->addWidget(frame_3);


        retranslateUi(realTournament);

        QMetaObject::connectSlotsByName(realTournament);
    } // setupUi

    void retranslateUi(QWidget *realTournament)
    {
        realTournament->setWindowTitle(QApplication::translate("realTournament", "Form", 0, QApplication::UnicodeUTF8));
        t_logo->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        tname->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        tdesc->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        starttime->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        tfee->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("realTournament", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("realTournament", "Join Lobby", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class realTournament: public Ui_realTournament {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REALTOURNAMENT_H
