/********************************************************************************
** Form generated from reading UI file 'profilepage.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFILEPAGE_H
#define UI_PROFILEPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Profilepage
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *profilemain;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_5;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_10;
    QFrame *frame_12;
    QFormLayout *formLayout;
    QLabel *userlabel;
    QLabel *label_2;
    QLabel *rank_amblam;
    QFrame *frame_10;
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame_11;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QLabel *label_10;
    QFrame *onlinenowsection;
    QVBoxLayout *verticalLayout_4;
    QLabel *label;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *addfriendbtn;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_8;
    QSplitter *splitter;
    QFrame *frame_6;
    QVBoxLayout *verticalLayout_5;
    QFrame *frame_13;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_3;
    QTableWidget *tableWidget_2;
    QLabel *label_6;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_7;
    QTableWidget *tableWidget;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_7;
    QFrame *frame_9;
    QVBoxLayout *verticalLayout_12;
    QListWidget *arenalist;

    void setupUi(QWidget *Profilepage)
    {
        if (Profilepage->objectName().isEmpty())
            Profilepage->setObjectName(QString::fromUtf8("Profilepage"));
        Profilepage->resize(808, 533);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Profilepage->sizePolicy().hasHeightForWidth());
        Profilepage->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(10);
        Profilepage->setFont(font);
        Profilepage->setStyleSheet(QString::fromUtf8("QWidget{\n"
"\n"
"background-color: rgb(40, 40, 42);\n"
"}\n"
"\n"
"\n"
"QFrame{\n"
"	\n"
"	border:none;\n"
"	background-color: rgb(40, 40, 42);\n"
"\n"
"}\n"
"\n"
"QLabel{\n"
"\n"
"\n"
"\n"
"color:#fff;\n"
"\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-image: url(:/new/prefix1/resources/profile_pg_btn.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"\n"
"color:#fff;\n"
"}\n"
"#profilemain{\n"
"border:1px solid #fff;\n"
" background-color:rgba(24, 24, 24,254);\n"
"}\n"
"QToolButton{\n"
"\n"
"}\n"
""));
        verticalLayout = new QVBoxLayout(Profilepage);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        profilemain = new QFrame(Profilepage);
        profilemain->setObjectName(QString::fromUtf8("profilemain"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(profilemain->sizePolicy().hasHeightForWidth());
        profilemain->setSizePolicy(sizePolicy1);
        profilemain->setMaximumSize(QSize(16777215, 119));
        profilemain->setStyleSheet(QString::fromUtf8(""));
        profilemain->setFrameShape(QFrame::StyledPanel);
        profilemain->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(profilemain);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(8, 8, 8, 1);
        frame_3 = new QFrame(profilemain);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy2);
        frame_3->setMinimumSize(QSize(124, 0));
        frame_3->setMaximumSize(QSize(16777215, 16777215));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border-image: url(:/new/prefix1/resources/profile-frame.png);\n"
"background-color:rgba(24, 24, 24,143);\n"
"}"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_3);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setStyleSheet(QString::fromUtf8("background-color:rgba(24, 24, 24,143);"));

        verticalLayout_2->addWidget(label_5);


        horizontalLayout->addWidget(frame_3);

        frame_4 = new QFrame(profilemain);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy1.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy1);
        frame_4->setMinimumSize(QSize(168, 0));
        frame_4->setMaximumSize(QSize(16777215, 16777215));
        frame_4->setStyleSheet(QString::fromUtf8(" background-color:rgba(24, 24, 24,143);"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout_10 = new QVBoxLayout(frame_4);
        verticalLayout_10->setSpacing(0);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(-1, 0, 2, 0);
        frame_12 = new QFrame(frame_4);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        frame_12->setFrameShape(QFrame::StyledPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        formLayout = new QFormLayout(frame_12);
        formLayout->setContentsMargins(0, 0, 0, 0);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setHorizontalSpacing(4);
        formLayout->setVerticalSpacing(4);
        userlabel = new QLabel(frame_12);
        userlabel->setObjectName(QString::fromUtf8("userlabel"));
        QFont font1;
        font1.setPointSize(12);
        userlabel->setFont(font1);
        userlabel->setStyleSheet(QString::fromUtf8(""));

        formLayout->setWidget(0, QFormLayout::LabelRole, userlabel);

        label_2 = new QLabel(frame_12);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Candara"));
        font2.setPointSize(12);
        label_2->setFont(font2);
        label_2->setScaledContents(true);
        label_2->setWordWrap(true);

        formLayout->setWidget(1, QFormLayout::SpanningRole, label_2);

        rank_amblam = new QLabel(frame_12);
        rank_amblam->setObjectName(QString::fromUtf8("rank_amblam"));

        formLayout->setWidget(0, QFormLayout::FieldRole, rank_amblam);


        verticalLayout_10->addWidget(frame_12);

        frame_10 = new QFrame(frame_4);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        sizePolicy2.setHeightForWidth(frame_10->sizePolicy().hasHeightForWidth());
        frame_10->setSizePolicy(sizePolicy2);
        frame_10->setMinimumSize(QSize(301, 0));
        frame_10->setStyleSheet(QString::fromUtf8(""));
        frame_10->setFrameShape(QFrame::StyledPanel);
        frame_10->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_10);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        frame_11 = new QFrame(frame_10);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(frame_11->sizePolicy().hasHeightForWidth());
        frame_11->setSizePolicy(sizePolicy3);
        frame_11->setMaximumSize(QSize(16777215, 120));
        frame_11->setFrameShape(QFrame::StyledPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        frame_11->setLineWidth(3);
        verticalLayout_3 = new QVBoxLayout(frame_11);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_8 = new QLabel(frame_11);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Candara"));
        font3.setPointSize(11);
        label_8->setFont(font3);

        verticalLayout_3->addWidget(label_8);

        label_10 = new QLabel(frame_11);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font3);

        verticalLayout_3->addWidget(label_10);


        horizontalLayout_2->addWidget(frame_11);


        verticalLayout_10->addWidget(frame_10);


        horizontalLayout->addWidget(frame_4);

        onlinenowsection = new QFrame(profilemain);
        onlinenowsection->setObjectName(QString::fromUtf8("onlinenowsection"));
        sizePolicy2.setHeightForWidth(onlinenowsection->sizePolicy().hasHeightForWidth());
        onlinenowsection->setSizePolicy(sizePolicy2);
        onlinenowsection->setMinimumSize(QSize(128, 0));
        onlinenowsection->setMaximumSize(QSize(164, 16777215));
        onlinenowsection->setLayoutDirection(Qt::RightToLeft);
        onlinenowsection->setStyleSheet(QString::fromUtf8("#onlinenowsection{\n"
"background-color:rgba(24, 24, 24,143);\n"
"\n"
"}\n"
"\n"
"\n"
""));
        onlinenowsection->setFrameShape(QFrame::StyledPanel);
        onlinenowsection->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(onlinenowsection);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(-1, 0, -1, 0);
        label = new QLabel(onlinenowsection);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        label->setStyleSheet(QString::fromUtf8("background-color:rgba(24, 24, 24,143);\n"
"font: 12pt \"MS Shell Dlg 2\";\n"
"Color:#fff;"));

        verticalLayout_4->addWidget(label);

        verticalSpacer = new QSpacerItem(24, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer);

        pushButton = new QPushButton(onlinenowsection);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy4);

        verticalLayout_4->addWidget(pushButton);

        pushButton_3 = new QPushButton(onlinenowsection);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_4->addWidget(pushButton_3);

        addfriendbtn = new QPushButton(onlinenowsection);
        addfriendbtn->setObjectName(QString::fromUtf8("addfriendbtn"));

        verticalLayout_4->addWidget(addfriendbtn);


        horizontalLayout->addWidget(onlinenowsection);

        frame_3->raise();
        onlinenowsection->raise();
        frame_4->raise();

        verticalLayout->addWidget(profilemain);

        frame_2 = new QFrame(Profilepage);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setStyleSheet(QString::fromUtf8(""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(frame_2);
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        splitter = new QSplitter(frame_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setFrameShape(QFrame::Panel);
        splitter->setLineWidth(0);
        splitter->setOrientation(Qt::Horizontal);
        splitter->setOpaqueResize(false);
        frame_6 = new QFrame(splitter);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        sizePolicy1.setHeightForWidth(frame_6->sizePolicy().hasHeightForWidth());
        frame_6->setSizePolicy(sizePolicy1);
        frame_6->setMaximumSize(QSize(16777215, 16777215));
        frame_6->setStyleSheet(QString::fromUtf8(""));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_6);
        verticalLayout_5->setSpacing(9);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(10, -1, 9, 15);
        frame_13 = new QFrame(frame_6);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(frame_13->sizePolicy().hasHeightForWidth());
        frame_13->setSizePolicy(sizePolicy5);
        frame_13->setMinimumSize(QSize(0, 0));
        frame_13->setMaximumSize(QSize(16777215, 16777215));
        frame_13->setStyleSheet(QString::fromUtf8(""));
        frame_13->setFrameShape(QFrame::StyledPanel);
        frame_13->setFrameShadow(QFrame::Raised);
        verticalLayout_11 = new QVBoxLayout(frame_13);
        verticalLayout_11->setSpacing(0);
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_3 = new QLabel(frame_13);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy3.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy3);
        label_3->setFont(font3);

        verticalLayout_11->addWidget(label_3);

        tableWidget_2 = new QTableWidget(frame_13);
        if (tableWidget_2->columnCount() < 4)
            tableWidget_2->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        if (tableWidget_2->rowCount() < 1)
            tableWidget_2->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(0, __qtablewidgetitem4);
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::NoBrush);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        __qtablewidgetitem5->setFont(font3);
        __qtablewidgetitem5->setForeground(brush);
        tableWidget_2->setItem(0, 0, __qtablewidgetitem5);
        QBrush brush1(QColor(0, 85, 0, 255));
        brush1.setStyle(Qt::NoBrush);
        QBrush brush2(QColor(0, 85, 0, 255));
        brush2.setStyle(Qt::NoBrush);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        __qtablewidgetitem6->setFont(font3);
        __qtablewidgetitem6->setBackground(brush2);
        __qtablewidgetitem6->setForeground(brush1);
        tableWidget_2->setItem(0, 1, __qtablewidgetitem6);
        QBrush brush3(QColor(170, 0, 0, 255));
        brush3.setStyle(Qt::NoBrush);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        __qtablewidgetitem7->setFont(font3);
        __qtablewidgetitem7->setForeground(brush3);
        tableWidget_2->setItem(0, 2, __qtablewidgetitem7);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));
        sizePolicy3.setHeightForWidth(tableWidget_2->sizePolicy().hasHeightForWidth());
        tableWidget_2->setSizePolicy(sizePolicy3);
        tableWidget_2->setMinimumSize(QSize(187, 0));
        tableWidget_2->setMaximumSize(QSize(404, 86));
        tableWidget_2->setLayoutDirection(Qt::LeftToRight);
        tableWidget_2->setStyleSheet(QString::fromUtf8("QTableView{\n"
"\n"
"\n"
"gridline-color: white;\n"
"}\n"
"QHeaderView::section\n"
"{\n"
" background-color:rgba(24, 24, 24,143);\n"
"color: white;\n"
"border: 1px solid #fff;\n"
"text-align: right;\n"
"font-family: candara;\n"
"font-size:13px;\n"
"}\n"
"\n"
"QTableView::item {\n"
" background-color:rgba(24, 24, 24,143);\n"
"font: 11pt \"Candara\";\n"
"\n"
"}\n"
""));
        tableWidget_2->setAutoScrollMargin(22);
        tableWidget_2->setCornerButtonEnabled(false);
        tableWidget_2->horizontalHeader()->setVisible(false);
        tableWidget_2->horizontalHeader()->setDefaultSectionSize(80);
        tableWidget_2->horizontalHeader()->setMinimumSectionSize(48);
        tableWidget_2->horizontalHeader()->setStretchLastSection(true);
        tableWidget_2->verticalHeader()->setVisible(false);
        tableWidget_2->verticalHeader()->setDefaultSectionSize(29);
        tableWidget_2->verticalHeader()->setMinimumSectionSize(18);

        verticalLayout_11->addWidget(tableWidget_2);


        verticalLayout_5->addWidget(frame_13);

        label_6 = new QLabel(frame_6);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(16777215, 22));
        label_6->setFont(font3);
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_6->setMargin(-2);

        verticalLayout_5->addWidget(label_6);

        frame_8 = new QFrame(frame_6);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        sizePolicy1.setHeightForWidth(frame_8->sizePolicy().hasHeightForWidth());
        frame_8->setSizePolicy(sizePolicy1);
        frame_8->setMinimumSize(QSize(0, 0));
        frame_8->setStyleSheet(QString::fromUtf8(""));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(frame_8);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        tableWidget = new QTableWidget(frame_8);
        if (tableWidget->columnCount() < 5)
            tableWidget->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem12);
        if (tableWidget->rowCount() < 1)
            tableWidget->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem13);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        sizePolicy1.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy1);
        tableWidget->setMinimumSize(QSize(187, 0));
        tableWidget->setMaximumSize(QSize(404, 16777215));
        tableWidget->setStyleSheet(QString::fromUtf8("QTableView{\n"
"\n"
"\n"
"gridline-color: white;\n"
"}\n"
"QHeaderView::section\n"
"{\n"
" background-color:rgba(24, 24, 24,143);\n"
"color: white;\n"
"border: 1px solid #fff;\n"
"text-align: right;\n"
"font-family: candara;\n"
"font-size:13px;\n"
"}\n"
"\n"
"QTableView::item {\n"
" background-color:rgba(24, 24, 24,143);\n"
"font: 11pt \"Candara\";\n"
"color: #fff;\n"
"}\n"
"\n"
""));
        tableWidget->horizontalHeader()->setVisible(false);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setDefaultSectionSize(77);
        tableWidget->horizontalHeader()->setMinimumSectionSize(20);
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setDefaultSectionSize(28);
        tableWidget->verticalHeader()->setStretchLastSection(true);

        verticalLayout_7->addWidget(tableWidget);


        verticalLayout_5->addWidget(frame_8);

        splitter->addWidget(frame_6);
        frame_7 = new QFrame(splitter);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(frame_7->sizePolicy().hasHeightForWidth());
        frame_7->setSizePolicy(sizePolicy6);
        frame_7->setMaximumSize(QSize(236, 16777215));
        frame_7->setStyleSheet(QString::fromUtf8(""));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_7);
        verticalLayout_6->setSpacing(8);
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_7 = new QLabel(frame_7);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QSizePolicy sizePolicy7(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy7);
        label_7->setMinimumSize(QSize(0, 21));
        label_7->setMaximumSize(QSize(16777215, 29));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Candara"));
        font4.setPointSize(11);
        font4.setBold(false);
        font4.setWeight(50);
        label_7->setFont(font4);
        label_7->setLayoutDirection(Qt::LeftToRight);
        label_7->setStyleSheet(QString::fromUtf8(""));
        label_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_7->setMargin(3);
        label_7->setIndent(-1);

        verticalLayout_6->addWidget(label_7);

        frame_9 = new QFrame(frame_7);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        sizePolicy1.setHeightForWidth(frame_9->sizePolicy().hasHeightForWidth());
        frame_9->setSizePolicy(sizePolicy1);
        frame_9->setMaximumSize(QSize(1677215, 16777215));
        frame_9->setStyleSheet(QString::fromUtf8(""));
        frame_9->setFrameShape(QFrame::StyledPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        verticalLayout_12 = new QVBoxLayout(frame_9);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(2, 4, 11, 15);
        arenalist = new QListWidget(frame_9);
        arenalist->setObjectName(QString::fromUtf8("arenalist"));
        sizePolicy1.setHeightForWidth(arenalist->sizePolicy().hasHeightForWidth());
        arenalist->setSizePolicy(sizePolicy1);
        arenalist->setStyleSheet(QString::fromUtf8("border: 1px solid #fff;\n"
"background-color:rgba(24, 24, 24,143);"));
        arenalist->setIconSize(QSize(44, 44));
        arenalist->setProperty("isWrapping", QVariant(false));
        arenalist->setResizeMode(QListView::Adjust);
        arenalist->setSpacing(8);
        arenalist->setGridSize(QSize(52, 60));
        arenalist->setViewMode(QListView::ListMode);
        arenalist->setModelColumn(0);
        arenalist->setUniformItemSizes(false);

        verticalLayout_12->addWidget(arenalist);


        verticalLayout_6->addWidget(frame_9);

        splitter->addWidget(frame_7);

        verticalLayout_8->addWidget(splitter);


        verticalLayout->addWidget(frame_2);


        retranslateUi(Profilepage);

        QMetaObject::connectSlotsByName(Profilepage);
    } // setupUi

    void retranslateUi(QWidget *Profilepage)
    {
        Profilepage->setWindowTitle(QApplication::translate("Profilepage", "Form", 0, QApplication::UnicodeUTF8));
        label_5->setText(QString());
        userlabel->setText(QApplication::translate("Profilepage", "Username of Games", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Profilepage", "Challege me for game at 8 PM GMT", 0, QApplication::UnicodeUTF8));
        rank_amblam->setText(QString());
        label_8->setText(QApplication::translate("Profilepage", "Rank :", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("Profilepage", "Chips Earnt : 345", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Profilepage", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p></body></html>", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Profilepage", "Message User", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("Profilepage", "Challenge User", 0, QApplication::UnicodeUTF8));
        addfriendbtn->setText(QApplication::translate("Profilepage", "Add Friend", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Profilepage", "Statistics", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("Profilepage", "No Of Games", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("Profilepage", "Wins", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("Profilepage", "Looses", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("Profilepage", "% Win", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_2->verticalHeaderItem(0);
        ___qtablewidgetitem4->setText(QApplication::translate("Profilepage", "1", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = tableWidget_2->isSortingEnabled();
        tableWidget_2->setSortingEnabled(false);
        tableWidget_2->setSortingEnabled(__sortingEnabled);

        label_6->setText(QApplication::translate("Profilepage", "History Of Games", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem5->setText(QApplication::translate("Profilepage", "Game Name", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem6->setText(QApplication::translate("Profilepage", "No Of Player", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem7->setText(QApplication::translate("Profilepage", "Description", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem8->setText(QApplication::translate("Profilepage", "W/L", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem9->setText(QApplication::translate("Profilepage", "Chips", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem10->setText(QApplication::translate("Profilepage", "New Row", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("Profilepage", "Arena Profile", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Profilepage: public Ui_Profilepage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFILEPAGE_H
