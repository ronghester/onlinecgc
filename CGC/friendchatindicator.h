#ifndef FRIENDCHATINDICATOR_H
#define FRIENDCHATINDICATOR_H

#include <QWidget>
#include <QMouseEvent>

namespace Ui {
class FriendChatIndicator;
}

class FriendChatIndicator : public QWidget
{
    Q_OBJECT
    
public:
    explicit FriendChatIndicator(QString Fname,QWidget *parent = 0);
    ~FriendChatIndicator();
    QString thisname;
    void setActivationDue();
    
private:
    Ui::FriendChatIndicator *ui;
protected:

    void mousePressEvent ( QMouseEvent * e );
private slots:
    void on_toolButton_clicked();
};

#endif // FRIENDCHATINDICATOR_H
