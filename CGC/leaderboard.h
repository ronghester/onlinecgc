#ifndef LEADERBOARD_H
#define LEADERBOARD_H

#include <QWidget>
#include <QNetworkReply>
#include <QTableWidget>
#include <ranksdialog.h>

namespace Ui {
class LeaderBoard;
}

class LeaderBoard : public QWidget
{
    Q_OBJECT
    
public:
    explicit LeaderBoard(QWidget *parent = 0);
    ~LeaderBoard();
    RanksDialog* RD;
    void moveRD();
private slots:

    void loadFinished();

private slots:
     void plainRequest(QNetworkReply* reply);
     //void on_tableWidget_cellEntered(int row, int column);

     void on_tableWidget_cellDoubleClicked(int row, int column);

     void on_tableWidget_cellClicked(int row, int column);

     void on_pushButton_clicked();

private:
    Ui::LeaderBoard *ui;
     QTableWidget* tableWidget;
     QString suffix(int n);
     QTableWidgetItem* createTableWidgetItem( const QString& text );
};

#endif // LEADERBOARD_H
