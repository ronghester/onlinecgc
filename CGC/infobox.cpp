#include "infobox.h"
#include "ui_infobox.h"

InfoBox::InfoBox(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::InfoBox)
{
    ui->setupUi(this);
}

InfoBox::~InfoBox()
{
    delete ui;
}

void InfoBox::on_toolButton_clicked()
{
    this->close();
}

void InfoBox::setMessage(QString msg){
    ui->infolabel->setText(msg);
}

void InfoBox::on_toolButton_2_clicked()
{
     this->close();
}
