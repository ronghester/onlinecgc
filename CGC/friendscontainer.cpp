#include "friendscontainer.h"
#include "QDebug"
FriendsContainer::FriendsContainer()
{
}

void FriendsContainer::addFriendElement(QString name, QString chips, QString status, QString onlinestatus,QString ranks){
   if(!lnames.contains(name)){
    lnames<<name;
    lchips<<chips;
    lstatus<<status;
    lonlinestatus<<onlinestatus;
    lranks<<ranks;
   }
}



std::vector<FriendsWidget*> FriendsContainer::getFriendsWidget(){
    std::vector<FriendsWidget*> thefriends;
        qDebug()<<"In get Friends";
    for(int i=0;i<lnames.size();i++){
        qDebug()<<"In Friends Update:"<<lnames.at(i);
        FriendsWidget* thisfrnd = new FriendsWidget(0);
        thisfrnd->setValues(lnames.at(i),lstatus.at(i),lchips.at(i),lonlinestatus.at(i),lranks.at(i));
        thisfrnd->setImage(friendsimages[i]);
        thefriends.push_back(thisfrnd);
    }
    return thefriends;
}



void FriendsContainer::updateOnlineStatusOfUser(QString name){
    if(lnames.contains(name)){
        int index = lnames.indexOf(name);
        lonlinestatus.insert(index,"online");
    }
}

bool FriendsContainer::isFriend(QString name){
    return lnames.contains(name);
}
