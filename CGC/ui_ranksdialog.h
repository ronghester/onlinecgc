/********************************************************************************
** Form generated from reading UI file 'ranksdialog.ui'
**
** Created: Sat 4. Apr 00:08:42 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RANKSDIALOG_H
#define UI_RANKSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_RanksDialog
{
public:
    QFormLayout *formLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_7;
    QLabel *label_4;
    QLabel *label_8;
    QLabel *label_5;
    QLabel *label_9;
    QLabel *label_6;
    QLabel *label_10;

    void setupUi(QDialog *RanksDialog)
    {
        if (RanksDialog->objectName().isEmpty())
            RanksDialog->setObjectName(QString::fromUtf8("RanksDialog"));
        RanksDialog->resize(158, 138);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(RanksDialog->sizePolicy().hasHeightForWidth());
        RanksDialog->setSizePolicy(sizePolicy);
        RanksDialog->setMaximumSize(QSize(16777215, 16777215));
        RanksDialog->setStyleSheet(QString::fromUtf8("#RanksDialog{\n"
"\n"
"border: 2px solid #fff;\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QLineEdit{\n"
"height:18;\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 10pt \"Candara\";\n"
"}\n"
""));
        formLayout = new QFormLayout(RanksDialog);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetNoConstraint);
        label_2 = new QLabel(RanksDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Admiral.png")));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        label = new QLabel(RanksDialog);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::FieldRole, label);

        label_3 = new QLabel(RanksDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(28, 23));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Platinum.png")));
        label_3->setScaledContents(true);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        label_7 = new QLabel(RanksDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMaximumSize(QSize(16777215, 24));

        formLayout->setWidget(1, QFormLayout::FieldRole, label_7);

        label_4 = new QLabel(RanksDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Gold.png")));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        label_8 = new QLabel(RanksDialog);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout->setWidget(2, QFormLayout::FieldRole, label_8);

        label_5 = new QLabel(RanksDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMaximumSize(QSize(20, 20));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Silver.png")));
        label_5->setScaledContents(true);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_5);

        label_9 = new QLabel(RanksDialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(3, QFormLayout::FieldRole, label_9);

        label_6 = new QLabel(RanksDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(24, 16777215));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/Bronze.png")));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_6);

        label_10 = new QLabel(RanksDialog);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout->setWidget(4, QFormLayout::FieldRole, label_10);


        retranslateUi(RanksDialog);

        QMetaObject::connectSlotsByName(RanksDialog);
    } // setupUi

    void retranslateUi(QDialog *RanksDialog)
    {
        RanksDialog->setWindowTitle(QApplication::translate("RanksDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label_2->setText(QString());
        label->setText(QApplication::translate("RanksDialog", "Admiral: Top 10%", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        label_7->setText(QApplication::translate("RanksDialog", "Platinum: 10% - 25%", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_8->setText(QApplication::translate("RanksDialog", "Gold: 25% - 50%", 0, QApplication::UnicodeUTF8));
        label_5->setText(QString());
        label_9->setText(QApplication::translate("RanksDialog", "Silver: 50% - 75%", 0, QApplication::UnicodeUTF8));
        label_6->setText(QString());
        label_10->setText(QApplication::translate("RanksDialog", "Bronze: 75% - 100%", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RanksDialog: public Ui_RanksDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RANKSDIALOG_H
