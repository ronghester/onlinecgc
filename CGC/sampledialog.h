#ifndef SAMPLEDIALOG_H
#define SAMPLEDIALOG_H

#include <QDialog>
#include <QNetworkReply>
#include <infobox.h>
namespace Ui {
class SampleDialog;
}

class SampleDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SampleDialog(QWidget *parent = 0);
    ~SampleDialog();
    
private slots:
    void on_closedlg_clicked();
    void plainRequest(QNetworkReply* reply);
    void on_createTeam_clicked();

    void on_pushButton_clicked();

    void on_toolButton_clicked();

private:
    Ui::SampleDialog *ui;
    InfoBox* IBox;
};

#endif // SAMPLEDIALOG_H
