#include "loginbox.h"
#include "ui_loginbox.h"
#include "QDebug"
#include <jsonparser.h>
#include <mainwindow.h>
extern int mainid;
extern QString sessionid;
extern QString sessionname;
extern QString thisuser;
extern QString statusline;
extern int userchips;
extern QByteArray userspic;
LoginBox::LoginBox(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::LoginBox)
{
    ui->setupUi(this);
    ui->label_3->setText("");

    //Hide The Register element...
    ui->label_7->hide();
    ui->lineEdit_4->hide();
    ui->label_4->hide();
    ui->lineEdit_3->hide();
    ui->checkBox->hide();
     setTabOrder(ui->lineEdit_2,ui->lineEdit_4);
      setTabOrder(ui->lineEdit_4,ui->lineEdit_3);
//setAttribute(Qt::WA_NoBackground);
//setAttribute(Qt::WA_TranslucentBackground);
   // this->ui



}

LoginBox::~LoginBox()
{
    delete ui;
}

void LoginBox::on_pushButton_clicked()
{
    //this->close();
     QString local = "<url>localhost:9091/drupal-7.28/?q=api/user/login";
    QString live =  "<url>api/user/login";
    getRequest(live);
}

void LoginBox::getRequest(QString surl){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl params;
    params.addQueryItem("username", ui->lineEdit->text());
    params.addQueryItem("password", ui->lineEdit_2->text());
    // QSslConfiguration config(QSslConfiguration::defaultConfiguration());
    QNetworkRequest request = QNetworkRequest(surl);
    //request.setSslConfiguration(config);
 ui->label_3->setText("Connecting to Server...");


      request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

 connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));

    manager->post(request,params.encodedQuery());
}
void LoginBox::RrequestReceived(QNetworkReply *reply){
   // qDebug() << "Received ";
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    //qDebug() << textData;

    if(textData.length()<5){
         ui->label_3->setText("Could Not Connect Try Again Later ");
    }
    if(textData.contains("uid")){
        //IBox = new InfoBox(this);
        ui->label_3->setText("Registration Successfull! You can login now");
        //IBox->show();
       // this->close();
        ui->pushButton->show();
        ui->pushButton_2->hide();

        ui->label_7->hide();
        ui->lineEdit_4->hide();
        ui->label_4->hide();
        ui->lineEdit_3->hide();
        ui->checkBox->hide();

    }
    else{
        Json::Reader reader;
        Json::Value root;
        reader.parse(textData.toStdString(), root );

        Json::Value error = root.get("form_errors","none");
        if(error != "none"){
            QString errorname = QString().fromStdString(error.get("name","none").asCString());
            ui->label_3->setText("Error: " + errorname);
        }
        else{
            ui->label_3->setText("Error In Registration..");
        }


    }

}

void LoginBox::requestReceived(QNetworkReply* reply){
    //qDebug() << "Received ";
   // QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
   // qDebug() << textData;
    if(textData=="[\"Wrong username or password.\"]"){
        ui->label_3->setText("Wrong username or password");
        qDebug()<<"Wrong username ";
        return;
    }
    if(textData!=""){
        ui->label_3->setText("Success.......");
    //check if the requetwas suucess
    Json::Reader reader;
    Json::Value root;
      reader.parse(textData.toStdString(), root );

     Json::Value user = root.get("user","user");
     sessionid = QString().fromStdString(root.get("sessid","user").asString());
     sessionname =  QString().fromStdString(root.get("session_name","user").asString());
     Json::Value fstatus = user.get("field_statusline","none");
     if(fstatus != "none" && fstatus.size()!=0){
         const Json::Value und = fstatus.get("und","none");
         if(und != "none"){
             for(int i=0;i<und.size();i++) {
                 Json::Value undval = und[i];
         statusline = QString().fromStdString(undval.get("value","none").asString());
         break;
             }
         }
     }
      fstatus = user.get("field_chips","none");
     if(fstatus != "none" && fstatus.size()!=0){
         const Json::Value und = fstatus.get("und","none");
         if(und != "none"){
             for(int i=0;i<und.size();i++) {
                 Json::Value undval = und[i];
         userchips = QString().fromStdString(undval.get("value","none").asString()).toInt();
         break;
             }
         }
     }
     fstatus = user.get("field_displaypic","none");
     if(fstatus != "none" && fstatus.size()!=0){
         const Json::Value und = fstatus.get("und","none");
         if(und != "none"){
             for(int i=0;i<und.size();i++) {
                 Json::Value undval = und[i];
                 QByteArray tem = undval.get("value","none").asCString();
                 userspic = QByteArray::fromBase64(tem.replace(" ","+"));
        //userspic = userspic.replace(" ","+");
        // userimage = userimage.trimmed();
         qDebug()<<"**************";
         qDebug()<<userspic;

         break;
             }
         }
     }


     else statusline = "No Status Available";


     QString id  = user.get("uid","0").asCString();
     thisuser  = user.get("name","none").asCString();
     mainid = id.toInt();
     emit success();

            qDebug()<<QString().number(mainid);
             qDebug()<<sessionid;
              qDebug()<<sessionname;

     this->close();

    }
    else  ui->label_3->setText("Could not Connect! Try Again");
}

//Exit button on login page.
void LoginBox::on_toolButton_clicked()
{
    emit closing();
}
//This is the login button
void LoginBox::on_pushButton_2_clicked()
{
    if(ui->label_7->isVisible()){
        if(ui->lineEdit_2->text() != ui->lineEdit_4->text()){
            ui->label_3->setText("Password does not match.");
            return;
         }
        if(!ui->checkBox->isChecked()){
            ui->label_3->setText("You must agree to the terms and condition!");
            return;
        }
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        QUrl params;
        params.addQueryItem("name", ui->lineEdit->text());
        params.addQueryItem("pass", ui->lineEdit_2->text());
        qDebug()<<"Password :"+ui->lineEdit_2->text();
        params.addQueryItem("mail", ui->lineEdit_3->text());
        // params.addQueryItem("field_name[und][0][value]", ui->lineEdit_4->text());
         params.addQueryItem("status", "1");
        //  params.addQueryItem("field_address[und][0][value]", ui->lineEdit_5->text());
        //  params.addQueryItem("field_dob[und][0][value]", ui->lineEdit_6->text());
        //  params.addQueryItem("field_statusline[und][0][value]",ui->lineEdit_7->text());
        // params.addQueryItem("pass", ui->lineEdit_2->text());
        // QSslConfiguration config(QSslConfiguration::defaultConfiguration());

        QString surl = "<url>secret/user/register";
        QNetworkRequest request = QNetworkRequest(surl);
        //request.setSslConfiguration(config);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(RrequestReceived(QNetworkReply*)));
        manager->post(request,params.encodedQuery());
        ui->label_3->setText("Connecting....");
    }
    else {
        ui->pushButton->hide();
        ui->label_7->show();
        ui->lineEdit_4->show();
        ui->label_4->show();
        ui->lineEdit_3->show();
        ui->checkBox->show();

        // setTabOrder(ui->lineEdit,ui->lineEdit_2);

    }
    /////////////////////////////






}

void LoginBox::mousePressEvent(QMouseEvent *event){

    mpos = event->pos();
}


void LoginBox::mouseMoveEvent(QMouseEvent *event){

    if (event->buttons() && Qt::LeftButton) {
                QPoint mp(event->x(),event->y());
              // if(ui->frame_4->geometry().contains(mp,true)){

                QPoint diff = event->pos() - mpos;
                QPoint newpos = this->pos() + diff;
                this->move(newpos);
              //  }

            }
}
