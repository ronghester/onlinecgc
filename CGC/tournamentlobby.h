#ifndef TOURNAMENTLOBBY_H
#define TOURNAMENTLOBBY_H

#include <QWidget>
#include <jsonparser.h>
#include <QTableWidgetItem>
#include <QNetworkReply>
#include <infobox.h>
namespace Ui {
class TournamentLobby;
}

class TournamentLobby : public QWidget
{
    Q_OBJECT
    
public:
    explicit TournamentLobby(QWidget *parent = 0);
    ~TournamentLobby();
    void setValues(Json::Value root);
    int pcounter;
    QString tid;
    int row,column;
    void addTeamPlayer(Json::Value Pname);
    bool isSignedup;
    InfoBox* IBox;
    int totalplayer;
    QString troomID;
    QString troomName;
    QString wageramount;
    QString gametype;
    QString troomdesc;
    QMap<QString, QString> lobbies_map;
    QString matchlevel;
    QString arenatitle;
    void Tclose(Json::Value tds);
private slots:
    void on_pushButton_2_clicked();

    void on_signup_clicked();
    void plainRequest2(QNetworkReply* reply );
    void on_join_tlobby_clicked();

private:
    Ui::TournamentLobby *ui;
    QTableWidgetItem* createTableWidgetItem( const QString& text );
};

#endif // TOURNAMENTLOBBY_H
