#ifndef LOGINBOX_H
#define LOGINBOX_H

#include <QDialog>
#include <QNetworkReply>
namespace Ui {
class LoginBox;
}

class LoginBox : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoginBox(QWidget *parent = 0);
    ~LoginBox();
    void getRequest(QString surl);
 QPoint mpos;
signals:
    void success();
    void closing();
public slots:
        void requestReceived(QNetworkReply* reply);
        void RrequestReceived(QNetworkReply* reply);

private slots:
    void on_pushButton_clicked();
    //void post(QString &url, QString &user, QString &password, QString &data);

    void on_toolButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::LoginBox *ui;
       void mousePressEvent(QMouseEvent *);
       void mouseMoveEvent(QMouseEvent *);
};

#endif // LOGINBOX_H
