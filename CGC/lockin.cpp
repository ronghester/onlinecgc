#include "lockin.h"
#include "ui_lockin.h"
#include <QWebFrame>
#include <QDebug>
#include <mainwindow.h>
#include <QTimer>
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <QScrollBar>
#include <userblockinlockin.h>

extern int mainid;
extern QString sessionname;
extern QString sessionid;
extern QString thisuser;
extern QString cind;
extern QString thisuser_rank;
extern QString currentArenaName;
extern int userchips;
 QPushButton *reportloss;
 extern MainWindow* w;

extern int ADMIN;
/*
  [23:34:25] Sushil Ronghe: 1. user enter the room he will be shown in the chat box list
  [23:37:13] Nathan: when he clicks sit down he appears in the team section
  [23:35:56] Sushil Ronghe: 3. User click the place bet and the bet will be placed for him and then his name and amount will appear in the team section

*/
LockIn::LockIn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LockIn)
{
    reportloss = 0;
    ui->setupUi(this);
    //LeaveTable = ui->pushButton_7;
   // LeaveTable->hide();
    ViewLobby = ui->homebtn;
    placebet = ui->placebet_button;
    wagerslide = ui->lineEdit;
    placebet->hide();
    sliderFrame = ui->frame_11;
    betslider = ui->horizontalSlider;
    closelockin = ui->toolButton;
    has_connected= false;
    onlineuser =0;
    satdownuser = 0;
    has_madethebet = false;
    has_satdown = false;
    gamestarted = false;
    /*M = new Mediator();
    qDebug()<<"creating webview ...";
    webviewholder = new QWidget(this);
    webview = new QWebView(webviewholder);
    //connect(webview,SIGNAL(loadFinished(bool)), this, SLOT(loadMeeting(bool)));
    webview->setStyleSheet("background-color:none;color:black;");
    QVBoxLayout *newlayp = new QVBoxLayout();
    newlayp->setMargin(0);
    newlayp->addWidget(webview);
    webviewholder->setLayout(newlayp);
    webviewholder->hide();
    QString urld = "<url>node/31"; //<url>localhost:9091/drupal-7.28/?q=node/16";
    QNetworkRequest request = QNetworkRequest(urld);
    QString ss = sessionname+"="+sessionid;
    request.setRawHeader("Cookie",ss.toUtf8() );
    webview->load(request);
    // webview->connect()    
    webview->settings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls, true);
    //webviewholder->hide();
    //webview->settings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls, true);
    webview->page()->mainFrame()->addToJavaScriptWindowObject(QString("Mediator"), M);
    //QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    //webview->page()->setNetworkAccessManager(manager);
    //connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(subscribethisroom()));
   // connect(M,SIGNAL(msg), this, SLOT());

    M->lastMsg ="";
    connect(webview,SIGNAL(loadFinished(bool)),this,SLOT(loadFinished()));
*/
     count =0;
     count2 = 0;
     chatcounter = 0;
     issatdown = false;
    // ui->username_lb->setText(thisuser);
     qDebug()<<"Inside Lock End ...";
     //ui->friendly->setChecked(true);
     sitdown = ui->sitdown_btn;
     ui->warning_lbl->setText("Waiting to Join the Room...");
    // QTimer::singleShot(8000, this, SLOT(animatedMsg()));
    //gameheader = ui->gameheader;
   teamawager = 0;
   teambwager = 0;
   ui->horizontalSlider->hide();
   roomtype = "lroom";
   ui->lineEdit->hide();
   Your_teamtype = "A";
   has_connected = false;
   ui->horizontalSlider->setMaximum(userchips);
 //ui->usermsg->installEventFilter(this);
 //QTimer::singleShot(4000, this, SLOT(subscribethisroom()));
   ui->teamAwin->hide();
   ui->teamBwin->hide();
   betamountedit = ui->lineEdit;
   if(mainid == ADMIN){
       ui->teamAwin->show();
       ui->teamBwin->show();
   }
   resultdeclared = false;
   isthisadmin = false;
   ui->onlineplayers_lb->hide();
   ui->friendlylist->setStyleSheet("QListWidget:item { min-height:48px;border-image: url(:/new/prefix1/resources/newfriendsbg.jpg);}");
   ui->enemylist->setStyleSheet("QListWidget:item { min-height:48px;border-image: url(:/new/prefix1/resources/newfriendsbg.jpg);}");
}

void LockIn::processMessage(Json::Value root){

    QString channel = QString().fromStdString(root.get("channel","none").asCString());
    qDebug()<<"channel";
    if(channel != roomname) return;
    Json::Value data = root.get("data","none");
    if(data!="none"){

         QString sub = QString().fromStdString(data.get("subject","none").asCString());
        /* if(sub == "adduser"){
                QString bd = QString().fromStdString(data.get("body","none").asCString());
             addPlayer(bd);
         }*/
         if(sub == "ChatMsg"){
            QString chatmsg = QString().fromStdString(data.get("body","none").asCString());
            QListWidgetItem* QWL = new QListWidgetItem(chatmsg);
            ui->chatbox->insertItem(chatcounter,QWL);
            ui->chatbox->scrollToBottom();
            //  chatcounter++;
            //This is fake item
            chatcounter++;
         }
           if(resultdeclared) return;

         if(sub == "usercount"){
             const Json::Value dt= data["body"];  //"none");
             {
                 const Json::Value userinfo_dataarray = data.get("userinfo","none");
                  for(int userindex=0;userindex < userinfo_dataarray.size(); ++userindex){
                     Json::Value userinfo_data = userinfo_dataarray[userindex];
                     if(userinfo_data != "none"){
                     // qDebug()<< QString().fromStdString(userinfo_data.toStyledString());
                      QString uname = QString().fromStdString(userinfo_data.get("username","none").asString());
                     // if(map_userranks.contains(uname)) break;
                      QString urank = QString().fromStdString(userinfo_data.get("userrank","none").asString());
                      QString totalgames = QString().fromStdString(userinfo_data.get("totalgames","none").asString());
                      QString uid = QString().fromStdString(userinfo_data.get("uid","none").asString());
                      QByteArray tem =  userinfo_data.get("userpic","none").asCString();
                      QByteArray temm = QByteArray::fromBase64(tem.replace(" ","+"));
                      qDebug()<<"Filling Data For "<<uname <<"totalgames"<<totalgames;
                      map_userranks[uname] = urank;
                      map_userpic[uname] = temm;
                      map_totalgames[uname] = totalgames;
                      map_uids[uname] = uid;
                         qDebug()<<"Pringting here "<<map_totalgames;
                     }
                 }
             }

             for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.

                  QString userstr =  QString().fromStdString(dt[index].asCString());
                  addPlayerToChatList(userstr);
                  if(isthisadmin){
                      if(userstr!= "CGC_Admin"){
                      betusers<<userstr;
                      addPlayerAdvance(userstr);
                      }
                  }
             }


              const Json::Value st= data["satdownusers"];  //"none");
              for ( int index = 0; index < st.size(); ++index ) { // Iterates over the sequence elements.
                 QString userstr =  QString().fromStdString(st[index].asCString());
                 if(userstr.length()>1 && !satdownuserlist.contains(userstr)){
                     qDebug()<<"In chat";
                     updatePlayerInChatList(userstr);
                     qDebug()<<"In add player";
                     addPlayer(userstr);
                 }
             }
              QString ct = QString().fromStdString(data.get("userct","none").asCString());


             onlineuser = dt.size();
             ui->onlineplayers_lb->setText("User Online: "+ct+"/"+QString().number(dt.size()));

            //qDebug()<<"Done";

             if(roomtype == "Troom"){
                 ui->warning_lbl->setText("");
                 placebet->show();
                 ui->horizontalSlider->show();
                 ui->lineEdit->show();
                 ui->warning_lbl->setText("Place Your Bet Now!");
             }
         }
         if(sub == "unsitdown"){
              const Json::Value dt= data["body"];
              qDebug()<<QString().fromStdString(dt.toStyledString());
              QString theuser = QString().fromStdString(dt.asCString());
              refresh_List_After_plr_unsit(theuser);
              for(int i=0;i<satdownuserlist.size();i++){
                  if(satdownuserlist[i] == theuser){
                      satdownuserlist.removeAt(i);
                  }
              }
         }
         if(sub == "sitdown"){
             const Json::Value dt= data["body"];
             const Json::Value userinfo_dataarray = data.get("userinfo","none");
             for(int userindex=0;userindex < userinfo_dataarray.size(); ++userindex){
                 Json::Value userinfo_data = userinfo_dataarray[userindex];
                 if(userinfo_data != "none"){

                 // qDebug()<< QString().fromStdString(userinfo_data.toStyledString());
                  QString uname = QString().fromStdString(userinfo_data.get("username","none").asString());
                 // if(map_userranks.contains(uname)) break;
                    QString totalgames = QString().fromStdString(userinfo_data.get("totalgames","none").asString());
                  QString urank = QString().fromStdString(userinfo_data.get("userrank","none").asString());
                    QString uid = QString().fromStdString(userinfo_data.get("uid","none").asString());

                  QByteArray tem =  userinfo_data.get("userpic","none").asCString();
                  QByteArray temm = QByteArray::fromBase64(tem.replace(" ","+"));
                  qDebug()<<"Filling Data For "<<uname;
                  map_userranks[uname] = urank;
                  map_userpic[uname] = temm;
                   map_totalgames[uname] = totalgames;
                   map_uids[uname] = uid;
                 }
             }
             for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.
                  QString userstr =  QString().fromStdString(dt[index].asCString());
                 if(!satdownuserlist.contains(userstr)){
                    updatePlayerInChatList(userstr);
                    addPlayer(userstr);

                 }
             }
             ///If this is the tournament lobby then all players are sat down and bet can be started immediatly



         }
         if(sub == "placebet"){
             const Json::Value dt= data["body"];
             for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.
                  QString userstr =  QString().fromStdString(dt[index].asCString());
                 if(!betusers.contains(userstr)){
                    updatePlayerInChatList(userstr);
                    addPlayerAdvance(userstr);
                    betusers<<userstr;
                 }
             }
           /*QString bd = QString().fromStdString(data.get("body","none").asCString());
             for(int i=0;i<betusers.length();i++){
                 if(betusers.at(i).split(":")[0]==thisuser){
                      if(!has_madethebet) has_madethebet = true;
                      else{
                          ui->warning_lbl->setText("You have already placed the bet! \nPlease wait untill room is filled..");
                           QTimer::singleShot(2000, this, SLOT(animatedMsg()));
                      }
                 }
             }
             if(!has_madethebet){
                    QString bd = QString().fromStdString(data.get("body","none").asCString());
                     addPlayerAdvance(bd);
                     betusers<<bd;
                     satdownuser++;
             }*/
            // ui->onlineplayers_lb->setText("User Online: "+);
         }
         if(sub == "addandclose"){
              ui->warning_lbl->setText("All players have placed their bet,\n Please play out your game and report the loss");
              const Json::Value dt= data["body"];
              for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.
                   QString userstr =  QString().fromStdString(dt[index].asCString());
                   qDebug()<<"Processing user :"<<userstr;
                  if(!betusers.contains(userstr)){
                     updatePlayerInChatList(userstr);
                     addPlayerAdvance(userstr);
                     betusers<<userstr;
                  }
              }
              // Now adjust the bet if required..

              adjustTheBet();
              //ui->lineEdit->setEnabled(false);
             // ui->lineEdit->hide();

              //Change the ui only for the users who have placed the bet..
              if(has_madethebet){
              QLayout* layout = ui->frame_11->layout ();
              QWidget *w;
                 if (layout != 0)
                 {
                 QLayoutItem *item;
                 while ((item = layout->takeAt(0)) != 0){
                    // qDebug()<<"Inside Cleaning Code ";

                     layout->removeItem (item);
                         if ((w = item->widget()) != 0) {w->hide(); delete w;}
                         else {delete item;}

                 }
                 delete layout;
                 }

                 QVBoxLayout *top = new QVBoxLayout;
                 reportloss = new QPushButton("Report Loss");
                 top->addWidget(reportloss);
                 ui->frame_11->setLayout(top);

                 connect(reportloss,SIGNAL(clicked()),this,SLOT(onReportLossClicked()));
              }

         }
         if(sub == "roomfull"){
             //Now user can start making the bet before that add this last user to the list
             ui->warning_lbl->setText("");
             const Json::Value dt= data["body"];
             const Json::Value userinfo_dataarray = data.get("userinfo","none");
             for(int userindex=0;userindex < userinfo_dataarray.size(); ++userindex){
                 Json::Value userinfo_data = userinfo_dataarray[userindex];
                 if(userinfo_data != "none"){

                 // qDebug()<< QString().fromStdString(userinfo_data.toStyledString());
                  QString uname = QString().fromStdString(userinfo_data.get("username","none").asString());
                 // if(map_userranks.contains(uname)) break;
                    QString totalgames = QString().fromStdString(userinfo_data.get("totalgames","none").asString());
                  QString urank = QString().fromStdString(userinfo_data.get("userrank","none").asString());
                  QByteArray tem =  userinfo_data.get("userpic","none").asCString();
                  QByteArray temm = QByteArray::fromBase64(tem.replace(" ","+"));
                  qDebug()<<"Filling Data For "<<uname;
                  map_userranks[uname] = urank;
                  map_userpic[uname] = temm;
                  map_totalgames[uname] = totalgames;
                 }
             }
             for ( int index = 0; index < dt.size(); ++index ) { // Iterates over the sequence elements.
                  QString userstr =  QString().fromStdString(dt[index].asCString());
                 if(!satdownuserlist.contains(userstr)){
                    updatePlayerInChatList(userstr);
                    addPlayer(userstr);

                 }
             }
             placebet->show();
             ui->horizontalSlider->show();
             ui->lineEdit->show();
             ui->warning_lbl->setText("Place Your Bet Now!");
             gamestarted = true;
         }
         if(sub == "lobbyclosed"){
             if(roomtype == "Troom"){
                  ui->warning_lbl->setText("Lobby is closed for placing the bet!");
             }
             /*QLayout* layout = ui->frame_11->layout ();
             QWidget *w;
                if (layout != 0)
                {
                QLayoutItem *item;

                while ((item = layout->takeAt(0)) != 0){
                   // qDebug()<<"Inside Cleaning Code ";
                    layout->removeItem (item);
                        if ((w = item->widget()) != 0) {w->hide(); delete w;}
                        else {delete item;}

                }
                delete layout;
                }
                QVBoxLayout *top = new QVBoxLayout;
                QPushButton *reportloss = new QPushButton("Report Loss");
                top->addWidget(reportloss);
                ui->frame_11->setLayout(top);
                connect(reportloss,SIGNAL(clicked()),this,SLOT(onReportLossClicked()));*/
         }

    }
   // bool broadcast = root.get("broadcast","none").asBool();
    //qDebug()<<"Channel: "+channel +" Boradcast: "+broadcast;
}

/* Function required to fill the bets for user who haven't place any bet.If someone is leaving the room and bets are already placed.
  * Then we must declare the winner for the game.
  **/

void LockIn::fillEmptyBets(){
    if(betusers.size() != satdownuserlist.size()){
        for(int i=0;i<satdownuserlist.size();i++){
            bool foundinsatdown = false;
            for(int j=0;j<betusers.size();j++){
                QStringList pair =betusers.at(j).split(":");
                if(satdownuserlist[i] == pair.at(0)){
                    foundinsatdown = true;
                }
            }
            if(!foundinsatdown){
                //lets place a bet for this user
                QString username = satdownuserlist[i];
                QString team = getUserTeam(username);
                QString uid = map_uids.value(username);
                QString betstring = username+":"+QString().number(minimumWager)+":"+team+":"+uid;
                betusers<<betstring;
                addPlayerAdvance(betstring);
            }
        }
    }
}

void LockIn::createDispute(){
     QString surl = "<url>api/TModule/callReferee?";
     qDebug()<<"Nid :"<<currentRoomId;

     surl += "title=ticket_"+currentRoomId+"&roomid="+roomname+"&arenaid="+currentRoomId+"&gametype="+roomtype;
     QNetworkAccessManager *manager = new QNetworkAccessManager(this);
     qDebug()<<surl;
     connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
     QNetworkRequest request2 = QNetworkRequest(surl);
     QString ss = sessionname+"="+sessionid;
     request2.setRawHeader("Cookie",ss.toUtf8() );
     manager->get(request2);
}

void LockIn::onPlaceButtonClicked(){
    if(roomtype == "lroom" && satdownuserlist.contains(thisuser)){
        IBox = new InfoBox(this);
        IBox->setMessage("Spectators can't make the bet");
        IBox->show();
        return;
    }
    if(has_madethebet){
        setRedLabel("You have already Placed the bet!\n wait for room to fill");
        return;
    }

    else if(!has_connected){
            IBox = new InfoBox(this);
            IBox->setMessage("Please wait!");
            IBox->show();
            return;
        }

    bool okay = false;
    int betamt = ui->lineEdit->text().toInt(&okay,10);
    if(!okay || betamt%10 != 0){
        IBox = new InfoBox(this);
        IBox->setMessage("Bet amount should be number in order of 10.");
        IBox->show();
        return;
    }

    int bet = betamt;
    qDebug()<<bet<<" w:uchips "<<currentWagerAmount;
    if(roomtype != "Troom" && bet < currentWagerAmount){
        IBox = new InfoBox(this);
        IBox->setMessage("Your bet must be greater than\n Wager Amount.!");
        IBox->show();
        return;
    }

    if(roomtype != "Troom" && bet > userchips){
        IBox = new InfoBox(this);
        IBox->setMessage("You Don't have enough Chips to bet!");
        IBox->show();
        return;

    }

    //
    hasMadeAPlaceBetRequest = true;
     QString nid = currentRoomId;
    int gametype = 0;
     if(currentGameType == "") gametype = 0;
     else if(currentGameType == "1V1") gametype = 2;
     else if(currentGameType == "2V2") gametype = 4;
     else if(currentGameType == "3V3") gametype = 6;
     else if(currentGameType == "4V4") gametype = 8;
   QString teamType = getTeamType();
   //don't allow him to place multiple bets..
   has_madethebet = true;
    //QString gametype = currentGameType;

   QString surl = "<url>api/";
   QString dynamic;

   if(roomtype == "Croom"){
       surl += "CGC/placebet?";
       dynamic = "&fieldname=field_c_player_online";
   }
   else if(roomtype == "Troom"){
       surl += "TModule/placebet?";
       dynamic = "&fieldname=field_troom_players_online";
   }


    surl += "nid="+roomname+"&title=placebet&desc="+currentArenaName+":"+QString().number(bet)+":"+teamType+":"+QString().number(mainid)+"&roomid="+nid+"&gametype="+QString().number(gametype)+dynamic;
      qDebug()<<surl;
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

}
void LockIn::onReportingLoss(){
    //Instead of reporting the loss we are saying claim winner...
    //get all the bet amount and make him winnner..
    qDebug()<<"You are obsolte winner";
    //defeated team variable
    QString desc;
    QString nitems;
    //winner team variable
    QString wager;
    QString number;
    int intdesc=0;
    QStringList defeatedteam;
    QStringList winnerteam;
    qDebug()<<"Checking pre..";
    for(int i=0;i<betusers.size();i++){
         QStringList pair = betusers.at(i).split(":");
          if(pair.at(2) == getTeamType()){
              defeatedteam += betusers.at(i);
          }
          else{
              winnerteam += betusers.at(i);
          }
    }
    qDebug()<<"Checking bfore";

    qSort(defeatedteam.begin(),defeatedteam.end(),caseInsensitiveLessThan);
    qSort(winnerteam.begin(),winnerteam.end(),caseInsensitiveLessThan);

  for(int i=0;i<defeatedteam.size();i++){

        QStringList pair =defeatedteam.at(i).split(":");

            nitems += pair.at(3)+":";
            number += pair.at(1)+":"; //They should loose what they bet for..
             intdesc += QString(pair.at(1)).toInt();

  }

  for(int i=0;i<winnerteam.size();i++){
      QStringList pair =winnerteam.at(i).split(":");

      wager += pair.at(3)+":";
      desc += pair.at(1)+":"; //They should gain what they bet for
  }

   /* for(int i=0;i<betusers.size();i++){
        QStringList pair = betusers.at(i).split(":");
        qDebug()<<betusers.at(i);
        if(pair.at(2) == getTeamType()){ //this is defeated team
            nitems += pair.at(3)+":";
            desc += pair.at(1)+":";
            intdesc += QString(pair.at(1)).toInt();
        }
        else{
            wager += pair.at(3)+":";
            number += pair.at(1)+":";
        }
    }*/

    if(roomtype == "Troom"){
         qDebug()<<"T rooom winner "<<this->lobby_title;
        QStringList tt = this->lobby_title.split(":");
         qDebug()<<"T rooom winner "<<tt.at(1);
        if(tt.at(1) == matchlevel){
            int betdesc = intdesc;
            betdesc += playercount * currentWagerAmount;
            //we need to divide this based on the number of player
            betdesc = betdesc/(betusers.count()/2);
            number = QString().number(betdesc)+":";
            qDebug()<<"This is the last lobby: "<<desc;
        }
    }

     QString surl = "<url>api/";

    if(roomtype == "Croom"){
        surl += "CGC/setWinner?";
    }
    else if(roomtype == "Troom"){
        surl += "TModule/setWinner?";
    }

   surl += "nid="+roomname+"&roomid="+currentRoomId+"&gametype="+nitems+"&desc="+desc+"&uid="+QString().number(mainid)+"&wager="+wager+"&when="+number;
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

      qDebug()<<"Hey I am here";
       // ViewLobby->show();
       closelockin->hide();
}


void LockIn::onSitBack(){
    //This button
    //call the service now with sit back code..
    if(issatdown){
        IBox = new InfoBox(this);
        IBox->setMessage("You are already Sat Down!\n Wait for the room to fill");
        IBox->show();
        return;
    }
    /*else if(has_connected){
        IBox = new InfoBox(this);
        IBox->setMessage("Please wait!");
        IBox->show();
        return;
    }*/
   // QString nid = roomname;
   int gametype = 0;
    if(currentGameType == "") gametype = 0;
    else if(currentGameType == "1V1") gametype = 2;
    else if(currentGameType == "2V2") gametype = 4;
    else if(currentGameType == "3V3") gametype = 6;
    else if(currentGameType == "4V4") gametype = 8;
  QString teamType = getTeamType();
     //don't allow him to place multiple bets..
    // lock->has_madethebet = true;
   //QString gametype = currentGameType;
  QString surl = "<url>api/";
  QString dynamic;

  if(roomtype == "Croom"){
      surl += "CGC/sitdown?";
      dynamic = "&fieldname=field_field_c_player_satdown";
  }
  else if(roomtype == "Troom"){
      surl += "TModule/sitdown?";
      dynamic = "&fieldname=field_troom_players_satdown";
  }

  surl += "nid="+roomname+"&roomid="+currentRoomId+"&title=sitdown&desc="+currentArenaName+"&gametype="+QString().number(gametype)+dynamic;
   QNetworkAccessManager *manager = new QNetworkAccessManager(this);
     qDebug()<<surl;
     connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
     QNetworkRequest request2 = QNetworkRequest(surl);
     QString ss = sessionname+"="+sessionid;
     request2.setRawHeader("Cookie",ss.toUtf8() );
     manager->get(request2);
     issatdown = true;
}

LockIn::~LockIn()
{
    delete ui;
}

void LockIn::onReportLossClicked(){
    emit he_lost();
}

void LockIn::on_pushButton_6_clicked()
{

}

void LockIn::on_horizontalSlider_sliderMoved(int position)
{
   // ui->label_3->setText(QString().number(position));
    ui->lineEdit->setText(QString().number(position));
}
void LockIn::animatedMsg(){

    ui->warning_lbl->setText("");
    // ui->warning_lbl->setp

}

void LockIn::on_pushButton_4_clicked()
{
       //the place bet button
   // addPlayer("Dummy");

}

void LockIn::addPlayerToChatList(QString playername){
   // qDebug()<<"Adding user "<<playername;
    if(!chatuserholder.contains(playername)){       
        ui->allplayers->insertItem(chatuserholder.size(),new QListWidgetItem(playername+":[Spectator]"));
         chatuserholder<<playername;

         for(int j=0;j<ui->friendlylist->count();j++){
           UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->friendlylist->itemWidget(ui->friendlylist->item(j)));
           if(userclc->getUsername() == playername){
               if(roomtype == "Troom" || roomtype == "Croom"){
                   userclc->setOnlineStatus(true);
               }
           }
         }

         for(int j=0;j<ui->enemylist->count();j++){
           UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->enemylist->itemWidget(ui->enemylist->item(j)));
           if(userclc->getUsername() == playername){
               if(roomtype == "Troom" || roomtype == "Croom"){
                   userclc->setOnlineStatus(true);
               }
           }
         }
    }
}
void LockIn::updatePlayerInChatList(QString playername){
    if(chatuserholder.contains(playername)){
        for(int i=0;i<chatuserholder.size();i++){
            if(chatuserholder.at(i)==playername){
               // ui->allplayers->insertItem(ui->allplayers->item(i));
               delete ui->allplayers->item(i);
                ui->allplayers->insertItem(i,new QListWidgetItem(playername));
            }
        }
    }
}

QString LockIn::getTeamType(){
    return Your_teamtype; //ui->friendly->isChecked() ? "A" : "B";
}

void LockIn::cleanup(){
    chatuserholder.clear();
    betusers.clear();
    //satdownuser.clear();
    betusers.clear();
    satdownuserlist.clear();
    onlineuser =0;
    satdownuser = 0;
    has_madethebet = false;
    has_satdown = false;
    Your_teamtype = "A";
    has_connected = false;
}
void LockIn::setMatchStatus(bool won, QString wonchips){
    if(won){
        if(wonchips!="")
          ui->warning_lbl->setText("Congratulation! you won "+wonchips+" Chips");
        else
          ui->warning_lbl->setText("Congratulation, you won! chips are credited");
    }
    else
       ui->warning_lbl->setText("You Lost!\n Please proceed to lobbies.");
    if(reportloss) reportloss->hide();

    ViewLobby->show();
    closelockin->hide();
    resultdeclared = true;
}

QString LockIn::getUserTeam(QString thename){
    for(int i=0;i<ui->enemylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->enemylist->itemWidget(ui->enemylist->item(i)));
      qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == thename)
          return "B";
    }
    for(int i=0;i<ui->friendlylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->friendlylist->itemWidget(ui->friendlylist->item(i)));
      qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == thename){
          return "A";

      }
    }
    return "A";

}

void LockIn::refresh_List_After_plr_unsit(QString thename){
    for(int i=0;i<ui->enemylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->enemylist->itemWidget(ui->enemylist->item(i)));
      qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == thename){
          ui->enemylist->removeItemWidget(ui->enemylist->item(i));
          delete ui->enemylist->takeItem(i);
          count2--;
      }
    }
    /*********/
    for(int i=0;i<ui->friendlylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->friendlylist->itemWidget(ui->friendlylist->item(i)));
      qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == thename){
          ui->friendlylist->removeItemWidget(ui->friendlylist->item(i));
         delete ui->friendlylist->takeItem(i);
          count++;
      }
    }
}

void LockIn::adjustTheBet(){
    int lastbet = 0; int adjustedbet = 0;
    QStringList newbetuser;
    if(teamawager != teambwager){ //lets adjust the bet
        int diff = abs(teamawager - teambwager);
        QString team = teamawager > teambwager ? "A" : "B";
//Lets clear the teama and teambwager
        if(team == "A")
           teamawager = 0;
        else
           teambwager = 0;
            int totalplayer = 0;
            for(int i=0;i<betusers.size();i++){
                QStringList pair = betusers.at(i).split(":");
                if(pair.at(2) == team){
                    if(pair.at(1).toInt() > minimumWager){
                        totalplayer++;
                    }
                }
            }
            int divident = diff / totalplayer;
            for(int i=0;i<betusers.size();i++){
                QStringList pair = betusers.at(i).split(":");
                if(pair.at(2) == team){
                    if(pair.at(1).toInt() > minimumWager){
                        int adjustedbet = pair.at(1).toInt() - divident;
                        QString adjusted =  pair.at(0)+":"+QString().number(adjustedbet)+":"+pair.at(2)+":"+pair.at(3);
                        addPlayerAdvance(adjusted,true);

                        betusers[i] = adjusted;
                    }
                }
            }



    }

}



void LockIn::addPlayerAdvance(QString plr,bool type2){
    if(isthisadmin){
        //For admin friendlist and enemy list will be empty. so just create it from scratch..
        qDebug()<<"Printing User: "<<plr;
        QStringList pair = plr.split(":");
        if(pair.length() > 2){
        //First lets remove the sat down item
         QString wag =pair.at(1).trimmed();
         UserBlockInLockIn* thisuserblc = new UserBlockInLockIn();
         thisuserblc->setName(pair.at(0),"--");
         thisuserblc->setBetAmount(wag);
         QListWidgetItem *item = new QListWidgetItem();
         item->setSizeHint(QSize(160,48));
         item->setFont(QFont("Candara",14, QFont::Normal));
         if(pair.at(2) == "A"){
             ui->friendlylist->insertItem(count,item);
             ui->friendlylist->setItemWidget( ui->friendlylist->item(count),thisuserblc);
             count++;
         }
         else{
             ui->enemylist->insertItem(count2,item);
             ui->enemylist->setItemWidget( ui->enemylist->item(count2),thisuserblc);
             count2++;
         }
        }
        return;
    }

    qDebug()<<"Inside the add player "+plr;
    QStringList pair = plr.split(":");
    //First lets remove the sat down item
     QString wag =pair.at(1).trimmed();
     if(pair.at(0) == currentArenaName) this->has_madethebet = true;
    for(int i=0;i<ui->friendlylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->friendlylist->itemWidget(ui->friendlylist->item(i)));
      qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == pair.at(0)){
          userclc->setBetAmount(wag);
          if(map_userranks.contains(pair.at(0))){
              QString thisuser_maprank =  map_userranks.value(pair.at(0));
               userclc->setRank(thisuser_maprank);
          }
          if(map_userpic.contains(pair.at(0))){
                 userclc->setUserImage(map_userpic.value(pair.at(0)));
          }

            qDebug()<<map_totalgames;


          if(map_totalgames.contains(pair.at(0))){
              userclc->setTotalGames(map_totalgames.value(pair.at(0)));
          }
          if(roomtype == "Troom" || roomtype == "Croom"){
          chatuserholder.contains(pair.at(0)) ?
              userclc->setOnlineStatus(true):
              userclc->setOnlineStatus(false);
          }
      }
    }
    for(int i=0;i<ui->enemylist->count();i++){
      UserBlockInLockIn *userclc = qobject_cast<UserBlockInLockIn *>(ui->enemylist->itemWidget(ui->enemylist->item(i)));
        qDebug()<<"Player name "<<userclc->getUsername()<<" I:"<<i;
      if(userclc->getUsername() == pair.at(0)){
             userclc->setBetAmount(wag);
             if(map_userranks.contains(pair.at(0))){
                 QString thisuser_maprank =  map_userranks.value(pair.at(0));
                  userclc->setRank(thisuser_maprank);
             }
             if(map_totalgames.contains(pair.at(0))){
                 userclc->setTotalGames(map_totalgames.value(pair.at(0)));
             }
             if(map_userpic.contains(pair.at(0))){
                    userclc->setUserImage(map_userpic.value(pair.at(0)));
             }
      }
    }
    /*if(type2){
        for(int i=0;i<ui->friendlylist->count();i++){
            if(ui->friendlylist->item(i)->text().split("  ")[0] == pair.at(0)){
                ui->friendlylist->removeItemWidget(ui->friendlylist->item(i));
                delete ui->friendlylist->item(i);
            }
        }
        for(int i=0;i<ui->enemylist->count();i++){
            if(ui->enemylist->item(i)->text().split("  ")[0] == pair.at(0)){
                ui->enemylist->removeItemWidget(ui->enemylist->item(i));
                delete ui->enemylist->item(i);
            }
        }
    }
    else{
    for(int i=0;i<ui->friendlylist->count();i++){
        if(ui->friendlylist->item(i)->text().split("\n")[0] == pair.at(0)){
            ui->friendlylist->removeItemWidget(ui->friendlylist->item(i));
            delete ui->friendlylist->item(i);
        }
    }
    for(int i=0;i<ui->enemylist->count();i++){
        if(ui->enemylist->item(i)->text().split("\n")[0] == pair.at(0)){
            ui->enemylist->removeItemWidget(ui->enemylist->item(i));
            delete ui->enemylist->item(i);
        }
    }
    }

    QString thisuser_maprank = "N/A";
    QIcon icon;
    if(map_userranks.contains(pair.at(0))){
        thisuser_maprank =  map_userranks.value(pair.at(0));
    }
    UserBlockInLockIn* thisuserblock = new UserBlockInLockIn();

     QString wag =pair.at(1).trimmed();
     thisuserblock->setName(pair.at(0),"20",wag);
    if(map_userpic.contains(pair.at(0)))
       thisuserblock->setUserImageandrank(thisuser_maprank,map_userpic.value(pair.at(0)));

    QListWidgetItem *item = new QListWidgetItem();
    item->setSizeHint(QSize(120,65));
    item->setFont(QFont("Candara",14, QFont::Normal));
    if(pair.at(0) == thisuser) this->has_madethebet = true;
*/

    if(pair.at(2) == "A"){
        //  ui->friendlylist->insertItem(count,item);
        //  ui->friendlylist->setItemWidget( ui->friendlylist->item(count),thisuserblock);
        //  count++;
          teamawager += wag.toInt();
          ui->totalchips_a->setText("Team A Chips:"+QString().number(teamawager));
    }
    else{
         // ui->enemylist->insertItem(count2,item);
         // ui->enemylist->setItemWidget( ui->enemylist->item(count2),thisuserblock);
         // count2++;
          teambwager += wag.toInt();
          qDebug()<<"Teamwager is "<<teambwager;
          ui->totalchips_b->setText("Team B Chips:"+QString().number(teambwager));
    }

    /*if(map_userpic.contains(pair.at(0))){
        QIcon icon1(":/new/prefix1/resources/profile-frame-small.png");
        QPixmap pixmap= icon1.pixmap(QSize(64,64));
        QImage image = QImage::fromData(map_userpic.value(pair.at(0)), "PNG");
        QSize size;
        qDebug()<<"Image Found..";
        QPixmap pixmap2 = QPixmap::fromImage(image);
        QIcon pcon(pixmap2);
        icon = pcon;
        pixmap2 = pixmap2.scaled(QSize(54,54),Qt::KeepAspectRatio, Qt::SmoothTransformation);
        QPainter painter(&pixmap);
        painter.drawPixmap(6,4,54,54,pixmap2);
        QIcon ppcon(pixmap);
        icon = ppcon;
    }

   // if(pair.at(0) == thisuser) this->has_madethebet = true;

    qDebug()<<"Team type is: "+pair.at(2);
   // QString userranks_str = pair.at(2);
    if(pair.at(2) == "A"){

        QListWidgetItem* QWL = new QListWidgetItem(pair.at(0)+"              Bet :" +pair.at(1)+"\nRank:"+thisuser_maprank);
        QWL->setIcon(icon);

      //  ui->friendlylist->setItemDelegate(new MyDelegate(ui->friendlylist));
        ui->friendlylist->insertItem(count,QWL);
        //ui->friendlylist->verticalScrollBar()->setValue(ui->listWidget->verticalScrollBar()->maximum());
        count++;
        QString wag =pair.at(1).trimmed();
       // qDebug()<<"Teamwager is "<<teamawager;
       // wag.remove(QChar('$'), Qt::CaseInsensitive);
        teamawager += wag.toInt();
        qDebug()<<"Teamwager is "<<teamawager;
        //ui->textEdit->clear();
       ui->totalchips_a->setText("Total Chips:"+QString().number(teamawager));
    }
    else{
        QListWidgetItem* QWL = new QListWidgetItem(pair[0]+"              Bet :" +pair[1]+"\nRank:"+thisuser_maprank);
        QWL->setBackgroundColor(QColor(177,177,177,72));


        QWL->setIcon(icon);
       //QWL->setBackground(QBrush(QImage(":/new/prefix1/resources/Userbets.png")));
      //  ui->friendlylist->setItemDelegate(new MyDelegate(ui->friendlylist));
        ui->enemylist->insertItem(count2,QWL);
        //ui->friendlylist->verticalScrollBar()->setValue(ui->listWidget->verticalScrollBar()->maximum());
        count2++;
        QString wag =pair.at(1).trimmed();
       // wag.remove(QChar('$'), Qt::CaseInsensitive);
        teambwager += wag.toInt();
          qDebug()<<"Teamwager is "<<teambwager;
         //ui->teambwager->setText("$"+QString().number(teambwager)+".0");
        ui->totalchips_b->setText("Total Chips:"+QString().number(teambwager));
    }*/
}

void LockIn::addPlayer(QString plr){

    QListWidgetItem* QWL;
    satdownuserlist<<plr;
    QString thisuser_maprank = "N/A";
    QIcon icon;
    if(map_userranks.contains(plr)){
        thisuser_maprank =  map_userranks.value(plr);
    }
    UserBlockInLockIn* thisuserblc = new UserBlockInLockIn();
    thisuserblc->setName(plr,"20");
    if(map_userpic.contains(plr))
       thisuserblc->setUserImageandrank(thisuser_maprank,map_userpic.value(plr));

    qDebug()<<map_totalgames;

    if(map_totalgames.contains(plr)){
        thisuserblc->setTotalGames(map_totalgames.value(plr));
    }
    if(roomtype == "Troom" || roomtype == "Croom"){
    chatuserholder.contains(plr) ?
        thisuserblc->setOnlineStatus(true):
        thisuserblc->setOnlineStatus(false);
    }
    if(count==0 || count <= count2){
          QListWidgetItem *item = new QListWidgetItem();
          item->setSizeHint(QSize(160,48));
          item->setFont(QFont("Candara",14, QFont::Normal));
          ui->friendlylist->insertItem(count,item);
          ui->friendlylist->setItemWidget( ui->friendlylist->item(count),thisuserblc);
          count++;
          if(plr==currentArenaName){Your_teamtype = "A";}
    }
    else{
        QListWidgetItem *item = new QListWidgetItem();
        item->setSizeHint(QSize(160,48));
        ui->enemylist->insertItem(count2,item);
        ui->enemylist->setItemWidget( ui->enemylist->item(count2),thisuserblc);
        count2++;
        if(plr==currentArenaName){Your_teamtype = "B";}
    }


    /*if(map_userpic.contains(plr)){
        QIcon icon1(":/new/prefix1/resources/profile-frame-small.png");
        QPixmap pixmap= icon1.pixmap(QSize(64,64));
        QImage image = QImage::fromData(map_userpic.value(plr), "PNG");
        QSize size;
        qDebug()<<"Image Found..";
        QPixmap pixmap2 = QPixmap::fromImage(image);
        QIcon pcon(pixmap2);
        icon = pcon;
        pixmap2 = pixmap2.scaled(QSize(54,54),Qt::KeepAspectRatio, Qt::SmoothTransformation);
        QPainter painter(&pixmap);
        painter.drawPixmap(6,4,54,54,pixmap2);
        QIcon ppcon(pixmap);
        icon = ppcon;
    }


    if(count==0 || count <= count2){





        QWL= new QListWidgetItem(plr+"\nRank:"+thisuser_maprank);
        QWL->setBackgroundColor(QColor(177,177,177,72));
        QWL->setIcon(icon); //QIcon(":/new/prefix1/resources/profile-frame-small.png")

         // QWL->setBackground(QBrush(QImage(":/new/prefix1/resources/Userbets.png")));
      //  ui->friendlylist->setItemDelegate(new MyDelegate(ui->friendlylist));
        ui->friendlylist->insertItem(count,QWL);
        //ui->friendlylist->verticalScrollBar()->setValue(ui->listWidget->verticalScrollBar()->maximum());
        count++;
       // teamawager += pair.at(1).toInt();
        //ui->textEdit->clear();
       //  ui->teamawager->setText("$"+QString().number(teamawager)+".0");
        if(plr==thisuser){Your_teamtype = "A";}
    }
    else{

        QWL = new QListWidgetItem(plr+"\nRank:"+thisuser_maprank);
        QWL->setBackgroundColor(QColor(177,177,177,72));
        QWL->setIcon(icon); //QIcon(":/new/prefix1/resources/profile-frame-small.png")
  //QWL->setBackground(QBrush(QImage(":/new/prefix1/resources/Userbets.png")));
      //  ui->friendlylist->setItemDelegate(new MyDelegate(ui->friendlylist));
        ui->enemylist->insertItem(count2,QWL);
        //ui->friendlylist->verticalScrollBar()->setValue(ui->listWidget->verticalScrollBar()->maximum());
        count2++;
         if(plr==thisuser){Your_teamtype = "B";}
       // teambwager += pair.at(1).toInt();
       //  ui->teambwager->setText("$"+QString().number(teambwager)+".0");
    }*/


}

void LockIn::plainRequest(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << textData;

      if(textData == "[\"room_closed\"]"){
         IBox = new InfoBox(this);
         IBox->setMessage("Lobby is full..Please start playing");
         IBox->show();
         return;
     }
      if(textData == "[\"Ticket Created\"]"){
          IBox = new InfoBox(this);
          IBox->setMessage("Referee is called..");
          IBox->show();
          return;
      }
     //if(textData == '["user_added"]'){
         //this user is added..

    // }
    //IBox = new InfoBox(this);
   // IBox->setMessage(textData);
   // IBox->show();


}
void LockIn::loadFinished(){
   QTimer::singleShot(2000, this, SLOT(subscribethisroom()));
   // animatedMsg("Waiting to join the room...");
}
void LockIn::sendChatMsg(QString msg){
    msg.trimmed();
    QString surl = "<url>chatmsg?&nid="+This_Roomname+"&desc="+currentArenaName+": "+msg;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
          connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
          QNetworkRequest request2 = QNetworkRequest(surl);
          QString ss = sessionname+"="+sessionid;
          request2.setRawHeader("Cookie",ss.toUtf8() );
          manager->get(request2);
          ui->usermsg->setText("");
}
void LockIn::keyPressEvent(QKeyEvent *event){
     if(event->key() == Qt::Key_Return){
         if(ui->usermsg->text().length() > 0)
         sendChatMsg(ui->usermsg->text());
     }
}



//This is not used any more
void LockIn::subscribethisroom(){
    //we should wait for 1 min....
 emit joinChannel();
    //nid is the channel name and uid is the user ID
   /* QString surl = "<url>addtochannel?uid="+QString().number(mainid)+"&nid=room_"+cind;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);*/
}

void LockIn::setDetailLabel(QString msg,int minimum){
    ui->lobbydetail_lb->setText(msg);
    QTimer::singleShot(1000, this, SLOT(animatedMsg()));
    minimumWager = minimum;
}
void LockIn::setRedLabel(QString str){
    ui->warning_lbl->setText(str);
}

void LockIn::on_pushButton_3_clicked()
{
    //addPlayerToChatList("edfd");
    createDispute();
}

void LockIn::on_toolButton_clicked()
{
   w->isClosing = true;
    QCoreApplication::exit(0);
}

void LockIn::on_sendtochat_clicked()
{
    sendChatMsg(ui->usermsg->text());
}






/*void LockIn::on_lineEdit_editingFinished()
{
    bool okay = false;
    int money = wagerslide->text().toInt(&okay,10);
    if(okay){
        ui->horizontalSlider->setValue(money);
    }

}*/

void LockIn::on_teamAwin_clicked()
{
    QString desc;
    QString nitems;
    QString wager;
    QString number;
    int intdesc=0;
   // fillEmptyBets();

    for(int i=0;i<betusers.size();i++){
        QStringList pair = betusers.at(i).split(":");
        qDebug()<<betusers.at(i);
        if(pair.at(2) == "B"){ //this is defeated team
            nitems += pair.at(3)+":";
            desc += pair.at(1)+":";
            intdesc += QString(pair.at(1)).toInt();
        }
        else{
            wager += pair.at(3)+":";
            number += pair.at(1)+":";
        }
    }

    if(roomtype == "Troom"){
        QStringList tt = this->lobby_title.split(":");
         qDebug()<<"T rooom winner "<<tt.at(1);
        if(tt.at(1) == matchlevel){
            int betdesc = intdesc;
            betdesc += playercount * currentWagerAmount;
            desc = QString().number(betdesc);
            qDebug()<<"This is the last lobby: "<<desc;
        }
    }

     QString surl = "<url>api/";

    if(roomtype == "Croom"){
        surl += "CGC/setWinner?";
    }
    else if(roomtype == "Troom"){
        surl += "TModule/setWinner?";
    }
    else
         surl += "game/setWinner?";

    if(roomtype == "lroom")
     surl += "nid="+roomname+"&gametype="+currentRoomId+"&desc="+desc+"&nitems="+nitems+"&uid="+QString().number(mainid)+"&wager="+wager+"&number="+number;
    else
      surl += "nid="+roomname+"&roomid="+currentRoomId+"&gametype="+nitems+"&desc="+desc+"&uid="+QString().number(mainid)+"&wager="+wager+"&when="+number;
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

      qDebug()<<"Hey I am here";
       // ViewLobby->show();
       closelockin->hide();
}
bool LockIn::caseInsensitiveLessThan(const QString &s1, const QString &s2){
     QStringList pair = s1.split(":");
     QStringList pair1 = s2.split(":");
     //if(pa)
     if(pair.at(1).toInt() == pair1.at(1).toInt())
         return true;

     return pair.at(1).toInt() > pair1.at(1).toInt();
}
void LockIn::on_teamBwin_clicked()
{
    QString desc;
    QString nitems;
    QString wager;
    QString number;
    int intdesc=0;

   // fillEmptyBets();

    for(int i=0;i<betusers.size();i++){
        QStringList pair = betusers.at(i).split(":");
        qDebug()<<betusers.at(i);
        if(pair.at(2) == "A"){ //this is defeated team
            nitems += pair.at(3)+":";
            desc += pair.at(1)+":";
            intdesc += QString(pair.at(1)).toInt();
        }
        else{
            wager += pair.at(3)+":";
            number += pair.at(1)+":";
        }
    }

    if(roomtype == "Troom"){
        QStringList tt = this->lobby_title.split(":");
         qDebug()<<"T rooom winner "<<tt.at(1);
        if(tt.at(1) == matchlevel){
            int betdesc = intdesc;
            betdesc += playercount * currentWagerAmount;
            desc = QString().number(betdesc);
            qDebug()<<"This is the last lobby: "<<desc;
        }
    }

     QString surl = "<url>api/";

    if(roomtype == "Croom"){
        surl += "CGC/setWinner?";
    }
    else if(roomtype == "Troom"){
        surl += "TModule/setWinner?";
    }
    else
         surl += "game/setWinner?";

  if(roomtype == "lroom")
   surl += "nid="+roomname+"&gametype="+currentRoomId+"&desc="+desc+"&nitems="+nitems+"&uid="+QString().number(mainid)+"&wager="+wager+"&number="+number;
  else
    surl += "nid="+roomname+"&roomid="+currentRoomId+"&gametype="+nitems+"&desc="+desc+"&uid="+QString().number(mainid)+"&wager="+wager+"&when="+number;
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

      qDebug()<<"Hey I am here";
       // ViewLobby->show();
       closelockin->hide();
}
