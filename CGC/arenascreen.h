#ifndef ARENASCREEN_H
#define ARENASCREEN_H

#include <QWidget>
#include <jsonparser.h>
#include <QNetworkReply>
#include <infobox.h>
#include <sampledialog.h>
#include <teamfindjoin.h>
#include <createroom.h>
#include <QTableWidget>
#include <QVBoxLayout>
#include <lockin.h>
#include <arenaprofilecreate.h>
#include <QSignalMapper>
#include <alertdialog.h>
#include <ranksdialog.h>

namespace Ui {
class ArenaScreen;
}

class ArenaScreen : public QWidget
{
    Q_OBJECT
    
public:
    explicit ArenaScreen(QWidget *parent = 0, int type=1);
   virtual ~ArenaScreen();
     Json::Value lobbies;
     Json::Value newsarray;
      QTableWidget* tableWidget;
AlertDialog* alert;
    void setValues(Json::Value news);
    void getRequest(QString surl);
     LockIn *lock;
     bool unlocking;
     bool backtolobby;
     bool issatdown;
     int index;
     QString currentGameName;
     QString currentGameType;
     QString currentRoomId;
     int currentWagerAmount;
     //From lobby to game
     bool hasMovedToGame;
     bool is_subscribed;
     void checkForNewRooms();
     void leavesitdown();
     RanksDialog* RD;
     void setPlayerCount(QString br);
    static bool caseInsensitiveLessThan(const QString &s1, const QString &s2);
signals:
    void moveme(QPoint pt);
public slots:
           void addArenaResponse(QNetworkReply* reply);
           void requestReceived(QNetworkReply* reply);
           void plainRequest(QNetworkReply* reply);
           void allRoomRequest(QNetworkReply* reply);
           void allRoomRequest_2(QNetworkReply* reply);
           void LeaderBoard_per_Arena(QNetworkReply* reply);
           void creatRoomClicked();
           void trycreateroom();
           void onLobbyTableClicked(int r, int c);
           void onReportingLoss();
          //join channel slot
          void addUserToChannel();
          void onAPCClicked();
          void on_GameButton_clicked();
          void on_home();
          void on_gamelabel_clicked(QString cmd);
          void showRanks();
         // void moveme();
private slots:
         void on_createTeamProfile_clicked();
         void on_createArenaProfile_clicked();
         void on_findATeam_clicked();
         void clearArenaStuff();
         void leaveTable();
         void onPlaceButtonClicked();
         void onSitBack();
         void on_Leaderboard_clicked();

void reallyLeaveTable();

         void on_toolButton_2_clicked();

private:
    Ui::ArenaScreen *ui;
    InfoBox* IBox;
    SampleDialog* createTeam;
    TeamFindJoin *TFJ;
    CreateRoom *room;
 QVBoxLayout *final;
 ArenaProfileCreate* APC;
  QSignalMapper *signalMapper;

 //This variable to control whether user has moved to the lobby screen or not...to avoid the crash when news are
 //received and no label to update
bool hasMovedToLobby;
    QPoint mpos;
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);


bool hasMadeAPlaceBetRequest;

 QTableWidgetItem* createTableWidgetItem( const QString& text );



};

#endif // ARENASCREEN_H
