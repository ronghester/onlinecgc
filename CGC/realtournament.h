#ifndef REALTOURNAMENT_H
#define REALTOURNAMENT_H

#include <QWidget>
#include <jsonparser.h>

namespace Ui {
class realTournament;
}

class realTournament : public QWidget
{
    Q_OBJECT
    
public:
    explicit realTournament(QWidget *parent = 0);
    ~realTournament();
    void setValues(Json::Value root);
    Json::Value M_root;
    QString thisttitle;
    
private slots:
    void on_pushButton_clicked();


private:
    Ui::realTournament *ui;
};

#endif // REALTOURNAMENT_H
