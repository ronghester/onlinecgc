#include "profilepage.h"
#include "ui_profilepage.h"
#include "QDebug"
#include <mainwindow.h>
#include <QTimer>
extern int mainid;
extern QString sessionname;
extern QString sessionid;
 extern QString cind;
 extern QString thisuser;
extern QString statusline;
extern int userchips;
extern QByteArray userspic;
//extern QString userimage;
extern MainWindow* w;
extern FriendsContainer* FC;
extern QString thatuser;
extern QString thatuser_id;
 std::vector<arenaobject> availablearenas2;

Profilepage::Profilepage(QWidget *parent,QString user) :
    QWidget(parent),
    ui(new Ui::Profilepage)
{
    ui->setupUi(this);
    testuser = user;
    thatuser = user;
    //teamlist = ui->teammembers;
    username = ui->userlabel;
    count =0;

    QString surl = "<url>userprofilestat?uid="+user;
    //QString surl = "<url>api/views/custom_user_teams?field_team_member_target_id="+QString().number(mainid);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(plainRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
      //CustomDelegate* CD = new CustomDelegate();
       // ui->tableWidget_2->setShowGrid(false);
    // statusline =  statusline.trimmed();
      ui->userlabel->setText("Username: "+user);

      if(FC->isFriend(testuser) || testuser == thisuser){
          ui->addfriendbtn->hide();
      }
     if(user == thisuser){

      ui->label_2->setText(statusline);
      ui->label_10->setText(QString().number(userchips));
      if(!userspic.isEmpty()){

          QImage image = QImage::fromData(userspic, "PNG");
          QIcon icon;
          QSize size;
          if (!image.isNull())
            icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

          QPixmap pixmap = icon.pixmap(QSize(100,120), QIcon::Normal, QIcon::On);
          ui->label_5->setPixmap(pixmap);

          qDebug()<<"Data set ";
         // ui->label_5->setStyleSheet("{border-image: url(\"data:image/PNG;base64,"+userimage+"\")}");
      }
     }
     //availablearenas.clear();
}

Profilepage::~Profilepage()
{
    delete ui;
}

QTableWidgetItem* Profilepage::createTableWidgetItem( const QString& text )
{
    QTableWidgetItem* item = new QTableWidgetItem( text );
    item->setTextAlignment( Qt::AlignCenter );
    return item;
}
void Profilepage::plainRequest(QNetworkReply* reply ){
    try{
   QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
        // qDebug()<<"hey";
         QString username = "No Teams To Show";

         return;
     }
     else if(textData == "[\"\Friend Added\"]"){
        ui->addfriendbtn->hide();
        return;
     }
     Json::Reader reader;
     Json::Value root;
     QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
         if(root.size() == 0) return;

         Json::Value preroot = root.get("global_status",0);
         Json::Value postroot = root.get("history",0);
         Json::Value userarena = root.get("Arenas",0);
         Json::Value  mygames = root.get("mygames",0);
         Json::Value  userinfo = root.get("thisuser",0);
         bool onlinestatus = root.get("online_status",0).asBool();
         onlinestatus ? ui->label->setText("<img height='14' width= '14' src=\":/new/prefix1/resources/status-green.png\"> Online</>") : ui->label->setText("<img height='14' width= '14' src=\":/new/prefix1/resources/white-dot.png\"> Offline</img>");
         if(onlinestatus && FC->isFriend(testuser)){
             FC->updateOnlineStatusOfUser(testuser);
         }
         int ZERO =0;
         Json::Value useritem = userinfo[ZERO];
         QString user_status = QString().fromStdString(useritem.get("status_line","none").asCString());
         QString user_chips = QString().fromStdString(useritem.get("chips","none").asCString());
         QString user_id = QString().fromStdString(useritem.get("userid","none").asCString());
         QString user_rank = QString().fromStdString(useritem.get("userrank","none").asCString());
QString totalchips = QString().fromStdString(useritem.get("totalchips","none").asCString());
         QByteArray thisuserpic = useritem.get("img","none").asCString();
         thisuserpic = QByteArray::fromBase64(thisuserpic.replace(" ","+"));

         int intuserchips = user_chips.toInt();
         ui->label_2->setText(user_status);
         if(intuserchips >0)
           ui->label_10->setText("Chips Earnt : "+user_chips);
         else
             ui->label_10->setText("Chips Earnt : 0");
         if(user_id == QString().number(mainid)){
            userchips = totalchips.toInt();
            w->updateUserChips(QString().number(userchips));
         }
         ui->label_8->setTextFormat(Qt::RichText);
         ui->label_8->setText("Rank : "+user_rank);
         QString thisicon = ":/new/prefix1/resources/"+user_rank+".png";
         ui->rank_amblam->setPixmap(QPixmap(thisicon));

         thatuser_id = user_id;

             QImage image = QImage::fromData(thisuserpic, "PNG");
             QIcon icon;
             QSize size;
             if (!image.isNull())
               icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

             QPixmap pixmap = icon.pixmap(QSize(100,120), QIcon::Normal, QIcon::On);
             ui->label_5->setPixmap(pixmap);



         ui->tableWidget_2->horizontalHeader()->show();
         ui->tableWidget_2->setColumnWidth(0,98);
         ui->tableWidget_2->setColumnWidth(1,54);
         ui->tableWidget_2->setColumnWidth(2,66);
         for(int i=0;i<preroot.size();i++){
              const Json::Value tem = preroot[i];
              QString win = QString().fromStdString(tem.get("win","none").asCString());
              QString lost = QString().fromStdString(tem.get("lost","none").asCString());
              QString total = QString().fromStdString(tem.get("games","none").asCString());
              QTableWidgetItem* tt = createTableWidgetItem(total);
                tt->setFont(QFont("Candara",10, QFont::Bold));
                tt->setForeground(QBrush(Qt::white));
              ui->tableWidget_2->setItem(0,0,tt);
              tt = createTableWidgetItem(win);
              tt->setFont(QFont("Candara",10, QFont::Bold));
              tt->setForeground(QBrush(Qt::green));
               ui->tableWidget_2->setItem(0,1,tt);
               tt = createTableWidgetItem(lost);
               tt->setFont(QFont("Candara",10, QFont::Bold));
               tt->setForeground(QBrush(Qt::red));
                ui->tableWidget_2->setItem(0,2,tt);
                bool okay = false;
                               int totalgames = total.toInt(&okay);
                               int totalwins = win.toInt(&okay);
                               qDebug()<<"Total Games "<<totalgames;
                                qDebug()<<"Total Games "<<totalwins;
                                float percent = 0;
                                if(totalgames != 0) percent = (totalwins*100)/totalgames;

                //Setting % of win
              /*  bool okay = false;
                int totalgames = total.toInt(&okay);
                int totalwins = win.toInt(&okay);

                if(okay){
                  int percent = (totalwins*100)/totalgames;
                  tt = createTableWidgetItem(QString().number(percent)+"%");
                }
                else{
                    tt = createTableWidgetItem("0.0%");
                }
               //
            */
                                tt = createTableWidgetItem(QString().number(percent,'f',2)+"%");
                                tt->setFont(QFont("Candara",10, QFont::Bold));
                                tt->setForeground(QBrush(Qt::white));
                                ui->tableWidget_2->setItem(0,3,tt);
         }
         ui->tableWidget->horizontalHeader()->show();
         ui->tableWidget->setColumnWidth(0,94);
         ui->tableWidget->setColumnWidth(1,92);
         ui->tableWidget->setColumnWidth(2,90);
         ui->tableWidget->setColumnWidth(3,32);
         ui->tableWidget->setColumnWidth(4,44);
         for(int j=0;j<postroot.size();j++){
             ui->tableWidget->insertRow(j);
             const Json::Value tem = postroot[j];
             QString name = QString().fromStdString(tem.get("source_id","none").asCString());
             for(int i=0;i<mygames.size();i++){
                  const Json::Value gems = mygames[i];
                  QString thisnid = QString().fromStdString(gems.get("nid","none").asCString());
                  if(name == thisnid){
                      name = QString().fromStdString(gems.get("node_title","none").asCString());
                      break;
                  }
             }

             QString number = QString().fromStdString(tem.get("gametype","none").asCString());
             QString desc = QString().fromStdString(tem.get("gamedesc","none").asCString());
             QString result = QString().fromStdString(tem.get("result","none").asCString());
             QString pt = QString().fromStdString(tem.get("point","none").asCString());
            // int intpt = pt.toInt();
            // if(intpt<0) pt = "0";
             //////////////////////////
             ui->tableWidget->setItem(j,0,createTableWidgetItem(name));
             ui->tableWidget->setItem(j,1,createTableWidgetItem(number));
             ui->tableWidget->setItem(j,2,createTableWidgetItem(desc));
             ui->tableWidget->setItem(j,3,createTableWidgetItem(result));
             ui->tableWidget->setItem(j,4,createTableWidgetItem(pt));
         }
         int j = postroot.size();
         ui->tableWidget->insertRow(postroot.size());

         for(int i=0;i<mygames.size();i++){
             const Json::Value gems = mygames[i];
             QString thisnid = QString().fromStdString(gems.get("nid","none").asCString());
             QString thisarena = QString().fromStdString(gems.get("node_title","none").asCString());
              QString thisarena_rank = QString().fromStdString(gems.get("rank","none").asCString());
                QString thisarena_name = QString().fromStdString(gems.get("arenaname","none").asCString());
             arenaobject thisobj;
             thisobj.arenacode = thisnid;
             thisobj.arenaname = thisarena;
             thisobj.arenarank = thisarena_rank;
             thisobj.userarenaname = thisarena_name;
             availablearenas2.push_back(thisobj);
         }
         //lets fill the arena now

         for(int k=0;k<userarena.size();k++){
              const Json::Value tem = userarena[k];
              QImage image;
              QString string = QString().fromStdString(tem.get("product_string","none").asCString());
              QString thisarenaname = QString().fromStdString(tem.get("arenaname","none").asCString());

              QString aname = "Solo Profile - '"+thisarenaname+"'";
              QString urank = "unranked";

              int wincount = 0; int losscount=0; int totalchips = 0;
              // qDebug()<<aname;
              for(int i=0;i<mygames.size();i++){
                   const Json::Value gems = mygames[i];
                   QString thisnid = QString().fromStdString(gems.get("nid","none").asCString());
                   QString thisgamename = QString().fromStdString(gems.get("node_title","none").asCString());
                   if(thisnid == string){
                       QByteArray by = QByteArray::fromBase64(gems.get("img","none").asCString());
                       image = QImage::fromData(by, "PNG");
                       image.scaled(52,52);
                       break;
                   }

              }
               for(int j=0;j<postroot.size();j++){
                    const Json::Value usr = postroot[j];
                     QString name = QString().fromStdString(usr.get("source_id","none").asCString());
                     //qDebug()<<j<<name<<"Matching to"<<string;
                     if(name == string){
                          QString result = QString().fromStdString(usr.get("result","none").asCString());
                         // qDebug()<<"result is:"<<result;
                          if(result == "W") wincount++;
                          else losscount++;
                          totalchips += QString().fromStdString(usr.get("point","0").asCString()).toInt();

                         // qDebug()<<"result chips:"<<totalchips;
                     }
               }

               for(int h=0;h<availablearenas2.size();h++){
                   if(availablearenas2[h].arenacode == string)
                       urank = availablearenas2[h].arenarank;
               }

               QString strprp = aname+"\n"+urank+" Chips Earnt:"+QString::number(totalchips)+"\n"+"Wins:"+QString::number(wincount)+" Losses:"+QString::number(losscount);
               qDebug()<<strprp;
               QListWidgetItem* QWL = new QListWidgetItem(strprp);
               QWL->setFont(QFont("Candara",10, QFont::Normal));
               QWL->setTextColor(QColor("#fff"));

               QIcon ic = QIcon(QPixmap().fromImage(image));
                 ic.actualSize(QSize(295,295));
                 QWL->setIcon(ic);

                  ui->arenalist->insertItem(k,QWL);


         }

     }
    }
    catch(...){
         // signalMapper = new QSignalMapper(this);
    }
}
void Profilepage::addFriendRequest(QNetworkReply* reply ){

   QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
     qDebug() << "RM"+ textData;
     if(textData.length() < 5) {
         qDebug()<<"hey";
         QString username = "Some Error";

         return;
     }

     Json::Reader reader;
     Json::Value root;
     QVBoxLayout *newsbox=new QVBoxLayout();
     bool parsingSuccessful = reader.parse(textData.toStdString(), root );

     if(!parsingSuccessful || root.size() == 0){
           return;
     }
     else{
         if(root.size() == 0) return;
        int index = 0;
         const Json::Value friends = root[index];
         QString username = QString().fromStdString(friends.get("name","none").asCString());
         QString userchips = QString().fromStdString(friends.get("chips","none").asCString());
         QString onlinestatus = QString().fromStdString(friends.get("online","none").asCString());
         QString userstatusline = QString().fromStdString(friends.get("statusline","none").asCString());
         QString userrank = QString().fromStdString(friends.get("userrank","none").asCString());

         QByteArray tem = friends.get("friendsimg","none").asCString();
         QByteArray newimage = QByteArray::fromBase64(tem.replace(" ","+"));

         FC->friendsimages.push_back(newimage);

         qDebug()<<username<<" ** "<<userchips<< " ** " <<onlinestatus<<"**"<<userrank;
        // FriendsWidget* thisfrnd = new FriendsWidget(0);
        FC->addFriendElement(username,userchips,userstatusline,onlinestatus,userrank);

       //  myFriends.push_back(thisfrnd);
          QTimer::singleShot(2000, w, SLOT(createFriendsWidgets()));

     }

}

void Profilepage::createFriendsPanel(){
      w->createFriendsWidgets();
}

void Profilepage::on_addfriendbtn_clicked()
{
      QString surl = "<url>addFriend?uid="+thisuser+"&nid="+testuser;
      //QString surl = "<url>api/views/custom_user_teams?field_team_member_target_id="+QString().number(mainid);
      QNetworkAccessManager *manager = new QNetworkAccessManager(this);
      connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(addFriendRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
}

void Profilepage::on_pushButton_3_clicked()
{
     QTimer::singleShot(2000, w, SLOT(createChallengeToFriend()));
}
