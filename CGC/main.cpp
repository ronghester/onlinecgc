#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QCleanlooksStyle>


class MyApplication : public QApplication {
public:
  MyApplication(int& argc, char ** argv) :
    QApplication(argc, argv) { }
  virtual ~MyApplication() { }

  // reimplemented from QApplication so we can throw exceptions in slots
  virtual bool notify(QObject * receiver, QEvent * event) {
    try {
      return QApplication::notify(receiver, event);
    } catch(std::exception& e) {
      qCritical() << "Exception thrown:" << e.what();
    }
    return false;
  }
};

MainWindow* w;
int main(int argc, char *argv[])
{

    MyApplication a(argc, argv);
  //  QApplication::setStyle(new QCleanlooksStyle);
    a.setStyleSheet("QStatusBar::item { border: 0px solid black };");
     //  QApplication::setStyle(new QWindowsStyle);
     QApplication::addLibraryPath("plugins");
    //MainWindow w;
     w = new MainWindow(0);
    //w.show();
    bool first = false;
    w->on_loginButton_clicked();
   return a.exec();

      /*  while (!w->isClosing)
        {
                //w.startTime = w.mTimer->getMillisecondsCPU();
            if(!first && w->isloggedin){ w->onLoginSuccess();first = true;}

            w->runLoop();
            a.processEvents();
               // w.renderOgre();
               // w.timeSinceLastFrame =  w.mTimer->getMillisecondsCPU() - w.startTime;
                //Sleep(10);
        }

   return 0;*/
    //return a.exec();
}
