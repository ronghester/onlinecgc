#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <loginbox.h>
#include <leaderboard.h>
#include <QNetworkReply>
#include <QSignalMapper>
#include <QAction>
#include <arenascreen.h>
#include <infobox.h>
#include <profilepage.h>
#include <tournament.h>
#include <registerdialog.h>
#include <QScrollArea>
#include <editprofile.h>
#include <friendswidget.h>
#include <friendscontainer.h>
#include <challengeuser.h>
#include <challengepage.h>
#include <lockin.h>
#include <alertdialog.h>
#include <chatdialog.h>
#include <tournamentlobby.h>
#include <QMap>
#include <QSystemTrayIcon>
namespace Ui {
class MainWindow;
}

struct arenaobject{
    QString arenaname;
    QString arenacode;
    QString arenarank;
    QString userarenaname;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
     Json::Value lastValue;
     bool isClosing;
     QString whichuser;
     bool lockflag;
     bool isloggedin;
     void mouseMoveEvent(QMouseEvent *);
       QPoint mpos;
       void setUserImage();
       void setUserRank(QString rank,QString label);
       void closeLockRoom();
       void openLockRoom();
       void setHeaderNews(QString subject,QString body);
       void createChatFriends(QString Fname,QString status,int Findex);
       void onFriendIndicatorClicked(QString Fname,bool status);
       void createChatFriends_adv(QString channel);
       void onTournamentLevel_1(Json::Value r);
       void showTournamentBracket(int teams,QMap<QString,QString> lobbies,int lsize);
       void ChatDialog_init(QString Fname);
       void addto_admin_Channel(QString nid,QString roomtype,QString ticketid);
       void updateUserChips(QString betamount);
       void showNotification(QString msg);
public slots:
     void on_loginButton_clicked();
     void onLoginSuccess();
     void onLoginClose();
     void runLoop();
     void on_profileButton_clicked();
     void createFriendsWidgets();
     void createChallengeToFriend();
     void createLockInScreen(Json::Value info);
     void Tournament_LockInScreen(QString wageramount,QString nid,QString gametype,QString title,QString roomname,QString matchlevel,int playerct);
     void enterLockIn_asAdmin(QString roomid,QString room_type,QString nid);
     void showLastTournamentLobyy();
     void onreInitArena();
 void startChallenge();
private slots:
    void on_toolButton_2_clicked();
    void rumble();
    void on_leaderboardButton_clicked();
    void on_competeButton_clicked();
    void on_toolButton_3_clicked();
    void on_gamelabel_clicked(QString);
    void on_toolButton_clicked();
    void requestReceived(QNetworkReply* reply);
    void requestReceived2(QNetworkReply* reply);
    void on_tournamentButton_clicked();
    void on_arenaButton_clicked();
    void on_pushButton_8_clicked();
    void processFriendsInfo(QNetworkReply* reply);
    void challengeRequest2(QNetworkReply* reply);
    void on_registerButton_clicked();
    void on_useroption_clicked();
    void challengeRequest(QNetworkReply* reply );
    void goToHome();
    void doSomething(QListWidgetItem *item);

    void quit();
    void logout();
    void on_label_linkActivated(const QString &link);

    void on_toolButton_4_clicked();
    void on_show_hide( QSystemTrayIcon::ActivationReason reason );

    void on_toolButton_4_pressed();

    void on_pushButton_3_clicked();

    void on_username_clicked();



protected:
     LoginBox *lb;
     LeaderBoard *leaderboard;
     ArenaScreen *arena;
     QSystemTrayIcon* m_tray_icon ;
     virtual void resizeEvent(QResizeEvent* event);

private:
    Ui::MainWindow *ui;
    LockIn* lock;
    QSignalMapper *signalMapper;
    InfoBox *IBox;
    Profilepage* pp;
    EditProfile* EditP;
    TournamentLobby* TL;
    //LockIn *lock;
    Tournament *tour;
    RegisterDialog* reg;
    Challengeuser* challengeuser_widget;
    ChallengePage* CP;
    void mousePressEvent(QMouseEvent *);
    void RemoveLayout (QWidget* widget);
    void getRequest(QString surl);
    QScrollArea* prepareGames(Json::Value val);
    QListWidgetItem* getFriendsWidget(QString fname);
    void getFriendsInfo(QString nid);
    void addto_Challenge_Channel(QString gametype,QString challengeID,QString type);
    Mediator* M;
    AlertDialog* alert;
    ChatDialog* chatdgl;
};

#endif // MAINWINDOW_H
