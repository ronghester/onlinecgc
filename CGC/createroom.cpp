#include "createroom.h"
#include "ui_createroom.h"

CreateRoom::CreateRoom(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint),
    ui(new Ui::CreateRoom)
{
    ui->setupUi(this);
    onceclicked = false;
    setTabOrder(ui->label,ui->comboBox);
    setTabOrder(ui->lineEdit_2,ui->lineEdit_5);

}

CreateRoom::~CreateRoom()
{
    delete ui;
}
QPushButton* CreateRoom::getButton(){
    return ui->pushButton;
}

QString CreateRoom::getURLString(QString nid){
  //  title=john&desc=sample&when=2014-12-24%2018:00:00&number=4V4&gametype=0&nid=8
    if(ui->lineEdit->text().isEmpty() ||ui->lineEdit_5->text().isEmpty()   )
        return "null";

            QString num = ui->comboBox->currentText();
            QString gametype = "0";
             QString title = ui->lineEdit->text();
             QString desc = ui->lineEdit_2->text();
             QString time = "2014-12-24%2018:00:00";
             QString wager = ui->lineEdit_5->text();
             bool ok;
             int w = wager.toInt(&ok);
             bool bets = w % 10 ==0 ? true : false;
             if(!ok) return "NAN";
             if(w < 10) return "NAN";
             if(!bets) return "NAN";

    return "title="+title+"&desc="+desc+"&when="+time+"&number="+num+"&gametype="+gametype+"&nid="+nid+"&wager="+wager;
}

void CreateRoom::on_toolButton_clicked()
{
    this->hide();
}



void CreateRoom::on_pushButton_clicked()
{

}
