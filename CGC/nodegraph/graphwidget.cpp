/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: <url>www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "graphwidget.h"
#include "edge.h"
#include "node.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QWheelEvent>
#include <QDebug>
#include <math.h>

GraphWidget::GraphWidget(int teams,QMap<QString,QString> lobbies,int lsize)
    : timerId(0)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(-10, 0, 500, 400);
    setScene(scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setResizeAnchor(AnchorViewCenter);
QStringList alphanumeric;
alphanumeric << "A" << "B" << "C" << "D" << "E" << "F" <<"G"<<"H"<<"I"<<"J"<<"K"<<"L"<<"M"<<"N"<<"O"<<"P"<<"Q"<<"R";
    std::vector<Node*> allnodes;
    std::vector<int> sets;
    int reducingteam = 0;
    int j=0;int i=0;
    while(reducingteam != 1){

        for(i=0;i<teams;i++){
             Node *node1 = new Node(this," ");
             scene->addItem(node1);
             node1->setPos(j*160, (45*j)+(i*60));// node1->setPos(j*160, (75*j)+(i*50));
             allnodes.push_back(node1);
        }
        sets.push_back(allnodes.size());
      //  teams = teams/2;
        j++;
        reducingteam = teams/2;
        teams = reducingteam;
        qDebug()<<"Reducing Team is: "<<reducingteam;
    }

    int m = 0;int s;int constt=0;

    for(s=0;s<sets.size()-1;s++){
        int thisset = sets[s];int n = 0;
        while(m < thisset){
            qDebug()<<"m: "<<m<<" n: "<<n <<"Thisset; "<<thisset;
            Node * teamA = allnodes[m];          
            Node * teamB = allnodes[m+1];
            if(s==0){
                teamA->title = "Team-"+alphanumeric.at(constt);
                teamB->title = "Team-"+alphanumeric.at(constt+1);
            }
            //Node in next level
            Node *teamAB = allnodes[thisset+n];
            QString lobbyname = "Match:"+QString().number(s+1)+":"+QString().number(n)+":"+QString().number(lsize);
            qDebug()<<lobbyname;
            qDebug()<<lobbies;
            if(lobbies.contains(lobbyname)){
                 QString lname = lobbies.value(lobbyname);
                 qDebug()<<"Team Name is :"+lname+" Title:"+teamB->title;
                 if(lname == "A")
                     teamAB->title = "Team-"+alphanumeric.at(constt);
                 else if(lname == "B")
                     teamAB->title = "Team-"+alphanumeric.at(constt+1);
            }
            scene->addItem(new Edge(allnodes[m], teamAB));
            scene->addItem(new Edge(allnodes[m+1], teamAB));
            m+=2;n++;
            constt+=2;
        }
    }

    QString lobbyname = "Match:"+QString().number(s+1)+":"+QString().number(0)+":"+QString().number(lsize);

    Node *node1 = new Node(this,"");
    if(lobbies.contains(lobbyname)){
        QString lname = lobbies.value(lobbyname);
        if(lname == "A")  {
         Node *teamA = allnodes[allnodes.size()-1];
         node1->title = teamA->title;
        }
        else{
            Node *teamB = allnodes[allnodes.size()-2];
            node1->title = teamB->title;
        }
    }
    scene->addItem(node1);
    Node *test = allnodes[allnodes.size()-1];
    node1->setPos(test->pos().x()+160, test->pos().y()-25);
    scene->addItem(new Edge(allnodes[allnodes.size()-1], node1));
    scene->addItem(new Edge(allnodes[allnodes.size()-2], node1));

  //scene->addItem(centerNode);


    //scene->addItem(new Edge(node6, node8));
   // scene->addItem(new Edge(node7, node8));


   // node8->setPos(150, -100);

    scale(qreal(0.9), qreal(0.82));
    setMinimumSize(500, 400);
    setWindowTitle(tr("Elastic Nodes"));
}

void GraphWidget::itemMoved()
{
    if (!timerId)
        timerId = startTimer(1000 / 25);
}

void GraphWidget::keyPressEvent(QKeyEvent *event)
{

}

void GraphWidget::timerEvent(QTimerEvent *event)
{
   /* Q_UNUSED(event);

    QList<Node *> nodes;
    foreach (QGraphicsItem *item, scene()->items()) {
        if (Node *node = qgraphicsitem_cast<Node *>(item))
            nodes << node;
    }

    foreach (Node *node, nodes)
        node->calculateForces();

    bool itemsMoved = false;
    foreach (Node *node, nodes) {
        if (node->advance())
            itemsMoved = true;
    }

    if (!itemsMoved) {
        killTimer(timerId);
        timerId = 0;
    }*/
}

void GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, -event->delta() / 240.0));
}

void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Shadow
   /* QRectF sceneRect = this->sceneRect();
    QRectF rightShadow(sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height());
    QRectF bottomShadow(sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5);
    if (rightShadow.intersects(rect) || rightShadow.contains(rect))
	painter->fillRect(rightShadow, Qt::darkGray);
    if (bottomShadow.intersects(rect) || bottomShadow.contains(rect))
    painter->fillRect(bottomShadow, Qt::darkGray);

    // Fill
    QLinearGradient gradient(sceneRect.topLeft(), sceneRect.bottomRight());
    gradient.setColorAt(0, QColor(30,30,30));
    gradient.setColorAt(1, Qt::lightGray);
    painter->fillRect(rect.intersect(sceneRect), gradient);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);*/

    // Text


}

void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    scale(scaleFactor, scaleFactor);
}
