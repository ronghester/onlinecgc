/********************************************************************************
** Form generated from reading UI file 'challengeuser.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHALLENGEUSER_H
#define UI_CHALLENGEUSER_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Challengeuser
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_2;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLineEdit *challenge_user;
    QLabel *label_3;
    QComboBox *arenacombo;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLineEdit *desc;
    QDateTimeEdit *dateTimeEdit;
    QLineEdit *lineEdit_3;
    QFrame *frame_3;
    QFrame *frame_4;
    QComboBox *comboBox_2;
    QLabel *label_8;
    QPushButton *createChallenge;

    void setupUi(QWidget *Challengeuser)
    {
        if (Challengeuser->objectName().isEmpty())
            Challengeuser->setObjectName(QString::fromUtf8("Challengeuser"));
        Challengeuser->resize(586, 444);
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(12);
        Challengeuser->setFont(font);
        Challengeuser->setStyleSheet(QString::fromUtf8("\n"
"QLineEdit{\n"
"height:18;\n"
"	background-color: rgb(255, 255, 255);\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(Challengeuser);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(Challengeuser);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(223, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(222, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addWidget(frame);

        frame_2 = new QFrame(Challengeuser);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setStyleSheet(QString::fromUtf8(""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        challenge_user = new QLineEdit(frame_2);
        challenge_user->setObjectName(QString::fromUtf8("challenge_user"));

        gridLayout->addWidget(challenge_user, 0, 1, 1, 1);

        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        arenacombo = new QComboBox(frame_2);
        arenacombo->setObjectName(QString::fromUtf8("arenacombo"));
        arenacombo->setStyleSheet(QString::fromUtf8(""));

        gridLayout->addWidget(arenacombo, 1, 1, 1, 1);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        label_5 = new QLabel(frame_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        label_6 = new QLabel(frame_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 5, 0, 1, 1);

        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 7, 0, 1, 1);

        desc = new QLineEdit(frame_2);
        desc->setObjectName(QString::fromUtf8("desc"));

        gridLayout->addWidget(desc, 3, 1, 1, 1);

        dateTimeEdit = new QDateTimeEdit(frame_2);
        dateTimeEdit->setObjectName(QString::fromUtf8("dateTimeEdit"));
        dateTimeEdit->setDate(QDate(2014, 9, 13));
        dateTimeEdit->setTime(QTime(0, 0, 0));
        dateTimeEdit->setCalendarPopup(true);

        gridLayout->addWidget(dateTimeEdit, 5, 1, 1, 1);

        lineEdit_3 = new QLineEdit(frame_2);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        gridLayout->addWidget(lineEdit_3, 7, 1, 1, 1);

        frame_3 = new QFrame(frame_2);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(200, 0));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(frame_3, 1, 2, 1, 1);

        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy1);
        frame_4->setMinimumSize(QSize(0, 126));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(frame_4, 9, 0, 1, 1);

        comboBox_2 = new QComboBox(frame_2);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout->addWidget(comboBox_2, 2, 1, 1, 1);

        label_8 = new QLabel(frame_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 8, 0, 1, 1);

        createChallenge = new QPushButton(frame_2);
        createChallenge->setObjectName(QString::fromUtf8("createChallenge"));

        gridLayout->addWidget(createChallenge, 8, 1, 1, 1);


        verticalLayout->addWidget(frame_2);


        retranslateUi(Challengeuser);

        QMetaObject::connectSlotsByName(Challengeuser);
    } // setupUi

    void retranslateUi(QWidget *Challengeuser)
    {
        Challengeuser->setWindowTitle(QApplication::translate("Challengeuser", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Challengeuser", "Challenge User", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Challengeuser", "User Name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Challengeuser", "Arena", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Challengeuser", "Players", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Challengeuser", "Description", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Challengeuser", "When", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("Challengeuser", "Wager", 0, QApplication::UnicodeUTF8));
        dateTimeEdit->setDisplayFormat(QApplication::translate("Challengeuser", "MM/dd/yyyy hh:mm", 0, QApplication::UnicodeUTF8));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("Challengeuser", "1V1", 0, QApplication::UnicodeUTF8)
        );
        label_8->setText(QString());
        createChallenge->setText(QApplication::translate("Challengeuser", "Challenge User", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Challengeuser: public Ui_Challengeuser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHALLENGEUSER_H
