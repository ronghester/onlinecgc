#ifndef CHALLENGEUSER_H
#define CHALLENGEUSER_H

#include <QWidget>
#include <QNetworkReply>
#include <infobox.h>
namespace Ui {
class Challengeuser;
}

class Challengeuser : public QWidget
{
    Q_OBJECT

    public:
        explicit Challengeuser(QWidget *parent = 0,QString friendsname=0);
        ~Challengeuser();
    private slots:
        void Response(QNetworkReply* QNR);
        void on_createChallenge_clicked();

private:
        Ui::Challengeuser *ui;
        void GetRequest(QString url);
        QString friendsname;
          InfoBox *IBox;
};

#endif // CHALLENGEUSER_H
