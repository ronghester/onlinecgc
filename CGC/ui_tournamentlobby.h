/********************************************************************************
** Form generated from reading UI file 'tournamentlobby.ui'
**
** Created: Fri 24. Apr 23:09:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOURNAMENTLOBBY_H
#define UI_TOURNAMENTLOBBY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TournamentLobby
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_7;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QPushButton *join_tlobby;
    QFrame *frame;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_5;
    QVBoxLayout *verticalLayout_4;
    QLabel *t_logo;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_3;
    QLabel *tname;
    QLabel *tdesc;
    QLabel *starttime;
    QLabel *tfee;
    QFrame *frame_6;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_5;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_2;
    QPushButton *signup;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_2;
    QTableWidget *tableWidget;

    void setupUi(QWidget *TournamentLobby)
    {
        if (TournamentLobby->objectName().isEmpty())
            TournamentLobby->setObjectName(QString::fromUtf8("TournamentLobby"));
        TournamentLobby->resize(553, 351);
        TournamentLobby->setStyleSheet(QString::fromUtf8("#TournamentLobby{\n"
"background-color:  rgb(38, 38, 38);\n"
"border:none;\n"
"\n"
"}\n"
"QLabel{\n"
"	font: 11pt \"Candara\";\n"
"color:#fff;\n"
"}\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(TournamentLobby);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_7 = new QFrame(TournamentLobby);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setStyleSheet(QString::fromUtf8("border:none;"));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_7);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(frame_7);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        join_tlobby = new QPushButton(frame_7);
        join_tlobby->setObjectName(QString::fromUtf8("join_tlobby"));
        join_tlobby->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/placebet.png);\n"
"height:32px;\n"
"width:94px;"));

        horizontalLayout_2->addWidget(join_tlobby);


        verticalLayout->addWidget(frame_7);

        frame = new QFrame(TournamentLobby);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("border:none;"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame);
        verticalLayout_2->setSpacing(4);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame_3 = new QFrame(frame);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border: 1px solid #fff;\n"
"}"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_3);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(2, 2, 2, 2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame_5 = new QFrame(frame_3);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setStyleSheet(QString::fromUtf8("#frame{\n"
"border-left:1px solid #fff;\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"border-right:none;\n"
"}"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_5);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        t_logo = new QLabel(frame_5);
        t_logo->setObjectName(QString::fromUtf8("t_logo"));

        verticalLayout_4->addWidget(t_logo);


        horizontalLayout->addWidget(frame_5);

        frame_4 = new QFrame(frame_3);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setStyleSheet(QString::fromUtf8("#frame_2{\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"border-left:none;\n"
"}"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_4);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        tname = new QLabel(frame_4);
        tname->setObjectName(QString::fromUtf8("tname"));
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        font.setPointSize(14);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        tname->setFont(font);
        tname->setStyleSheet(QString::fromUtf8("font: 75 14pt \"Candara\";"));

        verticalLayout_3->addWidget(tname);

        tdesc = new QLabel(frame_4);
        tdesc->setObjectName(QString::fromUtf8("tdesc"));

        verticalLayout_3->addWidget(tdesc);

        starttime = new QLabel(frame_4);
        starttime->setObjectName(QString::fromUtf8("starttime"));

        verticalLayout_3->addWidget(starttime);

        tfee = new QLabel(frame_4);
        tfee->setObjectName(QString::fromUtf8("tfee"));

        verticalLayout_3->addWidget(tfee);


        horizontalLayout->addWidget(frame_4);

        frame_6 = new QFrame(frame_3);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border-right:1px solid #fff;\n"
"border-top:1px solid #fff;\n"
"border-bottom:1px solid #fff;\n"
"}"));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_5 = new QLabel(frame_6);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setStyleSheet(QString::fromUtf8("font: 75 14pt \"Candara\";"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_5->addWidget(label_5);

        verticalSpacer = new QSpacerItem(20, 25, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        pushButton_2 = new QPushButton(frame_6);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/placebet.png);\n"
"height:28px;"));

        verticalLayout_5->addWidget(pushButton_2);

        signup = new QPushButton(frame_6);
        signup->setObjectName(QString::fromUtf8("signup"));
        signup->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/placebet.png);\n"
"height:28px;"));

        verticalLayout_5->addWidget(signup);


        horizontalLayout->addWidget(frame_6);


        verticalLayout_2->addWidget(frame_3);


        verticalLayout->addWidget(frame);

        frame_2 = new QFrame(TournamentLobby);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setStyleSheet(QString::fromUtf8("border:none;"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_6->addWidget(label_2);

        tableWidget = new QTableWidget(frame_2);
        if (tableWidget->rowCount() < 2)
            tableWidget->setRowCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setMaximumSize(QSize(16777215, 16777215));
        tableWidget->setStyleSheet(QString::fromUtf8("QTableView{\n"
"\n"
"background-color:rgba(38, 38, 38,90);\n"
"\n"
"}\n"
"QHeaderView{\n"
"background-color:rgb(40,40,42);\n"
"}\n"
"\n"
"QHeaderView::section\n"
"{\n"
"\n"
"background-color:rgba(38, 38, 38,90);\n"
"color: white;\n"
"\n"
"text-align: right;\n"
"font-family: candara;\n"
"font-size:13px;\n"
"}\n"
"\n"
"QTableView::item {\n"
" background-color:rgba(38, 38, 38,43);\n"
"font: 11pt \"Candara\";\n"
"color: #fff;\n"
"padding:12px;\n"
"margin:4px;\n"
"border:1px solid #fff;\n"
"}\n"
"\n"
"QTableWidget::item:selected{ background-color: ffa02f; }\n"
""));
        tableWidget->setFrameShadow(QFrame::Plain);
        tableWidget->setShowGrid(false);
        tableWidget->setCornerButtonEnabled(false);
        tableWidget->horizontalHeader()->setVisible(false);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setHighlightSections(true);

        verticalLayout_6->addWidget(tableWidget);


        verticalLayout->addWidget(frame_2);


        retranslateUi(TournamentLobby);

        QMetaObject::connectSlotsByName(TournamentLobby);
    } // setupUi

    void retranslateUi(QWidget *TournamentLobby)
    {
        TournamentLobby->setWindowTitle(QApplication::translate("TournamentLobby", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TournamentLobby", "Tournament Lobby", 0, QApplication::UnicodeUTF8));
        join_tlobby->setText(QApplication::translate("TournamentLobby", "Join Lobby", 0, QApplication::UnicodeUTF8));
        t_logo->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        tname->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        tdesc->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        starttime->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        tfee->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("TournamentLobby", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("TournamentLobby", "Knockout Bracket", 0, QApplication::UnicodeUTF8));
        signup->setText(QApplication::translate("TournamentLobby", "Sign Up", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("TournamentLobby", "Players & Teams", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("TournamentLobby", "New Row", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->verticalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("TournamentLobby", "New Row", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TournamentLobby: public Ui_TournamentLobby {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOURNAMENTLOBBY_H
