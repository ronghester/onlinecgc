#ifndef FRIENDSWIDGET_H
#define FRIENDSWIDGET_H

#include <QWidget>

namespace Ui {
class FriendsWidget;
}

class FriendsWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit FriendsWidget(QWidget *parent = 0);
    ~FriendsWidget();
    void setValues(QString name,QString statusline,QString chips,QString status,QString mrank);
    void updateStatus(QString name);
    void setImage(QByteArray userpic);
QString status;
protected:
    // re-implement processing of mouse events
    void mouseReleaseEvent ( QMouseEvent * e );
//    void mousePressEvent ( QMouseEvent * e );
 void mouseDoubleClickEvent(QMouseEvent *e);

private:
    Ui::FriendsWidget *ui;
};

#endif // FRIENDSWIDGET_H
