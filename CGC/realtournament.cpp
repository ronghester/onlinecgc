#include "realtournament.h"
#include "ui_realtournament.h"
#include <QDebug>
#include <mainwindow.h>
extern MainWindow* w;
extern QString currentArenaName;
extern std::vector<arenaobject> availablearenas;
 extern QString cind;
 extern QString currentTournament;

realTournament::realTournament(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::realTournament)
{
    ui->setupUi(this);
}

realTournament::~realTournament()
{
    delete ui;
}

void realTournament::on_pushButton_clicked()
{
    w->onTournamentLevel_1(M_root);
    currentTournament = thisttitle;
}
void realTournament::setValues(Json::Value root){

         M_root = root;
         const Json::Value tem = root;
         QString title = QString().fromStdString(tem.get("title","none").asCString());
         thisttitle = title;
         qDebug()<<"Title: "<<title;
         QString des = QString().fromStdString(tem.get("body","none").asCString());
         QString gametype = QString().fromStdString(tem.get("gametype","none").asCString());
         QString playercount = QString().fromStdString(tem.get("playercount","none").asCString());
         QString wager = QString().fromStdString(tem.get("wager","none").asCString());
         QString time = QString().fromStdString(tem.get("time","none").asCString());
         QByteArray by = QByteArray::fromBase64(tem.get("arenaimage","none").asCString());
         QImage image = QImage::fromData(by, "JPG");
         QString arenatitle = QString().fromStdString(tem.get("arenatitle","none").asCString());

           Json::Value players = tem.get("players",0);
         for(int h=0;h<availablearenas.size();h++){
             if(availablearenas[h].arenaname == arenatitle){
                 cind = availablearenas[h].arenacode;
                currentArenaName = availablearenas[h].userarenaname;
             }
         }
         ui->tname->setText(title);
         ui->tname->setFont(QFont("Candara",12));
         ui->tdesc->setText(des);
         ui->tfee->setText("Entry Fee "+wager+" Chips");
         ui->starttime->setText("Start Time: "+time);
         if(players!=0)
            ui->label_5->setText(QString().number(players.size())+"/"+playercount);
         else
             ui->label_5->setText("0/"+playercount);
          ui->label_5->setFont(QFont("Candara",12));
          QSize buttonSize(114,102);
          ui->t_logo->setFixedSize(buttonSize);
          ui->t_logo->setStyleSheet("border-image: url(:/new/prefix1/resources/games-back.png);");
          ui->t_logo->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
          QString html = "<img style='display:block; width:100px;height:100px;' id='base64image'";
          QString part2 = "src='data:image/jpeg;base64,"+QString().fromStdString(tem.get("arenaimage","none").asCString())+"'/><br/><br/>";
          ui->t_logo->setTextFormat(Qt::RichText);
          ui->t_logo->setText(html+part2);

}

