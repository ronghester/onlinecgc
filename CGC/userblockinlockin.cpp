#include "userblockinlockin.h"
#include "ui_userblockinlockin.h"

UserBlockInLockIn::UserBlockInLockIn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserBlockInLockIn)
{
    ui->setupUi(this);
}
QString UserBlockInLockIn::getUsername(){
    return ui->username->text();
}

void UserBlockInLockIn::setBetAmount(QString betamount){
    ui->betamount->show();
    ui->label->show();
     ui->label->setText(betamount);
}
void UserBlockInLockIn::setTotalGames(QString games){
    ui->totalgames->setText("Games Played: "+games);
}

void UserBlockInLockIn::setName(QString username,QString totalgames){
    ui->username->setText(username);
    //ui->totalgames->setText(totalgames+" Games Played");
    ui->betamount->hide();
    ui->label->hide();
}
void UserBlockInLockIn::setName(QString username,QString totalgames,QString bet){
    ui->username->setText(username);
   // ui->totalgames->setText(totalgames+" Games Played");
    ui->label->setText(bet);
}

void UserBlockInLockIn::setOnlineStatus(bool status){
    if(status)
         ui->onlinestatus->setPixmap(QPixmap(":/new/prefix1/resources/status-green.png"));
    else
        ui->onlinestatus->setPixmap(QPixmap(":/new/prefix1/resources/white-dot.png"));


}

void UserBlockInLockIn::setRank(QString mrank){
    if(mrank != "Unranked"){
    QString thisicon = ":/new/prefix1/resources/"+mrank+".png";
    ui->userrank->setText("");
    ui->userrank->setPixmap(QPixmap(thisicon));
    }
    else{
        ui->userrank->setText("n/a");
    }

}
void UserBlockInLockIn::setUserImage(QByteArray userpic){
    QImage image = QImage::fromData(userpic, "PNG");
    QIcon icon;
    QSize size;
    if (!image.isNull())
      icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

    QPixmap pixmap = icon.pixmap(QSize(42,52), QIcon::Normal, QIcon::On);
      ui->userpic->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
      // ui->userpic->setStyleSheet("border-image:url(:/new/prefix1/resources/newframe.png)");
      ui->userpic->setPixmap(pixmap);
}

void UserBlockInLockIn::setUserImageandrank(QString mrank,QByteArray userpic){
    if(mrank != "Unranked"){
    QString thisicon = ":/new/prefix1/resources/"+mrank+".png";
    ui->userrank->setPixmap(QPixmap(thisicon));
    }
    ui->userpic->setStyleSheet("border-image:url(:/new/prefix1/resources/newframe.png)");
   QImage image = QImage::fromData(userpic, "PNG");
   QIcon icon;
   QSize size;
   if (!image.isNull())
     icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

   QPixmap pixmap = icon.pixmap(QSize(42,52), QIcon::Normal, QIcon::On);
     ui->userpic->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
     ui->userpic->setPixmap(pixmap);
}





UserBlockInLockIn::~UserBlockInLockIn()
{
    delete ui;
}
