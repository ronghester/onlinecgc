/********************************************************************************
** Form generated from reading UI file 'createroom.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATEROOM_H
#define UI_CREATEROOM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CreateRoom
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QToolButton *toolButton;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_3;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_5;
    QComboBox *comboBox;
    QLineEdit *lineEdit_5;
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QFrame *frame_4;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QDialog *CreateRoom)
    {
        if (CreateRoom->objectName().isEmpty())
            CreateRoom->setObjectName(QString::fromUtf8("CreateRoom"));
        CreateRoom->resize(366, 276);
        QFont font;
        font.setFamily(QString::fromUtf8("Rockwell"));
        font.setPointSize(10);
        font.setBold(false);
        font.setWeight(50);
        CreateRoom->setFont(font);
        CreateRoom->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	border: 1px solid rgb(189, 189, 189);\n"
"\n"
"	background-color: rgb(40, 40, 42);\n"
"color:#fff;\n"
"	\n"
"	\n"
"}\n"
"QLineEdit{\n"
"height:18;\n"
"}\n"
"QFrame{\n"
"border:none;\n"
"}\n"
"\n"
"QLabel{\n"
"color:#fff;\n"
"	font: 12pt \"Candara\";\n"
"}\n"
"\n"
"\n"
"#toolButton{\n"
"height:28px;\n"
"width:38px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}\n"
"\n"
"#toolButton_2{\n"
"height:28px;\n"
"width:32px;\n"
"	border-image: url(:/new/prefix1/resources/minimize.png);\n"
"	\n"
"}\n"
"#toolButton_2:hover{\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"#toolButton_3{\n"
"height:28px;\n"
"width:32px;\n"
"border-image: url(:/new/prefix1/resources/maximize.png);\n"
"}\n"
"#toolButton_3:hover{\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #000;\n"
"	border-"
                        "image: url(:/new/prefix1/resources/menu.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"	\n"
"\n"
"\n"
"}"));
        verticalLayout = new QVBoxLayout(CreateRoom);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 10);
        frame_3 = new QFrame(CreateRoom);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy);
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_3);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        toolButton = new QToolButton(frame_3);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setMaximumSize(QSize(27, 19));

        horizontalLayout->addWidget(toolButton);


        verticalLayout->addWidget(frame_3);

        frame_2 = new QFrame(CreateRoom);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label_7);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addWidget(frame_2);

        frame = new QFrame(CreateRoom);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setVerticalSpacing(16);
        gridLayout->setContentsMargins(24, -1, 24, -1);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 2);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_4, 3, 0, 1, 2);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_5, 5, 0, 1, 1);

        comboBox = new QComboBox(frame);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 1, 2, 1, 1);

        lineEdit_5 = new QLineEdit(frame);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));

        gridLayout->addWidget(lineEdit_5, 5, 2, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        lineEdit = new QLineEdit(frame);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 2, 1, 1);

        lineEdit_2 = new QLineEdit(frame);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        gridLayout->addWidget(lineEdit_2, 3, 2, 1, 1);


        verticalLayout->addWidget(frame);

        frame_4 = new QFrame(CreateRoom);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy);
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_4);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        pushButton = new QPushButton(frame_4);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy1);
        pushButton->setMinimumSize(QSize(120, 0));
        pushButton->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_3->addWidget(pushButton);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);


        verticalLayout->addWidget(frame_4);


        retranslateUi(CreateRoom);

        QMetaObject::connectSlotsByName(CreateRoom);
    } // setupUi

    void retranslateUi(QDialog *CreateRoom)
    {
        CreateRoom->setWindowTitle(QApplication::translate("CreateRoom", "Dialog", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("CreateRoom", "X", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("CreateRoom", "Create Room Form", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("CreateRoom", "Number of Players :", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("CreateRoom", "Description:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("CreateRoom", "Wager", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("CreateRoom", "1V1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CreateRoom", "2V2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CreateRoom", "3V3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CreateRoom", "4V4", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CreateRoom", "5V5", 0, QApplication::UnicodeUTF8)
        );
        label->setText(QApplication::translate("CreateRoom", "Game Name:", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("CreateRoom", "Create Room", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CreateRoom: public Ui_CreateRoom {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATEROOM_H
