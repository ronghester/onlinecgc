#ifndef TOURNAMENT_H
#define TOURNAMENT_H

#include <QWidget>
#include <QNetworkReply>
#include <jsonparser.h>
#include <QSignalMapper>
namespace Ui {
class Tournament;
}

class Tournament : public QWidget
{
    Q_OBJECT
    
public:
    explicit Tournament(QWidget *parent = 0);
    ~Tournament();
    void setUserCount(QString ucount);
    Json::Value root;
private slots:
    void requestReceived(QNetworkReply* reply);
    void plainRequest(QNetworkReply* reply);
    void on_gamelabel_clicked(QString cmd);
private:
    Ui::Tournament *ui;
    void getRequest(QString surl);
     QSignalMapper *signalMapper;
};

#endif // TOURNAMENT_H
