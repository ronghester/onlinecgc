#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsDropShadowEffect>
#include <QWindowsVistaStyle>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QDebug>
#include <jsonparser.h>
#include <gamelabel.h>
#include <QPropertyAnimation>
#include <QScrollArea>
#include <QtWebKit/QWebView>
#include <QWebFrame>
#include <mediator.h>
#include <friendswidget.h>
#include <friendscontainer.h>
#include <QTimer>
#include <QDate>
#include <friendchatindicator.h>
#include <realtournament.h>
#include <nodegraph/graphwidget.h>
#include <supportframe.h>
#include <QMenu>
#include <QDesktopServices>
#include <QMessageBox>

//#include "winsparkle.h"
 int mainid;
 QString sessionid;
 QString sessionname;
 QString cind;
 QString thisuser;
 QString statusline;
 QString token;
 QString thatuser;
 QString thatuser_id;
 QString thisuser_rank;
 QString currentArenaName;
 int userchips;
 QString onlineuser_string;
 QString currentTournament;
 QByteArray userspic;
    QWebView* l_webview;
    QWidget* l_webviewholder;
    FriendsContainer* FC;
    QString currentVersion;
    QStringList arenanames;
//Json::Value mygames;
Json::Value challengeInfo;
Json::Value Tournament_jinfo;
std::vector<arenaobject> availablearenas;
int ADMIN;
//All friends chat in one container.
std::vector<ChatDialog*> fchats;
//Stores the names of friends in order of fchat creations
std::vector<QString> fchatindicators;

std::vector<FriendChatIndicator*> fchatindicator_widget;
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent,Qt::FramelessWindowHint|Qt::WindowMinimizeButtonHint | Qt::WindowSystemMenuHint),
    ui(new Ui::MainWindow)
{
        ADMIN = 114;
        isClosing = false;
        isloggedin = false;
        ui->setupUi(this);
        lockflag = false;
       //userimage = "";
       setStyleSheet("background:transparent;");
       setAttribute(Qt::WA_TranslucentBackground);
       //setWindowFlags(Qt::FramelessWindowHint);
       signalMapper = new QSignalMapper(this);
       mainid = 0;
       FC  = new FriendsContainer();
       lb = new LoginBox();
       QString dateTimeString = QTime::currentTime().toString("h:m")+"-"+QDate::currentDate().toString("d/M/yy");
       ui->timestamp->setText(dateTimeString);
       connect(lb,SIGNAL(success()),this,SLOT(onLoginSuccess()));
       connect(lb,SIGNAL(closing()),this,SLOT(onLoginClose()));
       lb->hide();

       /*************CHECK FOR UPDATE******************/
      // win_sparkle_set_appcast_url("<url>54.86.162.64/updater/appcast.xml");
      // win_sparkle_set_app_details(L"CGCOnline.com", L"CGC Online", L"1.0.0");
      // win_sparkle_init();
       /************************************/

       M = new Mediator();
       //connect(webview,SIGNAL(loadFinished(bool)),this,SLOT(loadFinished()));
       QString part1 = "<span style=\"color:#6dcbe9;\">";
       QString part2 = "New Randomly Assigned Tournament!";
       QString part3 = "</span><br/>";
       QString part4 = "This is the sample attraction page;This is the sample attraction page;This is the sample attraction page;";
       ui->promolabel->setTextFormat(Qt::RichText);
       ui->promolabel->setText(part1+part2+part3+part4);

       //bool ok =  connect(M,SIGNAL(M->msgReceived()),this,SLOT(rumble()));
       Q_ASSERT(ok);
       alert = new AlertDialog(this);
       alert->hide();
       connect(alert,SIGNAL(okClicked()),this,SLOT(startChallenge()));

      // connect(alert,SIGNAL(rejected()),this,SLOT(startChallenge()));
       lock = 0;
       CP = 0;
       ////
       m_tray_icon = new QSystemTrayIcon(QIcon(":/new/prefix1/resources/Icon.ico"), this);

      /* connect( m_tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(on_show_hide(QSystemTrayIcon::ActivationReason)) );

           QAction *quit_action = new QAction( "Exit", m_tray_icon );
           connect( quit_action, SIGNAL(triggered()), this, SLOT(on_exit()) );

           QAction *hide_action = new QAction( "Show/Hide", m_tray_icon );
           connect( hide_action, SIGNAL(triggered()), this, SLOT(on_show_hide()) );

           QMenu *tray_icon_menu = new QMenu();
           tray_icon_menu->addAction( hide_action );
           tray_icon_menu->addAction( quit_action );

          // m_tray_icon->setContextMenu( tray_icon_menu );*/

           m_tray_icon->show();

}

void MainWindow::on_show_hide( QSystemTrayIcon::ActivationReason reason )
{
   /*if( reason )
    {
        if( reason != QSystemTrayIcon::DoubleClick )
        return;
    }

    if( isVisible() )
    {
        hide();
    }
    else
    {
        show();
        raise();
        setFocus();
    }*/
}

void MainWindow::startChallenge(){
  alert->hide();

  if(CP)
      CP->WIP = false;
  openLockRoom();
  createLockInScreen(challengeInfo);
}


void MainWindow::rumble(){
    qDebug()<<"Testing the rumble";
}

void MainWindow::showNotification(QString msg){
     m_tray_icon->showMessage("CGC Info",msg);
     QApplication::beep();
}

void MainWindow::onLoginSuccess(){
    isloggedin = true;
    ui->userchips->setText("Chips: "+QString::number(userchips));
    QString tuser = thisuser;
    tuser[0] = tuser.at(0).toTitleCase();
    ui->username->setText(tuser);
    //ui->username->setStyleSheet("{margin-left:20px;}");
    show();


        // m_tray_icon->showMessage("Alert","You are logged in");
       // QApplication::beep();
       /*QMessageBox *mbox = new QMessageBox;
      // mbox->setWindowTitle(tr("Title"));
      mbox->setText("Let this disappear automagically");
      mbox->setStyleSheet("{background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);}");
      mbox->setMinimumSize(400, 300);
      mbox->setWindowFlags( Qt::FramelessWindowHint );
      mbox->setStandardButtons(QMessageBox::Ok);
      mbox->show();
      QTimer::singleShot(6000, mbox, SLOT(hide()));*/

    //Quit me here and start the update process..

    /*************************/


    //  win_sparkle_check_update_with_ui();
      /*******************************/
    whichuser = thisuser;
    tour = new Tournament(this);
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(tour);
    ui->mainview_2->setLayout(top);
    arena = 0;
    TL = 0;
    QString thisVersion = "1.0.2";
    qDebug()<<thisVersion<<" : "<<currentVersion;

    if(!userspic.isEmpty()){
       // userspic = userspic.simplified();
         ui->label_2->setStyleSheet("border-image:url(:/new/prefix1/resources/newframe.png)");
         QImage image = QImage::fromData(userspic, "PNG");
         QIcon icon;
         QSize size;
         if (!image.isNull())
           icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

         QPixmap pixmap = icon.pixmap(QSize(70,84), QIcon::Normal, QIcon::On);
          // ui->label_2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
         ui->label_2->setPixmap(pixmap);
         ui->label_2->show();
    }
    qDebug()<<"creating webview ...";
    l_webviewholder = new QWidget(this);
    l_webview = new QWebView(l_webviewholder);
    l_webview->setStyleSheet("background-color:none;color:black;");
    QVBoxLayout *newlayp = new QVBoxLayout();
    newlayp->setMargin(0);
    newlayp->addWidget(l_webview);
    l_webviewholder->setLayout(newlayp);
    l_webviewholder->hide();
    QString urld = "<url>node/31";
    QNetworkRequest request = QNetworkRequest(urld);
    QString ss = sessionname+"="+sessionid;
    request.setRawHeader("Cookie",ss.toUtf8() );
    l_webview->load(request);
    l_webview->settings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls, true);
    l_webview->page()->mainFrame()->addToJavaScriptWindowObject(QString("Mediator"), M);
    M->lastMsg ="";


}
void MainWindow::createFriendsWidgets(){
     RemoveLayout(ui->sidebar);
    //Lets add few friends
    //myFriends = frnds;


     //wid->setStyleSheet("QListWidget:item { min-height:64px;border-image: url(:/new/prefix1/resources/Userbets.png); }");
     QVBoxLayout *ufrnd = new QVBoxLayout();
     QListWidget *listWidget = new QListWidget(this);
        qDebug()<<"In Friends Update";
     //listWidget->setStyleSheet("QListWidget:item { min-height:64px;border-image: url(:/new/prefix1/resources/Userbets.png);  }");
     listWidget->setStyleSheet("QListWidget:item { min-height:64px;border-image: url(:/new/prefix1/resources/newfriendsbg.jpg);}");
     listWidget->setFont(QFont("Candara",14, QFont::Normal));
      std::vector<FriendsWidget*> myFriends = FC->getFriendsWidget();
     for(int i=0;i<myFriends.size();i++){
         FriendsWidget* FW = myFriends[i];
         //qDebug()<<"In Friends Update" << i;
         QListWidgetItem *item = new QListWidgetItem();
         item->setFont(QFont("Candara",14, QFont::Normal));
        // qDebug()<<"In Friends Update" << i;
         item->setSizeHint(QSize(120,65));
       //  qDebug()<<"In Friends Update" << i;
         listWidget->insertItem(i,item);
       //  qDebug()<<"In Friends Update" << i;
         listWidget->setItemWidget(listWidget->item(i),FW);
         //FW->setImage(friendsimages.value("Nathan"));
     }
      qDebug()<<"In Friends Update";
     //ufrnd->setSizeConstraint(SizeConstraint);
     ufrnd->setContentsMargins(0,0,0,0);
     ufrnd->addWidget(listWidget);
     ui->sidebar->setLayout(ufrnd);
    // connect(listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(doSomething(QListWidgetItem*)));


}



void MainWindow::doSomething(QListWidgetItem *item)
{

  //  chatdgl = new ChatDialog(this);
    //chatdgl->move(ui->statusBar->geometry().center());
  //  chatdgl->show();
}

QListWidgetItem* MainWindow::getFriendsWidget(QString fname){
    QListWidgetItem* QWL = new QListWidgetItem(fname);
    //QWL->setBackground(QBrush(QImage(":/new/prefix1/resources/loginbtn.png")));
    QIcon ic = QIcon(QPixmap().fromImage(QImage(":/new/prefix1/resources/green-dot.png")));
    QWL->setIcon(ic);
    QWL->setFont(QFont("Candara",14, QFont::Normal));
    return QWL;
}

void MainWindow::setUserImage(){
    if(!userspic.isEmpty()){
       // userspic = userspic.simplified();
          ui->label_2->setStyleSheet("border-image:url(:/new/prefix1/resources/profile-frame-small.png)");
          QImage image = QImage::fromData(userspic, "PNG");
          QIcon icon;
          QSize size;
          if (!image.isNull())
            icon.addPixmap(QPixmap::fromImage(image), QIcon::Normal, QIcon::On);

          QPixmap pixmap = icon.pixmap(QSize(100,100), QIcon::Normal, QIcon::On);
          //QPixmap pixmap = icon.pixmap(QSize(36,52), QIcon::Normal, QIcon::On);
          ui->label_2->setAlignment(Qt::AlignHCenter |Qt::AlignVCenter);
          ui->label_2->setPixmap(pixmap);
          //ui->label_2->setStyleSheet("border-image:url(:/new/prefix1/resources/profile-frame-small.png)");
          ui->label_2->show();
    }
}

void MainWindow::onLoginClose(){
    isClosing =true;


    //win_sparkle_cleanup();
     m_tray_icon->hide();
    QCoreApplication::exit(0);
}
void MainWindow::setUserRank(QString rank,QString label){
    ui->label_5->setText("Rank: "+rank);
    //ui->username->setTextFormat(Qt::RichText);
    ui->userrankpic->setPixmap(QPixmap(label));

}

MainWindow::~MainWindow()
{
    delete ui;
    m_tray_icon->hide();
    //qDebug()<<"IN Deleting";
}

QScrollArea* MainWindow::prepareGames(Json::Value root){
    QWidget *wid = new QWidget(this);
    signalMapper = new QSignalMapper(this);

    QGridLayout *top = new QGridLayout(wid); int j=0; int k=0;
        connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(on_gamelabel_clicked(QString)));
        qDebug()<<"In prepare games";
          for(int i=0;i<root.size();i++){
               const Json::Value tem = root[i];
               GameLabel *b41=new GameLabel("Hey There "+j,0);
               QSize buttonSize(154,152);
               b41->setFixedSize(buttonSize);
               // b41->setAlignment(Qt::AlignCenter);
               //b41->setProperty("nid",);
               QString cmd = QString().fromStdString(tem.get("nid","none").asCString());
               QString gamename = QString().fromStdString(tem.get("node_title","none").asCString());
               QString server = QString().fromStdString(tem.get("server","none").asCString());
              connect(b41, SIGNAL(clicked()), signalMapper, SLOT(map()));
              signalMapper->setMapping(b41, cmd);
              QObject::connect(b41,SIGNAL(pressed(cmd)),this,SLOT(on_gamelabel_clicked(cmd)));
                         // QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
                         // effect->setBlurRadius(9.0);
                          b41->setStyleSheet("background-image: url(:/new/prefix1/resources/games-back.png);");
                         // effect->setColor(QColor(254, 254, 254, 255));
                         // effect->setOffset(0.2);
                          QByteArray by = QByteArray::fromBase64(tem.get("img","none").asCString());
                          QImage image = QImage::fromData(by, "PNG");
                 b41->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
         QString html = "<img style='display:block; width:100px;height:100px;' id='base64image'";
         QString part2 = "src='data:image/jpeg;base64,"+QString().fromStdString(tem.get("img","none").asCString())+"'/>";
         QString part3 = "<br/><br/><span style=\" font-family:'candara'; font-size:11pt;\"><b>"+gamename+"</b></span>";
         QString part4 = "<br/><span style=\" font-family:'candara'; font-size:10pt;\">(<i>"+server+")</i></span>";              
         b41->setTextFormat(Qt::RichText);
         b41->setText(html+part2+part3+part4);
                                   // label.show();
                                   // b41->setGraphicsEffect(effect);
                                    top->addWidget(b41,k,j,1,1,Qt::AlignCenter);

                                  //  top->addWidget(new QSpacerItem());
                                    j++;
                                    if(j>3){ j=0;k++;}
          }

          wid->setLayout(top);
          QScrollArea *ara = new QScrollArea(this);
          ara->setWidget(wid);
          return ara;
}
void MainWindow::getFriendsInfo(QString nid){
    QString surl = "<url>FriendInfo?nid="+nid;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(processFriendsInfo(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);
}

void MainWindow::processFriendsInfo(QNetworkReply* reply){
    QString replyText;
     QByteArray rawData = reply->readAll();
     QString textData(rawData);
      qDebug() << "RM"+ textData;
      if(textData.length() < 5) {
          qDebug()<<"hey";
          QString username = "Some Error";

          return;
      }

      Json::Reader reader;
      Json::Value root;
      QVBoxLayout *newsbox=new QVBoxLayout();
      bool parsingSuccessful = reader.parse(textData.toStdString(), root );

      if(!parsingSuccessful || root.size() == 0){
            return;
      }
      else{
          if(root.size() == 0) return;
          int index = 0;
          const Json::Value friends = root[index];
          QString username = QString().fromStdString(friends.get("name","none").asCString());
          QString userchips = QString().fromStdString(friends.get("chips","none").asCString());
          QString onlinestatus = QString().fromStdString(friends.get("online","none").asCString());
          QString userstatusline = QString().fromStdString(friends.get("statusline","none").asCString());
           QString userrank = QString().fromStdString(friends.get("userrank","unrank").asCString());
             QByteArray userpic = friends.get("userpic","none").asCString();
              qDebug()<<"Hey there "<<userrank;
           qDebug()<<"Hey there "<<username<<" ** "<<userchips<< " ** " <<onlinestatus<<" ** "<<userrank;
         // FriendsWidget* thisfrnd = new FriendsWidget(0);
         FC->addFriendElement(username,userchips,userstatusline,onlinestatus,userrank);
            QByteArray thisupic = QByteArray::fromBase64(userpic.replace(" ","+"));
       FC->friendsimages.push_back(thisupic);
           QTimer::singleShot(2000, this, SLOT(createFriendsWidgets()));

      }
}
void MainWindow::requestReceived2(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << textData;
    textData = textData.trimmed();
      isClosing = true;
      //win_sparkle_cleanup();
}
void MainWindow::requestReceived(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << textData;
    textData = textData.trimmed();
    if(textData == "" || textData == "[\"Done\"]"){
        return;
    }
    else if(textData.size()< 13){
        IBox = new InfoBox(this);
        IBox->setMessage("Nothing!, Please Sign up for games or Re-login");
        IBox->show();
        return ;
     }
     try{
     Json::Reader reader;
     Json::Value preroot;
     Json::Value root;
     Json::Value mygames;
     reader.parse(textData.toStdString(), preroot);
     availablearenas.clear();
     root = preroot.get("globals",0);
     mygames = preroot.get("mygames",0);
     for(int i=0;i<mygames.size();i++){
          const Json::Value tem = mygames[i];
          QString cmd = QString().fromStdString(tem.get("nid","none").asCString());
          QString gamename = QString().fromStdString(tem.get("node_title","none").asCString());
          QString arenaname = QString().fromStdString(tem.get("user_arenaname","none").asCString());
          arenaobject thisobj; thisobj.arenacode = cmd; thisobj.arenaname = gamename;thisobj.userarenaname = arenaname;
          availablearenas.push_back(thisobj);
          arenanames<<gamename;
     }
     lastValue = preroot;
     QVBoxLayout *toppanel = new QVBoxLayout;
     QLabel* lab = new QLabel("Arena Profiles");
     QFont f( "Candara", 10, QFont::Bold);
     lab->setStyleSheet("color:#fff");
     lab->setFont(f);
     toppanel->addWidget(lab);
     QScrollArea* ara = prepareGames(mygames);

     toppanel->addWidget(ara);
     ara->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
       QLabel* lab4 = new QLabel("Available Profiles");
       //QFont f( "Candara", 10, QFont::Bold);
       lab4->setStyleSheet("color:#fff");
       lab4->setFont(f);
        toppanel->addWidget(lab4);
        QScrollArea* ara2 = prepareGames(root);
        toppanel->addWidget(ara2);
        //toppanel->addWidget(ara);
       //toppanel->addLayout(top);
       //toppanel->setMargin(0);
       toppanel->setContentsMargins(0,0,0,0);
       // toppanel->;
       // toppanel->setSpacing(0);
       ui->mainview_2->setLayout(toppanel);
 //
    }
    catch(...){
       qDebug()<<"[Main Screen] : Error";
    }
}
void MainWindow::getRequest(QString surl){
    if(mainid == 0){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Login! To View Games");
        IBox->show();
    }
    else{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    QNetworkRequest request = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request.setRawHeader("Cookie",ss.toUtf8() );
 // manager->get(request);
    manager->get(request);
    }
    //ui->statusBar->
}

void MainWindow::onreInitArena(){
    //let find out in which part this id belongs
    Json::Value root = lastValue.get("globals",0);
    Json::Value mygames = lastValue.get("mygames",0);
QString cmd = cind;
 //QSingnal = new QSignalMapper(this);


      for(int i=0;i<mygames.size();i++){
          Json::Value tem = mygames[i];
          QString thiscmd =  tem.get("nid","none").asCString();
         if(thiscmd == cmd) {
              try{
              RemoveLayout(ui->mainview_2);
              qDebug()<< QString().fromStdString(tem.toStyledString());
             arena  = new ArenaScreen(this->centralWidget(),1);
             arena->setValues(tem);
             qDebug()<< "label clicked "+cmd;
             QVBoxLayout *top = new QVBoxLayout;
             top->addWidget(arena);
             ui->mainview_2->setLayout(top);
              }
              catch(...){
                   qDebug()<<"Error: In Switching to Leaderboard";
              }
          }
      }
      for(int i=0;i<root.size();i++){
          Json::Value tem = root[i];
          QString thiscmd =  tem.get("nid","none").asCString();
          if(thiscmd == cmd) {
             // const Json::Value tem = lastValue[cint];
              try{
              RemoveLayout(ui->mainview_2);
              qDebug()<< QString().fromStdString(tem.toStyledString());
             arena  = new ArenaScreen(this->centralWidget(),0);
            // connect(arena,SIGNAL(moveme(pt)),this,SLOT(childMoveEvent(pt)));
             arena->setValues(tem);
             qDebug()<< "label clicked "+cmd;
             QVBoxLayout *top = new QVBoxLayout;
             top->addWidget(arena);
             ui->mainview_2->setLayout(top);
              }
              catch(...){
                   qDebug()<<"Error: In Switching to Leaderboard";
              }
          }
      }
}

void MainWindow::on_gamelabel_clicked(QString cmd){
    qDebug()<< "label clicked "+cmd;
    cind = cmd.toInt();

    //let find out in which part this id belongs
    Json::Value root = lastValue.get("globals",0);
    Json::Value mygames = lastValue.get("mygames",0);

 //QSingnal = new QSignalMapper(this);


      for(int i=0;i<mygames.size();i++){
          Json::Value tem = mygames[i];
          QString thiscmd =  tem.get("nid","none").asCString();
         if(thiscmd == cmd) {
              try{
              RemoveLayout(ui->mainview_2);
              qDebug()<< QString().fromStdString(tem.toStyledString());
             arena  = new ArenaScreen(this->centralWidget(),1);
             for(int h=0;h<availablearenas.size();h++){
                 if(availablearenas[h].arenacode == thiscmd)
                     currentArenaName = availablearenas[h].userarenaname;
            }
             arena->setValues(tem);
             qDebug()<< "label clicked "+cmd;
             QVBoxLayout *top = new QVBoxLayout;
             top->addWidget(arena);
             ui->mainview_2->setLayout(top);
              }
              catch(...){
                   qDebug()<<"Error: In Switching to Leaderboard";
              }
          }
      }
      for(int i=0;i<root.size();i++){
          Json::Value tem = root[i];
          QString thiscmd =  tem.get("nid","none").asCString();
          if(thiscmd == cmd) {
             // const Json::Value tem = lastValue[cint];
              try{
              RemoveLayout(ui->mainview_2);
              qDebug()<< QString().fromStdString(tem.toStyledString());
             arena  = new ArenaScreen(this->centralWidget(),0);
            // connect(arena,SIGNAL(moveme(pt)),this,SLOT(childMoveEvent(pt)));
             arena->setValues(tem);
             qDebug()<< "label clicked "+cmd;
             QVBoxLayout *top = new QVBoxLayout;
             top->addWidget(arena);
             ui->mainview_2->setLayout(top);
              }
              catch(...){
                   qDebug()<<"Error: In Switching to Leaderboard";
              }
          }
      }


    //  connect(arena->tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(onLobbyTableClicked(int,int)));
}



void MainWindow::mousePressEvent(QMouseEvent *event){

    mpos = event->pos();
}

void MainWindow::on_toolButton_4_pressed()
{
     mpos = QCursor::pos();
}
void MainWindow::onFriendIndicatorClicked(QString Fname,bool status){
    qDebug()<<Fname;

    if(status){
        //lets delete this indicator
        for(int i=0;i<fchatindicators.size();i++){
            if(fchatindicators[i] == Fname){
                fchatindicators.erase(fchatindicators.begin()+i);
                fchatindicator_widget.erase(fchatindicator_widget.begin()+i);
               // statusBar()->removeWidget(fchats[i]);
                QWidget* cdgl = fchats[i]; //.begin()+i);
                delete cdgl;
                fchats.erase(fchats.begin()+i);
                break;
            }
        }
        //Now lets adjust the other chats
        int bint = 0;
             for(int i=0;i<fchats.size();i++){
            // QPoint newmp(newpos.x(),newpos.y()+280);



                     FriendChatIndicator* FCI = fchatindicator_widget[i];
                      int xFactor = bint*(FCI->logicalDpiX()+FCI->logicalDpiX());
                      QPoint newmp(this->pos().x()+xFactor,fchats[i]->pos().y());
                      fchats[i]->move(newmp);


             }
    }
    else{
 //If status it means user wants to close this chat...
     for(int i=0;i<fchatindicators.size();i++){
         if(fchatindicators[i] == Fname){
             fchats[i]->isVisible()?fchats[i]->hide():fchats[i]->show();
             //fchatindicator_widget[i]->show();
            //statusBar()->removeWidget();

         }
     }
     //Lets adjust every other chat box
    /* int bint = 0;
     for(int i=0;i<fchats.size();i++){
    // QPoint newmp(newpos.x(),newpos.y()+280);

       {
             if(fchats[i]->isVisible()){
             FriendChatIndicator* FCI = fchatindicator_widget[i];
              int xFactor = bint*(FCI->logicalDpiX()+FCI->logicalDpiX());
              QPoint newmp(this->pos().x()+xFactor,fchats[i]->pos().y());
              fchats[i]->move(newmp);
             }
         }
     }*/
    }

}

//Create chat pannel from loop

void MainWindow::createChatFriends_adv(QString channel){
    QStringList set = channel.split("_");
    QString Fname = set[2];

    for(int i=0;i<fchatindicators.size();i++){
        if(fchatindicators[i] == Fname){
            fchats[i]->show();
            return;
        }
    }
    //if channel is not empty then add this user to the existing channel

    FriendChatIndicator* FCI = new FriendChatIndicator(Fname);
    FCI->setFixedSize(142,20);
    statusBar()->addWidget(FCI);
    thatuser = Fname;

    qDebug()<<"position of fci is: "<<statusBar()->logicalDpiX();
    int xFactor = fchats.size()*160;
    ChatDialog *cdgl = new ChatDialog(this,true,channel);
    cdgl->show();
    QPoint newmp(this->pos().x()+xFactor,this->pos().y()+this->height()-(cdgl->height()+18));

    if(cdgl) cdgl->move(newmp);
    fchats.push_back(cdgl);
    fchatindicators.push_back(Fname);
    fchatindicator_widget.push_back(FCI);
}

void MainWindow::createChatFriends(QString Fname,QString status,int Findex){
    if(status != "online") {
        IBox = new InfoBox(this);
        IBox->setMessage("User is offline!");
        IBox->show();
        return;
    }
     thatuser = Fname;
     int bint = 0;
     for(int i=0;i<fchatindicators.size();i++){
        if(fchats[i]->isVisible()) bint++;
        if(fchatindicators[i] == Fname){
            fchats[i]->show();
            fchatindicator_widget[i]->show();
            FriendChatIndicator* FCI = fchatindicator_widget[i];
             int xFactor = bint*(FCI->logicalDpiX()+FCI->logicalDpiX());
             QPoint newmp(this->pos().x()+xFactor,this->pos().y()+this->height()-(281));
             fchats[i]->move(newmp);
            return;
        }
    }
    QString chat_room = "Chatroom_"+thatuser+"_"+thisuser;
    QString surl = "<url>addToChatGroup?&uid="+QString().number(mainid)+"&nid="+Fname+"&desc="+chat_room;
    qDebug()<<surl;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);

}


void MainWindow::ChatDialog_init(QString Fname){
    FriendChatIndicator* FCI = new FriendChatIndicator(Fname,this);
    FCI->setFixedSize(142,20);
    statusBar()->addWidget(FCI);
    thatuser = Fname;
    qDebug()<<"position of fci is: "<<FCI->logicalDpiX();
    int xFactor = fchats.size()*(FCI->logicalDpiX()+FCI->logicalDpiX());
    ChatDialog *cdgl = new ChatDialog(this);
    cdgl->show();
    QPoint newmp(this->pos().x()+xFactor,this->pos().y()+this->height()-(cdgl->height()+18));

    if(cdgl) cdgl->move(newmp);
    fchats.push_back(cdgl);
    fchatindicators.push_back(Fname);
    fchatindicator_widget.push_back(FCI);
}

void MainWindow::updateUserChips(QString betamount){
    ui->userchips->setText("Chips:"+betamount);

}


void MainWindow::mouseMoveEvent(QMouseEvent *event){

    if (event->buttons() && Qt::LeftButton) {
                QPoint mp(event->x(),event->y());
              // if(ui->frame_4->geometry().contains(mp,true)){

                QPoint diff = event->pos() - mpos;
                QPoint newpos = this->pos() + diff;
                this->move(newpos);
                int bint = 0;
                for(int i=0;i<fchats.size();i++){
               // QPoint newmp(newpos.x(),newpos.y()+280);

                  {
                        FriendChatIndicator* FCI = fchatindicator_widget[i];

                         int xFactor = bint*(FCI->logicalDpiX()+FCI->logicalDpiX());
                        QPoint newmp(newpos.x()+xFactor,newpos.y()+this->height()-(fchats[i]->height()+18));
                         fchats[i]->move(newmp);
                          if(fchats[i]->isVisible()) bint++;
                    }
                }

               // if(leaderboard) leaderboard->moveRD();
              //  }

            }
}

void MainWindow::resizeEvent(QResizeEvent *event){
    //qDebug()<<"Resizing";
    QSize cg = event->oldSize() - event->size();
    //int changeHeight = cg
   // if(chatdgl) chatdgl->move(chatdgl->pos().x(),chatdgl->pos().y()-cg.height());


    for(int i=0;i<fchats.size();i++){
        QPoint newmp(this->pos().x()+(i*160),this->pos().y()+this->height()-(fchats[i]->height()+18));
         fchats[i]->move(newmp);
    }
}


void MainWindow::on_toolButton_2_clicked()
{
   // exit(0);
    this->showMinimized();

}
void MainWindow::setHeaderNews(QString subject,QString body){
    QString part2 =  subject;
    QString part4 =  body;
    QString part1 = "<span style=\"color:#6dcbe9;\">";
    QString part3 = "</span><br/>";
  //  QString part4 = "This is the sample attraction page;This is the sample attraction page;This is the sample attraction page;";
    ui->promolabel->setTextFormat(Qt::RichText);
    ui->promolabel->setText(part1+part2+part3+part4);
}

void MainWindow::runLoop(){
    //if(M->lastMsg=="") return;

    if(M->lastMsg!= ""){
        Json::Reader reader;
        Json::Value root;
        std::string text = M->lastMsg.toStdString();
        qDebug()<<QString().fromStdString(text);
        bool parsingSuccessful =reader.parse(text, root );
          if(!parsingSuccessful || root.size() == 0){
                return;
          }
          else{
              Json::Value data = root.get("data","none");
              if(data!="none"){
                   QString sub = QString().fromStdString(data.get("subject","none").asCString());
                    if(sub == "CheckForRoom"){
                        qDebug()<<"llllllllll  :: ";
                        if(arena) arena->checkForNewRooms(); //also make sure he is on table screen
                    }
                    else if(sub == "updateUserCount"){
                        const Json::Value dt= data["body"];

                       int index = 0; // Iterates over the sequence elements.
                        QString part2 =  QString().fromStdString(dt[index].asCString());
                        index++;
                        QString part4 =  QString().fromStdString(dt[index].asCString());
                        //if(arena) arena->setPlayerCount(part2+"<br/>"+part4);
                    }
                    else if(sub =="SomeOne_Added_YouFriend"){
                        const Json::Value dt= data["body"];
                       int index = 0; // Iterates over the sequence elements.
                        QString his_username =  QString().fromStdString(dt[index].asCString());
                        index++;
                        QString my_username =  QString().fromStdString(dt[index].asCString());
                        if(thisuser == my_username){
                            getFriendsInfo(his_username);
                        }

                    }
                    else if(sub == "Adminmsg"){
                        if(mainid == ADMIN){
                         QString adminmsg = QString().fromStdString(data.get("body","none").asCString());
                        //QString adminmsg =  QString().fromStdString(dt[index].asCString());
                        showNotification(adminmsg);
                        }
                    }
                    else if(sub == "UpdateFriendList"){
                        const Json::Value dt= data["body"];
                       int index = 0; // Iterates over the sequence elements.
                        QString loggeduser =  QString().fromStdString(dt[index].asCString());
                        FC->updateOnlineStatusOfUser(loggeduser);

                        QTimer::singleShot(2000, this, SLOT(createFriendsWidgets()));
                    }                   
                    else if(sub == "UpdateNews"){
                        const Json::Value dt= data["body"];
                       int index = 0; // Iterates over the sequence elements.
                        QString part2 =  QString().fromStdString(dt[index].asCString());
                        index++;
                        QString part4 =  QString().fromStdString(dt[index].asCString());

                        QString part1 = "<span style=\"color:#6dcbe9;\">";
                        QString part3 = "</span><br/>";
                      //  QString part4 = "This is the sample attraction page;This is the sample attraction page;This is the sample attraction page;";
                        ui->promolabel->setTextFormat(Qt::RichText);
                        ui->promolabel->setText(part1+part2+part3+part4);

                    }
                    else if(sub == "StartChallenge"){
                        if(lock == 0){
                             challengeInfo= data.get("body","none");
                             int index = 0; // Iterates over the sequence elements.
                             qDebug()<<"Now animating the stuff";
                             QString cha = challengeInfo.get("challenger","none").asCString();
                             QString chato = challengeInfo.get("challenge_to","none").asCString();
                             if(cha == QString().number(mainid) || chato == QString().number(mainid)){
                                alert->show();
                             }
                         }

                       //  QTimer::singleShot(1000, this, SLOT(openLockRoom()));

                    }
                    else if(sub == "Friends_chatmsg"){
                         QString channel = root.get("channel","none").asCString();
                         qDebug()<<"channel is:"<<channel;
                          QString chatmsg = QString().fromStdString(data.get("body","none").asCString());
                         for(int i=0;i<fchats.size();i++){
                             qDebug()<<fchats[i]->chat_room;
                             if(channel == fchats[i]->chat_room){
                                 QStringList qs = chatmsg.split(":");
                                 if(qs.at(0) != thisuser)
                                     fchatindicator_widget[i]->setActivationDue();
                                 fchats[i]->addChatMsg(chatmsg);


                             }
                         }
                    }
                    else if(sub == "Notify_FriendsChat"){
                        const Json::Value dt= data["body"];
                        int index = 0; // Iterates over the sequence elements.
                        QString username =  QString().fromStdString(dt[index].asCString());
                        if(username == thisuser){
                            index++;
                            QString channel =  QString().fromStdString(dt[index].asCString());
                            createChatFriends_adv(channel);

                        }

                    }
                    else if(TL){
                        if(sub == "TournamentSignup"){
                            const Json::Value dt= data["body"];

                            int index = 0; // Iterates over the sequence elements.
                             QString tid =  QString().fromStdString(dt[index].asCString());index++;
                             Json::Value users =  dt[index];
                           // if(username != thisuser){
                                TL->addTeamPlayer(users);
                          //  }

                        }
                        else if(sub=="TournamentClose"){
                            const Json::Value dt= data["body"];
                            int index = 0; // Iterates over the sequence elements.
                             QString tid =  QString().fromStdString(dt[index].asCString());
                             index++;
                              Json::Value users =  dt[index];
                              index++;
                             Json::Value tdata =  dt[index];
                            //if(username != thisuser){
                                TL->addTeamPlayer(users);
                          //  }
                           // TL->setValues(tdata);
                            showNotification("Tournament is ready to start. Join your lobby");
                            TL->Tclose(tdata);

                        }
                    }
                    else if(arena && !arena->hasMovedToGame && arena->lock ){
                        QString channel = QString().fromStdString(root.get("channel","none").asCString());
                        if(channel != arena->lock->roomname) return;
                        qDebug()<<"in arena condition";
                        if(sub == "Chipsstat"){
                            if(arena->lock->hasMadeAPlaceBetRequest){
                             qDebug()<<"In arena condition  :: "<<thisuser;
                             //std::string temuser = thisuser.toStdString().c_str();
                             int newuserchips = data.get(QString().number(mainid).toStdString(),"none").asInt();
                             int wonchips = abs(userchips - newuserchips);
                             if(newuserchips > userchips)
                                 arena->lock->setMatchStatus(true,QString::number(wonchips));
                             else
                                 arena->lock->setMatchStatus(false,QString("0"));


                             userchips = newuserchips;
                              ui->userchips->setText("Chips:"+QString::number(userchips));
                            }
                           // qDebug()<<"This user chips :: "<<uchips;
                         // userchips =  uchips.toInt();
                           // userchips = QString().fromStdString(st.asCString()).toInt();
                        }

                        else if(sub == "CheckForRoom"){
                            // qDebug()<<"llllllllll  :: ";
                            //If you are in lockin screen don't check for the game button
                            if(!arena->lock)
                               arena->on_GameButton_clicked();
                        }
                        else if(arena->lock)arena->lock->processMessage(root);
                    }
                    else if(lock){

                          qDebug()<<"in lock condition";
                        if(sub == "Chipsstat"){
                          //  if(lock->isthisadmin) return;
                           // if(lock->roomtype == "Troom"){

                                Json::Value allloosers = data["loosers"];bool gate = false;
                                QString channel = QString().fromStdString(root.get("channel","none").asCString());
                                if(channel != lock->roomname) return;

                                for(int i=0;i<allloosers.size();i++){
                                   QString pname = QString().fromStdString(allloosers[i].asCString());
                                   qDebug()<<"Loosers are "<<pname;
                                   if(pname == QString().number(mainid)){
                                       gate = true;
                                   }
                                }
                                gate ?  lock->setMatchStatus(true,"") :  lock->setMatchStatus(false,"");
                               // int newuserchips  = data.get(thisuser.toStdString(),"none").asInt();
                              //  userchips = newuserchips;
                                   //ui->userchips->setText("Chips:"+QString::number(userchips));

                          //  }
                            /*else{
                             int newuserchips = data.get(thisuser.toStdString(),"none").asInt();
                             if(newuserchips > userchips)
                                 lock->setMatchStatus(true,"");
                             else
                                 lock->setMatchStatus(false,"");
                              userchips = newuserchips;
                              //ui->userchips->setText("Chips:"+QString::number(userchips));
                            }*/
                        }

                        else lock->processMessage(root);
                    }

                    else {
                       // if(arena->lock)arena->lock->processMessage(root);

                    }

              }
          }
           M->lastMsg = "";
    }



}

void MainWindow::openLockRoom(){
    qDebug()<<"Now Inside the lockies";
    ui->mainview_2->layout()->setContentsMargins(0,0,0,0);
    ui->mainview_2->setStyleSheet("border:none;");
   /* ui->frame_6->setMaximumHeight(0);
    ui->frame_12->setMaximumHeight(0);
    ui->frame->setMaximumWidth(0);*/

      QPropertyAnimation *animation3 = new QPropertyAnimation(ui->frame_6, "maximumHeight");
       animation3->setDuration(600);
       animation3->setStartValue(ui->frame_6->height());
       animation3->setEndValue(0);
       animation3->setEasingCurve(QEasingCurve::InOutSine);
       animation3->start(QAbstractAnimation::DeleteWhenStopped);
      // connect(animation3, SIGNAL( finished() ), this, SLOT( loadSliders() ));

       QPropertyAnimation *animation4 = new QPropertyAnimation(ui->frame_12, "maximumHeight");
           animation4->setDuration(600);
           animation4->setStartValue(ui->frame_12->maximumHeight());
           animation4->setEndValue(0);
           animation4->setEasingCurve(QEasingCurve::InOutSine);
           animation4->start(QAbstractAnimation::DeleteWhenStopped);
          // connect(animation3, SIGNAL( finished() ), this, SLOT( openChannel() ));


           QPropertyAnimation *animation5 = new QPropertyAnimation(ui->frame, "maximumWidth");
               animation5->setDuration(100);
               animation5->setStartValue(204);
               animation5->setEndValue(0);
               animation5->setEasingCurve(QEasingCurve::InCurve);
               animation5->start(QAbstractAnimation::DeleteWhenStopped);
               if(arena) arena->unlocking = false;
}

void MainWindow::closeLockRoom(){
    ui->mainview_2->setStyleSheet("#mainview_2{border-left: 1px solid grey;border-right: 1px solid grey;border-bottom: 1px solid grey;}");
        QPropertyAnimation *animation3 = new QPropertyAnimation(ui->frame_6, "maximumHeight");
            animation3->setDuration(200);
            animation3->setStartValue(0);
            animation3->setEndValue(124);
            animation3->setEasingCurve(QEasingCurve::InCurve);
            animation3->start(QAbstractAnimation::DeleteWhenStopped);
           // connect(animation3, SIGNAL( finished() ), this, SLOT( loadSliders() ));

           QPropertyAnimation *animation4 = new QPropertyAnimation(ui->frame_12, "maximumHeight");
                animation4->setDuration(200);
                animation4->setStartValue(0);
                animation4->setEndValue(24);
                animation4->setEasingCurve(QEasingCurve::InCurve);
                animation4->start(QAbstractAnimation::DeleteWhenStopped);
               // connect(animation3, SIGNAL( finished() ), this, SLOT( openChannel() ));*/

                QPropertyAnimation *animation5 = new QPropertyAnimation(ui->frame, "maximumWidth");
                    animation5->setDuration(100);
                    animation5->setStartValue(ui->frame->width());
                    animation5->setEndValue(234);
                    animation5->setEasingCurve(QEasingCurve::InCurve);
                    animation5->start(QAbstractAnimation::DeleteWhenStopped);
                    if(arena) arena->backtolobby = false;
}


void MainWindow::createLockInScreen(Json::Value info){

    RemoveLayout(ui->mainview_2);
    lock  = new LockIn();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(lock);
    top->setSpacing(0);
    top->setContentsMargins(0,0,0,0);
    ui->mainview_2->setLayout(top);
    qDebug()<<QString().fromStdString(info.toStyledString());
    QString nid = info.get("nid","none").asCString();
    QString wageramount = info.get("wager","none").asCString();
    QString gametype = info.get("gametype","none").asCString();
    QString title = info.get("body","none").asCString();
    //QString nid = info.get("nid","none").asCString();

    lock->roomname = "Challenge_"+nid;;
    lock->This_Roomname = "Challenge_"+nid;
    lock->currentGameType = gametype;
    lock->currentRoomId = nid;
    lock->roomtype = "Croom";
    lock->currentWagerAmount = wageramount.toInt();
        QString lbtext = "Number of Player: "+gametype+"\nWager Amount: "+wageramount+"\nChallenge Details: "+title;
        lock->setDetailLabel(lbtext,wageramount.toInt());
        lock->betslider->setMinimum(wageramount.toInt());
        lock->wagerslide->setText(wageramount);


      //  lock->ViewLobby->setText("Home");
     connect(lock->ViewLobby,SIGNAL(clicked()),this,SLOT(goToHome()));
     connect(lock->placebet,SIGNAL(clicked()),lock,SLOT(onPlaceButtonClicked()));
     connect(lock->sitdown,SIGNAL(clicked()),lock,SLOT(onSitBack()));
     connect(lock,SIGNAL(he_lost()),lock,SLOT(onReportingLoss()));
     //connect(lock,SIGNAL(joinChannel()),this,SLOT(addUserToChannel()));
    // addto_Challenge_Channel("nanm","namo");
     lock->ViewLobby->hide();
     addto_Challenge_Channel(gametype,nid,"Croom");
}
void MainWindow::enterLockIn_asAdmin(QString roomid,QString roomtitle,QString nid){
    openLockRoom();
    currentArenaName = "CGCAdmin";
    RemoveLayout(ui->mainview_2);
    lock  = new LockIn();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(lock);
    top->setSpacing(0);
    top->setContentsMargins(0,0,0,0);
    ui->mainview_2->setLayout(top);

    lock->roomname =roomid;
    lock->This_Roomname = roomid;
    lock->isthisadmin = true;
    //lock->currentGameType = gametype;
    lock->currentRoomId = nid;
    lock->roomtype = roomtitle;
  //  lock->roomtype = "Troom";
    lock->currentWagerAmount = 100;
    QString lbtext = "You Joined as Admin ";
        //lock->setDetailLabel(lbtext,wageramount.toInt());
      //  lock->betslider->setMinimum(wageramount.toInt());
       // lock->wagerslide->setText(wageramount);
    // lock->ViewLobby->setText("Home");
     connect(lock->ViewLobby,SIGNAL(clicked()),this,SLOT(goToHome()));
     connect(lock->placebet,SIGNAL(clicked()),lock,SLOT(onPlaceButtonClicked()));
     connect(lock->sitdown,SIGNAL(clicked()),lock,SLOT(onSitBack()));
     connect(lock,SIGNAL(he_lost()),lock,SLOT(onReportingLoss()));
     //connect(lock,SIGNAL(joinChannel()),this,SLOT(addUserToChannel()));
    // addto_Challenge_Channel("nanm","namo");
     //lock->ViewLobby->hide();
     addto_admin_Channel(roomid,roomtitle,nid);
}


void MainWindow::Tournament_LockInScreen(QString wageramount,QString nid,QString gametype,QString title,QString roomname,QString matchlevel,int playerct){
    openLockRoom();
    RemoveLayout(ui->mainview_2);
    lock  = new LockIn();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(lock);
    top->setSpacing(0);
    top->setContentsMargins(0,0,0,0);
    ui->mainview_2->setLayout(top);

    lock->roomname ="Challenge_"+nid;;
    lock->This_Roomname = "Challenge_"+nid;
    lock->currentGameType = gametype;
    lock->currentRoomId =  nid;
    lock->roomtype = "Troom";
    lock->currentWagerAmount = wageramount.toInt();
    lock->sitdown->hide();
    lock->matchlevel = matchlevel;
    lock->playercount = playerct;
    lock->lobby_title = roomname;
        QString lbtext = "Details:\n Number of Player:"+gametype+"\nWager Amount:"+wageramount+"\nDetails: "+title;
        lock->setDetailLabel(lbtext,wageramount.toInt());
        lock->betslider->setMinimum(0);
        lock->wagerslide->setText("0");
    // lock->ViewLobby->setText("Home");
     connect(lock->ViewLobby,SIGNAL(clicked()),this,SLOT(goToHome()));
     connect(lock->placebet,SIGNAL(clicked()),lock,SLOT(onPlaceButtonClicked()));
     connect(lock->sitdown,SIGNAL(clicked()),lock,SLOT(onSitBack()));
     connect(lock,SIGNAL(he_lost()),lock,SLOT(onReportingLoss()));
     //connect(lock,SIGNAL(joinChannel()),this,SLOT(addUserToChannel()));
    // addto_Challenge_Channel("nanm","namo");
    // lock->ViewLobby->hide();
     addto_Challenge_Channel(gametype,nid,"Troom");
        //qDebug()<<"RoomName: "<<lock->currentWagerAmount<<matchlevel.toInt();
     TL= 0;
}

void MainWindow::addto_admin_Channel(QString roomid,QString roomtype,QString nid){
    QString surl = "<url>api/TModule/rootJoin?roomid="+roomid+"&uid=1&gametype="+roomtype+"&nid="+nid;
     QNetworkAccessManager *manager = new QNetworkAccessManager(this);
     qDebug()<<surl;
     connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);
}

void MainWindow::addto_Challenge_Channel(QString gametype,QString challengeID,QString roomtype){
    //int gametype = 2;
    QString nid = "Challenge_"+lock->currentRoomId;
    QString currentRoomId = lock->currentRoomId;
    QString surl = "<url>api/";
    QString dynamic = "&fieldname=field_c_player_joined";
    if(roomtype == "Croom"){
        surl += "CGC/joinroom?";
        dynamic = "&fieldname=field_c_player_joined";
    }
    else if(roomtype == "Troom"){
        surl += "TModule/joinroom?";
        dynamic = "&fieldname=field_troom_players_joined";
    }

       surl += "roomid="+nid+"&desc="+currentArenaName+"&when="+cind+"&uid="+QString().number(mainid)+"&arenaid="+challengeID+dynamic;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    qDebug()<<surl;
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest(QNetworkReply*)));
      QNetworkRequest request2 = QNetworkRequest(surl);
      QString ss = sessionname+"="+sessionid;
      request2.setRawHeader("Cookie",ss.toUtf8() );
      manager->get(request2);

}

void MainWindow::challengeRequest2(QNetworkReply* reply){
    QString replyText;
    QByteArray rawData = reply->readAll();
    QString textData(rawData);
    qDebug() << "RM"+ textData;
            if(textData.length() < 5) {
                qDebug() << "No data received";
                return;
            }
            Json::Reader reader;
            Json::Value groot;
            bool parsingSuccessful = reader.parse(textData.toStdString(), groot );
            if(!parsingSuccessful || groot.size() == 0){
                  return;
            }
            else{
                const Json::Value friends = groot;
                QString messageType = QString().fromStdString(friends.get("MessageType","none").asCString());
                 if(messageType == "Tournament"){
                    QString Subject = QString().fromStdString(friends.get("Subject","none").asCString());
                    if(Subject == "GetTournament"){
                        qDebug()<<"Inside the get tournament";
                        RemoveLayout(ui->mainview_2);
                        QVBoxLayout *top = new QVBoxLayout;
                        Json::Value tdata = friends.get("body",0);
                        QListWidget *listWidget = new QListWidget(this);
                        listWidget->setStyleSheet("QListWidget::item:hover{background:rgb(38, 38, 38)};QListWidget::item:selected{background:none}");
                        for(int i=1;i<tdata.size()+1;i++){
                            Json::Value troot = tdata[i-1];
                             QString title = QString().fromStdString(troot.get("title","none").asCString());
                             if(title == currentTournament){
                                 //Tournament_jinfo = r;
                                 RemoveLayout(ui->mainview_2);
                                TL = new TournamentLobby();
                                TL->setValues(troot);
                                QVBoxLayout *top = new QVBoxLayout;
                                top->addWidget(TL);
                                ui->mainview_2->setLayout(top);
                             }



                        }

                    }

            }
         }
}

void MainWindow::challengeRequest(QNetworkReply* reply ){
     QString replyText;
     QByteArray rawData = reply->readAll();
     QString textData(rawData);
     qDebug() << "RM"+ textData;
             if(textData.length() < 5) {
                 qDebug() << "No data received";
                 return;
             }
             Json::Reader reader;
             Json::Value groot;

             bool parsingSuccessful = reader.parse(textData.toStdString(), groot );

             if(!parsingSuccessful || groot.size() == 0){
                   return;
             }
             else{
                 const Json::Value friends = groot;
                 QString messageType = QString().fromStdString(friends.get("MessageType","none").asCString());
                 if(messageType == "Challenge"){
                      QString Subject = QString().fromStdString(friends.get("Subject","none").asCString());
                      if(Subject == "RoomJoined"){

                          IBox = new InfoBox(this);
                          IBox->setMessage("You have successfully entered the lobby..\n You can sit down to place the bet");
                          IBox->show();
                          if(lock) {lock->has_connected=true;lock->setRedLabel("Waiting For Players to Join");}
                      }
                      else if(Subject == "RoomClosed"){
                          IBox = new InfoBox(this);
                          IBox->setMessage("Lobby Closed..\n No Bid can be placed!");
                          IBox->show();
                      }
                 }
                 else if(messageType == "Tournament"){
                     QString Subject = QString().fromStdString(friends.get("Subject","none").asCString());
                     if(Subject == "GetTournament"){
                         qDebug()<<"Inside the get tournament";
                         RemoveLayout(ui->mainview_2);
                         QVBoxLayout *top = new QVBoxLayout;
                         Json::Value tdata = friends.get("body",0);
                         QListWidget *listWidget = new QListWidget(this);
                         listWidget->setStyleSheet("QListWidget::item:hover{background:rgb(38, 38, 38)};QListWidget::item:selected{background:none}");
                         for(int i=1;i<tdata.size()+1;i++){
                             qDebug()<<"TData "<<i;
                             QListWidgetItem *item = new QListWidgetItem();
                             item->setFont(QFont("Candara",14, QFont::Normal));
                             realTournament* RT = new realTournament(this);
                             RT->setValues(tdata[i-1]);
                             item->setSizeHint(QSize(400,132));
                             listWidget->insertItem(i,item);
                             listWidget->setItemWidget(item,RT);
                         }
                         //top->setContentsMargins(0,0,0,0);
                         QLabel *tt = new QLabel("Tournament");
                         tt->setFont(QFont("Candara",12, QFont::Bold));
                         tt->setStyleSheet("color:#fff");
                         //tt->setForeground(QBrush(Qt::white));
                         top->addWidget(tt);
                         top->addWidget(listWidget);
                         ui->mainview_2->setLayout(top);
                     }
                     else if(Subject == "RoomJoined"){

                         IBox = new InfoBox(this);
                         IBox->setMessage("You have successfully entered the lobby..\n You can sit down to place the bet");
                         IBox->show();
                         if(lock){ lock->has_connected=true;lock->setRedLabel("Waiting For Players to Join");}
                     }
                     else if(Subject == "RoomClosed"){
                         IBox = new InfoBox(this);
                         IBox->setMessage("Lobby Closed..\n No Bid can be placed!");
                         IBox->show();
                     }

                 }
                 else if(messageType == "ChatGroup"){
                     QString Subject = QString().fromStdString(friends.get("Subject","none").asCString());
                     if(Subject == "Chatstart"){
                         QString Fname = QString().fromStdString(friends.get("Fname","none").asCString());
                         ChatDialog_init(Fname);
                     }
                     else if(Subject == "Chatfailed"){
                                  IBox = new InfoBox(this);
                                  IBox->setMessage("Problem initiating the chat!");
                                  IBox->show();
                     }
                 }
             }

}

void MainWindow::goToHome(){
    closeLockRoom();
    if(lock->roomtype == "Troom")
        on_tournamentButton_clicked();
    else
       on_leaderboardButton_clicked();
    lock = 0;
}

void MainWindow::on_loginButton_clicked()
{
    if(mainid==0){

    lb->show();
    }
    else{
        IBox = new InfoBox(this);
        IBox->setMessage("You are already logged In");
        IBox->show();
    }
}

void MainWindow::on_leaderboardButton_clicked()
{
    if(mainid == 0){
        IBox = new InfoBox(this);
        IBox->setMessage("Please Login! To View Games");
        IBox->show();
        return;
    }
    try{
        RemoveLayout(ui->mainview_2);
        leaderboard = new LeaderBoard();
        QVBoxLayout *top = new QVBoxLayout;
        top->addWidget(leaderboard);
        ui->mainview_2->setLayout(top);
        arena = 0;
        tour = 0;
        TL = 0;
    }
    catch(...){
        qDebug()<<"Error: In Switching to Leaderboard";
    }
}
//This shows the games that user is signed up for
void MainWindow::on_competeButton_clicked()
{
    arena = 0;
TL = 0;
     //make sure the mainview is clear
      RemoveLayout(ui->mainview_2);
      signalMapper = new QSignalMapper(this);
      getRequest("<url>usergames?uid="+QString().number(mainid));
      /* QGridLayout *top = new QGridLayout;
      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            QLabel *b41=new QLabel("Hey There "+j,0);
            QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect( );
            effect->setBlurRadius(9.0);
            effect->setColor(QColor(254, 254, 254, 255));
            effect->setOffset(0.2);
            QByteArray by = QByteArray::fromBase64("/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2OTApLCBxdWFsaXR5ID0gNzUK/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAZABkAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+f6KKKACnKjN0FTW9s0rDg8nAHrWvbWltFtkuX2w7gGYDPHsM8/oKaQm7GQlpI5wAcntjNXU0C/kRXW0uCjdG8s4NdTo/iDR7G8h+z6dEzISG+1EMJgfbGEPp1rsLvXLC0W0lhkKxZ8wA8HHOQ3pzwc1TUSbs8ln0K9t/wDW28q56ZWqTWkiHB4+oxXa6l44V715bdMfMSNo4H4nr+VYFz4imnVgIVUnqSc/pjFO0R3ZitGy9R+VMq79pDffTj/Z4prwK67kPBqWh3KlFKQVOCKSpGFFFFABVqytXuJVVELMx2qo6sfSq6KXdVUZJOBXQaXqX9iXUN1FFHK0Z+QSDg88njkGqjG5MnY2r7T7Lw1YQiVkuLuQfvCDwT/cX/ZHdu54HHJ5C6uJbqdpJSC2ccDAHsBXVI+ha5CIoJHs5wMJaXUgK/SKbtz/AAvx/tVh32iXVmXdFaSNDhxtwyHuGXt/KquJIy3fLAjNWL3U7q9ihimmLxwoEQZ4wKptTScipbKQu72pcgg00DNLjANCuBL8p6nNIrtG+VNJDE8rYUZx19B9a3tK8OTX8clxgC1hGZZ34VBgnOOp6H8qdwsZDoJU3KPwqoRg16F4futBufP0aLTPOaVSwuJWCu+0EkKMYUkZwPXGT6cbrGnNpt9JAW3AHKuOjqRlWH1BB/Gk0JPoZ1FFFSUXdOhaWVii7n4VB6seAK1fFmlf2F4gudM3E/ZtiH/e2gn9c1J4Ns1vNYsIX+5LeRh/oP8A9danxIshB4hEihhviAbd/eUkH9MVqloiG9Tis85rZ0vVbiOUbXLSqBw3IdR2Oe4H6cVlBDt4GaltIZFuoZGRxGHBZiMADPPNSykd3r/hixu4jeC0n0ySRuCsRaHPXDAcqevTP0rDPgi4sLJL/UriFYHbESxNuMgxknPYf417V4a1U6p4jtXtGgdL+xi8xJ13ISAC+R3P3h9a8/8AiwsFl4p1q1tYUhjgWMJGgwq7lXOPqWNQM8xlYXV0wiARScRqeMDsKkj06d5RG6iMnoWPWqYyDmu58M3Yu5InlQMWJRuOhx1Hp2pgLaeHo7OOMzsrNwdhHCn1Pqf5UeOL77JZWGk27lUliF1OAcA5JCLj2UA/U1rX6mG1nQg7hC5z/wABNcp47keTxM5kYM3lKCQMD2A9sYpAZemmYXcTW+/zw4Mezru7YrqPHMNqfK8hszwExyLjGFPzoPqNzD2AWtbwQNJsdJjvoI/tF07eXOz4DQnoP+A8545wP++bPjPUH1PwzIZoVjaK+wqhQCDtO4kjucp+VaXujN6M8mooIwaKzNDsfApKaxp5UZd7kqgzj5iBiu8+KmkSanpUOrWqbltmdZQB2GAxH5A/Rq8w8O3b2s6TRMFlglSVSe2Dj+v6V9Q+HtNtNc8P3sUoVYbjZPHkZCApt/H7pz9TWknZJma3Z8npMFG0AH2YVdtbm3eZSQ+wHMkO4ZZe+0kdceo/OtnxdoMGhalIbGa3mtZGO2Mndt68qeu3347cVhwiJ8CPh8cx+v0P+P61L1KWh7HoslvaeJNAksPltmhjWP3B7+xOc47Zx2rlfjA7Dxn4g/2/s4P/AHyp/pXU+HIorrVfCohhaOPyUJVjk5BJz+RFZPxrsRD4k1FvLIe4jjkVv7wXAP8AI1JR43Xo3wvgjnuiroG2rdEZH/TDg151ivRPhk8tnezXDoVtkt53lY9ArR7R+JbAFU9gOi120VtJmuU6iF8/98mvOPF/z6+7nvFH/wCgivS9Rmxo97A3e0Lj/vk15l4r411s/wDPKMfkoH9KSEw8M6g9hqKxH5oLnCSIf0P6/kTXaa/G0vg57lcsizqjse7cKPxwlee6Zn+0Ldgekq/zFdV4j1WS302bSBJuiaSO5wOgbDhv/Ql/Kr2JerOFPU0UlFQWWrCcQXILn5GBV/oa9e0Hxjd3Hgg6EJStzaM0bBDh54m+ZVB92BH0I9a8XrodD1iSwuoryONJpIQUlhfpNERgqfw/zxVx1ViJLW5V1O8uL24Mk4wfYdPaqljC32pJcHbGwbjuew/Gus1/R47iH+2NLZrjT7glgx5ZG6lH/wBoZ59eCODgczHNc2pBgmeIg5G045pyjdXRMZWdme8+GbWS01zQRKjpb20CQtLINv7wLtwQfu528A13Hi7wlbeNLaS3ZJrW+s1Dw3bx/u33jJX/AGhxz6V8zv4w126sjZX1491bccOcMPow5/PNdh4W+JOpJYXOjy628dvJbusQuCN8bYyAsh7Hpz+VZuLRomnsYc/h3StFv5hqFubmSNsJDbudjH3PYe2Ofatbw7ONWunsl8sQyIURIsYWQKWAz1Y8Ac+tcVruuvfSPBbsVtwxye8n1Pp7VQ07U7jTZt8JO0kFlzjOOhz2NFhnq97Gbm3u1jGT9lMYHqSuBXnvjRQfEJ24wYUIwfUZrd0zxTJMWWN0YFcGOQ/Ox9v8mq+taK2rL9tsT5ssCbJ7UD94ignDAdwBx68fWnFXdhSdkc/otrJcanaxqCAZUBPpyKPEV0k+pXBjYFS+1SDwVXgH8cZqGOd4ZP3DlAoO5x7jpWbLJ5khbp6CrnZaIiCe7GUUUVkaBTo5GicMjEMOQRTaKAN7RdduNPml8lI5IpgBcWrj5JgPbsR2I5Har89rpl8Fl0+4MTP1guOGQ+m7ofrxn0HSuTBIOQasx3Z3AvncOjqcN/8AXq1KxLimWryzu7SQx3EEkR9xwfoe9VPKyRkmtyw8S3VpAYFaKeE9I5lBAP8Autla2LPxBoTuo1LwzZOpI3PEJIiB3PDMD+Qqrp7kpNbHFGLB6VIlu7kBAWJ6Ac12OsXnhdrhBpVtMIBnIe2IbP13cj8qybjV4UytvahF7byE/EgZz+dP3UFpEFtoDuN93MlvH7jcx/Af1IrYi8QRaJsOku7zQ52TytuEZ6Zz0J9FHA77u3MXGoNKMSOZMdFX5V/+vVKSZ5Op4HQDoKltDUSa6u2nZuWJZizMxyWJ6k1VooqG7lhRRRSAKKKKACiiigApcnPWiigA3N6mkNFFABRRRQAUUUUAFFFFAH//2Q==");
            //effect->setBlurRadius( 5 );
            //b41->setStyleSheet("background-image: url(:/new/prefix1/resources/background.png);");
            //b41->setText("<img><url>localhost:9091/drupal-7.28/sites/default/files/styles/thumbnail/public/unreal.jpg</img>");
            QImage image = QImage::fromData(by, "JPG");
           // QLabel label;
            b41->setPixmap(QPixmap::fromImage(image));
           // label.show();
            b41->setGraphicsEffect(effect);
            top->addWidget(b41,i,j,1,1);
        }*/
    //b41->setGeometry();



}

void MainWindow::showTournamentBracket(int teams,QMap<QString,QString> lobbies,int lsize){
    RemoveLayout(ui->mainview_2);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    qDebug()<<teams<<"_"<<lobbies;
    GraphWidget *widget = new GraphWidget(teams,lobbies,lsize);
    QVBoxLayout *top = new QVBoxLayout;



   QLabel *tt = new QLabel("Tournament Lobbies");
   QPushButton* home = new QPushButton("Back");
   home->setMaximumSize(96,24);
   connect(home,SIGNAL(clicked()),this,SLOT(showLastTournamentLobyy()));
   QWidget* topw = new QWidget();
   QHBoxLayout* topbar = new QHBoxLayout;
   topbar->setContentsMargins(0,0,0,0);
   //topbar->setSpacing(0);
   //topbar->SetMinAndMaxSize();
   tt->setFont(QFont("Candara",12, QFont::Bold));
   tt->setStyleSheet("{color:#fff;}");

   topbar->addWidget(tt);
   topbar->addWidget(home);
   topw->setLayout(topbar);


  // tt->setForeground(QBrush(Qt::white));
   top->addWidget(topw);
   top->addWidget(widget);
   ui->mainview_2->setLayout(top);

}
void MainWindow::showLastTournamentLobyy(){
 /*   RemoveLayout(ui->mainview_2);
    TL = new TournamentLobby();
    TL->setValues(Tournament_jinfo);
   QVBoxLayout *top = new QVBoxLayout;
   top->addWidget(TL);
   ui->mainview_2->setLayout(top);*/
    qDebug()<<currentArenaName;
    QString surl = "<url>api/TModule/getTournaments?uid="+currentArenaName;

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest2(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);

}

void MainWindow::onTournamentLevel_1(Json::Value r){
    //
    qDebug()<<currentArenaName;
    QString surl = "<url>api/TModule/getTournaments?uid="+currentArenaName;

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest2(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8() );
    manager->get(request2);

    //


}

void MainWindow::RemoveLayout (QWidget* widget){

    if(CP){CP = 0;}

    QLayout* layout = widget->layout ();
    QWidget *w;
       if (layout != 0)
       {
       QLayoutItem *item;
       try{
       while ((item = layout->takeAt(0)) != 0){
           layout->removeItem (item);
               if ((w = item->widget()) != 0) {w->hide(); delete w;}
               else {delete item;}

       }
       }
       catch(std::exception &ex){
           //QDebug()<<ex.what();
       }

       delete layout;
       }
}

void MainWindow::on_toolButton_3_clicked()
{
    showMinimized();
}

void MainWindow::on_toolButton_clicked()
{
    //before closing the application logout the use..



    this->hide();
      for(int i=0;i<fchats.size();i++){
          fchats[i]->hide();
      }
    QTimer::singleShot(2000, this, SLOT(logout()));
    if(arena)  RemoveLayout(ui->mainview_2);
     this->hide();
       for(int i=0;i<fchats.size();i++){
           fchats[i]->hide();
       }
       if(arena!=0){arena=0;}
    QString surl = "<url>customLogout?uid="+QString::number(mainid);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived2(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8());
     request2.setRawHeader("X-CSRF-Token",token.toUtf8());
    manager->get(request2);
     QTimer::singleShot(1000, this, SLOT(quit()));


}
void MainWindow::logout(){
    QString surl = "<url>user/logout";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(requestReceived(QNetworkReply*)));
    QNetworkRequest request2 = QNetworkRequest(surl);
    QString ss = sessionname+"="+sessionid;
    request2.setRawHeader("Cookie",ss.toUtf8());
     request2.setRawHeader("X-CSRF-Token",token.toUtf8());
    manager->get(request2);
     QTimer::singleShot(1000, this, SLOT(quit()));

}
void MainWindow::quit(){
     m_tray_icon->hide();
     QCoreApplication::exit(0);
}

void MainWindow::on_tournamentButton_clicked()
{
    arena = 0;

    try{
        qDebug()<<currentArenaName;
        QString surl = "<url>api/TModule/getTournaments?uid="+currentArenaName;

        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(challengeRequest(QNetworkReply*)));
        QNetworkRequest request2 = QNetworkRequest(surl);
        QString ss = sessionname+"="+sessionid;
        request2.setRawHeader("Cookie",ss.toUtf8() );
        manager->get(request2);
    }
    catch(...){
     qDebug()<<"Error: In Switching to Leaderboard";
    }
}
//The challenger Butoon
void MainWindow::on_arenaButton_clicked()
{
 //Retrive the ids for this user.
    if(CP)
        if(CP->WIP)
              return;
     RemoveLayout(ui->mainview_2);
    CP = new ChallengePage();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(CP);
    ui->mainview_2->setLayout(top);
}
//This is a staff button.
void MainWindow::on_pushButton_8_clicked()
{
    arena = 0;
    TL = 0;
    RemoveLayout(ui->mainview_2);
    SupportFrame* SF = new SupportFrame();
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(SF);
    ui->mainview_2->setLayout(top);
}

void MainWindow::on_profileButton_clicked()
{
    arena = 0;TL = 0;
    RemoveLayout(ui->mainview_2);
    pp = new Profilepage(this,whichuser);

    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(pp);
    ui->mainview_2->setLayout(top);
    //***************************//
    pp->username->setText(whichuser);//statusline*
      whichuser = thisuser;
}

void MainWindow::createChallengeToFriend(){
    RemoveLayout(ui->mainview_2);
    challengeuser_widget = new Challengeuser(this,"friendsname");
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(challengeuser_widget);
    ui->mainview_2->setLayout(top);
    //***************************//
}

void MainWindow::on_registerButton_clicked()
{
    //<url>secret/user/register
    if(mainid==0){
    reg = new RegisterDialog(this);
    reg->show();
    }
    else{
        IBox = new InfoBox(this);
        IBox->setMessage("You are already logged In");
        IBox->show();
    }
}

void MainWindow::on_useroption_clicked()
{
    EditP = new EditProfile();
    EditP->show();
}

void MainWindow::on_label_linkActivated(const QString &link)
{
    RemoveLayout(ui->mainview_2);
    tour = new Tournament(this);
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(tour);
    ui->mainview_2->setLayout(top);
}

void MainWindow::on_toolButton_4_clicked()
{
    RemoveLayout(ui->mainview_2);
    tour = new Tournament(this);
    QVBoxLayout *top = new QVBoxLayout;
    top->addWidget(tour);
    ui->mainview_2->setLayout(top);
}



void MainWindow::on_pushButton_3_clicked()
{

    QDesktopServices::openUrl(QUrl("<url>onlinecgc.freeforums.org/", QUrl::TolerantMode));
}



void MainWindow::on_username_clicked()
{
    on_profileButton_clicked();
}


