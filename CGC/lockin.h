#ifndef LOCKIN_H
#define LOCKIN_H

#include <QWidget>
#include <QtWebKit/QWebView>
#include <mediator.h>
#include <QTcpSocket>
#include <QStyledItemDelegate>
#include <QNetworkReply>
using namespace std;
#include <jsonparser.h>
#include <QPushButton>
#include <QLabel>
#include <QToolButton>
#include <QKeyEvent>
#include <infobox.h>
#include <QListWidgetItem>

namespace Ui {
class LockIn;
}

class LockIn : public QWidget
{
    Q_OBJECT
    
public:
    explicit LockIn(QWidget *parent = 0);
    ~LockIn();

     void processMessage(Json::Value root);
    // void addPlayer(QString player);
    QPushButton *LeaveTable;
    QToolButton *ViewLobby;
    QPushButton *placebet;
    QPushButton *sitdown;
    QToolButton *closelockin;
    QLineEdit *betamountedit;
     int onlineuser;
     int satdownuser;
     int teamawager;
     int teambwager;
     int minimumWager;
     int playercount; //
     bool resultdeclared;
     bool isthisadmin;
     QFrame* sliderFrame;
     QStringList betusers;
     QString This_Roomname;
     QString lobby_title;
     bool has_madethebet;
     QMap<QString,QString> map_userranks;
     QMap<QString,QByteArray> map_userpic;
     QMap<QString,QString> map_totalgames;
     QMap<QString,QString> map_uids;
     //This is only for tournament
     QString matchlevel;
     void setDetailLabel(QString msg,int minimum);
     QLineEdit *wagerslide;
       void animatedMsg();
       QString getTeamType();
       void addPlayerAdvance(QString plr,bool type=false);
       void addPlayer(QString plr);
       QToolButton* gameheader;
       void addPlayerToChatList(QString player);
       //when user sit down from spectator
       void updatePlayerInChatList(QString player);
       QStringList chatuserholder;
       QStringList satdownuserlist;
       QString Your_teamtype;
       QSlider* betslider;
       bool has_satdown;
       bool has_connected;
       void cleanup();
       void setRedLabel(QString str);
       Mediator* M;
       void setMatchStatus(bool won,QString wonchips);
       void adjustTheBet();
       void sendChatMsg(QString msg);
       int currentWagerAmount;
       bool hasMadeAPlaceBetRequest;
       QString roomname;
       QString currentRoomId;
       QString currentGameType;
       QString roomtype;
       bool issatdown;
       bool gamestarted;
       void createDispute();
       void fillEmptyBets();
       QString getUserTeam(QString name);
      static bool caseInsensitiveLessThan(const QString &s1, const QString &s2);
      void refresh_List_After_plr_unsit(QString name);
signals:
     void he_lost();
     void joinChannel();


private slots:
    void on_pushButton_6_clicked();
    void on_horizontalSlider_sliderMoved(int position);
    void on_pushButton_4_clicked();
    void on_pushButton_3_clicked();
    void on_toolButton_clicked();
    void on_sendtochat_clicked();
    void onPlaceButtonClicked();
    void onSitBack();
    void onReportingLoss();

   // void on_lineEdit_editingFinished();

   // void on_chatbox_itemEntered(QListWidgetItem *item);

    void on_teamAwin_clicked();

    void on_teamBwin_clicked();

public slots:
     void plainRequest(QNetworkReply* reply);
     void subscribethisroom();
     void loadFinished();
     void onReportLossClicked();

private:
    Ui::LockIn *ui;
    QWebView* webview;
    QWidget* webviewholder;
    QTcpSocket* tcpSocket_;
    int count;
    int chatcounter;
    int count2;
    void keyPressEvent(QKeyEvent *event);
    InfoBox* IBox;




};

#endif // LOCKIN_H
