/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_7;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_5;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_3;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_4;
    QFrame *frame_6;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *toolButton_4;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_6;
    QLabel *promolabel;
    QFrame *frame_5;
    QHBoxLayout *horizontalLayout_4;
    QFrame *mainview;
    QVBoxLayout *verticalLayout_7;
    QFrame *frame_12;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *competeButton;
    QPushButton *leaderboardButton;
    QPushButton *tournamentButton;
    QPushButton *profileButton;
    QPushButton *arenaButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_8;
    QFrame *mainview_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QFrame *frame_9;
    QHBoxLayout *horizontalLayout;
    QLabel *timestamp;
    QToolButton *toolButton_2;
    QToolButton *toolButton_3;
    QToolButton *toolButton;
    QFrame *frame_11;
    QVBoxLayout *verticalLayout_8;
    QFrame *userpanel;
    QHBoxLayout *horizontalLayout_8;
    QFrame *frame_10;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QFrame *frame_13;
    QVBoxLayout *verticalLayout_5;
    QPushButton *username;
    QLabel *label_5;
    QLabel *userchips;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_10;
    QLabel *userrankpic;
    QFrame *rightpanel;
    QVBoxLayout *verticalLayout_9;
    QFrame *frame_14;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *useroption;
    QFrame *userranklogo;
    QVBoxLayout *verticalLayout_11;
    QWidget *sidebar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(893, 600);
        QFont font;
        font.setFamily(QString::fromUtf8("MS Reference Sans Serif"));
        font.setPointSize(22);
        MainWindow->setFont(font);
        MainWindow->setMouseTracking(true);
        MainWindow->setAutoFillBackground(true);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setMouseTracking(true);
        centralWidget->setStyleSheet(QString::fromUtf8("QFrame{\n"
"border:none;\n"
"background-color:  rgb(38, 38, 38);\n"
"}\n"
"#frame{\n"
"border-top: 1px solid #000;\n"
"border-right: 1px solid #000;\n"
"border-bottom:none;\n"
"\n"
"}\n"
"#frame_2{\n"
"border-top: 1px solid #000;\n"
"border-left: 1px solid #000;\n"
"border-bottom:none;\n"
"}\n"
"\n"
"#toolButton{\n"
"height:28px;\n"
"width:38px;\n"
"border-image: url(:/new/prefix1/resources/Close.png);\n"
"}\n"
"#toolButton:hover{\n"
"	border-image: url(:/new/prefix1/resources/Close Rollover.png);\n"
"}\n"
"\n"
"#toolButton_2{\n"
"height:28px;\n"
"width:32px;\n"
"	border-image: url(:/new/prefix1/resources/minimize.png);\n"
"	\n"
"}\n"
"#toolButton_2:hover{\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"#toolButton_3{\n"
"height:28px;\n"
"width:32px;\n"
"border-image: url(:/new/prefix1/resources/maximize.png);\n"
"}\n"
"#toolButton_3:hover{\n"
"	\n"
"	border-image: url(:/new/prefix1/resources/Minimize Rollover.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"height: 44px;\n"
"color: "
                        "#000;\n"
"	\n"
"	font: 11pt \"Candara\";\n"
"	border-image: url(:/new/prefix1/resources/lockinBtns2.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	color: #fff;\n"
"	\n"
"\n"
"\n"
"}\n"
"\n"
"\n"
"\n"
"#userpanel{\n"
"border: 1px solid grey;\n"
"}\n"
"\n"
"#rightpanel{\n"
"border: 1px solid grey;\n"
"}\n"
"\n"
"#mainview_2{\n"
"border-left: 1px solid grey;\n"
"border-right: 1px solid grey;\n"
"border-bottom: 1px solid grey;\n"
"border-top:none;\n"
"\n"
"}\n"
"\n"
"QScrollBar:horizontal {\n"
"     border: 1px solid #222222;\n"
"     background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"     height: 7px;\n"
"     margin: 0px 16px 0 16px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #71D3F3, stop: 0.5 #71D3F3, stop: 1 #71D3F3);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
""
                        "      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #c0c0c0, stop: 1 #e6e6e6);\n"
"      width: 14px;\n"
"      subcontrol-position: right;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #c0c0c0, stop: 1 #e6e6e6);\n"
"      width: 14px;\n"
"     subcontrol-position: left;\n"
"     subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::right-arrow:horizontal, QScrollBar::left-arrow:horizontal\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828 , stop: 1 #484848);\n"
"   "
                        "   width: 7px;\n"
"      margin: 16px 0 16px 0;\n"
"      border: 1px solid #222222;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #71D3F3, stop: 0.5 #71D3F3, stop: 1 #71D3F3);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #c0c0c0, stop: 1 #e6e6e6);\n"
"      height: 14px;\n"
"      subcontrol-position: bottom;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #c0c0c0, stop: 1 #e6e6e6);\n"
"      height: 14px;\n"
"      subcontrol-position: top;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertic"
                        "al\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical\n"
"{\n"
"      background: none;\n"
"}\n"
" QComboBox {\n"
"     border: 1px solid gray;\n"
"     border-radius: 3px;\n"
"     padding: 1px 18px 1px 3px;\n"
"     min-width: 6em;\n"
" }\n"
"\n"
" QComboBox:editable {\n"
"     background: white;\n"
" }\n"
"\n"
" QComboBox:!editable, QComboBox::drop-down:editable {\n"
"      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
" }\n"
"\n"
" /* QComboBox gets the \"on\" state when the popup is open */\n"
" QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
"         "
                        "                        stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
" }\n"
""));
        horizontalLayout_7 = new QHBoxLayout(centralWidget);
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(0, 0));
        frame_2->setMaximumSize(QSize(16777215, 16777215));
        frame_2->setStyleSheet(QString::fromUtf8(""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_5 = new QHBoxLayout(frame_2);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        frame_3 = new QFrame(frame_2);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setStyleSheet(QString::fromUtf8(""));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_3);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(9, 0, 0, 0);
        frame_4 = new QFrame(frame_3);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy);
        frame_4->setStyleSheet(QString::fromUtf8(""));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_4);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        frame_6 = new QFrame(frame_4);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_6->sizePolicy().hasHeightForWidth());
        frame_6->setSizePolicy(sizePolicy1);
        frame_6->setMaximumSize(QSize(16777215, 124));
        frame_6->setStyleSheet(QString::fromUtf8(""));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_6);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        toolButton_4 = new QToolButton(frame_6);
        toolButton_4->setObjectName(QString::fromUtf8("toolButton_4"));
        toolButton_4->setMinimumSize(QSize(0, 80));
        toolButton_4->setMaximumSize(QSize(150, 75));
        toolButton_4->setMouseTracking(true);
        toolButton_4->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/logo1.png);"));

        horizontalLayout_2->addWidget(toolButton_4);

        frame_8 = new QFrame(frame_6);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setMouseTracking(true);
        frame_8->setStyleSheet(QString::fromUtf8(""));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_8);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        promolabel = new QLabel(frame_8);
        promolabel->setObjectName(QString::fromUtf8("promolabel"));
        sizePolicy1.setHeightForWidth(promolabel->sizePolicy().hasHeightForWidth());
        promolabel->setSizePolicy(sizePolicy1);
        promolabel->setMaximumSize(QSize(16777215, 58));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Candara"));
        font1.setPointSize(9);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        promolabel->setFont(font1);
        promolabel->setMouseTracking(false);
        promolabel->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"border: 1px solid grey;\n"
"font: 9pt \"Candara\";"));
        promolabel->setLineWidth(7);
        promolabel->setTextFormat(Qt::RichText);
        promolabel->setScaledContents(true);
        promolabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        promolabel->setWordWrap(true);
        promolabel->setMargin(-9);

        verticalLayout_6->addWidget(promolabel);


        horizontalLayout_2->addWidget(frame_8);


        verticalLayout_4->addWidget(frame_6);


        verticalLayout_3->addWidget(frame_4);

        frame_5 = new QFrame(frame_3);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setStyleSheet(QString::fromUtf8(""));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_5);
        horizontalLayout_4->setSpacing(9);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(4, 4, 4, 0);
        mainview = new QFrame(frame_5);
        mainview->setObjectName(QString::fromUtf8("mainview"));
        mainview->setStyleSheet(QString::fromUtf8(""));
        mainview->setFrameShape(QFrame::StyledPanel);
        mainview->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(mainview);
        verticalLayout_7->setSpacing(0);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        frame_12 = new QFrame(mainview);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        sizePolicy1.setHeightForWidth(frame_12->sizePolicy().hasHeightForWidth());
        frame_12->setSizePolicy(sizePolicy1);
        frame_12->setMaximumSize(QSize(16777215, 31));
        frame_12->setFrameShape(QFrame::StyledPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        horizontalLayout_6 = new QHBoxLayout(frame_12);
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        competeButton = new QPushButton(frame_12);
        competeButton->setObjectName(QString::fromUtf8("competeButton"));

        horizontalLayout_6->addWidget(competeButton);

        leaderboardButton = new QPushButton(frame_12);
        leaderboardButton->setObjectName(QString::fromUtf8("leaderboardButton"));

        horizontalLayout_6->addWidget(leaderboardButton);

        tournamentButton = new QPushButton(frame_12);
        tournamentButton->setObjectName(QString::fromUtf8("tournamentButton"));

        horizontalLayout_6->addWidget(tournamentButton);

        profileButton = new QPushButton(frame_12);
        profileButton->setObjectName(QString::fromUtf8("profileButton"));

        horizontalLayout_6->addWidget(profileButton);

        arenaButton = new QPushButton(frame_12);
        arenaButton->setObjectName(QString::fromUtf8("arenaButton"));

        horizontalLayout_6->addWidget(arenaButton);

        pushButton_3 = new QPushButton(frame_12);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_6->addWidget(pushButton_3);

        pushButton_8 = new QPushButton(frame_12);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        horizontalLayout_6->addWidget(pushButton_8);


        verticalLayout_7->addWidget(frame_12);

        mainview_2 = new QFrame(mainview);
        mainview_2->setObjectName(QString::fromUtf8("mainview_2"));
        mainview_2->setMaximumSize(QSize(16777215, 650));
        mainview_2->setStyleSheet(QString::fromUtf8(""));
        mainview_2->setFrameShape(QFrame::StyledPanel);
        mainview_2->setFrameShadow(QFrame::Raised);

        verticalLayout_7->addWidget(mainview_2);


        horizontalLayout_4->addWidget(mainview);


        verticalLayout_3->addWidget(frame_5);


        horizontalLayout_5->addWidget(frame_3);


        horizontalLayout_7->addWidget(frame_2);

        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy2);
        frame->setMinimumSize(QSize(0, 0));
        frame->setMaximumSize(QSize(233, 16777215));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setSpacing(1);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_9 = new QFrame(frame);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        sizePolicy2.setHeightForWidth(frame_9->sizePolicy().hasHeightForWidth());
        frame_9->setSizePolicy(sizePolicy2);
        frame_9->setMaximumSize(QSize(16777215, 20));
        frame_9->setStyleSheet(QString::fromUtf8("border:none;"));
        frame_9->setFrameShape(QFrame::StyledPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_9);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        timestamp = new QLabel(frame_9);
        timestamp->setObjectName(QString::fromUtf8("timestamp"));
        sizePolicy2.setHeightForWidth(timestamp->sizePolicy().hasHeightForWidth());
        timestamp->setSizePolicy(sizePolicy2);
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(11);
        timestamp->setFont(font2);
        timestamp->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        timestamp->setScaledContents(true);
        timestamp->setMargin(5);
        timestamp->setIndent(4);

        horizontalLayout->addWidget(timestamp);

        toolButton_2 = new QToolButton(frame_9);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));
        toolButton_2->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(toolButton_2);

        toolButton_3 = new QToolButton(frame_9);
        toolButton_3->setObjectName(QString::fromUtf8("toolButton_3"));
        toolButton_3->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/resources/maximize.png);"));

        horizontalLayout->addWidget(toolButton_3);

        toolButton = new QToolButton(frame_9);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(toolButton);


        verticalLayout->addWidget(frame_9);

        frame_11 = new QFrame(frame);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        sizePolicy2.setHeightForWidth(frame_11->sizePolicy().hasHeightForWidth());
        frame_11->setSizePolicy(sizePolicy2);
        frame_11->setMinimumSize(QSize(0, 0));
        frame_11->setStyleSheet(QString::fromUtf8(""));
        frame_11->setFrameShape(QFrame::StyledPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(frame_11);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(-1, -1, -1, 0);
        userpanel = new QFrame(frame_11);
        userpanel->setObjectName(QString::fromUtf8("userpanel"));
        userpanel->setMaximumSize(QSize(16777215, 68));
        userpanel->setFrameShape(QFrame::StyledPanel);
        userpanel->setFrameShadow(QFrame::Raised);
        userpanel->setLineWidth(3);
        horizontalLayout_8 = new QHBoxLayout(userpanel);
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(6, 4, 5, 4);
        frame_10 = new QFrame(userpanel);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        frame_10->setMaximumSize(QSize(50, 16777215));
        frame_10->setAutoFillBackground(false);
        frame_10->setStyleSheet(QString::fromUtf8("#frame_10{\n"
"color: #fff;\n"
"	background-color: rgba(255, 255, 255,0);\n"
"\n"
"	border-image: url(:/new/prefix1/resources/newframe.png);\n"
"}"));
        frame_10->setFrameShape(QFrame::Box);
        frame_10->setFrameShadow(QFrame::Plain);
        frame_10->setLineWidth(3);
        verticalLayout_2 = new QVBoxLayout(frame_10);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(4, 5, 5, 5);
        label_2 = new QLabel(frame_10);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy2.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy2);
        label_2->setMaximumSize(QSize(16777215, 16777215));
        label_2->setStyleSheet(QString::fromUtf8("border:none;"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_2);


        horizontalLayout_8->addWidget(frame_10);

        frame_13 = new QFrame(userpanel);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        frame_13->setMinimumSize(QSize(0, 25));
        frame_13->setMaximumSize(QSize(16777215, 72));
        frame_13->setFrameShape(QFrame::StyledPanel);
        frame_13->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_13);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(9, 0, -1, 1);
        username = new QPushButton(frame_13);
        username->setObjectName(QString::fromUtf8("username"));
        username->setStyleSheet(QString::fromUtf8("Text-align:left;\n"
"color: rgb(113, 211, 243);\n"
"	font: 75 12pt \"Candara\";\n"
"background-color:  rgb(38, 38, 38);\n"
"border-image:none;\n"
""));
        username->setFlat(true);

        verticalLayout_5->addWidget(username);

        label_5 = new QLabel(frame_13);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Candara"));
        font3.setPointSize(11);
        label_5->setFont(font3);
        label_5->setStyleSheet(QString::fromUtf8("color:rgb(255, 255, 255)"));
        label_5->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        label_5->setMargin(0);

        verticalLayout_5->addWidget(label_5);

        userchips = new QLabel(frame_13);
        userchips->setObjectName(QString::fromUtf8("userchips"));
        userchips->setFont(font3);
        userchips->setStyleSheet(QString::fromUtf8("color:rgb(255, 255, 255)"));
        userchips->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        userchips->setMargin(0);

        verticalLayout_5->addWidget(userchips);


        horizontalLayout_8->addWidget(frame_13);

        frame_7 = new QFrame(userpanel);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        sizePolicy2.setHeightForWidth(frame_7->sizePolicy().hasHeightForWidth());
        frame_7->setSizePolicy(sizePolicy2);
        frame_7->setMinimumSize(QSize(8, 0));
        frame_7->setMaximumSize(QSize(46, 16777215));
        frame_7->setStyleSheet(QString::fromUtf8(""));
        frame_7->setFrameShape(QFrame::NoFrame);
        frame_7->setFrameShadow(QFrame::Raised);
        verticalLayout_10 = new QVBoxLayout(frame_7);
        verticalLayout_10->setSpacing(0);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(0, 0, 0, 22);
        userrankpic = new QLabel(frame_7);
        userrankpic->setObjectName(QString::fromUtf8("userrankpic"));
        sizePolicy2.setHeightForWidth(userrankpic->sizePolicy().hasHeightForWidth());
        userrankpic->setSizePolicy(sizePolicy2);
        userrankpic->setMinimumSize(QSize(0, 0));
        userrankpic->setLayoutDirection(Qt::RightToLeft);
        userrankpic->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/resources/gold.png")));
        userrankpic->setScaledContents(false);
        userrankpic->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(userrankpic);


        horizontalLayout_8->addWidget(frame_7);


        verticalLayout_8->addWidget(userpanel);

        rightpanel = new QFrame(frame_11);
        rightpanel->setObjectName(QString::fromUtf8("rightpanel"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Candara"));
        rightpanel->setFont(font4);
        rightpanel->setStyleSheet(QString::fromUtf8(""));
        rightpanel->setFrameShape(QFrame::StyledPanel);
        rightpanel->setFrameShadow(QFrame::Raised);
        verticalLayout_9 = new QVBoxLayout(rightpanel);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(2, 4, 2, 0);
        frame_14 = new QFrame(rightpanel);
        frame_14->setObjectName(QString::fromUtf8("frame_14"));
        frame_14->setMaximumSize(QSize(16777215, 31));
        frame_14->setFrameShape(QFrame::StyledPanel);
        frame_14->setFrameShadow(QFrame::Raised);
        horizontalLayout_9 = new QHBoxLayout(frame_14);
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        pushButton = new QPushButton(frame_14);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_9->addWidget(pushButton);

        pushButton_2 = new QPushButton(frame_14);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_9->addWidget(pushButton_2);

        useroption = new QPushButton(frame_14);
        useroption->setObjectName(QString::fromUtf8("useroption"));

        horizontalLayout_9->addWidget(useroption);


        verticalLayout_9->addWidget(frame_14);

        userranklogo = new QFrame(rightpanel);
        userranklogo->setObjectName(QString::fromUtf8("userranklogo"));
        sizePolicy2.setHeightForWidth(userranklogo->sizePolicy().hasHeightForWidth());
        userranklogo->setSizePolicy(sizePolicy2);
        userranklogo->setStyleSheet(QString::fromUtf8(""));
        userranklogo->setFrameShape(QFrame::StyledPanel);
        userranklogo->setFrameShadow(QFrame::Raised);
        userranklogo->setLineWidth(3);
        verticalLayout_11 = new QVBoxLayout(userranklogo);
        verticalLayout_11->setSpacing(0);
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        sidebar = new QWidget(userranklogo);
        sidebar->setObjectName(QString::fromUtf8("sidebar"));
        sizePolicy2.setHeightForWidth(sidebar->sizePolicy().hasHeightForWidth());
        sidebar->setSizePolicy(sizePolicy2);
        sidebar->setMaximumSize(QSize(16777215, 425));
        sidebar->setMouseTracking(false);
        sidebar->setLayoutDirection(Qt::LeftToRight);

        verticalLayout_11->addWidget(sidebar);


        verticalLayout_9->addWidget(userranklogo);


        verticalLayout_8->addWidget(rightpanel);


        verticalLayout->addWidget(frame_11);


        horizontalLayout_7->addWidget(frame);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setMaximumSize(QSize(16777215, 20));
        statusBar->setStyleSheet(QString::fromUtf8("QStatusBar {\n"
"     background-color:  rgb(38, 38, 38);\n"
"border-bottom: 1px solid #000;\n"
"border-left: 1px solid #000;\n"
"border-right: 1px solid #000;\n"
" }\n"
"\n"
"QStatusBar QSizeGrip {\n"
"   \n"
"	\n"
"	image: url(:/new/prefix1/resources/size-grip.png);\n"
"width: 16px;\n"
"     height: 16px;\n"
" }"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        toolButton_4->setText(QString());
        competeButton->setText(QApplication::translate("MainWindow", "Arenas", 0, QApplication::UnicodeUTF8));
        leaderboardButton->setText(QApplication::translate("MainWindow", "LeaderBoard", 0, QApplication::UnicodeUTF8));
        tournamentButton->setText(QApplication::translate("MainWindow", "Tournaments", 0, QApplication::UnicodeUTF8));
        profileButton->setText(QApplication::translate("MainWindow", "Profile", 0, QApplication::UnicodeUTF8));
        arenaButton->setText(QApplication::translate("MainWindow", "Challenges", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("MainWindow", "Forum", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("MainWindow", "Support", 0, QApplication::UnicodeUTF8));
        timestamp->setText(QApplication::translate("MainWindow", "TextLabel", 0, QApplication::UnicodeUTF8));
        toolButton_2->setText(QApplication::translate("MainWindow", "_", 0, QApplication::UnicodeUTF8));
        toolButton_3->setText(QString());
        toolButton->setText(QString());
        label_2->setText(QString());
        username->setText(QApplication::translate("MainWindow", "UserName", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Candara'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#ffffff;\">Rank : --</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        userchips->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'candara'; font-size:8pt; font-weight:600; color:#ffffff;\">Chips: 250</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        userrankpic->setText(QString());
        pushButton->setText(QApplication::translate("MainWindow", "Alert", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("MainWindow", "Friends", 0, QApplication::UnicodeUTF8));
        useroption->setText(QApplication::translate("MainWindow", "Options", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
