#ifndef SUPPORTFRAME_H
#define SUPPORTFRAME_H
#include <QNetworkReply>
#include <QWidget>
#include <QSignalMapper>
#include <QTableWidget>
#include <jsonparser.h>
namespace Ui {
class SupportFrame;
}

class SupportFrame : public QWidget
{
    Q_OBJECT
    
public:
    explicit SupportFrame(QWidget *parent = 0);
    ~SupportFrame();
    QWidget* getButtonWidgets(int RowNum);
     QWidget* createDeleteButton(int RowNum);
    QSignalMapper* ButtonSignalMapper;
     QSignalMapper* DeleteSignalMapper;
    QTableWidget* tableWidget;
Json::Value root;
int rownumber;
int rownumber2;

    private slots:
    void plainRequest(QNetworkReply* reply );
    void CellButtonClicked(int RowNum);
     void DeleteButtonClicked(int RowNum);
    void enterAdmin();
    QTableWidgetItem* createTableWidgetItem( const QString& text );

    
private:
    Ui::SupportFrame *ui;
};

#endif // SUPPORTFRAME_H
