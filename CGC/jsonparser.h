#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <stdio.h>
#include <jsonutil/json/json.h>
class jsonparser{

private:
    int X;
    int Y;
    int Z;

public:

    jsonparser(std::string fName)
    {	X = 0;Y = 1;Z = 2;
    bool parsingSuccessful = reader.parse(readInputTestFile(fName.c_str()), root );
    if ( !parsingSuccessful )
    {
        // report to the user the failure and their locations in the document.
        std::cout  << "Failed to parse configuration\n"
            << reader.getFormatedErrorMessages();
        //return 0;
    }
    //jsonLog = Ogre::LogManager::getSingleton().createLog("jsonLog.log");

    }

    ~jsonparser();

    Json::Value getValue(std::string valueName, std::string missing = "json_not_found")
    {
        return root.get(valueName, missing);
    }

    Json::Value getRoot()
    {
        return root;
    }

private:
    Json::Value root;   // will contains the root value after parsing.
    Json::Reader reader;

private:
    std::string readInputTestFile( const char *path )
    {
        FILE *file = fopen( path, "rb" );
        if ( !file )
            return std::string("");
        fseek( file, 0, SEEK_END );
        long size = ftell( file );
        fseek( file, 0, SEEK_SET );
        std::string text;
        char *buffer = new char[size+1];
        buffer[size] = 0;
        if ( fread( buffer, 1, size, file ) == (unsigned long)size )
            text = buffer;
        fclose( file );
        delete[] buffer;
        return text;
    }

};


#endif // JSONPARSER_H
