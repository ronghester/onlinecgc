/********************************************************************************
** Form generated from reading UI file 'tournament.ui'
**
** Created: Sat 4. Apr 00:08:41 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOURNAMENT_H
#define UI_TOURNAMENT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Tournament
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QLabel *usernumber;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame;

    void setupUi(QWidget *Tournament)
    {
        if (Tournament->objectName().isEmpty())
            Tournament->setObjectName(QString::fromUtf8("Tournament"));
        Tournament->resize(807, 508);
        QFont font;
        font.setFamily(QString::fromUtf8("Candara"));
        Tournament->setFont(font);
        Tournament->setStyleSheet(QString::fromUtf8("QFrame{\n"
"\n"
"border:none;\n"
"	background-color: rgb(40, 40, 42);\n"
"}\n"
"\n"
"QWidget{\n"
"\n"
"border:none;\n"
"\n"
"}\n"
"\n"
"QLabel{\n"
"	background-color:none;\n"
"\n"
"}\n"
"QToolButton{\n"
"background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #88d, stop: 0.1 #99e, stop: 0.49 #77c, stop: 0.5 #66b, stop: 1 #77c);\n"
"}\n"
"QPushButton{\n"
"height: 32px;\n"
"color: #fff;\n"
"	border-image: url(:/new/prefix1/resources/menu.png);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	\n"
"	color: rgb(134, 134, 134);\n"
"	\n"
"\n"
"\n"
"}\n"
"\n"
"QStatusbar{\n"
"	background-color: rgb(0, 0, 0);\n"
"}\n"
""));
        verticalLayout = new QVBoxLayout(Tournament);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_3 = new QFrame(Tournament);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy);
        frame_3->setStyleSheet(QString::fromUtf8("QLabel{\n"
"color:#fff;\n"
"}"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label = new QLabel(frame_3);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Candara"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);
        label->setTextFormat(Qt::PlainText);

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(278, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_2 = new QLabel(frame_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);

        usernumber = new QLabel(frame_3);
        usernumber->setObjectName(QString::fromUtf8("usernumber"));
        usernumber->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout->addWidget(usernumber);


        verticalLayout->addWidget(frame_3);

        frame_2 = new QFrame(Tournament);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setStyleSheet(QString::fromUtf8(""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        frame = new QFrame(frame_2);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("#frame{\n"
"border:none;\n"
"background-color: rgb(40, 40, 40);\n"
"}\n"
"\n"
"QWidget{\n"
"	background-color: rgb(40, 40, 40);\n"
"border: 2px solid grey;\n"
"}\n"
"\n"
"QLabel{\n"
"border:none;\n"
"	font: 11pt \"Candara\";\n"
"color:#fff;\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(frame);


        verticalLayout->addWidget(frame_2);


        retranslateUi(Tournament);

        QMetaObject::connectSlotsByName(Tournament);
    } // setupUi

    void retranslateUi(QWidget *Tournament)
    {
        Tournament->setWindowTitle(QApplication::translate("Tournament", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Tournament", "Welcome To CGC", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Tournament", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/new/prefix1/resources/status-green.png\" width=\"12\" height=\"12\" /><span style=\" font-size:8pt;\"> Player Online : </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/new/prefix1/resources/white-dot.png\" width=\"12\" height=\"12\" /><span style=\" font-size:8pt;\"> Active Player :</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        usernumber->setText(QApplication::translate("Tournament", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">36</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">0</span></p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Tournament: public Ui_Tournament {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOURNAMENT_H
