#ifndef RANKSFORM_H
#define RANKSFORM_H

#include <QWidget>

namespace Ui {
class RanksForm;
}

class RanksForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit RanksForm(QWidget *parent = 0);
    ~RanksForm();
    
private:
    Ui::RanksForm *ui;
};

#endif // RANKSFORM_H
